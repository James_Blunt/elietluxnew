// Generated code from Butter Knife. Do not modify!
package com.elietlux.shopifyapp.productlistingsection;

import android.support.annotation.UiThread;
import android.view.View;
import android.widget.GridView;
import android.widget.RelativeLayout;
import butterknife.internal.Utils;
import com.elietlux.shopifyapp.R;
import com.elietlux.shopifyapp.maincontainer.MainActivity_ViewBinding;
import java.lang.IllegalStateException;
import java.lang.Override;

public class AllProductListing_ViewBinding extends MainActivity_ViewBinding {
  private AllProductListing target;

  @UiThread
  public AllProductListing_ViewBinding(AllProductListing target) {
    this(target, target.getWindow().getDecorView());
  }

  @UiThread
  public AllProductListing_ViewBinding(AllProductListing target, View source) {
    super(target, source);

    this.target = target;

    target.grid = Utils.findOptionalViewAsType(source, R.id.grid, "field 'grid'", GridView.class);
    target.MageNative_sortsection = Utils.findOptionalViewAsType(source, R.id.MageNative_sortsection, "field 'MageNative_sortsection'", RelativeLayout.class);
  }

  @Override
  public void unbind() {
    AllProductListing target = this.target;
    if (target == null) throw new IllegalStateException("Bindings already cleared.");
    this.target = null;

    target.grid = null;
    target.MageNative_sortsection = null;

    super.unbind();
  }
}
