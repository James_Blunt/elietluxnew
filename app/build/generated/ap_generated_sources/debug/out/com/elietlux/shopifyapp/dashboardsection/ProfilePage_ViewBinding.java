// Generated code from Butter Knife. Do not modify!
package com.elietlux.shopifyapp.dashboardsection;

import android.support.annotation.UiThread;
import android.view.View;
import android.widget.Button;
import android.widget.EditText;
import android.widget.TextView;
import butterknife.internal.Utils;
import com.elietlux.shopifyapp.R;
import com.elietlux.shopifyapp.maincontainer.MainActivity_ViewBinding;
import java.lang.IllegalStateException;
import java.lang.Override;

public class ProfilePage_ViewBinding extends MainActivity_ViewBinding {
  private ProfilePage target;

  @UiThread
  public ProfilePage_ViewBinding(ProfilePage target) {
    this(target, target.getWindow().getDecorView());
  }

  @UiThread
  public ProfilePage_ViewBinding(ProfilePage target, View source) {
    super(target, source);

    this.target = target;

    target.MageNative_createaccounttext = Utils.findOptionalViewAsType(source, R.id.MageNative_createaccounttext, "field 'MageNative_createaccounttext'", TextView.class);
    target.firstname = Utils.findOptionalViewAsType(source, R.id.firstname, "field 'firstname'", EditText.class);
    target.lastname = Utils.findOptionalViewAsType(source, R.id.lastname, "field 'lastname'", EditText.class);
    target.email = Utils.findOptionalViewAsType(source, R.id.email, "field 'email'", EditText.class);
    target.password = Utils.findOptionalViewAsType(source, R.id.password, "field 'password'", EditText.class);
    target.confirmpassword = Utils.findOptionalViewAsType(source, R.id.confirmpassword, "field 'confirmpassword'", EditText.class);
    target.MageNative_register = Utils.findOptionalViewAsType(source, R.id.MageNative_register, "field 'MageNative_register'", Button.class);
  }

  @Override
  public void unbind() {
    ProfilePage target = this.target;
    if (target == null) throw new IllegalStateException("Bindings already cleared.");
    this.target = null;

    target.MageNative_createaccounttext = null;
    target.firstname = null;
    target.lastname = null;
    target.email = null;
    target.password = null;
    target.confirmpassword = null;
    target.MageNative_register = null;

    super.unbind();
  }
}
