// Generated code from Butter Knife. Do not modify!
package com.elietlux.shopifyapp.searchsection;

import android.support.annotation.UiThread;
import android.view.View;
import android.widget.AutoCompleteTextView;
import android.widget.TextView;
import butterknife.internal.Utils;
import com.elietlux.shopifyapp.R;
import com.elietlux.shopifyapp.maincontainer.MainActivity_ViewBinding;
import java.lang.IllegalStateException;
import java.lang.Override;

public class AutoSearch_ViewBinding extends MainActivity_ViewBinding {
  private AutoSearch target;

  @UiThread
  public AutoSearch_ViewBinding(AutoSearch target) {
    this(target, target.getWindow().getDecorView());
  }

  @UiThread
  public AutoSearch_ViewBinding(AutoSearch target, View source) {
    super(target, source);

    this.target = target;

    target.search = Utils.findOptionalViewAsType(source, R.id.search, "field 'search'", AutoCompleteTextView.class);
    target.category = Utils.findOptionalViewAsType(source, R.id.category, "field 'category'", TextView.class);
  }

  @Override
  public void unbind() {
    AutoSearch target = this.target;
    if (target == null) throw new IllegalStateException("Bindings already cleared.");
    this.target = null;

    target.search = null;
    target.category = null;

    super.unbind();
  }
}
