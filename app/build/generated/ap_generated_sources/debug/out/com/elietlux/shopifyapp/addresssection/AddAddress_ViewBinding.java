// Generated code from Butter Knife. Do not modify!
package com.elietlux.shopifyapp.addresssection;

import android.support.annotation.UiThread;
import android.view.View;
import android.widget.Button;
import android.widget.EditText;
import android.widget.Spinner;
import butterknife.internal.Utils;
import com.elietlux.shopifyapp.R;
import com.elietlux.shopifyapp.maincontainer.MainActivity_ViewBinding;
import java.lang.IllegalStateException;
import java.lang.Override;

public class AddAddress_ViewBinding extends MainActivity_ViewBinding {
  private AddAddress target;

  @UiThread
  public AddAddress_ViewBinding(AddAddress target) {
    this(target, target.getWindow().getDecorView());
  }

  @UiThread
  public AddAddress_ViewBinding(AddAddress target, View source) {
    super(target, source);

    this.target = target;

    target.firstname = Utils.findOptionalViewAsType(source, R.id.firstname, "field 'firstname'", EditText.class);
    target.lastname = Utils.findOptionalViewAsType(source, R.id.lastname, "field 'lastname'", EditText.class);
    target.company = Utils.findOptionalViewAsType(source, R.id.company, "field 'company'", EditText.class);
    target.address1 = Utils.findOptionalViewAsType(source, R.id.address1, "field 'address1'", EditText.class);
    target.address2 = Utils.findOptionalViewAsType(source, R.id.address2, "field 'address2'", EditText.class);
    target.city = Utils.findOptionalViewAsType(source, R.id.city, "field 'city'", EditText.class);
    target.country = Utils.findOptionalViewAsType(source, R.id.country, "field 'country'", Spinner.class);
    target.provinces = Utils.findOptionalViewAsType(source, R.id.provinces, "field 'provinces'", Spinner.class);
    target.statetext = Utils.findOptionalViewAsType(source, R.id.statetext, "field 'statetext'", EditText.class);
    target.zip = Utils.findOptionalViewAsType(source, R.id.zip, "field 'zip'", EditText.class);
    target.phone = Utils.findOptionalViewAsType(source, R.id.phone, "field 'phone'", EditText.class);
    target.submit = Utils.findOptionalViewAsType(source, R.id.submit, "field 'submit'", Button.class);
  }

  @Override
  public void unbind() {
    AddAddress target = this.target;
    if (target == null) throw new IllegalStateException("Bindings already cleared.");
    this.target = null;

    target.firstname = null;
    target.lastname = null;
    target.company = null;
    target.address1 = null;
    target.address2 = null;
    target.city = null;
    target.country = null;
    target.provinces = null;
    target.statetext = null;
    target.zip = null;
    target.phone = null;
    target.submit = null;

    super.unbind();
  }
}
