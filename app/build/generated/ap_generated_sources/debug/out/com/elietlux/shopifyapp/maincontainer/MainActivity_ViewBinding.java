// Generated code from Butter Knife. Do not modify!
package com.elietlux.shopifyapp.maincontainer;

import android.support.annotation.CallSuper;
import android.support.annotation.UiThread;
import android.support.v4.widget.DrawerLayout;
import android.support.v7.widget.Toolbar;
import android.view.View;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.RelativeLayout;
import android.widget.TextView;
import butterknife.Unbinder;
import butterknife.internal.Utils;
import com.elietlux.shopifyapp.R;
import java.lang.IllegalStateException;
import java.lang.Override;

public class MainActivity_ViewBinding implements Unbinder {
  private MainActivity target;

  @UiThread
  public MainActivity_ViewBinding(MainActivity target) {
    this(target, target.getWindow().getDecorView());
  }

  @UiThread
  public MainActivity_ViewBinding(MainActivity target, View source) {
    this.target = target;

    target.mToolbar = Utils.findOptionalViewAsType(source, R.id.MageNative_toolbar, "field 'mToolbar'", Toolbar.class);
    target.toolimage = Utils.findOptionalViewAsType(source, R.id.toolimage, "field 'toolimage'", ImageView.class);
    target.tooltext = Utils.findOptionalViewAsType(source, R.id.tooltext, "field 'tooltext'", TextView.class);
    target.drawerLayout = Utils.findOptionalViewAsType(source, R.id.MageNative_drawer_layout, "field 'drawerLayout'", DrawerLayout.class);
    target.homeSelection = Utils.findRequiredViewAsType(source, R.id.homeSelection, "field 'homeSelection'", LinearLayout.class);
    target.categorySelection = Utils.findRequiredViewAsType(source, R.id.categorySelection, "field 'categorySelection'", LinearLayout.class);
    target.cartSelection = Utils.findRequiredViewAsType(source, R.id.cartSelection, "field 'cartSelection'", LinearLayout.class);
    target.wishSelection = Utils.findRequiredViewAsType(source, R.id.wishSelection, "field 'wishSelection'", LinearLayout.class);
    target.profileSelection = Utils.findRequiredViewAsType(source, R.id.profileSelection, "field 'profileSelection'", LinearLayout.class);
    target.imageHome = Utils.findRequiredViewAsType(source, R.id.imageHome, "field 'imageHome'", ImageView.class);
    target.textHome = Utils.findRequiredViewAsType(source, R.id.textHome, "field 'textHome'", TextView.class);
    target.imageCats = Utils.findRequiredViewAsType(source, R.id.imageCats, "field 'imageCats'", ImageView.class);
    target.textCats = Utils.findRequiredViewAsType(source, R.id.textCats, "field 'textCats'", TextView.class);
    target.imageCart = Utils.findRequiredViewAsType(source, R.id.imageCart, "field 'imageCart'", ImageView.class);
    target.textCart = Utils.findRequiredViewAsType(source, R.id.textCart, "field 'textCart'", TextView.class);
    target.imageWish = Utils.findRequiredViewAsType(source, R.id.imageWish, "field 'imageWish'", ImageView.class);
    target.textWish = Utils.findRequiredViewAsType(source, R.id.textWish, "field 'textWish'", TextView.class);
    target.imageAccount = Utils.findRequiredViewAsType(source, R.id.imageAccount, "field 'imageAccount'", ImageView.class);
    target.textAccount = Utils.findRequiredViewAsType(source, R.id.textAccount, "field 'textAccount'", TextView.class);
    target.setting_section = Utils.findRequiredViewAsType(source, R.id.setting_section, "field 'setting_section'", RelativeLayout.class);
  }

  @Override
  @CallSuper
  public void unbind() {
    MainActivity target = this.target;
    if (target == null) throw new IllegalStateException("Bindings already cleared.");
    this.target = null;

    target.mToolbar = null;
    target.toolimage = null;
    target.tooltext = null;
    target.drawerLayout = null;
    target.homeSelection = null;
    target.categorySelection = null;
    target.cartSelection = null;
    target.wishSelection = null;
    target.profileSelection = null;
    target.imageHome = null;
    target.textHome = null;
    target.imageCats = null;
    target.textCats = null;
    target.imageCart = null;
    target.textCart = null;
    target.imageWish = null;
    target.textWish = null;
    target.imageAccount = null;
    target.textAccount = null;
    target.setting_section = null;
  }
}
