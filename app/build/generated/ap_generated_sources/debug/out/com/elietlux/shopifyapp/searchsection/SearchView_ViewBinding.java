// Generated code from Butter Knife. Do not modify!
package com.elietlux.shopifyapp.searchsection;

import android.support.annotation.UiThread;
import android.view.View;
import android.widget.GridView;
import android.widget.LinearLayout;
import android.widget.RelativeLayout;
import butterknife.internal.Utils;
import com.elietlux.shopifyapp.R;
import com.elietlux.shopifyapp.maincontainer.MainActivity_ViewBinding;
import java.lang.IllegalStateException;
import java.lang.Override;

public class SearchView_ViewBinding extends MainActivity_ViewBinding {
  private SearchView target;

  @UiThread
  public SearchView_ViewBinding(SearchView target) {
    this(target, target.getWindow().getDecorView());
  }

  @UiThread
  public SearchView_ViewBinding(SearchView target, View source) {
    super(target, source);

    this.target = target;

    target.grid = Utils.findOptionalViewAsType(source, R.id.grid, "field 'grid'", GridView.class);
    target.MageNative_main = Utils.findOptionalViewAsType(source, R.id.MageNative_main, "field 'MageNative_main'", RelativeLayout.class);
    target.MageNative_sortsection = Utils.findOptionalViewAsType(source, R.id.MageNative_sortsection, "field 'MageNative_sortsection'", RelativeLayout.class);
    target.MageNative_sortingsection = Utils.findOptionalViewAsType(source, R.id.MageNative_sortingsection, "field 'MageNative_sortingsection'", LinearLayout.class);
  }

  @Override
  public void unbind() {
    SearchView target = this.target;
    if (target == null) throw new IllegalStateException("Bindings already cleared.");
    this.target = null;

    target.grid = null;
    target.MageNative_main = null;
    target.MageNative_sortsection = null;
    target.MageNative_sortingsection = null;

    super.unbind();
  }
}
