// Generated code from Butter Knife. Do not modify!
package com.elietlux.shopifyapp.adaptersection;

import android.support.annotation.CallSuper;
import android.support.annotation.UiThread;
import android.view.View;
import android.widget.ImageView;
import android.widget.TextView;
import butterknife.Unbinder;
import butterknife.internal.Utils;
import com.elietlux.shopifyapp.R;
import java.lang.IllegalStateException;
import java.lang.Override;

public class Collections_Adapter_ViewBinding implements Unbinder {
  private Collections_Adapter target;

  @UiThread
  public Collections_Adapter_ViewBinding(Collections_Adapter target, View source) {
    this.target = target;

    target.cat_title = Utils.findOptionalViewAsType(source, R.id.cat_title, "field 'cat_title'", TextView.class);
    target.cat_image = Utils.findOptionalViewAsType(source, R.id.cat_image, "field 'cat_image'", ImageView.class);
    target.cat_id = Utils.findOptionalViewAsType(source, R.id.cat_id, "field 'cat_id'", TextView.class);
  }

  @Override
  @CallSuper
  public void unbind() {
    Collections_Adapter target = this.target;
    if (target == null) throw new IllegalStateException("Bindings already cleared.");
    this.target = null;

    target.cat_title = null;
    target.cat_image = null;
    target.cat_id = null;
  }
}
