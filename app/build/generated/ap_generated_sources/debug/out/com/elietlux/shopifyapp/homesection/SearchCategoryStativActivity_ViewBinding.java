// Generated code from Butter Knife. Do not modify!
package com.elietlux.shopifyapp.homesection;

import android.support.annotation.UiThread;
import android.view.View;
import android.widget.TextView;
import butterknife.internal.Utils;
import com.elietlux.shopifyapp.R;
import com.elietlux.shopifyapp.maincontainer.MainActivity_ViewBinding;
import java.lang.IllegalStateException;
import java.lang.Override;

public class SearchCategoryStativActivity_ViewBinding extends MainActivity_ViewBinding {
  private SearchCategoryStativActivity target;

  @UiThread
  public SearchCategoryStativActivity_ViewBinding(SearchCategoryStativActivity target) {
    this(target, target.getWindow().getDecorView());
  }

  @UiThread
  public SearchCategoryStativActivity_ViewBinding(SearchCategoryStativActivity target,
      View source) {
    super(target, source);

    this.target = target;

    target.searchBar = Utils.findOptionalViewAsType(source, R.id.searchBar, "field 'searchBar'", TextView.class);
    target.categorytitle = Utils.findOptionalViewAsType(source, R.id.categorytitle, "field 'categorytitle'", TextView.class);
  }

  @Override
  public void unbind() {
    SearchCategoryStativActivity target = this.target;
    if (target == null) throw new IllegalStateException("Bindings already cleared.");
    this.target = null;

    target.searchBar = null;
    target.categorytitle = null;

    super.unbind();
  }
}
