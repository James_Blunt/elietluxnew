// Generated code from Butter Knife. Do not modify!
package com.elietlux.shopifyapp.wishlistsection;

import android.support.annotation.UiThread;
import android.view.View;
import android.widget.LinearLayout;
import butterknife.internal.Utils;
import com.elietlux.shopifyapp.R;
import com.elietlux.shopifyapp.maincontainer.MainActivity_ViewBinding;
import java.lang.IllegalStateException;
import java.lang.Override;

public class WishListing_ViewBinding extends MainActivity_ViewBinding {
  private WishListing target;

  @UiThread
  public WishListing_ViewBinding(WishListing target) {
    this(target, target.getWindow().getDecorView());
  }

  @UiThread
  public WishListing_ViewBinding(WishListing target, View source) {
    super(target, source);

    this.target = target;

    target.wishlistsection = Utils.findOptionalViewAsType(source, R.id.wishlistsection, "field 'wishlistsection'", LinearLayout.class);
  }

  @Override
  public void unbind() {
    WishListing target = this.target;
    if (target == null) throw new IllegalStateException("Bindings already cleared.");
    this.target = null;

    target.wishlistsection = null;

    super.unbind();
  }
}
