// Generated code from Butter Knife. Do not modify!
package com.elietlux.shopifyapp.homesection;

import android.support.annotation.CallSuper;
import android.support.annotation.UiThread;
import android.view.View;
import android.widget.ListView;
import butterknife.Unbinder;
import butterknife.internal.Utils;
import com.elietlux.shopifyapp.R;
import java.lang.IllegalStateException;
import java.lang.Override;

public class CategoriesFragment_ViewBinding implements Unbinder {
  private CategoriesFragment target;

  @UiThread
  public CategoriesFragment_ViewBinding(CategoriesFragment target, View source) {
    this.target = target;

    target.categories_list = Utils.findOptionalViewAsType(source, R.id.categories_list, "field 'categories_list'", ListView.class);
  }

  @Override
  @CallSuper
  public void unbind() {
    CategoriesFragment target = this.target;
    if (target == null) throw new IllegalStateException("Bindings already cleared.");
    this.target = null;

    target.categories_list = null;
  }
}
