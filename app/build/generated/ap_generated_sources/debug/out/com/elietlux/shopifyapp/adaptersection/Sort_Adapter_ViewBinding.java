// Generated code from Butter Knife. Do not modify!
package com.elietlux.shopifyapp.adaptersection;

import android.support.annotation.CallSuper;
import android.support.annotation.UiThread;
import android.view.View;
import android.widget.TextView;
import butterknife.Unbinder;
import butterknife.internal.Utils;
import com.elietlux.shopifyapp.R;
import java.lang.IllegalStateException;
import java.lang.Override;

public class Sort_Adapter_ViewBinding implements Unbinder {
  private Sort_Adapter target;

  @UiThread
  public Sort_Adapter_ViewBinding(Sort_Adapter target, View source) {
    this.target = target;

    target.text = Utils.findOptionalViewAsType(source, R.id.MageNative_SortLabel, "field 'text'", TextView.class);
  }

  @Override
  @CallSuper
  public void unbind() {
    Sort_Adapter target = this.target;
    if (target == null) throw new IllegalStateException("Bindings already cleared.");
    this.target = null;

    target.text = null;
  }
}
