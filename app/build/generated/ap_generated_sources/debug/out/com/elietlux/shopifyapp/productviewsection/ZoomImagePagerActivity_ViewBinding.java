// Generated code from Butter Knife. Do not modify!
package com.elietlux.shopifyapp.productviewsection;

import android.support.annotation.CallSuper;
import android.support.annotation.UiThread;
import android.view.View;
import android.widget.Gallery;
import butterknife.Unbinder;
import butterknife.internal.Utils;
import com.elietlux.shopifyapp.R;
import java.lang.IllegalStateException;
import java.lang.Override;

public class ZoomImagePagerActivity_ViewBinding implements Unbinder {
  private ZoomImagePagerActivity target;

  @UiThread
  public ZoomImagePagerActivity_ViewBinding(ZoomImagePagerActivity target) {
    this(target, target.getWindow().getDecorView());
  }

  @UiThread
  public ZoomImagePagerActivity_ViewBinding(ZoomImagePagerActivity target, View source) {
    this.target = target;

    target.pager = Utils.findRequiredViewAsType(source, R.id.MageNative_pager, "field 'pager'", NonSwipeableViewPager.class);
    target.productgallery = Utils.findRequiredViewAsType(source, R.id.MageNative_productgallery, "field 'productgallery'", Gallery.class);
  }

  @Override
  @CallSuper
  public void unbind() {
    ZoomImagePagerActivity target = this.target;
    if (target == null) throw new IllegalStateException("Bindings already cleared.");
    this.target = null;

    target.pager = null;
    target.productgallery = null;
  }
}
