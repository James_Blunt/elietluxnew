// Generated code from Butter Knife. Do not modify!
package com.elietlux.shopifyapp.loginandregistrationsection;

import android.support.annotation.UiThread;
import android.view.View;
import android.widget.Button;
import android.widget.EditText;
import android.widget.RelativeLayout;
import android.widget.ScrollView;
import android.widget.TextView;
import butterknife.internal.Utils;
import com.elietlux.shopifyapp.R;
import com.elietlux.shopifyapp.maincontainer.MainActivity_ViewBinding;
import com.facebook.login.widget.LoginButton;
import com.google.android.gms.common.SignInButton;
import java.lang.IllegalStateException;
import java.lang.Override;

public class Login_ViewBinding extends MainActivity_ViewBinding {
  private Login target;

  @UiThread
  public Login_ViewBinding(Login target) {
    this(target, target.getWindow().getDecorView());
  }

  @UiThread
  public Login_ViewBinding(Login target, View source) {
    super(target, source);

    this.target = target;

    target.MageNative_signupwithustext = Utils.findOptionalViewAsType(source, R.id.MageNative_signupwithustext, "field 'MageNative_signupwithustext'", TextView.class);
    target.MageNative_forgotPassword = Utils.findOptionalViewAsType(source, R.id.MageNative_forgotPassword, "field 'MageNative_forgotPassword'", TextView.class);
    target.MageNative_usr_password = Utils.findOptionalViewAsType(source, R.id.MageNative_usr_password, "field 'MageNative_usr_password'", EditText.class);
    target.MageNative_user_name = Utils.findOptionalViewAsType(source, R.id.MageNative_user_name, "field 'MageNative_user_name'", EditText.class);
    target.MageNative_Login = Utils.findOptionalViewAsType(source, R.id.MageNative_Login, "field 'MageNative_Login'", Button.class);
    target.scrollable = Utils.findOptionalViewAsType(source, R.id.scrollable, "field 'scrollable'", ScrollView.class);
    target.Facebook_signin = Utils.findOptionalViewAsType(source, R.id.MageNative_social_login_facebook, "field 'Facebook_signin'", RelativeLayout.class);
    target.Google_signin = Utils.findOptionalViewAsType(source, R.id.MageNative_social_login_google, "field 'Google_signin'", RelativeLayout.class);
    target.google_button = Utils.findOptionalViewAsType(source, R.id.google_btn_sign_in, "field 'google_button'", SignInButton.class);
    target.facebook_button = Utils.findOptionalViewAsType(source, R.id.fb_login_button, "field 'facebook_button'", LoginButton.class);
  }

  @Override
  public void unbind() {
    Login target = this.target;
    if (target == null) throw new IllegalStateException("Bindings already cleared.");
    this.target = null;

    target.MageNative_signupwithustext = null;
    target.MageNative_forgotPassword = null;
    target.MageNative_usr_password = null;
    target.MageNative_user_name = null;
    target.MageNative_Login = null;
    target.scrollable = null;
    target.Facebook_signin = null;
    target.Google_signin = null;
    target.google_button = null;
    target.facebook_button = null;

    super.unbind();
  }
}
