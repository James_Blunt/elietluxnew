// Generated code from Butter Knife. Do not modify!
package com.elietlux.shopifyapp.productviewsection;

import android.support.annotation.UiThread;
import android.support.design.widget.AppBarLayout;
import android.support.design.widget.CollapsingToolbarLayout;
import android.support.v4.view.ViewPager;
import android.support.v4.widget.NestedScrollView;
import android.support.v7.widget.RecyclerView;
import android.view.View;
import android.webkit.WebView;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.RelativeLayout;
import android.widget.TextView;
import butterknife.internal.Utils;
import com.elietlux.shopifyapp.R;
import com.elietlux.shopifyapp.maincontainer.MainActivity_ViewBinding;
import java.lang.IllegalStateException;
import java.lang.Override;

public class ProductView_ViewBinding extends MainActivity_ViewBinding {
  private ProductView target;

  @UiThread
  public ProductView_ViewBinding(ProductView target) {
    this(target, target.getWindow().getDecorView());
  }

  @UiThread
  public ProductView_ViewBinding(ProductView target, View source) {
    super(target, source);

    this.target = target;

    target.main = Utils.findOptionalViewAsType(source, R.id.main, "field 'main'", RelativeLayout.class);
    target.MageNative_productimages = Utils.findOptionalViewAsType(source, R.id.MageNative_productimages, "field 'MageNative_productimages'", ViewPager.class);
    target.MageNative_indicator = Utils.findOptionalViewAsType(source, R.id.MageNative_indicator, "field 'MageNative_indicator'", CirclePageIndicator.class);
    target.MageNative_specialprice = Utils.findOptionalViewAsType(source, R.id.MageNative_specialprice, "field 'MageNative_specialprice'", TextView.class);
    target.MageNative_normalprice = Utils.findOptionalViewAsType(source, R.id.MageNative_normalprice, "field 'MageNative_normalprice'", TextView.class);
    target.MageNative_productname = Utils.findOptionalViewAsType(source, R.id.MageNative_productname, "field 'MageNative_productname'", TextView.class);
    target.product_id = Utils.findOptionalViewAsType(source, R.id.product_id, "field 'product_id'", TextView.class);
    target.horizontal = Utils.findOptionalViewAsType(source, R.id.optionsection, "field 'horizontal'", RelativeLayout.class);
    target.dynamic_fields_section = Utils.findOptionalViewAsType(source, R.id.dynamic_fields_section, "field 'dynamic_fields_section'", LinearLayout.class);
    target.description = Utils.findOptionalViewAsType(source, R.id.description, "field 'description'", TextView.class);
    target.products_details = Utils.findOptionalViewAsType(source, R.id.products_details, "field 'products_details'", WebView.class);
    target.MageNative_sharetext = Utils.findOptionalViewAsType(source, R.id.MageNative_sharetext, "field 'MageNative_sharetext'", LinearLayout.class);
    target.scrollmain = Utils.findOptionalViewAsType(source, R.id.scrollmain, "field 'scrollmain'", NestedScrollView.class);
    target.collapsingToolbar = Utils.findOptionalViewAsType(source, R.id.collapsingToolbar, "field 'collapsingToolbar'", CollapsingToolbarLayout.class);
    target.similarsection = Utils.findOptionalViewAsType(source, R.id.similarsection, "field 'similarsection'", RelativeLayout.class);
    target.products = Utils.findOptionalViewAsType(source, R.id.products, "field 'products'", RecyclerView.class);
    target.addtocart = Utils.findOptionalViewAsType(source, R.id.addtocart, "field 'addtocart'", TextView.class);
    target.MageNative_wishlistsection = Utils.findOptionalViewAsType(source, R.id.MageNative_wishlistsection, "field 'MageNative_wishlistsection'", RelativeLayout.class);
    target.MageNative_wishlist = Utils.findOptionalViewAsType(source, R.id.MageNative_wishlist, "field 'MageNative_wishlist'", ImageView.class);
    target.whatsapp = Utils.findOptionalViewAsType(source, R.id.whatsapp1, "field 'whatsapp'", ImageView.class);
    target.fbmessenger = Utils.findOptionalViewAsType(source, R.id.fbmessenger1, "field 'fbmessenger'", ImageView.class);
    target.mail = Utils.findOptionalViewAsType(source, R.id.mail1, "field 'mail'", ImageView.class);
    target.sizechart = Utils.findOptionalViewAsType(source, R.id.sizechart, "field 'sizechart'", TextView.class);
    target.MageNative_vendor = Utils.findOptionalViewAsType(source, R.id.MageNative_vendor, "field 'MageNative_vendor'", TextView.class);
    target.outline7 = Utils.findOptionalViewAsType(source, R.id.outline7, "field 'outline7'", TextView.class);
    target.outline6 = Utils.findOptionalViewAsType(source, R.id.outline6, "field 'outline6'", TextView.class);
    target.outline5 = Utils.findOptionalViewAsType(source, R.id.outline5, "field 'outline5'", TextView.class);
    target.appBar = Utils.findOptionalViewAsType(source, R.id.appBar, "field 'appBar'", AppBarLayout.class);
  }

  @Override
  public void unbind() {
    ProductView target = this.target;
    if (target == null) throw new IllegalStateException("Bindings already cleared.");
    this.target = null;

    target.main = null;
    target.MageNative_productimages = null;
    target.MageNative_indicator = null;
    target.MageNative_specialprice = null;
    target.MageNative_normalprice = null;
    target.MageNative_productname = null;
    target.product_id = null;
    target.horizontal = null;
    target.dynamic_fields_section = null;
    target.description = null;
    target.products_details = null;
    target.MageNative_sharetext = null;
    target.scrollmain = null;
    target.collapsingToolbar = null;
    target.similarsection = null;
    target.products = null;
    target.addtocart = null;
    target.MageNative_wishlistsection = null;
    target.MageNative_wishlist = null;
    target.whatsapp = null;
    target.fbmessenger = null;
    target.mail = null;
    target.sizechart = null;
    target.MageNative_vendor = null;
    target.outline7 = null;
    target.outline6 = null;
    target.outline5 = null;
    target.appBar = null;

    super.unbind();
  }
}
