// Generated code from Butter Knife. Do not modify!
package com.elietlux.shopifyapp.trialexpiresection;

import android.support.annotation.CallSuper;
import android.support.annotation.UiThread;
import android.view.View;
import android.widget.ImageView;
import android.widget.TextView;
import butterknife.Unbinder;
import butterknife.internal.DebouncingOnClickListener;
import butterknife.internal.Utils;
import com.elietlux.shopifyapp.R;
import java.lang.IllegalStateException;
import java.lang.Override;

public class TrialExpired_ViewBinding implements Unbinder {
  private TrialExpired target;

  private View view2131362415;

  @UiThread
  public TrialExpired_ViewBinding(TrialExpired target) {
    this(target, target.getWindow().getDecorView());
  }

  @UiThread
  public TrialExpired_ViewBinding(final TrialExpired target, View source) {
    this.target = target;

    View view;
    target.expired = Utils.findOptionalViewAsType(source, R.id.expired, "field 'expired'", ImageView.class);
    view = Utils.findRequiredView(source, R.id.tryagain, "method 'tryagain'");
    target.tryagain = Utils.castView(view, R.id.tryagain, "field 'tryagain'", TextView.class);
    view2131362415 = view;
    view.setOnClickListener(new DebouncingOnClickListener() {
      @Override
      public void doClick(View p0) {
        target.tryagain();
      }
    });
  }

  @Override
  @CallSuper
  public void unbind() {
    TrialExpired target = this.target;
    if (target == null) throw new IllegalStateException("Bindings already cleared.");
    this.target = null;

    target.expired = null;
    target.tryagain = null;

    view2131362415.setOnClickListener(null);
    view2131362415 = null;
  }
}
