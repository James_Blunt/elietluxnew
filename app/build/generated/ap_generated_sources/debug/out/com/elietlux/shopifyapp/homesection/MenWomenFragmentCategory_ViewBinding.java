// Generated code from Butter Knife. Do not modify!
package com.elietlux.shopifyapp.homesection;

import android.support.annotation.CallSuper;
import android.support.annotation.UiThread;
import android.support.v7.widget.LinearLayoutCompat;
import android.view.View;
import butterknife.Unbinder;
import butterknife.internal.Utils;
import com.elietlux.shopifyapp.R;
import java.lang.IllegalStateException;
import java.lang.Override;

public class MenWomenFragmentCategory_ViewBinding implements Unbinder {
  private MenWomenFragmentCategory target;

  @UiThread
  public MenWomenFragmentCategory_ViewBinding(MenWomenFragmentCategory target, View source) {
    this.target = target;

    target.mainLinear = Utils.findOptionalViewAsType(source, R.id.mainLinear, "field 'mainLinear'", LinearLayoutCompat.class);
  }

  @Override
  @CallSuper
  public void unbind() {
    MenWomenFragmentCategory target = this.target;
    if (target == null) throw new IllegalStateException("Bindings already cleared.");
    this.target = null;

    target.mainLinear = null;
  }
}
