// Generated code from Butter Knife. Do not modify!
package com.elietlux.shopifyapp.homesection;

import android.support.annotation.UiThread;
import android.support.v7.widget.LinearLayoutCompat;
import android.view.View;
import butterknife.internal.Utils;
import com.elietlux.shopifyapp.R;
import com.elietlux.shopifyapp.maincontainer.MainActivity_ViewBinding;
import java.lang.IllegalStateException;
import java.lang.Override;

public class Subcats_ViewBinding extends MainActivity_ViewBinding {
  private Subcats target;

  @UiThread
  public Subcats_ViewBinding(Subcats target) {
    this(target, target.getWindow().getDecorView());
  }

  @UiThread
  public Subcats_ViewBinding(Subcats target, View source) {
    super(target, source);

    this.target = target;

    target.mainLinear = Utils.findOptionalViewAsType(source, R.id.mainLinear, "field 'mainLinear'", LinearLayoutCompat.class);
  }

  @Override
  public void unbind() {
    Subcats target = this.target;
    if (target == null) throw new IllegalStateException("Bindings already cleared.");
    this.target = null;

    target.mainLinear = null;

    super.unbind();
  }
}
