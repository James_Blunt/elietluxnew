// Generated code from Butter Knife. Do not modify!
package com.elietlux.shopifyapp.adaptersection;

import android.support.annotation.CallSuper;
import android.support.annotation.UiThread;
import android.view.View;
import android.widget.TextView;
import butterknife.Unbinder;
import butterknife.internal.Utils;
import com.elietlux.shopifyapp.R;
import java.lang.IllegalStateException;
import java.lang.Override;

public class addressListAdapter_ViewBinding implements Unbinder {
  private addressListAdapter target;

  @UiThread
  public addressListAdapter_ViewBinding(addressListAdapter target, View source) {
    this.target = target;

    target.address_id = Utils.findOptionalViewAsType(source, R.id.address_id, "field 'address_id'", TextView.class);
    target.firstname = Utils.findOptionalViewAsType(source, R.id.firstname, "field 'firstname'", TextView.class);
    target.lastname = Utils.findOptionalViewAsType(source, R.id.lastname, "field 'lastname'", TextView.class);
    target.company = Utils.findOptionalViewAsType(source, R.id.company, "field 'company'", TextView.class);
    target.address1 = Utils.findOptionalViewAsType(source, R.id.address1, "field 'address1'", TextView.class);
    target.address2 = Utils.findOptionalViewAsType(source, R.id.address2, "field 'address2'", TextView.class);
    target.city = Utils.findOptionalViewAsType(source, R.id.city, "field 'city'", TextView.class);
    target.state = Utils.findOptionalViewAsType(source, R.id.state, "field 'state'", TextView.class);
    target.country = Utils.findOptionalViewAsType(source, R.id.country, "field 'country'", TextView.class);
    target.pincode = Utils.findOptionalViewAsType(source, R.id.pincode, "field 'pincode'", TextView.class);
    target.phone = Utils.findOptionalViewAsType(source, R.id.phone, "field 'phone'", TextView.class);
  }

  @Override
  @CallSuper
  public void unbind() {
    addressListAdapter target = this.target;
    if (target == null) throw new IllegalStateException("Bindings already cleared.");
    this.target = null;

    target.address_id = null;
    target.firstname = null;
    target.lastname = null;
    target.company = null;
    target.address1 = null;
    target.address2 = null;
    target.city = null;
    target.state = null;
    target.country = null;
    target.pincode = null;
    target.phone = null;
  }
}
