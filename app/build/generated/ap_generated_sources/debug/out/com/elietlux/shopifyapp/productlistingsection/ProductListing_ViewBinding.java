// Generated code from Butter Knife. Do not modify!
package com.elietlux.shopifyapp.productlistingsection;

import android.support.annotation.UiThread;
import android.view.View;
import android.widget.GridView;
import android.widget.RelativeLayout;
import android.widget.TextView;
import butterknife.internal.Utils;
import com.elietlux.shopifyapp.R;
import com.elietlux.shopifyapp.maincontainer.MainActivity_ViewBinding;
import java.lang.IllegalStateException;
import java.lang.Override;

public class ProductListing_ViewBinding extends MainActivity_ViewBinding {
  private ProductListing target;

  @UiThread
  public ProductListing_ViewBinding(ProductListing target) {
    this(target, target.getWindow().getDecorView());
  }

  @UiThread
  public ProductListing_ViewBinding(ProductListing target, View source) {
    super(target, source);

    this.target = target;

    target.grid = Utils.findOptionalViewAsType(source, R.id.grid, "field 'grid'", GridView.class);
    target.MageNative_main = Utils.findOptionalViewAsType(source, R.id.MageNative_main, "field 'MageNative_main'", RelativeLayout.class);
    target.oops = Utils.findOptionalViewAsType(source, R.id.oops, "field 'oops'", TextView.class);
    target.MageNative_sortsection = Utils.findOptionalViewAsType(source, R.id.MageNative_sortsection, "field 'MageNative_sortsection'", RelativeLayout.class);
    target.MageNative_filtersection = Utils.findOptionalViewAsType(source, R.id.MageNative_filtersection, "field 'MageNative_filtersection'", RelativeLayout.class);
  }

  @Override
  public void unbind() {
    ProductListing target = this.target;
    if (target == null) throw new IllegalStateException("Bindings already cleared.");
    this.target = null;

    target.grid = null;
    target.MageNative_main = null;
    target.oops = null;
    target.MageNative_sortsection = null;
    target.MageNative_filtersection = null;

    super.unbind();
  }
}
