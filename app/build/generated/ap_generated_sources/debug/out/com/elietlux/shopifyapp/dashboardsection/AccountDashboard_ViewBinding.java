// Generated code from Butter Knife. Do not modify!
package com.elietlux.shopifyapp.dashboardsection;

import android.support.annotation.UiThread;
import android.view.View;
import android.widget.Button;
import android.widget.LinearLayout;
import android.widget.RelativeLayout;
import android.widget.TextView;
import butterknife.internal.Utils;
import com.elietlux.shopifyapp.R;
import com.elietlux.shopifyapp.maincontainer.MainActivity_ViewBinding;
import java.lang.IllegalStateException;
import java.lang.Override;

public class AccountDashboard_ViewBinding extends MainActivity_ViewBinding {
  private AccountDashboard target;

  @UiThread
  public AccountDashboard_ViewBinding(AccountDashboard target) {
    this(target, target.getWindow().getDecorView());
  }

  @UiThread
  public AccountDashboard_ViewBinding(AccountDashboard target, View source) {
    super(target, source);

    this.target = target;

    target.appversion = Utils.findOptionalViewAsType(source, R.id.appversion, "field 'appversion'", TextView.class);
    target.copyright = Utils.findOptionalViewAsType(source, R.id.copyright, "field 'copyright'", TextView.class);
    target.wish_section = Utils.findOptionalViewAsType(source, R.id.wish_section, "field 'wish_section'", RelativeLayout.class);
    target.profile = Utils.findOptionalViewAsType(source, R.id.profile, "field 'profile'", RelativeLayout.class);
    target.address_section = Utils.findOptionalViewAsType(source, R.id.address_section, "field 'address_section'", RelativeLayout.class);
    target.order_section = Utils.findOptionalViewAsType(source, R.id.order_section, "field 'order_section'", RelativeLayout.class);
    target.currency_section = Utils.findOptionalViewAsType(source, R.id.currency_section, "field 'currency_section'", RelativeLayout.class);
    target.MageNative_FAQ = Utils.findOptionalViewAsType(source, R.id.MageNative_FAQ, "field 'MageNative_FAQ'", TextView.class);
    target.MageNative_ReturnsandRefunds = Utils.findOptionalViewAsType(source, R.id.MageNative_ReturnsandRefunds, "field 'MageNative_ReturnsandRefunds'", TextView.class);
    target.MageNative_TermsandConditions = Utils.findOptionalViewAsType(source, R.id.MageNative_TermsandConditions, "field 'MageNative_TermsandConditions'", TextView.class);
    target.MageNative_PrivacyPolicy = Utils.findOptionalViewAsType(source, R.id.MageNative_PrivacyPolicy, "field 'MageNative_PrivacyPolicy'", TextView.class);
    target.MageNative_ShippingPolicy = Utils.findOptionalViewAsType(source, R.id.MageNative_ShippingPolicy, "field 'MageNative_ShippingPolicy'", TextView.class);
    target.MageNative_TrackYourOrder = Utils.findOptionalViewAsType(source, R.id.MageNative_TrackYourOrder, "field 'MageNative_TrackYourOrder'", TextView.class);
    target.MageNative_Reviews = Utils.findOptionalViewAsType(source, R.id.MageNative_Reviews, "field 'MageNative_Reviews'", TextView.class);
    target.btnCreateAccount = Utils.findOptionalViewAsType(source, R.id.btnCreateAccount, "field 'btnCreateAccount'", Button.class);
    target.btnSignin = Utils.findOptionalViewAsType(source, R.id.btnSignin, "field 'btnSignin'", Button.class);
    target.loggedOutSection = Utils.findOptionalViewAsType(source, R.id.loggedOutSection, "field 'loggedOutSection'", LinearLayout.class);
    target.logout = Utils.findOptionalViewAsType(source, R.id.logout, "field 'logout'", TextView.class);
    target.loggedInSection = Utils.findOptionalViewAsType(source, R.id.loggedInSection, "field 'loggedInSection'", LinearLayout.class);
  }

  @Override
  public void unbind() {
    AccountDashboard target = this.target;
    if (target == null) throw new IllegalStateException("Bindings already cleared.");
    this.target = null;

    target.appversion = null;
    target.copyright = null;
    target.wish_section = null;
    target.profile = null;
    target.address_section = null;
    target.order_section = null;
    target.currency_section = null;
    target.MageNative_FAQ = null;
    target.MageNative_ReturnsandRefunds = null;
    target.MageNative_TermsandConditions = null;
    target.MageNative_PrivacyPolicy = null;
    target.MageNative_ShippingPolicy = null;
    target.MageNative_TrackYourOrder = null;
    target.MageNative_Reviews = null;
    target.btnCreateAccount = null;
    target.btnSignin = null;
    target.loggedOutSection = null;
    target.logout = null;
    target.loggedInSection = null;

    super.unbind();
  }
}
