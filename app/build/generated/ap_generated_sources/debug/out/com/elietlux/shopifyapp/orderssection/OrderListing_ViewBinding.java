// Generated code from Butter Knife. Do not modify!
package com.elietlux.shopifyapp.orderssection;

import android.support.annotation.UiThread;
import android.view.View;
import android.widget.ListView;
import butterknife.internal.Utils;
import com.elietlux.shopifyapp.R;
import com.elietlux.shopifyapp.maincontainer.MainActivity_ViewBinding;
import java.lang.IllegalStateException;
import java.lang.Override;

public class OrderListing_ViewBinding extends MainActivity_ViewBinding {
  private OrderListing target;

  @UiThread
  public OrderListing_ViewBinding(OrderListing target) {
    this(target, target.getWindow().getDecorView());
  }

  @UiThread
  public OrderListing_ViewBinding(OrderListing target, View source) {
    super(target, source);

    this.target = target;

    target.OrderList = Utils.findOptionalViewAsType(source, R.id.orderlist, "field 'OrderList'", ListView.class);
  }

  @Override
  public void unbind() {
    OrderListing target = this.target;
    if (target == null) throw new IllegalStateException("Bindings already cleared.");
    this.target = null;

    target.OrderList = null;

    super.unbind();
  }
}
