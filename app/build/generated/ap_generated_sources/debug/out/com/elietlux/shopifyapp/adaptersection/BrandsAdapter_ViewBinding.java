// Generated code from Butter Knife. Do not modify!
package com.elietlux.shopifyapp.adaptersection;

import android.support.annotation.CallSuper;
import android.support.annotation.UiThread;
import android.view.View;
import android.widget.TextView;
import butterknife.Unbinder;
import butterknife.internal.Utils;
import com.elietlux.shopifyapp.R;
import java.lang.IllegalStateException;
import java.lang.Override;

public class BrandsAdapter_ViewBinding implements Unbinder {
  private BrandsAdapter target;

  @UiThread
  public BrandsAdapter_ViewBinding(BrandsAdapter target, View source) {
    this.target = target;

    target.brands = Utils.findOptionalViewAsType(source, R.id.brands, "field 'brands'", TextView.class);
  }

  @Override
  @CallSuper
  public void unbind() {
    BrandsAdapter target = this.target;
    if (target == null) throw new IllegalStateException("Bindings already cleared.");
    this.target = null;

    target.brands = null;
  }
}
