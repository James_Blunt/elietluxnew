// Generated code from Butter Knife. Do not modify!
package com.elietlux.shopifyapp.homesection;

import android.support.annotation.UiThread;
import android.view.View;
import android.webkit.WebView;
import android.widget.ImageView;
import butterknife.internal.Utils;
import com.elietlux.shopifyapp.R;
import com.elietlux.shopifyapp.maincontainer.MainActivity_ViewBinding;
import java.lang.IllegalStateException;
import java.lang.Override;

public class HomeWeblink_ViewBinding extends MainActivity_ViewBinding {
  private HomeWeblink target;

  @UiThread
  public HomeWeblink_ViewBinding(HomeWeblink target) {
    this(target, target.getWindow().getDecorView());
  }

  @UiThread
  public HomeWeblink_ViewBinding(HomeWeblink target, View source) {
    super(target, source);

    this.target = target;

    target.webView = Utils.findOptionalViewAsType(source, R.id.MageNative_webview, "field 'webView'", WebView.class);
    target.cross = Utils.findOptionalViewAsType(source, R.id.cross, "field 'cross'", ImageView.class);
  }

  @Override
  public void unbind() {
    HomeWeblink target = this.target;
    if (target == null) throw new IllegalStateException("Bindings already cleared.");
    this.target = null;

    target.webView = null;
    target.cross = null;

    super.unbind();
  }
}
