// Generated code from Butter Knife. Do not modify!
package com.elietlux.shopifyapp.adaptersection;

import android.support.annotation.CallSuper;
import android.support.annotation.UiThread;
import android.view.View;
import android.widget.ImageView;
import android.widget.TextView;
import butterknife.Unbinder;
import butterknife.internal.Utils;
import com.elietlux.shopifyapp.R;
import java.lang.IllegalStateException;
import java.lang.Override;

public class Product_Adapter_Tags_ViewBinding implements Unbinder {
  private Product_Adapter_Tags target;

  @UiThread
  public Product_Adapter_Tags_ViewBinding(Product_Adapter_Tags target, View source) {
    this.target = target;

    target.product_id = Utils.findOptionalViewAsType(source, R.id.product_id, "field 'product_id'", TextView.class);
    target.MageNative_reguralprice = Utils.findOptionalViewAsType(source, R.id.MageNative_reguralprice, "field 'MageNative_reguralprice'", TextView.class);
    target.MageNative_specialprice = Utils.findOptionalViewAsType(source, R.id.MageNative_specialprice, "field 'MageNative_specialprice'", TextView.class);
    target.MageNative_title = Utils.findOptionalViewAsType(source, R.id.MageNative_title, "field 'MageNative_title'", TextView.class);
    target.MageNative_vendor = Utils.findOptionalViewAsType(source, R.id.MageNative_vendor, "field 'MageNative_vendor'", TextView.class);
    target.MageNative_image = Utils.findOptionalViewAsType(source, R.id.MageNative_image, "field 'MageNative_image'", ImageView.class);
  }

  @Override
  @CallSuper
  public void unbind() {
    Product_Adapter_Tags target = this.target;
    if (target == null) throw new IllegalStateException("Bindings already cleared.");
    this.target = null;

    target.product_id = null;
    target.MageNative_reguralprice = null;
    target.MageNative_specialprice = null;
    target.MageNative_title = null;
    target.MageNative_vendor = null;
    target.MageNative_image = null;
  }
}
