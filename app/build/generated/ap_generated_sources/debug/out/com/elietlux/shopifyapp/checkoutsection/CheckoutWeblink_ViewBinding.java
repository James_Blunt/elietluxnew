// Generated code from Butter Knife. Do not modify!
package com.elietlux.shopifyapp.checkoutsection;

import android.support.annotation.UiThread;
import android.view.View;
import android.webkit.WebView;
import butterknife.internal.Utils;
import com.elietlux.shopifyapp.R;
import com.elietlux.shopifyapp.maincontainer.MainActivity_ViewBinding;
import java.lang.IllegalStateException;
import java.lang.Override;

public class CheckoutWeblink_ViewBinding extends MainActivity_ViewBinding {
  private CheckoutWeblink target;

  @UiThread
  public CheckoutWeblink_ViewBinding(CheckoutWeblink target) {
    this(target, target.getWindow().getDecorView());
  }

  @UiThread
  public CheckoutWeblink_ViewBinding(CheckoutWeblink target, View source) {
    super(target, source);

    this.target = target;

    target.webView = Utils.findOptionalViewAsType(source, R.id.MageNative_webview, "field 'webView'", WebView.class);
  }

  @Override
  public void unbind() {
    CheckoutWeblink target = this.target;
    if (target == null) throw new IllegalStateException("Bindings already cleared.");
    this.target = null;

    target.webView = null;

    super.unbind();
  }
}
