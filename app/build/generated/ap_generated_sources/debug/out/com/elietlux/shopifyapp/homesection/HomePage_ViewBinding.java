// Generated code from Butter Knife. Do not modify!
package com.elietlux.shopifyapp.homesection;

import android.support.annotation.UiThread;
import android.support.design.widget.TabLayout;
import android.support.v4.view.ViewPager;
import android.view.View;
import android.widget.TextView;
import butterknife.internal.Utils;
import com.elietlux.shopifyapp.R;
import com.elietlux.shopifyapp.maincontainer.MainActivity_ViewBinding;
import java.lang.IllegalStateException;
import java.lang.Override;

public class HomePage_ViewBinding extends MainActivity_ViewBinding {
  private HomePage target;

  @UiThread
  public HomePage_ViewBinding(HomePage target) {
    this(target, target.getWindow().getDecorView());
  }

  @UiThread
  public HomePage_ViewBinding(HomePage target, View source) {
    super(target, source);

    this.target = target;

    target.pager = Utils.findOptionalViewAsType(source, R.id.MageNative_pager, "field 'pager'", ViewPager.class);
    target.tabLayout = Utils.findOptionalViewAsType(source, R.id.tabs, "field 'tabLayout'", TabLayout.class);
    target.searchBar = Utils.findOptionalViewAsType(source, R.id.searchBar, "field 'searchBar'", TextView.class);
    target.country = Utils.findOptionalViewAsType(source, R.id.country, "field 'country'", TextView.class);
  }

  @Override
  public void unbind() {
    HomePage target = this.target;
    if (target == null) throw new IllegalStateException("Bindings already cleared.");
    this.target = null;

    target.pager = null;
    target.tabLayout = null;
    target.searchBar = null;
    target.country = null;

    super.unbind();
  }
}
