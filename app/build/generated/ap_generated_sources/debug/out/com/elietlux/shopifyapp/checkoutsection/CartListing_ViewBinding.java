// Generated code from Butter Knife. Do not modify!
package com.elietlux.shopifyapp.checkoutsection;

import android.support.annotation.UiThread;
import android.view.View;
import android.widget.Button;
import android.widget.EditText;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.RelativeLayout;
import android.widget.TextView;
import butterknife.internal.Utils;
import com.elietlux.shopifyapp.R;
import com.elietlux.shopifyapp.maincontainer.MainActivity_ViewBinding;
import java.lang.IllegalStateException;
import java.lang.Override;

public class CartListing_ViewBinding extends MainActivity_ViewBinding {
  private CartListing target;

  @UiThread
  public CartListing_ViewBinding(CartListing target) {
    this(target, target.getWindow().getDecorView());
  }

  @UiThread
  public CartListing_ViewBinding(CartListing target, View source) {
    super(target, source);

    this.target = target;

    target.cartcontainer = Utils.findOptionalViewAsType(source, R.id.cartcontainer, "field 'cartcontainer'", LinearLayout.class);
    target.relativemain = Utils.findOptionalViewAsType(source, R.id.relativemain, "field 'relativemain'", LinearLayout.class);
    target.taxsection = Utils.findOptionalViewAsType(source, R.id.taxsection, "field 'taxsection'", RelativeLayout.class);
    target.subtotalprice = Utils.findOptionalViewAsType(source, R.id.subtotalprice, "field 'subtotalprice'", TextView.class);
    target.discountprice = Utils.findOptionalViewAsType(source, R.id.discountprice, "field 'discountprice'", TextView.class);
    target.taxprice = Utils.findOptionalViewAsType(source, R.id.taxprice, "field 'taxprice'", TextView.class);
    target.grandtotalprice = Utils.findOptionalViewAsType(source, R.id.grandtotalprice, "field 'grandtotalprice'", TextView.class);
    target.MageNative_applycoupantag = Utils.findOptionalViewAsType(source, R.id.MageNative_applycoupantag, "field 'MageNative_applycoupantag'", EditText.class);
    target.MageNative_applycoupan = Utils.findOptionalViewAsType(source, R.id.MageNative_applycoupan, "field 'MageNative_applycoupan'", Button.class);
    target.upperpart = Utils.findOptionalViewAsType(source, R.id.upperpart, "field 'upperpart'", LinearLayout.class);
    target.MageNative_couponcode = Utils.findOptionalViewAsType(source, R.id.MageNative_couponcode, "field 'MageNative_couponcode'", LinearLayout.class);
    target.imageView_applycoupon = Utils.findOptionalViewAsType(source, R.id.imageView_applycoupon, "field 'imageView_applycoupon'", ImageView.class);
    target.MageNative_checkout = Utils.findOptionalViewAsType(source, R.id.MageNative_checkout, "field 'MageNative_checkout'", TextView.class);
  }

  @Override
  public void unbind() {
    CartListing target = this.target;
    if (target == null) throw new IllegalStateException("Bindings already cleared.");
    this.target = null;

    target.cartcontainer = null;
    target.relativemain = null;
    target.taxsection = null;
    target.subtotalprice = null;
    target.discountprice = null;
    target.taxprice = null;
    target.grandtotalprice = null;
    target.MageNative_applycoupantag = null;
    target.MageNative_applycoupan = null;
    target.upperpart = null;
    target.MageNative_couponcode = null;
    target.imageView_applycoupon = null;
    target.MageNative_checkout = null;

    super.unbind();
  }
}
