// Generated code from Butter Knife. Do not modify!
package com.elietlux.shopifyapp.productviewsection;

import android.support.annotation.CallSuper;
import android.support.annotation.UiThread;
import android.view.View;
import android.widget.ImageView;
import butterknife.Unbinder;
import butterknife.internal.Utils;
import com.elietlux.shopifyapp.R;
import java.lang.IllegalStateException;
import java.lang.Override;

public class ProductImageAdapter2_ViewBinding implements Unbinder {
  private ProductImageAdapter2 target;

  @UiThread
  public ProductImageAdapter2_ViewBinding(ProductImageAdapter2 target, View source) {
    this.target = target;

    target.imageView = Utils.findOptionalViewAsType(source, R.id.MageNative_galleryimage, "field 'imageView'", ImageView.class);
  }

  @Override
  @CallSuper
  public void unbind() {
    ProductImageAdapter2 target = this.target;
    if (target == null) throw new IllegalStateException("Bindings already cleared.");
    this.target = null;

    target.imageView = null;
  }
}
