// Generated code from Butter Knife. Do not modify!
package com.elietlux.shopifyapp.maincontainer;

import android.support.annotation.CallSuper;
import android.support.annotation.UiThread;
import android.view.View;
import android.widget.LinearLayout;
import android.widget.RelativeLayout;
import android.widget.TextView;
import butterknife.Unbinder;
import butterknife.internal.Utils;
import com.elietlux.shopifyapp.R;
import java.lang.IllegalStateException;
import java.lang.Override;

public class FragmentDrawer_ViewBinding implements Unbinder {
  private FragmentDrawer target;

  @UiThread
  public FragmentDrawer_ViewBinding(FragmentDrawer target, View source) {
    this.target = target;

    target.appversion = Utils.findOptionalViewAsType(source, R.id.appversion, "field 'appversion'", TextView.class);
    target.section2 = Utils.findOptionalViewAsType(source, R.id.section2, "field 'section2'", RelativeLayout.class);
    target.menus = Utils.findOptionalViewAsType(source, R.id.sidemenus, "field 'menus'", LinearLayout.class);
    target.section3 = Utils.findOptionalViewAsType(source, R.id.section3, "field 'section3'", RelativeLayout.class);
    target.extramenus = Utils.findOptionalViewAsType(source, R.id.extramenus, "field 'extramenus'", LinearLayout.class);
    target.copyright = Utils.findOptionalViewAsType(source, R.id.copyright, "field 'copyright'", TextView.class);
    target.section1 = Utils.findOptionalViewAsType(source, R.id.section1, "field 'section1'", RelativeLayout.class);
    target.myaccount_section = Utils.findRequiredViewAsType(source, R.id.myaccount_section, "field 'myaccount_section'", RelativeLayout.class);
    target.setting_section = Utils.findRequiredViewAsType(source, R.id.setting_section, "field 'setting_section'", RelativeLayout.class);
    target.language_section = Utils.findRequiredViewAsType(source, R.id.language_section, "field 'language_section'", RelativeLayout.class);
    target.home_section = Utils.findRequiredViewAsType(source, R.id.home_section, "field 'home_section'", RelativeLayout.class);
    target.bag_section = Utils.findRequiredViewAsType(source, R.id.bag_section, "field 'bag_section'", RelativeLayout.class);
    target.wish_section = Utils.findRequiredViewAsType(source, R.id.wish_section, "field 'wish_section'", RelativeLayout.class);
    target.invitesection = Utils.findRequiredViewAsType(source, R.id.invitesection, "field 'invitesection'", RelativeLayout.class);
    target.MageNative_signin = Utils.findRequiredViewAsType(source, R.id.MageNative_signin, "field 'MageNative_signin'", TextView.class);
    target.privacy = Utils.findRequiredViewAsType(source, R.id.privacy, "field 'privacy'", TextView.class);
    target.refund = Utils.findRequiredViewAsType(source, R.id.refund, "field 'refund'", TextView.class);
    target.terms = Utils.findRequiredViewAsType(source, R.id.terms, "field 'terms'", TextView.class);
  }

  @Override
  @CallSuper
  public void unbind() {
    FragmentDrawer target = this.target;
    if (target == null) throw new IllegalStateException("Bindings already cleared.");
    this.target = null;

    target.appversion = null;
    target.section2 = null;
    target.menus = null;
    target.section3 = null;
    target.extramenus = null;
    target.copyright = null;
    target.section1 = null;
    target.myaccount_section = null;
    target.setting_section = null;
    target.language_section = null;
    target.home_section = null;
    target.bag_section = null;
    target.wish_section = null;
    target.invitesection = null;
    target.MageNative_signin = null;
    target.privacy = null;
    target.refund = null;
    target.terms = null;
  }
}
