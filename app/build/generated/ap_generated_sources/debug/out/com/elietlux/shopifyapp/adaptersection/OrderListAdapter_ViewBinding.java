// Generated code from Butter Knife. Do not modify!
package com.elietlux.shopifyapp.adaptersection;

import android.support.annotation.CallSuper;
import android.support.annotation.UiThread;
import android.view.View;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.TextView;
import butterknife.Unbinder;
import butterknife.internal.Utils;
import com.elietlux.shopifyapp.R;
import java.lang.IllegalStateException;
import java.lang.Override;

public class OrderListAdapter_ViewBinding implements Unbinder {
  private OrderListAdapter target;

  @UiThread
  public OrderListAdapter_ViewBinding(OrderListAdapter target, View source) {
    this.target = target;

    target.orderviewurl = Utils.findOptionalViewAsType(source, R.id.orderviewurl, "field 'orderviewurl'", TextView.class);
    target.order_id = Utils.findOptionalViewAsType(source, R.id.order_id, "field 'order_id'", TextView.class);
    target.order_date = Utils.findOptionalViewAsType(source, R.id.order_date, "field 'order_date'", TextView.class);
    target.linear_items = Utils.findOptionalViewAsType(source, R.id.linear_items, "field 'linear_items'", LinearLayout.class);
    target.track_order = Utils.findOptionalViewAsType(source, R.id.track_order, "field 'track_order'", TextView.class);
    target.view_details = Utils.findOptionalViewAsType(source, R.id.view_details, "field 'view_details'", TextView.class);
    target.order_status = Utils.findOptionalViewAsType(source, R.id.order_status, "field 'order_status'", TextView.class);
    target.orderimage = Utils.findOptionalViewAsType(source, R.id.orderimage, "field 'orderimage'", ImageView.class);
  }

  @Override
  @CallSuper
  public void unbind() {
    OrderListAdapter target = this.target;
    if (target == null) throw new IllegalStateException("Bindings already cleared.");
    this.target = null;

    target.orderviewurl = null;
    target.order_id = null;
    target.order_date = null;
    target.linear_items = null;
    target.track_order = null;
    target.view_details = null;
    target.order_status = null;
    target.orderimage = null;
  }
}
