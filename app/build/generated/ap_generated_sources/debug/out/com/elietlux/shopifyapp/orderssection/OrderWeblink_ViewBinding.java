// Generated code from Butter Knife. Do not modify!
package com.elietlux.shopifyapp.orderssection;

import android.support.annotation.UiThread;
import android.view.View;
import android.webkit.WebView;
import butterknife.internal.Utils;
import com.elietlux.shopifyapp.R;
import com.elietlux.shopifyapp.maincontainer.MainActivity_ViewBinding;
import java.lang.IllegalStateException;
import java.lang.Override;

public class OrderWeblink_ViewBinding extends MainActivity_ViewBinding {
  private OrderWeblink target;

  @UiThread
  public OrderWeblink_ViewBinding(OrderWeblink target) {
    this(target, target.getWindow().getDecorView());
  }

  @UiThread
  public OrderWeblink_ViewBinding(OrderWeblink target, View source) {
    super(target, source);

    this.target = target;

    target.webView = Utils.findOptionalViewAsType(source, R.id.MageNative_webview, "field 'webView'", WebView.class);
  }

  @Override
  public void unbind() {
    OrderWeblink target = this.target;
    if (target == null) throw new IllegalStateException("Bindings already cleared.");
    this.target = null;

    target.webView = null;

    super.unbind();
  }
}
