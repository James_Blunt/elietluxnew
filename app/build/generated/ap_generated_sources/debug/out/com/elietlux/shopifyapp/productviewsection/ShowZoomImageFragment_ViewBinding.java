// Generated code from Butter Knife. Do not modify!
package com.elietlux.shopifyapp.productviewsection;

import android.support.annotation.CallSuper;
import android.support.annotation.UiThread;
import android.view.View;
import android.widget.ImageView;
import butterknife.Unbinder;
import butterknife.internal.Utils;
import com.elietlux.shopifyapp.R;
import java.lang.IllegalStateException;
import java.lang.Override;

public class ShowZoomImageFragment_ViewBinding implements Unbinder {
  private ShowZoomImageFragment target;

  @UiThread
  public ShowZoomImageFragment_ViewBinding(ShowZoomImageFragment target, View source) {
    this.target = target;

    target.bmImage = Utils.findRequiredViewAsType(source, R.id.MageNative_image, "field 'bmImage'", ScaleImageView.class);
    target.cross = Utils.findRequiredViewAsType(source, R.id.MageNative_cross, "field 'cross'", ImageView.class);
  }

  @Override
  @CallSuper
  public void unbind() {
    ShowZoomImageFragment target = this.target;
    if (target == null) throw new IllegalStateException("Bindings already cleared.");
    this.target = null;

    target.bmImage = null;
    target.cross = null;
  }
}
