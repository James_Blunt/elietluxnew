// Generated code from Butter Knife. Do not modify!
package com.elietlux.shopifyapp.homesection;

import android.support.annotation.CallSuper;
import android.support.annotation.UiThread;
import android.view.View;
import butterknife.Unbinder;
import butterknife.internal.Utils;
import com.elietlux.shopifyapp.R;
import in.myinnos.alphabetsindexfastscrollrecycler.IndexFastScrollRecyclerView;
import java.lang.IllegalStateException;
import java.lang.Override;

public class BrandsFragment_ViewBinding implements Unbinder {
  private BrandsFragment target;

  @UiThread
  public BrandsFragment_ViewBinding(BrandsFragment target, View source) {
    this.target = target;

    target.categories_list = Utils.findOptionalViewAsType(source, R.id.categories_list, "field 'categories_list'", IndexFastScrollRecyclerView.class);
  }

  @Override
  @CallSuper
  public void unbind() {
    BrandsFragment target = this.target;
    if (target == null) throw new IllegalStateException("Bindings already cleared.");
    this.target = null;

    target.categories_list = null;
  }
}
