// Generated code from Butter Knife. Do not modify!
package com.elietlux.shopifyapp.addresssection;

import android.support.annotation.UiThread;
import android.view.View;
import android.widget.ListView;
import android.widget.TextView;
import butterknife.internal.Utils;
import com.elietlux.shopifyapp.R;
import com.elietlux.shopifyapp.maincontainer.MainActivity_ViewBinding;
import java.lang.IllegalStateException;
import java.lang.Override;

public class AddressList_ViewBinding extends MainActivity_ViewBinding {
  private AddressList target;

  @UiThread
  public AddressList_ViewBinding(AddressList target) {
    this(target, target.getWindow().getDecorView());
  }

  @UiThread
  public AddressList_ViewBinding(AddressList target, View source) {
    super(target, source);

    this.target = target;

    target.address_list = Utils.findOptionalViewAsType(source, R.id.address_list, "field 'address_list'", ListView.class);
    target.addaddress = Utils.findOptionalViewAsType(source, R.id.addaddress, "field 'addaddress'", TextView.class);
    target.addaddress2 = Utils.findOptionalViewAsType(source, R.id.addaddress2, "field 'addaddress2'", TextView.class);
  }

  @Override
  public void unbind() {
    AddressList target = this.target;
    if (target == null) throw new IllegalStateException("Bindings already cleared.");
    this.target = null;

    target.address_list = null;
    target.addaddress = null;
    target.addaddress2 = null;

    super.unbind();
  }
}
