// Generated code from Butter Knife. Do not modify!
package com.elietlux.shopifyapp.productviewsection;

import android.support.annotation.CallSuper;
import android.support.annotation.UiThread;
import android.view.View;
import android.widget.ImageView;
import butterknife.Unbinder;
import butterknife.internal.Utils;
import com.elietlux.shopifyapp.R;
import java.lang.IllegalStateException;
import java.lang.Override;

public class ProductViewImageFragment_ViewBinding implements Unbinder {
  private ProductViewImageFragment target;

  @UiThread
  public ProductViewImageFragment_ViewBinding(ProductViewImageFragment target, View source) {
    this.target = target;

    target.image = Utils.findOptionalViewAsType(source, R.id.MageNative_image, "field 'image'", ImageView.class);
  }

  @Override
  @CallSuper
  public void unbind() {
    ProductViewImageFragment target = this.target;
    if (target == null) throw new IllegalStateException("Bindings already cleared.");
    this.target = null;

    target.image = null;
  }
}
