// Generated code from Butter Knife. Do not modify!
package com.elietlux.shopifyapp.productlistingsection;

import android.support.annotation.UiThread;
import android.view.View;
import android.widget.Button;
import android.widget.LinearLayout;
import android.widget.RadioGroup;
import butterknife.internal.Utils;
import com.elietlux.shopifyapp.R;
import com.elietlux.shopifyapp.maincontainer.MainActivity_ViewBinding;
import java.lang.IllegalStateException;
import java.lang.Override;

public class FilterActivity_ViewBinding extends MainActivity_ViewBinding {
  private FilterActivity target;

  @UiThread
  public FilterActivity_ViewBinding(FilterActivity target) {
    this(target, target.getWindow().getDecorView());
  }

  @UiThread
  public FilterActivity_ViewBinding(FilterActivity target, View source) {
    super(target, source);

    this.target = target;

    target.lienar_main_cat_filter = Utils.findOptionalViewAsType(source, R.id.lienar_main_cat_filter, "field 'lienar_main_cat_filter'", LinearLayout.class);
    target.linear_sortSection = Utils.findOptionalViewAsType(source, R.id.linear_sortSection, "field 'linear_sortSection'", RadioGroup.class);
    target.applyFilter = Utils.findOptionalViewAsType(source, R.id.btn_apply, "field 'applyFilter'", Button.class);
    target.clear_filter = Utils.findOptionalViewAsType(source, R.id.clear_filter, "field 'clear_filter'", Button.class);
  }

  @Override
  public void unbind() {
    FilterActivity target = this.target;
    if (target == null) throw new IllegalStateException("Bindings already cleared.");
    this.target = null;

    target.lienar_main_cat_filter = null;
    target.linear_sortSection = null;
    target.applyFilter = null;
    target.clear_filter = null;

    super.unbind();
  }
}
