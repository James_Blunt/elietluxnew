package com.elietlux.shopifyapp.adaptersection;

import android.app.Activity;
import android.content.Context;
import android.graphics.Paint;
import android.os.Handler;
import android.support.annotation.NonNull;
import android.support.annotation.Nullable;
import android.util.Base64;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.BaseAdapter;
import android.widget.GridView;
import android.widget.ImageView;
import android.widget.TextView;

import com.bumptech.glide.Glide;
import com.bumptech.glide.load.engine.DiskCacheStrategy;
import com.bumptech.glide.request.RequestOptions;
import com.elietlux.shopifyapp.R;
import com.elietlux.shopifyapp.currencysection.CurrencyFormatter;
import com.elietlux.shopifyapp.currencysection.NewCurrencyFormatter;
import com.elietlux.shopifyapp.requestsection.ApiClient;
import com.elietlux.shopifyapp.storagesection.LocalData;
import com.shopify.buy3.GraphClient;
import com.shopify.buy3.Storefront;

import org.json.JSONArray;
import org.json.JSONObject;

import java.io.UnsupportedEncodingException;
import java.lang.ref.WeakReference;
import java.math.BigDecimal;
import java.security.acl.LastOwnerException;
import java.util.List;
import java.util.Objects;

import butterknife.BindView;
import butterknife.ButterKnife;

public class Product_Adapter_Tags extends BaseAdapter
{
    @Nullable
    private static LayoutInflater inflater = null;
    private final WeakReference<Activity> activity;
    private final List<Storefront.Product> data;
    @NonNull
    private final LocalData management;
    @Nullable @BindView(R.id.product_id) TextView product_id;
    @Nullable @BindView(R.id.MageNative_reguralprice) TextView MageNative_reguralprice;
    @Nullable @BindView(R.id.MageNative_specialprice) TextView MageNative_specialprice;
    @Nullable @BindView(R.id.MageNative_title) TextView MageNative_title;
    @Nullable @BindView(R.id.MageNative_vendor) TextView MageNative_vendor;
    @Nullable @BindView(R.id.MageNative_image) ImageView MageNative_image;
    String currency_symbol;
    View vi=null;
    GridView grid;
    GraphClient client = null;
    String presentmentcurrency = null;
    public Product_Adapter_Tags(Activity a, List<Storefront.Product> d, String currency_symbol, GridView grid)
    {
        activity = new WeakReference<Activity>(a);
        data = d;
        inflater = (LayoutInflater) activity.get().getSystemService(Context.LAYOUT_INFLATER_SERVICE);
        management = new LocalData(activity.get());
        this.currency_symbol=currency_symbol;
        this.grid=grid;
        client = ApiClient.getGraphClient(activity.get(), true);
        presentmentcurrency=management.getCurrencyCode();
    }
    @Override
    public int getCount() {
        return data.size();
    }

    @Override
    public Object getItem(int position) {
        return position;
    }

    @Override
    public long getItemId(int position) {
        return position;
    }

    @Nullable
    @Override
    public View getView(final int position, @Nullable View convertView, ViewGroup parent) {
        if (convertView == null) {
            vi = Objects.requireNonNull(inflater).inflate(R.layout.magenative_product_list_item, parent, false);
        } else {
            vi = convertView;
        }
        final ImageView wishlist= vi.findViewById(R.id.wishlist);
        //final TextView product_id = vi.findViewById(R.id.product_id);
        //final TextView MageNative_title = vi.findViewById(R.id.MageNative_title);
        //final ImageView MageNative_image = vi.findViewById(R.id.MageNative_image);
        ButterKnife.bind(this, vi);
        try
        {
           /* final Handler handler = new Handler();
            handler.postDelayed(new Runnable() {
                @Override
                public void run() {

                }
            },200);*/

            if(management.getWishList()!=null)
            {
                JSONObject object=new JSONObject(management.getWishList());
                if(object.has(data.get(position).getId().toString()))
                {
                    wishlist.setImageResource(R.drawable.wishred);
                }
                else
                {
                    wishlist.setImageResource(R.drawable.wishlike);
                }
            }
            else
            {
                wishlist.setImageResource(R.drawable.wishlike);
            }
            Log.e("data",""+data);
            product_id.setText(data.get(position).getId().toString());
            MageNative_title.setText(data.get(position).getTitle().toString());
            MageNative_vendor.setText(data.get(position).getVendor().toString());

            Storefront.ProductVariant variant= data.get(position).getVariants().getEdges().get(0).getNode();
            Storefront.ProductVariantPricePairEdge edge = getEdge(variant.getPresentmentPrices().getEdges());

            String regularprice= NewCurrencyFormatter.setsymbol(edge.getNode().getPrice().getAmount(), edge.getNode().getPrice().getCurrencyCode().toString());
            Log.d("regularprice",""+regularprice);

            if (data.get(position).getVariants().getEdges().get(0).getNode().getCompareAtPrice()==null)
            {
                MageNative_specialprice.setVisibility(View.GONE);
                MageNative_reguralprice.setText(regularprice);
                MageNative_reguralprice.setPaintFlags(MageNative_reguralprice.getPaintFlags() & (~Paint.STRIKE_THRU_TEXT_FLAG));
            }
            else
            {
                Double special = Double.valueOf(edge.getNode().getCompareAtPrice().getAmount());
                Double regular = Double.valueOf(edge.getNode().getPrice().getAmount());


                if(BigDecimal.valueOf(special).compareTo(BigDecimal.valueOf(regular))==1)
                {
                    regularprice = NewCurrencyFormatter.setsymbol(edge.getNode().getCompareAtPrice().getAmount(), edge.getNode().getCompareAtPrice().getCurrencyCode().toString());
                    String specialprice = NewCurrencyFormatter.setsymbol(edge.getNode().getPrice().getAmount(), edge.getNode().getPrice().getCurrencyCode().toString());

                    /*String specialprice= CurrencyFormatter.setsymbol(data.get(position).getVariants().getEdges().get(0).getNode().getCompareAtPrice(),activity.get(),currency_symbol);*/
                    MageNative_reguralprice.setText(regularprice);
                    MageNative_reguralprice.setPaintFlags(MageNative_reguralprice.getPaintFlags() | Paint.STRIKE_THRU_TEXT_FLAG);
                    MageNative_reguralprice.setTextColor(activity.get().getResources().getColor(R.color.black));
                    MageNative_specialprice.setVisibility(View.VISIBLE);
                    MageNative_specialprice.setText(specialprice);
                }
                else
                {
                   /* MageNative_specialprice.setVisibility(View.GONE);
                    MageNative_reguralprice.setText(regularprice);
                    MageNative_reguralprice.setPaintFlags(MageNative_reguralprice.getPaintFlags() & (~Paint.STRIKE_THRU_TEXT_FLAG));*/

                    regularprice = NewCurrencyFormatter.setsymbol(edge.getNode().getCompareAtPrice().getAmount(), edge.getNode().getCompareAtPrice().getCurrencyCode().toString());
                    String specialprice = NewCurrencyFormatter.setsymbol(edge.getNode().getPrice().getAmount(), edge.getNode().getPrice().getCurrencyCode().toString());


                    /*String specialprice=CurrencyFormatter.setsymbol(data.get(position).getNode().getVariants().getEdges().get(0).getNode().getCompareAtPrice(),context.get(),localData.getMoneyFormat());*/
                    MageNative_reguralprice.setText(regularprice);
                    MageNative_reguralprice.setPaintFlags(MageNative_reguralprice.getPaintFlags() | Paint.STRIKE_THRU_TEXT_FLAG);
                    MageNative_reguralprice.setTextColor(activity.get().getResources().getColor(R.color.black));
                    MageNative_specialprice.setVisibility(View.VISIBLE);
                    MageNative_specialprice.setText(specialprice);
                }
            }

            Glide.with(activity.get())
                    .load(data.get(position).getImages().getEdges().get(0).getNode().getTransformedSrc())
                    .apply(new RequestOptions()
                            .centerCrop()
                            .placeholder(R.drawable.placeholder)
                            .diskCacheStrategy(DiskCacheStrategy.ALL)
                            .dontTransform()
                            .error(R.drawable.placeholder))
                    .into(MageNative_image);

            wishlist.setOnClickListener(new View.OnClickListener()
            {
                @Override
                public void onClick(View view)
                {
                    processwishlist(wishlist,position);
                }
            });

        }
        catch (Exception e)
        {
            e.printStackTrace();
        }
        return vi;
    }

    private Storefront.ProductVariantPricePairEdge getEdge(List<Storefront.ProductVariantPricePairEdge> edges) {
        Storefront.ProductVariantPricePairEdge pairEdge = null;
        try {
            for (int i = 0; i < edges.size(); i++) {
                Log.i("presentment", edges.get(i).getNode().getPrice().getCurrencyCode().toString());
                Log.i("presentment", presentmentcurrency);

                if (edges.get(i).getNode().getPrice().getCurrencyCode().toString().equals(presentmentcurrency)) {
                    pairEdge = edges.get(i);
                    break;
                }
            }
        } catch (Exception e) {
            e.printStackTrace();
        }
        return  pairEdge;
    }


    private void processwishlist(ImageView wishlist,int position)
    {
        try
        {
            Log.i("Wishlist",""+position);
            int varinats=data.get(position).getVariants().getEdges().size();
            Log.i("Wishlist",""+varinats);
            String varinatid=data.get(position).getVariants().getEdges().get(0).getNode().getId().toString();
            if(management.getWishList()!=null)
            {
                JSONObject object=new JSONObject((management.getWishList()));
                if(object.has(data.get(position).getId().toString()))
                {
                    Log.i("Wishlist","Three");
                    object.remove(data.get(position).getId().toString());
                    management.saveWishList(object);
                    wishlist.setImageResource(R.drawable.wishlike);
                }
                else
                {
                    Log.i("Wishlist","TWO");
                    JSONObject subobject=new JSONObject();
                    subobject.put("product_id",data.get(position).getId().toString());
                    subobject.put("product_name",data.get(position).getTitle().trim());
                    if (data.get(position).getVariants().getEdges().get(0).getNode().getCompareAtPrice()==null) {
                        subobject.put("regular_price",data.get(position).getVariants().getEdges().get(0).getNode().getPrice());
                    }else {
                        subobject.put("regular_price",data.get(position).getVariants().getEdges().get(0).getNode().getPrice());
                        subobject.put("compare_price",data.get(position).getVariants().getEdges().get(0).getNode().getCompareAtPrice());

                    }
                    subobject.put("varinats",varinats);
                    subobject.put("varinatid",varinatid);
                    subobject.put("image",data.get(position).getImages().getEdges().get(0).getNode().getTransformedSrc());
                    object.put(data.get(position).getId().toString(),subobject);
                    management.saveWishList(object);
                    wishlist.setImageResource(R.drawable.wishred);
                }
            }
            else
            {
                Log.i("Wishlist","First");
                JSONObject object=new JSONObject();
                JSONObject subobject=new JSONObject();
                subobject.put("product_id",data.get(position).getId().toString());
                subobject.put("product_name",data.get(position).getTitle().trim());
                if (data.get(position).getVariants().getEdges().get(0).getNode().getCompareAtPrice()==null) {
                    subobject.put("regular_price",data.get(position).getVariants().getEdges().get(0).getNode().getPrice());
                }else {
                    subobject.put("regular_price",data.get(position).getVariants().getEdges().get(0).getNode().getPrice());
                    subobject.put("compare_price",data.get(position).getVariants().getEdges().get(0).getNode().getCompareAtPrice());

                }
                subobject.put("varinats",varinats);
                subobject.put("varinatid",varinatid);
                subobject.put("image",data.get(position).getImages().getEdges().get(0).getNode().getTransformedSrc());
                object.put(data.get(position).getId().toString(),subobject);
                management.saveWishList(object);
                wishlist.setImageResource(R.drawable.wishred);
            }
        }
        catch (Exception e)
        {
            e.printStackTrace();
        }
    }
    private String getBase64Encode(String id)
    {
        byte[] data = Base64.encode(id.getBytes(), Base64.DEFAULT);
        try {
            id = new String(data, "UTF-8").trim();
        } catch (UnsupportedEncodingException e) {
            e.printStackTrace();
        }
        return id;
    }

}

