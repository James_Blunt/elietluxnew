package com.elietlux.shopifyapp.adaptersection;

import android.app.Activity;
import android.content.Context;
import android.graphics.Paint;
import android.support.annotation.NonNull;
import android.support.annotation.Nullable;
import android.util.Base64;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.BaseAdapter;
import android.widget.GridView;
import android.widget.ImageView;
import android.widget.TextView;

import com.bumptech.glide.Glide;
import com.bumptech.glide.load.engine.DiskCacheStrategy;
import com.bumptech.glide.request.RequestOptions;
import com.elietlux.shopifyapp.R;
import com.elietlux.shopifyapp.addresssection.AddressList;
import com.elietlux.shopifyapp.currencysection.CurrencyFormatter;
import com.elietlux.shopifyapp.requestsection.ApiClient;
import com.elietlux.shopifyapp.storagesection.LocalData;
import com.elietlux.shopifyapp.storefrontqueries.Query;
import com.elietlux.shopifyapp.storefrontresponse.AsyncResponse;
import com.elietlux.shopifyapp.storefrontresponse.Response;
import com.shopify.buy3.GraphCallResult;
import com.shopify.buy3.GraphClient;
import com.shopify.buy3.GraphResponse;
import com.shopify.buy3.QueryGraphCall;
import com.shopify.buy3.Storefront;

import org.json.JSONObject;

import java.io.UnsupportedEncodingException;
import java.lang.ref.WeakReference;
import java.util.List;
import java.util.Objects;

import butterknife.BindView;
import butterknife.ButterKnife;

public class Product_Adapter_Filter extends BaseAdapter {
    @Nullable
    private static LayoutInflater inflater = null;
    private final WeakReference<Activity> activity;
    private final List<Storefront.Product> data;
    @NonNull
    private final LocalData management;
    String currency_symbol;
    View vi = null;
    GridView grid;
    GraphClient client;
    public Product_Adapter_Filter(Activity a, List d, String currency_symbol, GridView grid) {
        activity = new WeakReference<Activity>(a);
        data = d;
        inflater = (LayoutInflater) activity.get().getSystemService(Context.LAYOUT_INFLATER_SERVICE);
        management = new LocalData(activity.get());
        this.currency_symbol = currency_symbol;
        this.grid = grid;
        client= ApiClient.getGraphClient(activity.get(),true);
    }

    @Override
    public int getCount() {
        return data.size();
    }

    @Override
    public Object getItem(int position) {
        return position;
    }

    @Override
    public long getItemId(int position) {
        return position;
    }

    @Nullable
    @Override
    public View getView(final int position, @Nullable View convertView, ViewGroup parent) {
        if (convertView == null) {
            vi = Objects.requireNonNull(inflater).inflate(R.layout.magenative_product_list_item, parent, false);
        } else {
            vi = convertView;
        }
        final ImageView wishlist = vi.findViewById(R.id.wishlist);
        TextView product_id = vi.findViewById(R.id.product_id);
        TextView MageNative_reguralprice = vi.findViewById(R.id.MageNative_reguralprice);
        TextView MageNative_specialprice = vi.findViewById(R.id.MageNative_specialprice);
        TextView  MageNative_title = vi.findViewById(R.id.MageNative_title);
        TextView  MageNative_vendor = vi.findViewById(R.id.MageNative_vendor);
        ImageView MageNative_image = vi.findViewById(R.id.MageNative_image);
        try {


            Storefront.Product productEdge = data.get(position);
            product_id.setText(productEdge.getId().toString());
            Log.d("Vaibhavvvvvvv",""+data.get(position).getImages().getEdges().get(0).getNode().getOriginalSrc());
            MageNative_title.setText(productEdge.getTitle());
            MageNative_vendor.setText(productEdge.getVendor());
            String regularprice = CurrencyFormatter.setsymbol(productEdge.getVariants().getEdges().get(0).getNode().getPrice(),activity.get(),currency_symbol);
            if (productEdge.getVariants().getEdges().get(0).getNode().getCompareAtPrice() == null) {
                MageNative_specialprice.setVisibility(View.GONE);
                MageNative_reguralprice.setText(regularprice);
                MageNative_reguralprice.setPaintFlags(MageNative_reguralprice.getPaintFlags() & (~Paint.STRIKE_THRU_TEXT_FLAG));
            } else {
                if (productEdge.getVariants().getEdges().get(0).getNode().getCompareAtPrice().compareTo(productEdge.getVariants().getEdges().get(0).getNode().getPrice()) == 1) {
                    String specialprice = CurrencyFormatter.setsymbol(productEdge.getVariants().getEdges().get(0).getNode().getCompareAtPrice(),activity.get(),currency_symbol);
                    MageNative_reguralprice.setText(specialprice);
                    MageNative_reguralprice.setPaintFlags(MageNative_reguralprice.getPaintFlags() | Paint.STRIKE_THRU_TEXT_FLAG);
                    MageNative_reguralprice.setTextColor(activity.get().getResources().getColor(R.color.black));
                    MageNative_specialprice.setVisibility(View.VISIBLE);
                    MageNative_specialprice.setText(regularprice);
                } else {
                    MageNative_specialprice.setVisibility(View.GONE);
                    MageNative_reguralprice.setText(regularprice);
                    MageNative_reguralprice.setPaintFlags(MageNative_reguralprice.getPaintFlags() & (~Paint.STRIKE_THRU_TEXT_FLAG));
                }
            }

            Glide.with(activity.get())
                    .load(productEdge.getImages().getEdges().get(0).getNode().getTransformedSrc())
                    .apply(new RequestOptions().centerCrop().placeholder(R.drawable.placeholder).diskCacheStrategy(DiskCacheStrategy.ALL)
                            .dontTransform()
                            .error(R.drawable.placeholder))
                    .into(MageNative_image);
            if(management.getWishList()!=null)
            {
                JSONObject object=new JSONObject(management.getWishList());
                if(object.has(productEdge.getId().toString()))
                {
                    wishlist.setImageResource(R.drawable.wishred);
                }
                else
                {
                    wishlist.setImageResource(R.drawable.wishlike);
                }
            }
            else
            {
                wishlist.setImageResource(R.drawable.wishlike);
            }

        } catch (Exception e) {
            e.printStackTrace();
        }
        return vi;
    }

   /* private void processwishlist(ImageView wishlist,int position)
    {
        try
        {
            Log.i("Wishlist",""+position);
            int varinats=data.get(position).getNode().getVariants().getEdges().size();
            Log.i("Wishlist",""+varinats);
            String varinatid=data.get(position).getNode().getVariants().getEdges().get(0).getNode().getId().toString();
            if(management.getWishList()!=null)
            {
                JSONObject object=new JSONObject((management.getWishList()));
                if(object.has(data.get(position).getNode().getId().toString()))
                {
                    Log.i("Wishlist","Three");
                    object.remove(data.get(position).getNode().getId().toString());
                    management.saveWishList(object);
                    wishlist.setImageResource(R.drawable.wishlike);
                }
                else
                {
                    Log.i("Wishlist","TWO");
                    JSONObject subobject=new JSONObject();
                    subobject.put("product_id",data.get(position).getNode().getId().toString());
                    subobject.put("product_name",data.get(position).getNode().getTitle().trim());
                    subobject.put("varinats",varinats);
                    subobject.put("varinatid",varinatid);
                    subobject.put("image",data.get(position).getNode().getImages().getEdges().get(0).getNode().getTransformedSrc());
                    object.put(data.get(position).getNode().getId().toString(),subobject);
                    management.saveWishList(object);
                    wishlist.setImageResource(R.drawable.wishred);
                }
            }
            else
            {
                Log.i("Wishlist","First");
                JSONObject object=new JSONObject();
                JSONObject subobject=new JSONObject();
                subobject.put("product_id",data.get(position).getNode().getId().toString());
                subobject.put("product_name",data.get(position).getNode().getTitle().trim());
                subobject.put("varinats",varinats);
                subobject.put("varinatid",varinatid);
                subobject.put("image",data.get(position).getNode().getImages().getEdges().get(0).getNode().getTransformedSrc());
                object.put(data.get(position).getNode().getId().toString(),subobject);
                management.saveWishList(object);
                wishlist.setImageResource(R.drawable.wishred);
            }
        }
        catch (Exception e)
        {
            e.printStackTrace();
        }
    }*/

    private void getProductData(String product_id, TextView mageNative_title, TextView mageNative_reguralprice, TextView mageNative_specialprice, ImageView mageNative_image, ImageView wishlist,int position) {
        try {
            QueryGraphCall call = client.queryGraph(Query.getSingleProduct(product_id));
            Response.getGraphQLResponse(call, new AsyncResponse() {
                @Override
                public void finalOutput(@NonNull Object output, @NonNull boolean error) {
                    if (error) {
                        GraphResponse<Storefront.QueryRoot> response = ((GraphCallResult.Success<Storefront.QueryRoot>) output).getResponse();
                        activity.get().runOnUiThread(new Runnable() {
                            @Override
                            public void run() {
                                processProductData(response,mageNative_title,mageNative_reguralprice,mageNative_specialprice,mageNative_image,wishlist,position);
                            }
                        });
                    } else {
                        Log.i("ResponseError", " " + output.toString());
                    }
                }
            }, activity.get());
        } catch (Exception e) {
            e.printStackTrace();
        }
    }
    private void processProductData(GraphResponse<Storefront.QueryRoot> response, TextView mageNative_title, TextView mageNative_reguralprice, TextView mageNative_specialprice, ImageView mageNative_image, ImageView wishlist,int position) {
        try {
            Storefront.Product productEdge = (Storefront.Product) response.getData().getNode();
            Log.i("ResponseError", "arabic " + productEdge.getTitle());
            mageNative_title.setText(productEdge.getTitle());
            String regularprice = CurrencyFormatter.setsymbol(productEdge.getVariants().getEdges().get(0).getNode().getPrice(),activity.get(),currency_symbol);
            if (productEdge.getVariants().getEdges().get(0).getNode().getCompareAtPrice() == null) {
                mageNative_specialprice.setVisibility(View.GONE);
                mageNative_reguralprice.setText(regularprice);
                mageNative_reguralprice.setPaintFlags(mageNative_reguralprice.getPaintFlags() & (~Paint.STRIKE_THRU_TEXT_FLAG));
            } else {
                if (productEdge.getVariants().getEdges().get(0).getNode().getCompareAtPrice().compareTo(productEdge.getVariants().getEdges().get(0).getNode().getPrice()) == 1) {
                    String specialprice = CurrencyFormatter.setsymbol(productEdge.getVariants().getEdges().get(0).getNode().getCompareAtPrice(),activity.get(),currency_symbol);
                    mageNative_reguralprice.setText(specialprice);
                    mageNative_reguralprice.setPaintFlags(mageNative_reguralprice.getPaintFlags() | Paint.STRIKE_THRU_TEXT_FLAG);
                    mageNative_reguralprice.setTextColor(activity.get().getResources().getColor(R.color.black));
                    mageNative_specialprice.setVisibility(View.VISIBLE);
                    mageNative_specialprice.setText(regularprice);
                } else {
                    mageNative_specialprice.setVisibility(View.GONE);
                    mageNative_reguralprice.setText(regularprice);
                    mageNative_reguralprice.setPaintFlags(mageNative_reguralprice.getPaintFlags() & (~Paint.STRIKE_THRU_TEXT_FLAG));
                }
            }

            Glide.with(activity.get())
                    .load(productEdge.getImages().getEdges().get(0).getNode().getTransformedSrc())
                    .apply(new RequestOptions().centerCrop().placeholder(R.drawable.placeholder).diskCacheStrategy(DiskCacheStrategy.ALL)
                            .dontTransform()
                            .error(R.drawable.placeholder))
                    .into(mageNative_image);
            if(management.getWishList()!=null)
            {
                JSONObject object=new JSONObject(management.getWishList());
                if(object.has(productEdge.getId().toString()))
                {
                    wishlist.setImageResource(R.drawable.wishred);
                }
                else
                {
                    wishlist.setImageResource(R.drawable.wishlike);
                }
            }
            else
            {
                wishlist.setImageResource(R.drawable.wishlike);
            }
            // Log.d("dataa",""+data);

        } catch (Exception e) {
            e.printStackTrace();
        }
    }
    public String getBase64Encode(String id) {
        byte[] data = Base64.encode(id.getBytes(), Base64.DEFAULT);
        try {
            id = new String(data, "UTF-8").trim();
        } catch (UnsupportedEncodingException e) {
            e.printStackTrace();
        }
        return id;
    }

}