package com.elietlux.shopifyapp.adaptersection;


import android.app.Activity;
import android.content.Context;
import android.content.Intent;
import android.support.annotation.NonNull;
import android.support.annotation.Nullable;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.SectionIndexer;
import android.widget.TextView;

import com.elietlux.shopifyapp.R;
import com.elietlux.shopifyapp.productlistingsection.ProductListing;
import com.elietlux.shopifyapp.searchsection.SearchView;

import org.json.JSONArray;

import java.lang.ref.WeakReference;
import java.util.ArrayList;
import java.util.List;

import butterknife.BindView;

public class BrandsAdapter extends RecyclerView.Adapter<BrandsViewholder> implements SectionIndexer {
    @Nullable
    private static LayoutInflater inflater = null;
    @Nullable
    @BindView(R.id.brands)
    TextView brands;
    private WeakReference<Activity> activity;
    private JSONArray data;
    private ArrayList<Integer> mSectionPositions;


    public BrandsAdapter(Activity a, JSONArray d) {
        activity = new WeakReference<Activity>(a);
        data = d;
        inflater = (LayoutInflater) activity.get().getSystemService(Context.LAYOUT_INFLATER_SERVICE);
    }

    @NonNull
    @Override
    public BrandsViewholder onCreateViewHolder(@NonNull ViewGroup viewGroup, int i) {
        LayoutInflater mInflater = LayoutInflater.from(viewGroup.getContext());
        ViewGroup mainGroup = (ViewGroup) mInflater.inflate(R.layout.magenative_brands_listing, viewGroup, false);
        return new BrandsViewholder(mainGroup);
    }

    @Override
    public void onBindViewHolder(@NonNull BrandsViewholder brandsViewholder, int i) {
        try {
            brandsViewholder.brands.setText(data.getString(i));
            brandsViewholder.brands.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View v) {
                    Intent intent = new Intent(activity.get(), ProductListing.class);
                    intent.putExtra("cat_id",brandsViewholder.brands.getText());
                    intent.putExtra("isFrombrands","true");
                    activity.get().startActivity(intent);
                }
            });
        } catch (Exception e) {
            e.printStackTrace();
        }
    }

    @Override
    public int getItemCount() {
        return data.length();
    }



    @Override
    public Object[] getSections() {
        List<String> sections = new ArrayList<>(26);
        mSectionPositions = new ArrayList<>(26);
        try{
            for (int i = 0, size = data.length(); i < size; i++) {
                String section = String.valueOf(data.getString(i).charAt(0)).toUpperCase();
                if (!sections.contains(section)) {
                    sections.add(section);
                    mSectionPositions.add(i);
                }
            }
        }catch (Exception e){
            e.printStackTrace();
        }
        return sections.toArray(new String[0]);
    }

    @Override
    public int getSectionForPosition(int position) {
        return 0;

    }

    @Override
    public int getPositionForSection(int sectionIndex) {
        return mSectionPositions.get(sectionIndex);
    }

    public static String[] toStringArray(JSONArray array) {
        if(array==null)
            return null;

        String[] arr=new String[array.length()];
        for(int i=0; i<arr.length; i++) {
            arr[i]=array.optString(i);
        }
        return arr;
    }
}

