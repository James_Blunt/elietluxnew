package com.elietlux.shopifyapp.adaptersection;

import android.support.annotation.NonNull;
import android.support.v7.widget.RecyclerView;
import android.view.View;
import android.widget.TextView;

import com.elietlux.shopifyapp.R;

public class BrandsViewholder extends RecyclerView.ViewHolder {
    TextView brands;
    public BrandsViewholder(@NonNull View itemView) {
        super(itemView);
        brands = itemView.findViewById(R.id.brands);
    }
}
