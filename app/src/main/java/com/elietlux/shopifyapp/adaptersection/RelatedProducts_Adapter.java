package com.elietlux.shopifyapp.adaptersection;
import android.app.Activity;
import android.content.Intent;
import android.graphics.Paint;
import android.os.Handler;
import android.support.annotation.NonNull;
import android.support.v4.app.FragmentActivity;
import android.support.v7.widget.RecyclerView;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;

import com.blogspot.atifsoftwares.animatoolib.Animatoo;
import com.bumptech.glide.Glide;
import com.bumptech.glide.load.engine.DiskCacheStrategy;
import com.bumptech.glide.request.RequestOptions;
import com.elietlux.shopifyapp.currencysection.Currency;
import com.elietlux.shopifyapp.currencysection.NewCurrencyFormatter;
import com.shopify.buy3.Storefront;
import com.elietlux.shopifyapp.R;
import com.elietlux.shopifyapp.currencysection.CurrencyFormatter;
import com.elietlux.shopifyapp.productlistingsection.ProductListing;
import com.elietlux.shopifyapp.productviewsection.ProductView;
import com.elietlux.shopifyapp.storagesection.LocalData;
import com.elietlux.shopifyapp.viewholders.RelatedProductViewHolder;
import org.json.JSONArray;
import org.json.JSONObject;

import java.io.Serializable;
import java.lang.ref.WeakReference;
import java.math.BigDecimal;
import java.util.List;

public class RelatedProducts_Adapter extends RecyclerView.Adapter<RelatedProductViewHolder>
{
    private final List<Storefront.ProductEdge> data;
    private final WeakReference<Activity> context;
    LocalData localData=null;
    String catID="";
    int pos;
    boolean homepage=false;
    String presentmentcurrency = null;
    String regularprice =null;

    public RelatedProducts_Adapter(Activity context, List<Storefront.ProductEdge> d,String catID,int pos,Boolean homepage)
    {
        this.context = new WeakReference<Activity>(context);
        this.data = d;
        localData=new LocalData(context);
        this.catID=catID;
        this.pos=pos;
        this.homepage = homepage;
        presentmentcurrency=localData.getCurrencyCode();
    }
    public RelatedProducts_Adapter(Activity context, List<Storefront.ProductEdge> d)
    {
        this.context = new WeakReference<Activity>(context);
        this.data = d;
        localData=new LocalData(context);
        homepage=false;
        presentmentcurrency=localData.getCurrencyCode();
    }

    @Override
    public int getItemViewType(int position)
    {
        return super.getItemViewType(position);
    }

    @Override
    public void onBindViewHolder(@NonNull RelatedProductViewHolder holder, int position)
    {
        try
        {
           /* Storefront.ProductVariant variant = this.data.get(position).getNode().getVariants().getEdges().get(0).getNode();
            Storefront.ProductVariantPricePairEdge edge = getEdge(variant.getPresentmentPrices().getEdges());*/

            if(localData.getWishList()!=null)
            {
                JSONObject object=new JSONObject(localData.getWishList());
                if(object.has(data.get(position).getNode().getId().toString()))
                {
                    holder.wishlist.setImageResource(R.drawable.wishred);
                }
                else
                {
                    holder.wishlist.setImageResource(R.drawable.wishlike);
                }
            }
            holder.title.setText(data.get(position).getNode().getTitle().trim());
            holder.vedor.setText(data.get(position).getNode().getVendor().trim());
            holder.id.setText(data.get(position).getNode().getId().toString());

             regularprice = NewCurrencyFormatter.setsymbol(data.get(position).getNode().getVariants().getEdges().get(0).getNode().getPresentmentPrices().getEdges().get(0).getNode().getPrice().getAmount(), data.get(position).getNode().getVariants().getEdges().get(0).getNode().getPresentmentPrices().getEdges().get(0).getNode().getPrice().getCurrencyCode().toString());
            /* String regularprice= CurrencyFormatter.setsymbol(data.get(position).getNode().getVariants().getEdges().get(0).getNode().getPrice(),context.get(),localData.getMoneyFormat());*/



            if (homepage){
                holder.main_container.setBackground(context.get().getResources().getDrawable(R.drawable.border2));
            }
            if (data.get(position).getNode().getVariants().getEdges().get(0).getNode().getCompareAtPrice()==null)
            {
                holder.MageNative_specialprice.setVisibility(View.GONE);
                holder.MageNative_reguralprice.setText(regularprice);
                holder.MageNative_reguralprice.setPaintFlags(holder.MageNative_reguralprice.getPaintFlags() & (~Paint.STRIKE_THRU_TEXT_FLAG));
            }
            else
            {
                Double special = Double.valueOf(data.get(position).getNode().getVariants().getEdges().get(0).getNode().getCompareAtPrice().toString());
                Double regular = Double.valueOf(data.get(position).getNode().getVariants().getEdges().get(0).getNode().getPrice().toString());



                if(BigDecimal.valueOf(special).compareTo(BigDecimal.valueOf(regular))==1)
                {
                    regularprice = NewCurrencyFormatter.setsymbol(data.get(position).getNode().getVariants().getEdges().get(0).getNode().getPresentmentPrices().getEdges().get(0).getNode().getCompareAtPrice().getAmount(), data.get(position).getNode().getVariants().getEdges().get(0).getNode().getPresentmentPrices().getEdges().get(0).getNode().getCompareAtPrice().getCurrencyCode().toString());
                    String specialprice = NewCurrencyFormatter.setsymbol(data.get(position).getNode().getVariants().getEdges().get(0).getNode().getPresentmentPrices().getEdges().get(0).getNode().getPrice().getAmount(), data.get(position).getNode().getVariants().getEdges().get(0).getNode().getPresentmentPrices().getEdges().get(0).getNode().getPrice().getCurrencyCode().toString());


                    /*String specialprice=CurrencyFormatter.setsymbol(data.get(position).getNode().getVariants().getEdges().get(0).getNode().getCompareAtPrice(),context.get(),localData.getMoneyFormat());*/
                    holder.MageNative_reguralprice.setText(regularprice);
                    holder.MageNative_reguralprice.setPaintFlags(holder.MageNative_reguralprice.getPaintFlags() | Paint.STRIKE_THRU_TEXT_FLAG);
                    holder.MageNative_reguralprice.setTextColor(context.get().getResources().getColor(R.color.black));
                    holder.MageNative_specialprice.setVisibility(View.VISIBLE);
                    holder.MageNative_specialprice.setText(specialprice);
                }
                else
                {

                    regularprice = NewCurrencyFormatter.setsymbol(data.get(position).getNode().getVariants().getEdges().get(0).getNode().getPresentmentPrices().getEdges().get(0).getNode().getCompareAtPrice().getAmount(),data.get(position).getNode().getVariants().getEdges().get(0).getNode().getPresentmentPrices().getEdges().get(0).getNode().getCompareAtPrice().getCurrencyCode().toString());
                    String specialprice = NewCurrencyFormatter.setsymbol(data.get(position).getNode().getVariants().getEdges().get(0).getNode().getPresentmentPrices().getEdges().get(0).getNode().getPrice().getAmount(), data.get(position).getNode().getVariants().getEdges().get(0).getNode().getPresentmentPrices().getEdges().get(0).getNode().getPrice().getCurrencyCode().toString());



                    /*String specialprice=CurrencyFormatter.setsymbol(data.get(position).getNode().getVariants().getEdges().get(0).getNode().getCompareAtPrice(),context.get(),localData.getMoneyFormat());*/
                    holder.MageNative_reguralprice.setText(regularprice);
                    holder.MageNative_reguralprice.setPaintFlags(holder.MageNative_reguralprice.getPaintFlags() | Paint.STRIKE_THRU_TEXT_FLAG);
                    holder.MageNative_reguralprice.setTextColor(context.get().getResources().getColor(R.color.black));
                    holder.MageNative_specialprice.setVisibility(View.VISIBLE);
                    holder.MageNative_specialprice.setText(specialprice);

                }

            }
            Glide.with(context.get())
                    .load(data.get(position).getNode().getImages().getEdges().get(0).getNode().getTransformedSrc())
                    .apply(new RequestOptions().placeholder(R.drawable.placeholder).diskCacheStrategy(DiskCacheStrategy.ALL)
                            .dontTransform()
                            .error(R.drawable.placeholder))
                    .into(holder.imageview);



            holder.imageview.setOnClickListener(new View.OnClickListener()
            {
                @Override
                public void onClick(View view)
                {
                    Intent intent=new Intent(context.get(), ProductView.class);
                    intent.putExtra("object",(Serializable)data.get(position));
                    intent.putExtra("id",holder.id.getText().toString());
                    if(homepage)
                    {
                        intent.putExtra("catID",catID);
                        intent.putExtra("position",pos+1);
                    }
                    context.get().startActivity(intent);
                    Animatoo.animateZoom(context.get());
                   // context.get().overridePendingTransition(R.anim.magenative_slide_in, R.anim.magenative_slide_out);
                }
            });
            holder.wishlist.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View view) {
                    processwishlist(holder.wishlist,holder.id.getText().toString(),position);
                }
            });


        }
        catch (Exception e)
        {
            e.printStackTrace();
        }
    }

    @NonNull
    @Override
    public RelatedProductViewHolder onCreateViewHolder(@NonNull ViewGroup viewGroup, int viewType)
    {
        LayoutInflater mInflater = LayoutInflater.from(viewGroup.getContext());
        ViewGroup mainGroup = (ViewGroup) mInflater.inflate(R.layout.magenative_product_list_itemw, viewGroup, false);
        return new RelatedProductViewHolder(mainGroup, context.get());
    }
    @Override
    public int getItemCount() {
        if (!homepage){
            return (null != data ? data.size() : 0);
        }else {
            return 10;
        }

    }
    private Storefront.ProductVariantPricePairEdge getEdge(List<Storefront.ProductVariantPricePairEdge> edges) {
        Storefront.ProductVariantPricePairEdge pairEdge  = null;
    try {
        for (int i=0; i< edges.size();i++) {
            Log.i("presentment",edges.get(i).getNode().getPrice().getCurrencyCode().toString());
            Log.i("presentment",presentmentcurrency);

            if (edges.get(i).getNode().getPrice().getCurrencyCode().toString().equals(presentmentcurrency)) {
                pairEdge = edges.get(i);
                break;
            }
        }
    } catch (Exception e) {
        e.printStackTrace();
    }

    return pairEdge;
}

    private void processwishlist(ImageView wishlist, String s, int position)
    {
        try
        {
            int varinats=data.get(position).getNode().getVariants().getEdges().size();
            String varinatid=data.get(position).getNode().getVariants().getEdges().get(0).getNode().getId().toString();
            if(localData.getWishList()!=null)
            {
                JSONObject object=new JSONObject((localData.getWishList()));
                if(object.has(s))
                {
                    object.remove(s);
                    localData.saveWishList(object);
                    wishlist.setImageResource(R.drawable.wishlike);
                }
                else
                {
                    JSONObject subobject=new JSONObject();
                    subobject.put("product_id",data.get(position).getNode().getId().toString());
                    subobject.put("product_name",data.get(position).getNode().getTitle().trim());
                    subobject.put("varinats",varinats);
                    subobject.put("varinatid",varinatid);
                    subobject.put("image",data.get(position).getNode().getImages().getEdges().get(0).getNode().getTransformedSrc());
                    object.put(data.get(position).getNode().getId().toString(),subobject);
                    localData.saveWishList(object);
                    wishlist.setImageResource(R.drawable.wishred);
                }
            }
            else
            {
                JSONObject object=new JSONObject();
                JSONObject subobject=new JSONObject();
                subobject.put("product_id",data.get(position).getNode().getId().toString());
                subobject.put("product_name",data.get(position).getNode().getTitle().trim());
                subobject.put("varinats",varinats);
                subobject.put("varinatid",varinatid);
                subobject.put("image",data.get(position).getNode().getImages().getEdges().get(0).getNode().getTransformedSrc());
                object.put(data.get(position).getNode().getId().toString(),subobject);
                localData.saveWishList(object);
                wishlist.setImageResource(R.drawable.wishred);
            }
        }
        catch (Exception e)
        {
            e.printStackTrace();
        }
    }
}