package com.elietlux.shopifyapp.adaptersection;
import android.content.Context;
import android.content.Intent;
import android.support.annotation.NonNull;
import android.support.annotation.Nullable;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.BaseAdapter;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.QuickContactBadge;
import android.widget.TextView;

import com.blogspot.atifsoftwares.animatoolib.Animatoo;
import com.bumptech.glide.Glide;
import com.bumptech.glide.load.engine.DiskCacheStrategy;
import com.bumptech.glide.request.RequestOptions;
import com.elietlux.shopifyapp.checkoutsection.CartListing;
import com.elietlux.shopifyapp.maincontainer.MainActivity;
import com.elietlux.shopifyapp.orderssection.OrderListing;
import com.elietlux.shopifyapp.orderssection.OrderWeblink;
import com.elietlux.shopifyapp.storagesection.LocalData;
import com.shopify.buy3.Storefront;
import com.elietlux.shopifyapp.R;
import org.json.JSONObject;
import java.lang.ref.WeakReference;
import java.text.SimpleDateFormat;
import java.util.Date;
import java.util.Iterator;
import java.util.List;
import java.util.Locale;
import java.util.Objects;

import butterknife.BindView;
import butterknife.ButterKnife;

public class OrderListAdapter extends BaseAdapter
{
    @Nullable @BindView(R.id.orderviewurl) TextView orderviewurl;
    @Nullable @BindView(R.id.order_id) TextView order_id;
    @Nullable @BindView(R.id.order_date) TextView order_date;
    @Nullable @BindView(R.id.linear_items)
    LinearLayout linear_items;
    @Nullable @BindView(R.id.track_order)
    TextView track_order;
    @Nullable @BindView(R.id.view_details)
    TextView view_details;
    @Nullable @BindView(R.id.order_status)
    TextView order_status;
    @Nullable @BindView(R.id.orderimage)
    ImageView orderimage;

    private final List<Storefront.OrderEdge> array;
    @NonNull
    private final LayoutInflater infalInflater;
    WeakReference<Context> context;
    LocalData data;
    public OrderListAdapter(Context con, List<Storefront.OrderEdge> array)
    {
        this.context=new WeakReference<Context>(con);
        this.array = array;
        infalInflater = (LayoutInflater) Objects.requireNonNull(context.get().getSystemService(Context.LAYOUT_INFLATER_SERVICE));
        data = new LocalData(context.get());
    }
    @Override
    public int getCount()
    {
        return array.size();
    }

    @Override
    public Object getItem(int position) {
        return position;
    }

    @Override
    public long getItemId(int position) {
        return position;
    }


    @Nullable
    @Override
    public View getView(int position, @Nullable View convertView, ViewGroup parent)
    {
        View vi = null;
        try
        {
            if(convertView == null)
            {
                vi = infalInflater.inflate(R.layout.list_item, parent, false);
            }
            else
            {
                vi = convertView;
            }
            ButterKnife.bind(this,vi);
            order_id.setText(""+array.get(position).getNode().getOrderNumber().toString());
            order_status.setText(array.get(position).getNode().getFulfillmentStatus().toString());
            List<Storefront.OrderLineItemEdge> lineedges = array.get(position).getNode().getLineItems().getEdges();
            View view=null;
            TextView orderitem;
            if(lineedges.size()>0)
            {
                Storefront.OrderLineItemEdge edge=null;
                Iterator<Storefront.OrderLineItemEdge> iterator=lineedges.iterator();
                linear_items.removeAllViews();

                if (linear_items.getTag()==null){
                    while (iterator.hasNext())
                    {
                        edge = iterator.next();
                        view=View.inflate(context.get(),R.layout.ordersinlgeitem,null);
                        orderitem = view.findViewById(R.id.orderitem);
                        orderitem.setText(edge.getNode().getQuantity()+" X "+edge.getNode().getTitle());
                        Log.i("title_arr",""+edge.getNode().getTitle());
                        Log.i("title_arr",""+ edge.getNode().getQuantity());
                        linear_items.addView(orderitem);
                        //linear_items.setTag("Done");
                    }
                    linear_items.setVisibility(View.VISIBLE);
                }else {
                    linear_items.setVisibility(View.GONE);
                }
            }
            orderviewurl.setText(array.get(position).getNode().getCustomerUrl());
            SimpleDateFormat sdf2 = new SimpleDateFormat("MMM dd yyyy", Locale.getDefault());
            SimpleDateFormat sdf = new SimpleDateFormat("yyyy-MM-dd'T'HH:mm:ss", Locale.getDefault());
            Date expiretime = sdf.parse(array.get(position).getNode().getProcessedAt().toLocalDateTime().toString());
            String time=sdf2.format(expiretime);
            order_date.setText(time);

            if (array.get(position).getNode().getLineItems().getEdges().get(0).getNode().getVariant()!=null){
                Glide.with(context.get())
                        .load(array.get(position).getNode().getLineItems().getEdges().get(0).getNode().getVariant().getImage().getOriginalSrc())
                        //.load(array.get(position).getNode().getLineItems().getEdges().get(0).getNode().getVariant().getImage().getTransformedSrc())
                        .apply(new RequestOptions().centerCrop().placeholder(R.drawable.placeholder).diskCacheStrategy(DiskCacheStrategy.ALL)
                                .dontTransform()
                                .error(R.drawable.placeholder))
                        .into(orderimage);
            }else {
                Glide.with(context.get())
                        .load("https://icon-library.com/images/box-icon/box-icon-17.jpg")
                        .apply(new RequestOptions().centerCrop().placeholder(R.drawable.placeholder).diskCacheStrategy(DiskCacheStrategy.ALL)
                                .dontTransform()
                                .error(R.drawable.placeholder))
                        .into(orderimage);
            }


            view_details.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View v) {
                    Intent intent=new Intent(context.get(), OrderWeblink.class);
                    intent.putExtra("link",array.get(position).getNode().getStatusUrl());
                    intent.putExtra("order",array.get(position).getNode().getOrderNumber().toString());
                    context.get().startActivity(intent);
                    Animatoo.animateFade(context.get() );
                }
            });
            track_order.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View v) {
                    Intent intent=new Intent(context.get(), OrderWeblink.class);
                    intent.putExtra("link","https://www.elitelux.store/apps/parcelpanel?"+"order="+array.get(position).getNode().getOrderNumber().toString()+"&email="+data.getEmail());
                   Log.d("orderurl",""+"https://www.elitelux.store/apps/parcelpanel?"+"order="+order_id.getText().toString()+"&email="+data.getEmail());
                   Log.d("orederno",""+array.get(position).getNode().getOrderNumber().toString());
                   intent.putExtra("order",array.get(position).getNode().getOrderNumber().toString());
                    context.get().startActivity(intent);
                    Animatoo.animateFade(context.get() );
                }
            });
        }
        catch (Exception e)
        {
            e.printStackTrace();
        }

        return vi;
    }

}