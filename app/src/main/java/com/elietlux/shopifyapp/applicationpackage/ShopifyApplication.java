package com.elietlux.shopifyapp.applicationpackage;
import android.support.annotation.NonNull;
import android.support.multidex.MultiDex;
import android.support.multidex.MultiDexApplication;
import android.util.Log;

import com.bumptech.glide.Glide;
import com.bumptech.glide.load.engine.DiskCacheStrategy;
import com.shopify.buy3.GraphClient;
import com.elietlux.shopifyapp.R;
import com.elietlux.shopifyapp.requestsection.ApiClient;
import com.elietlux.shopifyapp.requestsection.ApiInterface;
import com.elietlux.shopifyapp.splashsection.Splash;
import com.elietlux.shopifyapp.storagesection.LocalData;
import com.elietlux.shopifyapp.storefrontresponse.AsyncResponse;
import com.smartlook.sdk.smartlook.Smartlook;
import com.squareup.okhttp.ResponseBody;

import org.json.JSONException;
import org.json.JSONObject;

import retrofit2.Call;

public class ShopifyApplication extends MultiDexApplication {
    private static ShopifyApplication mInstance;
    public static synchronized ShopifyApplication getInstance() {
        return mInstance;
    }
    @Override
    public void onCreate() {
        super.onCreate();
        MultiDex.install(this);
        mInstance = this;
        Smartlook.setupAndStartRecording("47ef429593a870f639f1b3b89e0192892c4432c0");
        Smartlook.setUserIdentifier("Android");
    }
}