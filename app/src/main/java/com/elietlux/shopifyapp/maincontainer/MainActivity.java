package com.elietlux.shopifyapp.maincontainer;

import android.annotation.SuppressLint;
import android.app.Dialog;
import android.content.Context;
import android.content.Intent;
import android.content.res.AssetManager;
import android.content.res.ColorStateList;
import android.graphics.PorterDuff;
import android.os.Bundle;
import android.support.annotation.NonNull;
import android.support.annotation.Nullable;
import android.support.v4.content.ContextCompat;
import android.support.v4.widget.DrawerLayout;
import android.support.v4.widget.ImageViewCompat;
import android.support.v7.app.AppCompatActivity;
import android.support.v7.widget.Toolbar;
import android.text.Html;
import android.util.Base64;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.Menu;
import android.view.MenuItem;
import android.view.View;
import android.view.ViewGroup;
import android.view.Window;
import android.view.WindowManager;
import android.widget.CompoundButton;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.RadioButton;
import android.widget.RadioGroup;
import android.widget.RelativeLayout;
import android.widget.TextView;
import android.widget.Toast;


import com.blogspot.atifsoftwares.animatoolib.Animatoo;
import com.bumptech.glide.Glide;
import com.elietlux.shopifyapp.R;
import com.elietlux.shopifyapp.checkoutsection.CartListing;
import com.elietlux.shopifyapp.checkoutsection.CheckoutLineItems;
import com.elietlux.shopifyapp.checkoutsection.CheckoutWeblink;
import com.elietlux.shopifyapp.dashboardsection.AccountDashboard;
import com.elietlux.shopifyapp.datasection.Data;
import com.elietlux.shopifyapp.homesection.HomePage;
import com.elietlux.shopifyapp.homesection.SearchCategories;
import com.elietlux.shopifyapp.homesection.SearchCategoryStativActivity;
import com.elietlux.shopifyapp.langauagesection.LoadLanguage;
import com.elietlux.shopifyapp.loginandregistrationsection.Login;
import com.elietlux.shopifyapp.productviewsection.ProductView;
import com.elietlux.shopifyapp.requestsection.ApiClient;
import com.elietlux.shopifyapp.searchsection.AutoSearch;
import com.elietlux.shopifyapp.splashsection.Splash;
import com.elietlux.shopifyapp.storagesection.LocalData;
import com.elietlux.shopifyapp.storefrontqueries.MutationQuery;
import com.elietlux.shopifyapp.storefrontqueries.Query;
import com.elietlux.shopifyapp.storefrontresponse.AsyncResponse;
import com.elietlux.shopifyapp.storefrontresponse.Response;
import com.elietlux.shopifyapp.wishlistsection.WishListing;
import com.shopify.buy3.GraphCallResult;
import com.shopify.buy3.GraphClient;
import com.shopify.buy3.GraphResponse;
import com.shopify.buy3.MutationGraphCall;
import com.shopify.buy3.QueryGraphCall;
import com.shopify.buy3.Storefront;
import com.smartlook.sdk.smartlook.Smartlook;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import java.io.InputStream;
import java.nio.charset.StandardCharsets;
import java.util.ArrayList;
import java.util.Currency;
import java.util.HashMap;
import java.util.Iterator;
import java.util.List;
import java.util.Objects;

import butterknife.BindView;
import butterknife.ButterKnife;
import io.reactivex.SingleEmitter;

public class MainActivity extends AppCompatActivity implements FragmentDrawer.FragmentDrawerListener {
    public static JSONObject filterJson;
    public static HashMap<String, ArrayList<String>> selectedValueMap;
    @Nullable
    @BindView(R.id.MageNative_toolbar)
    public Toolbar mToolbar;
    public JSONObject color_bank;
    @Nullable
    @BindView(R.id.toolimage)
    ImageView toolimage;
    @Nullable
    @BindView(R.id.tooltext)
    TextView tooltext;
    @Nullable
    @BindView(R.id.MageNative_drawer_layout)
    DrawerLayout drawerLayout;
    @BindView(R.id.homeSelection)
    LinearLayout homeSelection;
    @BindView(R.id.categorySelection)
    LinearLayout categorySelection;
    @BindView(R.id.cartSelection)
    LinearLayout cartSelection;
    @BindView(R.id.wishSelection)
    LinearLayout wishSelection;
    @BindView(R.id.profileSelection)
    LinearLayout profileSelection;
    @BindView(R.id.imageHome)
    ImageView imageHome;
    @BindView(R.id.textHome)
    TextView textHome;
    @BindView(R.id.imageCats)
    ImageView imageCats;
    @BindView(R.id.textCats)
    TextView textCats;
    @BindView(R.id.imageCart)
    ImageView imageCart;
    @BindView(R.id.textCart)
    TextView textCart;
    @BindView(R.id.imageWish)
    ImageView imageWish;
    @BindView(R.id.textWish)
    TextView textWish;
    @BindView(R.id.imageAccount)
    ImageView imageAccount;
    @BindView(R.id.textAccount)
    TextView textAccount;
    @BindView(R.id.setting_section)
    RelativeLayout setting_section;
    LocalData localData = null;
    GraphClient apiclient = null;
    @Nullable
    private Dialog langlistDialog = null;
    String count = "0";
    CheckoutLineItems items;
    public static TextView textView1_city;
    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);
        ButterKnife.bind(MainActivity.this);
        localData = new LocalData(MainActivity.this);
        LoadLanguage.setLocale(localData.getLangCode(), MainActivity.this);
        apiclient = ApiClient.getGraphClient(MainActivity.this, true);
        langlistDialog = new Dialog(MainActivity.this, R.style.PauseDialog);
        getMoneyFormat();
        Window window = getWindow();
        items = new CheckoutLineItems(MainActivity.this);
        window.setFlags(WindowManager.LayoutParams.FLAG_TRANSLUCENT_STATUS, WindowManager.LayoutParams.FLAG_TRANSLUCENT_STATUS);
        window.getAttributes().flags &= (~WindowManager.LayoutParams.FLAG_TRANSLUCENT_STATUS);
        setSupportActionBar(mToolbar);
        mToolbar.setBackgroundColor(getResources().getColor(R.color.white));
        Smartlook.startRecording();

        Log.d("selectedValueMapmain", "" + selectedValueMap);
        Log.d("Recording",""+ Smartlook.isRecording());

        runOnUiThread(new Runnable() {
            @Override
            public void run() {
                try {
                    Glide.with(MainActivity.this)
                            .asBitmap()
                            .load(localData.getHeaderLogo())
                            .into(toolimage);

                } catch (Exception e) {
                    e.printStackTrace();
                }
            }
        });

        homeSelection.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                selectHome();
                Intent in = new Intent(MainActivity.this, HomePage.class);
                in.setFlags(Intent.FLAG_ACTIVITY_CLEAR_TASK);
                in.setFlags(Intent.FLAG_ACTIVITY_NEW_TASK);
                startActivity(in);
                Animatoo.animateFade(MainActivity.this);
                //overridePendingTransition(R.anim.magenative_slide_in, R.anim.magenative_slide_out);
            }
        });

        categorySelection.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                selectCategory();
                Intent in = new Intent(MainActivity.this, SearchCategoryStativActivity.class);
                in.setFlags(Intent.FLAG_ACTIVITY_SINGLE_TOP);
                startActivity(in);
                Animatoo.animateFade(MainActivity.this);
            }
        });

        cartSelection.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                selectCart();
                Intent intent = new Intent(MainActivity.this, CartListing.class);
                intent.setFlags(Intent.FLAG_ACTIVITY_SINGLE_TOP);
                startActivity(intent);
                Animatoo.animateFade(MainActivity.this);
               //overridePendingTransition(R.anim.magenative_slide_in, R.anim.magenative_slide_out);
            }
        });

        wishSelection.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                selectWish();
                Intent intent = new Intent(MainActivity.this, WishListing.class);
                intent.setFlags(Intent.FLAG_ACTIVITY_SINGLE_TOP);
                startActivity(intent);
                Animatoo.animateFade(MainActivity.this);
               // overridePendingTransition(R.anim.magenative_slide_in, R.anim.magenative_slide_out);
            }
        });

        profileSelection.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                selectAcount();
             /*   if (localData.isLogin()) {*/
                    Intent intent = new Intent(MainActivity.this, AccountDashboard.class);
                    intent.setFlags(Intent.FLAG_ACTIVITY_SINGLE_TOP);
                    startActivity(intent);
                    overridePendingTransition(R.anim.magenative_slide_in, R.anim.magenative_slide_out);
                /*} else {
                    Intent intent = new Intent(MainActivity.this, Login.class);
                    intent.setFlags(Intent.FLAG_ACTIVITY_SINGLE_TOP);
                    startActivity(intent);
                    overridePendingTransition(R.anim.magenative_slide_in, R.anim.magenative_slide_out);
                }*/
            }
        });



        //toolimage.setImageResource(R.drawable.logo);
        Objects.requireNonNull(getSupportActionBar()).setDisplayShowTitleEnabled(false);

        drawerLayout.setDrawerLockMode(DrawerLayout.LOCK_MODE_LOCKED_CLOSED);

        //getSupportActionBar().setDisplayShowHomeEnabled(true);
       // FragmentDrawer drawerFragment = (FragmentDrawer) getSupportFragmentManager().findFragmentById(R.id.MageNative_fragment_navigation_drawer);
       // drawerFragment.setUp(R.id.MageNative_fragment_navigation_drawer, drawerLayout, mToolbar);
    }


    public void selectHome() {
        //imageHome.setColorFilter(ContextCompat.getColor(MainActivity.this, R.color.selectionColor), android.graphics.PorterDuff.Mode.MULTIPLY);
        imageHome.setColorFilter(ContextCompat.getColor(MainActivity.this, R.color.black), android.graphics.PorterDuff.Mode.MULTIPLY);
        textHome.setTextColor(getResources().getColor(R.color.black));

        imageCats.setColorFilter(ContextCompat.getColor(MainActivity.this, R.color.iconColor), PorterDuff.Mode.MULTIPLY);
        textCats.setTextColor(getResources().getColor(R.color.iconColor));

        imageCart.setColorFilter(ContextCompat.getColor(MainActivity.this, R.color.iconColor), android.graphics.PorterDuff.Mode.MULTIPLY);
        textCart.setTextColor(getResources().getColor(R.color.iconColor));

        imageWish.setColorFilter(ContextCompat.getColor(MainActivity.this, R.color.iconColor), android.graphics.PorterDuff.Mode.MULTIPLY);
        textWish.setTextColor(getResources().getColor(R.color.iconColor));

        imageAccount.setColorFilter(ContextCompat.getColor(MainActivity.this, R.color.iconColor), android.graphics.PorterDuff.Mode.MULTIPLY);
        textAccount.setTextColor(getResources().getColor(R.color.iconColor));

    }
    public void selectCategory() {
        imageHome.setColorFilter(ContextCompat.getColor(MainActivity.this, R.color.iconColor), android.graphics.PorterDuff.Mode.MULTIPLY);
        textHome.setTextColor(getResources().getColor(R.color.iconColor));

        imageCats.setColorFilter(ContextCompat.getColor(MainActivity.this, R.color.black), android.graphics.PorterDuff.Mode.MULTIPLY);
        // ImageViewCompat.setImageTintList(imageCats, ColorStateList.valueOf(ContextCompat.getColor(MainActivity.this, R.color.black)));
        textCats.setTextColor(getResources().getColor(R.color.black));

        imageCart.setColorFilter(ContextCompat.getColor(MainActivity.this, R.color.iconColor), android.graphics.PorterDuff.Mode.MULTIPLY);
        textCart.setTextColor(getResources().getColor(R.color.iconColor));

        imageWish.setColorFilter(ContextCompat.getColor(MainActivity.this, R.color.iconColor), android.graphics.PorterDuff.Mode.MULTIPLY);
        textWish.setTextColor(getResources().getColor(R.color.iconColor));

        imageAccount.setColorFilter(ContextCompat.getColor(MainActivity.this, R.color.iconColor), android.graphics.PorterDuff.Mode.MULTIPLY);
        textAccount.setTextColor(getResources().getColor(R.color.iconColor));
    }

    public void selectCart() {
        imageHome.setColorFilter(ContextCompat.getColor(MainActivity.this, R.color.iconColor), android.graphics.PorterDuff.Mode.MULTIPLY);
        textHome.setTextColor(getResources().getColor(R.color.iconColor));

        imageCats.setColorFilter(ContextCompat.getColor(MainActivity.this, R.color.iconColor), android.graphics.PorterDuff.Mode.MULTIPLY);
        textCats.setTextColor(getResources().getColor(R.color.iconColor));

        imageCart.setColorFilter(ContextCompat.getColor(MainActivity.this, R.color.black), android.graphics.PorterDuff.Mode.MULTIPLY);
        textCart.setTextColor(getResources().getColor(R.color.black));

        imageWish.setColorFilter(ContextCompat.getColor(MainActivity.this, R.color.iconColor), android.graphics.PorterDuff.Mode.MULTIPLY);
        textWish.setTextColor(getResources().getColor(R.color.iconColor));

        imageAccount.setColorFilter(ContextCompat.getColor(MainActivity.this, R.color.iconColor), android.graphics.PorterDuff.Mode.MULTIPLY);
        textAccount.setTextColor(getResources().getColor(R.color.iconColor));

    }

    public void selectWish() {
        imageHome.setColorFilter(ContextCompat.getColor(MainActivity.this, R.color.iconColor), android.graphics.PorterDuff.Mode.MULTIPLY);
        textHome.setTextColor(getResources().getColor(R.color.iconColor));

        imageCats.setColorFilter(ContextCompat.getColor(MainActivity.this, R.color.iconColor), android.graphics.PorterDuff.Mode.MULTIPLY);
        textCats.setTextColor(getResources().getColor(R.color.iconColor));

        imageCart.setColorFilter(ContextCompat.getColor(MainActivity.this, R.color.iconColor), android.graphics.PorterDuff.Mode.MULTIPLY);
        textCart.setTextColor(getResources().getColor(R.color.iconColor));

        imageWish.setColorFilter(ContextCompat.getColor(MainActivity.this, R.color.black), android.graphics.PorterDuff.Mode.MULTIPLY);
        textWish.setTextColor(getResources().getColor(R.color.black));

        imageAccount.setColorFilter(ContextCompat.getColor(MainActivity.this, R.color.iconColor), android.graphics.PorterDuff.Mode.MULTIPLY);
        textAccount.setTextColor(getResources().getColor(R.color.iconColor));

    }

    public void selectAcount() {
        imageHome.setColorFilter(ContextCompat.getColor(MainActivity.this, R.color.iconColor), android.graphics.PorterDuff.Mode.MULTIPLY);
        textHome.setTextColor(getResources().getColor(R.color.iconColor));

        imageCats.setColorFilter(ContextCompat.getColor(MainActivity.this, R.color.iconColor), android.graphics.PorterDuff.Mode.MULTIPLY);
        textCats.setTextColor(getResources().getColor(R.color.iconColor));

        imageCart.setColorFilter(ContextCompat.getColor(MainActivity.this, R.color.iconColor), android.graphics.PorterDuff.Mode.MULTIPLY);
        textCart.setTextColor(getResources().getColor(R.color.iconColor));

        imageWish.setColorFilter(ContextCompat.getColor(MainActivity.this, R.color.iconColor), android.graphics.PorterDuff.Mode.MULTIPLY);
        textWish.setTextColor(getResources().getColor(R.color.iconColor));

        imageAccount.setColorFilter(ContextCompat.getColor(MainActivity.this, R.color.black), android.graphics.PorterDuff.Mode.MULTIPLY);
        textAccount.setTextColor(getResources().getColor(R.color.black));

    }

    public String getBase64Decode(String id) {
        byte[] data = Base64.decode(id, Base64.DEFAULT);
        String text = new String(data, StandardCharsets.UTF_8);
        String datavalue[] = text.split("/");
        String valueid = datavalue[datavalue.length - 1];
        String datavalue2[] = valueid.split("key");
        text = datavalue2[0];
        return text;
    }

    @Override
    protected void onResume() {
        super.onResume();
        LoadLanguage.setLocale(localData.getLangCode(), MainActivity.this);
        if (localData.getLineItems() != null) {
            count = String.valueOf(items.getItemcounts());
        } else {
            count = "0";
        }
        invalidateOptionsMenu();
    }

    protected void showbackbutton()
    {  // Objects.requireNonNull(FragmentDrawer.mDrawerToggle).setDrawerIndicatorEnabled(false);
        Objects.requireNonNull(getSupportActionBar()).setDisplayHomeAsUpEnabled(true);
        Objects.requireNonNull(FragmentDrawer.mDrawerToggle).setToolbarNavigationClickListener(new View.OnClickListener()
        {
            @Override
            public void onClick(View v)
            {
                onBackPressed();
            }
        });
    }

    public void showhumburger() {
        getSupportActionBar().setDisplayHomeAsUpEnabled(false);
        FragmentDrawer.mDrawerToggle.setDrawerIndicatorEnabled(true);
        FragmentDrawer.mDrawerToggle.setToolbarNavigationClickListener(null);
    }

    protected void showTittle(String tittle) {
        Objects.requireNonNull(tooltext).setVisibility(View.VISIBLE);
        Objects.requireNonNull(toolimage).setVisibility(View.GONE);
        tooltext.setText(tittle);
    }

    @Override
    public void onDrawerItemSelected(View view, int position) {

    }

    public void getMoneyFormat() {
        try {
            Log.d("getMoneyFormat","1");
            if (localData.getMoneyFormat() == null) {
                Log.d("getMoneyFormat","2");
                QueryGraphCall call = apiclient.queryGraph(Query.getShopDetails());
                Response.getGraphQLResponse(call, new AsyncResponse() {
                    @Override
                    public void finalOutput(@NonNull Object output, @NonNull boolean error) {
                        if (error) {
                            Log.d("getMoneyFormat","3");
                            GraphResponse<Storefront.QueryRoot> response = ((GraphCallResult.Success<Storefront.QueryRoot>) output).getResponse();
                            Log.i("ResponseErrorcurr", "" + response.getData().getShop().getPaymentSettings().getCurrencyCode().toString());
                            localData.saveMoneyFormat(response.getData().getShop().getPaymentSettings().getCurrencyCode().toString());
                            localData.savePrivacyPolicy(response.getData().getShop().getPrivacyPolicy().getUrl(), response.getData().getShop().getPrivacyPolicy().getTitle());
                            localData.saveRefundPolicy(response.getData().getShop().getRefundPolicy().getUrl(), response.getData().getShop().getRefundPolicy().getTitle());
                            localData.saveTerms(response.getData().getShop().getTermsOfService().getUrl(), response.getData().getShop().getTermsOfService().getTitle());
                        } else {
                            Log.d("getMoneyFormat","4");
                            Log.i("ResponseError", "" + output.toString());
                        }
                    }
                }, MainActivity.this);


            }
        } catch (Exception e) {
            e.printStackTrace();
        }
    }

    public void showAppSetting() {
        try {
            ((ViewGroup) ((ViewGroup) Objects.requireNonNull(Objects.requireNonNull(langlistDialog).getWindow()).getDecorView()).getChildAt(0))
                    .getChildAt(1)
                    .setBackgroundColor(getResources().getColor(R.color.black));
            Objects.requireNonNull(langlistDialog).setTitle(Html.fromHtml("<center><font color='#ffffff'>" + getResources().getString(R.string.currency) + "</font></center>"));
            LayoutInflater li = (LayoutInflater) this.getSystemService(Context.LAYOUT_INFLATER_SERVICE);
            @SuppressLint("InflateParams") View v = Objects.requireNonNull(li).inflate(R.layout.setting, null, false);
            RadioGroup storelist = v.findViewById(R.id.langlist);
            RadioGroup currencylist = v.findViewById(R.id.currencylist);
            JSONObject object;
            RadioButton button;
            for (int i = 0; i < Data.getCountry().length(); i++) {
                object = Data.getCountry().getJSONObject(i);
                RadioButton cbutton = new RadioButton(MainActivity.this);
                cbutton.setText(object.getString("name"));
                cbutton.setTag(object.getString("code"));
                if (localData.getCountryCode().equals(object.getString("code"))) {
                    cbutton.setChecked(true);
                }
                final RadioButton finalButton = cbutton;
                cbutton.setOnCheckedChangeListener(new CompoundButton.OnCheckedChangeListener() {
                    @Override
                    public void onCheckedChanged(CompoundButton compoundButton, boolean b) {
                        if (b) {
                            localData.saveCountryCode(String.valueOf(finalButton.getTag()));
                            localData.saveLangCode(localData.getCountryCode());
                            LoadLanguage.setLocale(localData.getLangCode(), MainActivity.this);
                            localData.logout();
                            localData.clearData();
                            langlistDialog.dismiss();
                            Intent intent = new Intent(MainActivity.this, Splash.class);
                            intent.addFlags(Intent.FLAG_ACTIVITY_CLEAR_TASK);
                            intent.addFlags(Intent.FLAG_ACTIVITY_NEW_TASK);
                            startActivity(intent);

                        }
                    }
                });
                storelist.addView(cbutton);
            }
            for (int i = 0; i < Data.getCurrency().length(); i++) {
                object = Data.getCurrency().getJSONObject(i);
                button = new RadioButton(MainActivity.this);
                Log.d("currencytest",""+object);
                button.setText(object.getString("code"));
                button.setTag(object.getString("code"));
                if (localData.getCurrencyCode().equals(object.getString("code"))) {
                    button.setChecked(true);
                }
                final RadioButton finalButton = button;
                button.setOnCheckedChangeListener(new CompoundButton.OnCheckedChangeListener() {
                    @Override
                    public void onCheckedChanged(CompoundButton compoundButton, boolean b) {
                        if (b) {
                            localData.saveCurrencyCode(String.valueOf(finalButton.getTag()));
                            localData.saveisSeleceted(String.valueOf(finalButton.getTag()));

                           // Toast.makeText(MainActivity.this, ""+String.valueOf(finalButton.getTag()), Toast.LENGTH_SHORT).show();
                            Intent intent = new Intent(MainActivity.this, Splash.class);
                            intent.addFlags(Intent.FLAG_ACTIVITY_CLEAR_TASK);
                            intent.addFlags(Intent.FLAG_ACTIVITY_NEW_TASK);
                            startActivity(intent);
                            langlistDialog.dismiss();
                        }
                    }
                });
                currencylist.addView(button);
            }
            langlistDialog.setContentView(v);
            langlistDialog.setCancelable(true);
            langlistDialog.show();
        } catch (Exception e) {
            e.printStackTrace();
        }
    }

    @Override
    public boolean onCreateOptionsMenu(@NonNull Menu menu) {
        getMenuInflater().inflate(R.menu.menu_product, menu);

        MenuItem cityitem =menu.findItem(R.id.MageNative_action_city);
        cityitem.setActionView(R.layout.magenative_feed_update_city);
        View cityrtext = cityitem.getActionView();
        textView1_city = cityrtext.findViewById(R.id.city);
        if (localData.getCurrencyCode()!=null){

            textView1_city.setText(localData.getCurrencyCode());
        }

        textView1_city.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                showAppSetting();
            }
        });

        MenuItem item = menu.findItem(R.id.MageNative_action_cart);
        item.setActionView(R.layout.magenative_feed_update_count);

        View notifCount = item.getActionView();
        RelativeLayout red_circle = notifCount.findViewById(R.id.red_circle);
        TextView textView = notifCount.findViewById(R.id.MageNative_hotlist_hot);

        if(!count.equals("0")){
            red_circle.setVisibility(View.VISIBLE);
            textView.setText(count);
        }
        else {
            red_circle.setVisibility(View.GONE);
        }
        notifCount.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                Intent intent = new Intent(getApplicationContext(), CartListing.class);
                intent.setFlags(Intent.FLAG_ACTIVITY_SINGLE_TOP);
                startActivity(intent);
                overridePendingTransition(R.anim.magenative_slide_in, R.anim.magenative_slide_out);
            }
        });
        return true;
    }

    @Override
    public boolean onOptionsItemSelected(@NonNull MenuItem item) {
        switch (item.getItemId()) {
            case android.R.id.home:
                this.finish();
                return true;
            case R.id.wishlist:
                showAppSetting();
                return true;
            case R.id.MageNative_action_search:
                Intent searchintent = new Intent(MainActivity.this, AutoSearch.class);
                startActivity(searchintent);
                overridePendingTransition(R.anim.magenative_slide_in, R.anim.magenative_slide_out);
                return true;

            default:
                return super.onOptionsItemSelected(item);
        }

    }

    public void LoginUser(String username, String password, String origin, String weburl) {

        try {
            MutationGraphCall call = apiclient.mutateGraph(MutationQuery.createCustomerAccessToken(username, password));
            Response.getMutationGraphQLResponse(call, new AsyncResponse() {
                @Override
                public void finalOutput(@NonNull Object output, @NonNull boolean error) {
                    if (error) {
                        GraphResponse<Storefront.Mutation> response = ((GraphCallResult.Success<Storefront.Mutation>) output).getResponse();
                        runOnUiThread(new Runnable() {
                            @Override
                            public void run() {
                                List<Storefront.UserError> errors = response.getData().getCustomerAccessTokenCreate().getUserErrors();
                                if (errors.size() > 0) {
                                    Iterator iterator = errors.iterator();
                                    String err = "";
                                    while (iterator.hasNext()) {
                                        Storefront.UserError error = (Storefront.UserError) iterator.next();
                                        err += error.getMessage();
                                    }
                                    Toast.makeText(MainActivity.this, err, Toast.LENGTH_LONG).show();
                                } else {
                                    processLogin(response, username, password, origin, weburl);
                                    Login.getInstance().finish();
                                }
                            }
                        });
                    } else {
                        Log.i("ResponseError", "" + output.toString());
                    }
                }
            }, MainActivity.this);
        } catch (Exception e) {
            e.printStackTrace();
        }
    }

    private void processLogin(GraphResponse<Storefront.Mutation> response, String username, String password, String origin, String weburl) {
        try {
            Storefront.CustomerAccessToken token = response.getData().getCustomerAccessTokenCreate().getCustomerAccessToken();
            localData.createSession(username, password);
            Log.i("ExpireAt", "" + token.getExpiresAt().toLocalDateTime().toString());
            localData.saveAccesstokenWithExpiry(token.getAccessToken(), token.getExpiresAt().toLocalDateTime().toString());
            Toast.makeText(MainActivity.this, getResources().getString(R.string.successfullogin), Toast.LENGTH_LONG).show();
            if (localData.getFirstName() == null) {
                QueryGraphCall call = apiclient.queryGraph(Query.getCustomerDetails(token.getAccessToken()));
                Response.getGraphQLResponse(call, new AsyncResponse() {
                    @Override
                    public void finalOutput(@NonNull Object output, @NonNull boolean error) {
                        if (error) {
                            GraphResponse<Storefront.QueryRoot> response = ((GraphCallResult.Success<Storefront.QueryRoot>) output).getResponse();
                            runOnUiThread(new Runnable() {
                                @Override

                                public void run() {
                                    process(response, origin, weburl);
                                }
                            });
                        } else {
                            Log.i("ResponseError", "" + output.toString());
                        }
                    }
                }, MainActivity.this);
            } else {
                if (origin.equals("normal")) {
                    Intent intent = new Intent(MainActivity.this, HomePage.class);
                    intent.setFlags(Intent.FLAG_ACTIVITY_CLEAR_TASK);
                    intent.setFlags(Intent.FLAG_ACTIVITY_NEW_TASK);
                    startActivity(intent);
                } else {
                    Intent intent = new Intent(MainActivity.this, CheckoutWeblink.class);
                    intent.putExtra("link", weburl);
                    startActivity(intent);
                    overridePendingTransition(R.anim.magenative_slide_in, R.anim.magenative_slide_out);
                }
            }
        } catch (Exception e) {
            e.printStackTrace();
        }
    }

    private void process(GraphResponse<Storefront.QueryRoot> response, String origin, String weburl) {
        try {
            localData.saveFirstName(response.getData().getCustomer().getFirstName());
            localData.saveLastName(response.getData().getCustomer().getLastName());
            if (origin.equals("normal")) {
                Intent intent = new Intent(MainActivity.this, HomePage.class);
                intent.setFlags(Intent.FLAG_ACTIVITY_CLEAR_TASK);
                intent.setFlags(Intent.FLAG_ACTIVITY_NEW_TASK);
                startActivity(intent);
            } else {
                Intent intent = new Intent(MainActivity.this, CheckoutWeblink.class);
                intent.putExtra("link", weburl);
                startActivity(intent);
                overridePendingTransition(R.anim.magenative_slide_in, R.anim.magenative_slide_out);
            }
        } catch (Exception e) {
            e.printStackTrace();
        }
    }

    public ArrayList<String> getCountryList() {
        ArrayList<String> list = new ArrayList<String>();
        try {
            JSONArray array = new JSONObject(AssetJSONFile("country.json", MainActivity.this)).names();
            try {
                for (int i = 0, l = array.length(); i < l; i++) {
                    list.add(array.getString(i));
                }
            } catch (JSONException e) {
                e.printStackTrace();
            }

        } catch (Exception e) {
            e.printStackTrace();
        }
        return list;
    }

    public ArrayList<String> getStateList(String country) {
        ArrayList<String> list = new ArrayList<String>();
        try {
            JSONObject object = new JSONObject(AssetJSONFile("country.json", MainActivity.this));
            try {
                JSONArray array = object.getJSONArray(country);
                if (array.length() > 0) {
                    for (int i = 0, l = array.length(); i < l; i++) {
                        list.add(array.getString(i));
                    }
                    return list;
                }
            } catch (JSONException e) {
                e.printStackTrace();
            }

        } catch (Exception e) {
            e.printStackTrace();
        }
        return null;
    }

    public String AssetJSONFile(String filename, Context context) {
        byte[] formArray = new byte[0];
        try {
            AssetManager manager = context.getAssets();
            InputStream file = manager.open(filename);
            formArray = new byte[file.available()];
            file.read(formArray);
            file.close();
            return new String(formArray);
        } catch (Exception e) {
            e.printStackTrace();
        }
        return new String(formArray);
    }

    public void showlangpop(@NonNull JSONArray languages) {
        try {
            ((ViewGroup) ((ViewGroup) Objects.requireNonNull(Objects.requireNonNull(langlistDialog).getWindow()).getDecorView()).getChildAt(0))
                    .getChildAt(1)
                    .setBackgroundColor(getResources().getColor(R.color.black));
            Objects.requireNonNull(langlistDialog).setTitle(Html.fromHtml("<center><font color='#ffffff'>" + getResources().getString(R.string.lang) + "</font></center>"));
            LayoutInflater li = (LayoutInflater) this.getSystemService(Context.LAYOUT_INFLATER_SERVICE);
            @SuppressLint("InflateParams") View v = Objects.requireNonNull(li).inflate(R.layout.langlist, null, false);
            RadioGroup storelist = v.findViewById(R.id.langlist);
            JSONObject object;
            RadioButton button;
            for (int i = 0; i < languages.length(); i++) {
                object = languages.getJSONObject(i);
                button = new RadioButton(MainActivity.this);
                button.setText(object.getString("name"));
                button.setTag(object.getString("code"));
                if (localData.getLangCode().equals(object.getString("code"))) {
                    button.setChecked(true);
                }
                final RadioButton finalButton = button;
                button.setOnCheckedChangeListener(new CompoundButton.OnCheckedChangeListener() {
                    @Override
                    public void onCheckedChanged(CompoundButton compoundButton, boolean b) {
                        if (b) {
                            localData.saveLangCode(String.valueOf(finalButton.getTag()));
                            LoadLanguage.setLocale(localData.getLangCode(), MainActivity.this);
                            Intent intent = new Intent(MainActivity.this, Splash.class);
                            intent.addFlags(Intent.FLAG_ACTIVITY_CLEAR_TASK);
                            intent.addFlags(Intent.FLAG_ACTIVITY_NEW_TASK);
                            startActivity(intent);
                            langlistDialog.dismiss();
                        }
                    }
                });
                storelist.addView(button);
            }
            langlistDialog.setContentView(v);
            langlistDialog.setCancelable(true);
            langlistDialog.show();
        } catch (Exception e) {
            e.printStackTrace();
        }
    }
    @Override
    protected void onDestroy() {
        Smartlook.stopRecording();
        super.onDestroy();
    }

}
