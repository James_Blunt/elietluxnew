/*
 * Copyright/**
 *          * CedCommerce
 *           *
 *           * NOTICE OF LICENSE
 *           *
 *           * This source file is subject to the End User License Agreement (EULA)
 *           * that is bundled with this package in the file LICENSE.txt.
 *           * It is also available through the world-wide-web at this URL:
 *           * http://cedcommerce.com/license-agreement.txt
 *           *
 *           * @category  Ced
 *           * @package   MageNative
 *           * @author    CedCommerce Core Team <connect@cedcommerce.com >
 *           * @copyright Copyright CEDCOMMERCE (http://cedcommerce.com/)
 *           * @license      http://cedcommerce.com/license-agreement.txt
 *
 */
package com.elietlux.shopifyapp.wishlistsection;
import android.content.Intent;
import android.os.Bundle;
import android.support.annotation.NonNull;
import android.support.annotation.Nullable;
import android.support.design.widget.Snackbar;
import android.util.Log;
import android.view.Menu;
import android.view.MenuItem;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.TextView;
import android.widget.Toast;

import com.blogspot.atifsoftwares.animatoolib.Animatoo;
import com.bumptech.glide.Glide;
import com.bumptech.glide.load.engine.DiskCacheStrategy;
import com.bumptech.glide.request.RequestOptions;
import com.elietlux.shopifyapp.R;
import com.elietlux.shopifyapp.checkoutsection.CartListing;
import com.elietlux.shopifyapp.checkoutsection.CheckoutLineItems;
import com.elietlux.shopifyapp.maincontainer.MainActivity;
import com.elietlux.shopifyapp.productviewsection.ProductView;
import com.elietlux.shopifyapp.searchsection.SearchView;
import com.elietlux.shopifyapp.storagesection.LocalData;
import org.json.JSONArray;
import org.json.JSONObject;
import butterknife.BindView;
import butterknife.ButterKnife;

public class WishListing extends MainActivity
{
    LocalData data=null;
    CheckoutLineItems items=null;
    String count="0";
    @Nullable @BindView(R.id.wishlistsection) LinearLayout wishlistsection;
    @Override
    protected void onCreate(Bundle savedInstanceState)
    {
        super.onCreate(savedInstanceState);
        ViewGroup content = findViewById(R.id.MageNative_frame_container);
        getLayoutInflater().inflate(R.layout.wishlisting, content, true);
        ButterKnife.bind(WishListing.this);
        data=new LocalData(WishListing.this);
        items=new CheckoutLineItems(WishListing.this);
        //showbackbutton();
        showTittle(getResources().getString(R.string.saved_items));
        selectWish();
    }
    @Override
    protected void onResume()
    {
        if(data.getLineItems()!=null)
        {
            count= String.valueOf(items.getItemcounts());
        }
        else
        {
            count="0";
        }
        if(data.getWishList()!=null)
        {
            if(wishlistsection.getChildCount()>0)
            {
                wishlistsection.removeAllViews();
            }
            createWishList();
        }
        else
        {
            Toast.makeText(WishListing.this,getResources().getString(R.string.nowish),Toast.LENGTH_LONG).show();
            finish();
        }
        invalidateOptionsMenu();
        super.onResume();
    }
    private void createWishList()
    {
        try
        {
            JSONObject object=new JSONObject(data.getWishList());
            JSONArray array=object.names();
            JSONObject jsonObject=null;
            View view=null;
            ImageView image=null;
            TextView product_id=null;
            TextView name=null;
            TextView addtocart=null;
            TextView variant_id=null;
            TextView variant_count=null;
            ImageView deleteicon=null;
            for(int i=0;i<array.length();i++)
            {
                jsonObject=object.getJSONObject(array.getString(i));
                Log.d("wishlistdata",""+jsonObject);
                view=View.inflate(WishListing.this,R.layout.magenative_wish_comp,null);
                image=view.findViewById(R.id.image);
                product_id=view.findViewById(R.id.product_id);
                name=view.findViewById(R.id.name);
                deleteicon=view.findViewById(R.id.deleteicon);
                addtocart=view.findViewById(R.id.addtocart);
                variant_count=view.findViewById(R.id.variant_count);
                variant_id=view.findViewById(R.id.variant_id);
                Glide.with(WishListing.this)
                        .load(jsonObject.getString("image"))
                        .thumbnail(0.5f)
                        .apply(new RequestOptions()
                                .placeholder(R.drawable.placeholder)
                                )
                        .into(image);
                variant_id.setText(jsonObject.getString("varinatid"));
                variant_count.setText(jsonObject.getString("varinats"));
                product_id.setText(jsonObject.getString("product_id"));
                name.setText(jsonObject.getString("product_name"));
                TextView finalProduct_id = product_id;
                ImageView finalDeleteicon = deleteicon;
                deleteicon.setOnClickListener(new View.OnClickListener() {
                    @Override
                    public void onClick(View view) {
                        try{
                            JSONObject object1=new JSONObject(data.getWishList());
                            Log.d("wishdelete id",""+finalProduct_id.getText().toString());
                            Log.d("wishdelete bfr",""+object1);
                            if (finalProduct_id.getText().toString().endsWith("=")){
                                object1.remove(getBase64Decode(finalProduct_id.getText().toString()));
                                object1.remove(finalProduct_id.getText().toString());
                            }else {
                                object1.remove(finalProduct_id.getText().toString());
                            }
                            Log.d("wishdelete afr",""+object1);
                            data.saveWishList(object1);
                            View view1= (View) finalDeleteicon.getParent();
                            wishlistsection.removeView(view1);
                            if(wishlistsection.getChildCount()==0)
                            {
                                onResume();
                            }
                        }
                        catch (Exception e)
                        {
                            e.printStackTrace();
                        }
                    }
                });

                addtocart.setText(getResources().getString(R.string.viewproduct));
              /*  if(jsonObject.getString("varinats").equals("1"))
                {
                    addtocart.setText(getResources().getString(R.string.add_to_cart));
                }
                else
                {
                    addtocart.setText(getResources().getString(R.string.selectvariant));
                }*/
                TextView finalAddtocart = addtocart;
                TextView finalVariant_id = variant_id;
                TextView finalName = name;
                TextView finalAddtocart1 = addtocart;
                addtocart.setOnClickListener(new View.OnClickListener()
                {
                    @Override
                    public void onClick(View view)
                    {
                        try
                        {
                            if(finalAddtocart.getText().toString().equals(getResources().getString(R.string.add_to_cart)))
                            {
                                if(data.getWishList()!=null)
                                {
                                    JSONObject jsonObject=new JSONObject(data.getWishList());
                                    if(jsonObject.has(finalProduct_id.getText().toString()))
                                    {
                                        jsonObject.remove(finalProduct_id.getText().toString());
                                        data.saveWishList(jsonObject);
                                    }
                                }
                                if(data.getCheckoutId()!=null)
                                {
                                    data.clearCheckoutId();
                                    data.clearCoupon();
                                }
                                if(data.getLineItems()!=null)
                                {
                                    int qty=1;
                                    JSONObject object=new JSONObject(data.getLineItems());
                                    if(object.has(finalVariant_id.getText().toString()))
                                    {
                                        qty= Integer.parseInt(object.getString(finalVariant_id.getText().toString()));
                                        qty=qty+1;
                                    }
                                    object.put(finalVariant_id.getText().toString(),qty);
                                    data.saveLineItems(object);
                                }
                                else
                                {
                                    JSONObject object=new JSONObject();
                                    object.put(finalVariant_id.getText().toString(),"1");
                                    data.saveLineItems(object);
                                }
                                String message= finalName.getText().toString()+" "+ getResources().getString(R.string.addedtocart);
                                Snackbar snackbar = Snackbar.make(findViewById(android.R.id.content), message, Snackbar.LENGTH_LONG);
                                snackbar.show();
                                changecount();
                                View view1= (View) finalAddtocart1.getParent();
                                wishlistsection.removeView(view1);
                                if(wishlistsection.getChildCount()==0)
                                {
                                    onResume();
                                }
                            }
                            else
                            {
                                Intent intent=new Intent(WishListing.this, ProductView.class);
                                intent.putExtra("id",finalProduct_id.getText().toString());
                                startActivity(intent);
                                Animatoo.animateZoom(WishListing.this);
                                //overridePendingTransition(R.anim.magenative_slide_in, R.anim.magenative_slide_out);
                            }
                        }
                        catch (Exception e)
                        {
                            e.printStackTrace();
                        }
                    }
                });
                view.setOnClickListener(new View.OnClickListener() {
                    @Override
                    public void onClick(View view) {
                        Intent intent=new Intent(WishListing.this, ProductView.class);
                        intent.putExtra("id",finalProduct_id.getText().toString());
                        startActivity(intent);
                        Animatoo.animateZoom(WishListing.this);
                       // overridePendingTransition(R.anim.magenative_slide_in, R.anim.magenative_slide_out);
                    }
                });
                wishlistsection.addView(view);
            }
        }
        catch (Exception e)
        {
            e.printStackTrace();
        }
    }
    @Override
    public boolean onCreateOptionsMenu(@NonNull Menu menu)
    {
        getMenuInflater().inflate(R.menu.menu_wish, menu);
        MenuItem item = menu.findItem(R.id.MageNative_action_cart);
        item.setActionView(R.layout.magenative_feed_update_count);
        View notifCount = item.getActionView();
        TextView textView = notifCount.findViewById(R.id.MageNative_hotlist_hot);
        textView.setText(count);
        notifCount.setOnClickListener(new View.OnClickListener()
        {
            @Override
            public void onClick(View v)
            {
                Intent intent = new Intent(getApplicationContext(), CartListing.class);
                intent.setFlags(Intent.FLAG_ACTIVITY_SINGLE_TOP);
                startActivity(intent);
                overridePendingTransition(R.anim.magenative_slide_in, R.anim.magenative_slide_out);
            }
        });
        return true;
    }
    @Override
    public void onBackPressed() {
        invalidateOptionsMenu();
        super.onBackPressed();
    }
    public void changecount()
    {
        if(data.getLineItems()!=null)
        {
            count= String.valueOf(items.getItemcounts());
        }
        else
        {
            count="0";
        }
        invalidateOptionsMenu();
    }
}
