package com.elietlux.shopifyapp.loginandregistrationsection;

import android.annotation.SuppressLint;
import android.app.Dialog;
import android.content.Context;
import android.content.Intent;
import android.graphics.Bitmap;
import android.graphics.Point;
import android.graphics.drawable.BitmapDrawable;
import android.graphics.drawable.Drawable;
import android.os.Bundle;
import android.support.annotation.NonNull;
import android.support.annotation.Nullable;
import android.util.Log;
import android.view.Display;
import android.view.LayoutInflater;
import android.view.Menu;
import android.view.View;
import android.view.ViewGroup;
import android.widget.Button;
import android.widget.EditText;
import android.widget.ImageView;
import android.widget.RelativeLayout;
import android.widget.ScrollView;
import android.widget.TextView;
import android.widget.Toast;

import com.bumptech.glide.Glide;
import com.bumptech.glide.load.engine.DiskCacheStrategy;
import com.bumptech.glide.request.target.SimpleTarget;
import com.bumptech.glide.request.transition.Transition;
import com.elietlux.shopifyapp.loadersection.Loader;
import com.elietlux.shopifyapp.requestsection.ApiInterface;
import com.facebook.AccessToken;
import com.facebook.CallbackManager;
import com.facebook.FacebookCallback;
import com.facebook.FacebookException;
import com.facebook.GraphRequest;
import com.facebook.login.LoginManager;
import com.facebook.login.LoginResult;
import com.facebook.login.widget.LoginButton;
import com.google.android.gms.auth.api.Auth;
import com.google.android.gms.auth.api.signin.GoogleSignInAccount;
import com.google.android.gms.auth.api.signin.GoogleSignInOptions;
import com.google.android.gms.auth.api.signin.GoogleSignInResult;
import com.google.android.gms.common.ConnectionResult;
import com.google.android.gms.common.SignInButton;
import com.google.android.gms.common.api.GoogleApiClient;
import com.google.android.gms.common.api.ResultCallback;
import com.google.android.gms.common.api.Status;
import com.shopify.buy3.GraphCallResult;
import com.shopify.buy3.GraphClient;
import com.shopify.buy3.GraphResponse;
import com.shopify.buy3.MutationGraphCall;
import com.shopify.buy3.Storefront;
import com.elietlux.shopifyapp.R;
import com.elietlux.shopifyapp.maincontainer.MainActivity;
import com.elietlux.shopifyapp.requestsection.ApiClient;
import com.elietlux.shopifyapp.storagesection.LocalData;
import com.elietlux.shopifyapp.storefrontqueries.MutationQuery;
import com.elietlux.shopifyapp.storefrontresponse.AsyncResponse;
import com.elietlux.shopifyapp.storefrontresponse.Response;

import org.json.JSONException;
import org.json.JSONObject;

import java.util.HashMap;
import java.util.Iterator;
import java.util.List;
import java.util.Objects;

import butterknife.BindView;
import butterknife.ButterKnife;
import okhttp3.ResponseBody;
import retrofit2.Call;

public class Login extends MainActivity implements GoogleApiClient.OnConnectionFailedListener{
    static GraphClient apiclient;
    String origin = "normal";
    String weburl = "normal";
    ApiInterface apiService;
    CallbackManager callbackManager;
    private GoogleApiClient mGoogleApiClient;
    private static final int RC_SIGN_IN = 007;
    @Nullable
    @BindView(R.id.MageNative_signupwithustext)
    TextView MageNative_signupwithustext;
    @Nullable
    @BindView(R.id.MageNative_forgotPassword)
    TextView MageNative_forgotPassword;
    @Nullable
    @BindView(R.id.MageNative_usr_password)
    EditText MageNative_usr_password;
    @Nullable
    @BindView(R.id.MageNative_user_name)
    EditText MageNative_user_name;
    @Nullable
    @BindView(R.id.MageNative_Login)
    Button MageNative_Login;
    @Nullable
    @BindView(R.id.scrollable)
    ScrollView scrollable;
    LocalData localData = null;
    @Nullable
    @BindView(R.id.MageNative_social_login_facebook)
    RelativeLayout Facebook_signin;
    @Nullable
    @BindView(R.id.MageNative_social_login_google)
    RelativeLayout Google_signin;
    @Nullable
    @BindView(R.id.google_btn_sign_in)
    SignInButton google_button;
    @Nullable
    @BindView(R.id.fb_login_button)
    LoginButton facebook_button;
    Loader progress;

    private static boolean isValidEmail(String target) {
        boolean valid = false;
        String emailPattern = "[a-zA-Z0-9._-]+@[a-z]+\\.+[a-z]+";
        if (target.matches(emailPattern)) {
            valid = true;
        }
        return valid;
    }

    public static Login getInstance() {
        return new Login();
    }

    @SuppressLint("WrongViewCast")
    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        ViewGroup content = findViewById(R.id.MageNative_frame_container);
        getLayoutInflater().inflate(R.layout.magenative_login, content, true);
        //showbackbutton();
        showTittle(getResources().getString(R.string.login));
        ButterKnife.bind(Login.this);
        localData = new LocalData(Login.this);
        progress=new Loader(this);
        Display display = getWindowManager().getDefaultDisplay();
        Point size = new Point();
        display.getSize(size);
        int width = size.x;
        int height = size.y;
        selectAcount();
        runOnUiThread(new Runnable() {
            @Override
            public void run() {
                try {
                    Glide.with(Login.this)
                            .asBitmap()
                            .load(localData.getLoginback())
                            .into(new SimpleTarget<Bitmap>(width, height) {
                                @Override
                                public void onResourceReady(@NonNull Bitmap resource, @Nullable Transition<? super Bitmap> transition) {
                                    Drawable drawable = new BitmapDrawable(getResources(), resource);
                                    scrollable.setBackground(drawable);
                                }
                            });
                } catch (Exception e) {
                    e.printStackTrace();
                }
            }
        });

        if (getIntent().getStringExtra("link") != null) {
            origin = getIntent().getStringExtra("checkout");
            weburl = getIntent().getStringExtra("link");
        }
        apiclient = ApiClient.getGraphClient(Login.this, true);
        apiService = ApiClient.getClient(Login.this).create(ApiInterface.class);
        callbackManager = CallbackManager.Factory.create();
        facebook_button.setReadPermissions("public_profile email");
        facebook_button.registerCallback(callbackManager, new FacebookCallback<LoginResult>() {
            @Override
            public void onSuccess(LoginResult loginResult) {
                if (AccessToken.getCurrentAccessToken().getToken() != null) {
                    RequestData();
                }
            }

            @Override
            public void onCancel() {
            }

            @Override
            public void onError(FacebookException exception) {
            }
        });
        Facebook_signin.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                facebook_button.performClick();
            }
        });
        GoogleSignInOptions gso = new GoogleSignInOptions.Builder(GoogleSignInOptions.DEFAULT_SIGN_IN).requestEmail().build();
        mGoogleApiClient = new GoogleApiClient.Builder(this).enableAutoManage(this, this)
                .addApi(Auth.GOOGLE_SIGN_IN_API, gso).build();
        google_button.setSize(SignInButton.SIZE_STANDARD);
        google_button.setScopes(gso.getScopeArray());
        google_button.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                Intent signInIntent = Auth.GoogleSignInApi.getSignInIntent(mGoogleApiClient);
                startActivityForResult(signInIntent, RC_SIGN_IN);
            }
        });
        Google_signin.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                Intent signInIntent = Auth.GoogleSignInApi.getSignInIntent(mGoogleApiClient);
                startActivityForResult(signInIntent, RC_SIGN_IN);
            }
        });
        MageNative_Login.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                progress.show();
                if (MageNative_user_name.getText().toString().isEmpty()) {
                    MageNative_user_name.setError(getResources().getString(R.string.empty));
                    MageNative_user_name.requestFocus();
                } else {
                    if (!(isValidEmail(MageNative_user_name.getText().toString()))) {
                        MageNative_user_name.setError(getResources().getString(R.string.invalidemail));
                        MageNative_user_name.requestFocus();
                    } else {
                        if (MageNative_usr_password.getText().toString().isEmpty()) {
                            MageNative_usr_password.setError(getResources().getString(R.string.empty));
                            MageNative_usr_password.requestFocus();
                        } else {
                            LoginUser(MageNative_user_name.getText().toString(), MageNative_usr_password.getText().toString(), origin, weburl);
                            progress.hide();
                        }
                    }
                }
            }
        });
        MageNative_forgotPassword.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                forgotPasswordPopUp();
            }
        });
        MageNative_signupwithustext.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                Intent intent = new Intent(Login.this, Register.class);
                intent.putExtra("checkout", origin);
                intent.putExtra("link", weburl);
                startActivity(intent);
                overridePendingTransition(R.anim.magenative_slide_in, R.anim.magenative_slide_out);
            }
        });
    }
    private void RequestData() {
        GraphRequest request = GraphRequest.newMeRequest(AccessToken.getCurrentAccessToken(), new GraphRequest.GraphJSONObjectCallback() {
            @Override
            public void onCompleted(JSONObject object, com.facebook.GraphResponse response) {

                try {
                    sendsocialRequest(object.getString("first_name"),
                            object.getString("last_name"),
                            object.getString("email"),
                            "pass@kwd",
                            "");
                } catch (JSONException e) {
                    e.printStackTrace();
                }

               /* JSONObject json = response.getJSONObject();
                Log.i("qwertyy",""+json);
                try {
                    if (json != null) {
                        sendsocialRequest(json.getString("first_name"),
                                json.getString("last_name"),
                                json.getString("email"),
                                "pass@kwd",
                                "");
                    }
                    else
                    {
                        sendsocialRequest(object.getString("first_name"),
                                object.getString("last_name"),
                                object.getString("email"),
                                "pass@kwd",
                                "");
                    }
                } catch (JSONException e) {
                    e.printStackTrace();
                }*/
            }
        });
       /* Bundle parameters = new Bundle();
        parameters.putString("fields", "id,name,link,email,picture,first_name,last_name");
        request.setParameters(parameters);
        request.executeAsync();*/

        Bundle parameters = new Bundle();
        parameters.putString("fields", "id,first_name,email,last_name");
        request.setParameters(parameters);
        request.executeAsync();
    }
    @Override
    protected void onActivityResult(int requestCode, int resultCode, Intent data) {
        super.onActivityResult(requestCode, resultCode, data);
        callbackManager.onActivityResult(requestCode, resultCode, data);
        if (requestCode == RC_SIGN_IN) {
            GoogleSignInResult result = Auth.GoogleSignInApi.getSignInResultFromIntent(data);
            if (result.isSuccess()) {
                GoogleSignInAccount acct = result.getSignInAccount();
                sendsocialRequest(acct.getGivenName(), acct.getFamilyName(), acct.getEmail(), "pass@kwd", "");
            }
            else {
                Toast.makeText(this, "" + getResources().getString(R.string.loginfailed), Toast.LENGTH_SHORT).show();
            }
        }
    }
    private void sendsocialRequest(String firstname, String lastname, String email, String password, String phone)
    {
        Call<ResponseBody> call = apiService.getuserLogin(getResources().getString(R.string.mid), email);
        try {
            Response.getRetrofitResponse(call, new AsyncResponse() {
                @Override
                public void finalOutput(@NonNull Object output, @NonNull boolean noerror) throws JSONException {
                    if (noerror) {
                        JSONObject object = new JSONObject(output.toString());
                        if (object.getBoolean("success")) {
                            if (object.getBoolean("is_present")) {
                                if (object.getBoolean("is_changed")) {
                                    LoginUser(email, password, origin, weburl);
                                } else {
                                    Toast.makeText(Login.this, "Email is blocked !", Toast.LENGTH_LONG).show();
                                }
                            } else {
                                createAccount(firstname, lastname, email, password, phone);
                            }
                        }
                    } else {
                        Log.i("ErrorHomePage", "" + output.toString());
                    }
                }
            }, Login.this);
            sign_out();
        }
        catch (Exception e) {
            e.printStackTrace();
        }
    }
    private void sign_out() {
        LoginManager.getInstance().logOut();
        Auth.GoogleSignInApi.signOut(mGoogleApiClient).setResultCallback(new ResultCallback<Status>() {
            @Override
            public void onResult(Status status) {
            }
        });
    }
    private void createAccount(String firstname, String lastname, String email, String password,String phone)
    {
        try
        {
            MutationGraphCall call = apiclient.mutateGraph(MutationQuery.createCustomer(firstname,lastname,email,password,phone));
            Response.getMutationGraphQLResponse(call,new AsyncResponse()
            {
                @Override
                public void finalOutput(@NonNull Object output,@NonNull boolean error)
                {
                    if(error)
                    {
                        GraphResponse<Storefront.Mutation> response  = (GraphResponse<Storefront.Mutation>) output;
                        runOnUiThread(new Runnable()
                        {
                            @Override
                            public void run()
                            {
                                List<Storefront.UserError> errors = response.getData().getCustomerCreate().getUserErrors();
                                if(errors.size() > 0)
                                {
                                    boolean iserror=false;
                                    Iterator iterator = errors.iterator();
                                    String err = "";
                                    while (iterator.hasNext())
                                    {
                                        Storefront.UserError error = (Storefront.UserError) iterator.next();
                                        err += error.getMessage();
                                    }
                                    Toast.makeText(Login.this,err,Toast.LENGTH_LONG).show();
                                }
                                else
                                {
                                    processRegisterData(response);
                                }
                            }
                        });
                    }
                    else
                    {
                        Log.i("ResponseError",""+output.toString());
                    }
                }
            },Login.this);
        }
        catch (Exception e)
        {
            e.printStackTrace();
        }
    }
    private void processRegisterData(GraphResponse<Storefront.Mutation> response)
    {
        try
        {
            Storefront.Customer customer=response.getData().getCustomerCreate().getCustomer();
            localData.saveFirstName(customer.getFirstName());
            localData.saveLastName(customer.getLastName());
            LoginUser(customer.getEmail(),"pass@kwd",origin,weburl);
            finish();
        }
        catch (Exception e)
        {
            e.printStackTrace();
        }
    }
    private void forgotPasswordPopUp() {
        try {
            final Dialog listDialog = new Dialog(Login.this, R.style.PauseDialog);
            ((ViewGroup) ((ViewGroup) Objects.requireNonNull(listDialog.getWindow()).getDecorView()).getChildAt(0)).getChildAt(1).setBackgroundColor(Login.this.getResources().getColor(R.color.AppTheme));
            listDialog.setTitle(Login.this.getResources().getString(R.string.forgotpass));
            LayoutInflater li = (LayoutInflater) Login.this.getSystemService(Context.LAYOUT_INFLATER_SERVICE);
            @SuppressLint("InflateParams") View v = Objects.requireNonNull(li).inflate(R.layout.enter_email, null, false);
            final EditText email = v.findViewById(R.id.email);
            TextView send = v.findViewById(R.id.conti);
            send.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View view) {
                    if (email.getText().toString().isEmpty()) {
                        email.setError(Login.this.getResources().getString(R.string.empty));
                        email.requestFocus();
                    } else {
                        if (isValidEmail(email.getText().toString())) {
                            listDialog.cancel();
                            requestForgotPass(email.getText().toString());
                        } else {
                            email.setError(Login.this.getResources().getString(R.string.invalidemail));
                            email.requestFocus();
                        }
                    }
                }
            });
            listDialog.setContentView(v);
            listDialog.setCancelable(true);
            listDialog.show();
        } catch (Exception e) {
            e.printStackTrace();
        }
    }

    private void requestForgotPass(String s) {
        try {
            MutationGraphCall call = apiclient.mutateGraph(MutationQuery.recoverCustomer(s));
            Response.getMutationGraphQLResponse(call, new AsyncResponse() {
                @Override
                public void finalOutput(@NonNull Object output, @NonNull boolean error) {
                    if (error) {
                        GraphResponse<Storefront.Mutation> response = ((GraphCallResult.Success<Storefront.Mutation>) output).getResponse();
                        runOnUiThread(new Runnable() {
                            @Override
                            public void run() {
                                List<Storefront.UserError> errors = response.getData().getCustomerRecover().getUserErrors();
                                if (errors.size() > 0) {
                                    Iterator iterator = errors.iterator();
                                    String err = "";
                                    while (iterator.hasNext()) {
                                        Storefront.UserError error = (Storefront.UserError) iterator.next();
                                        err += error.getMessage();
                                    }
                                    Toast.makeText(Login.this, err, Toast.LENGTH_LONG).show();
                                } else {
                                    Toast.makeText(Login.this, getResources().getString(R.string.pleasecheckyourmail), Toast.LENGTH_SHORT).show();
                                }
                            }
                        });
                    } else {
                        Log.i("ResponseError", "" + output.toString());
                    }
                }
            }, Login.this);

        } catch (Exception e) {
            e.printStackTrace();
        }
    }

    @Override
    public boolean onCreateOptionsMenu(@NonNull Menu menu) {
        return false;
    }
    public void onConnectionFailed(@NonNull ConnectionResult connectionResult) {
        Log.i("onConnectionFailed", ":" + connectionResult);
    }


}
