package com.elietlux.shopifyapp.productviewsection;

import android.app.Dialog;
import android.content.Intent;
import android.content.pm.PackageInfo;
import android.content.pm.PackageManager;
import android.graphics.Paint;
import android.net.Uri;
import android.os.Bundle;
import android.os.Handler;
import android.support.annotation.NonNull;
import android.support.annotation.Nullable;
import android.support.design.widget.AppBarLayout;
import android.support.design.widget.CollapsingToolbarLayout;
import android.support.design.widget.Snackbar;
import android.support.v4.view.ViewPager;
import android.support.v4.widget.NestedScrollView;
import android.support.v7.widget.GridLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.support.v7.widget.Toolbar;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.Menu;
import android.view.MenuItem;
import android.view.View;
import android.view.ViewGroup;
import android.view.Window;
import android.webkit.WebView;
import android.widget.AdapterView;
import android.widget.ArrayAdapter;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.RelativeLayout;
import android.widget.Spinner;
import android.widget.TextView;
import android.widget.Toast;

import com.blogspot.atifsoftwares.animatoolib.Animatoo;
import com.elietlux.shopifyapp.R;
import com.elietlux.shopifyapp.adaptersection.RelatedProducts_Adapter;
import com.elietlux.shopifyapp.checkoutsection.CartListing;
import com.elietlux.shopifyapp.checkoutsection.CheckoutLineItems;
import com.elietlux.shopifyapp.currencysection.CurrencyFormatter;
import com.elietlux.shopifyapp.currencysection.NewCurrencyFormatter;
import com.elietlux.shopifyapp.langauagesection.LoadLanguage;
import com.elietlux.shopifyapp.maincontainer.MainActivity;
import com.elietlux.shopifyapp.maincontainer.MainContainerWeblink;
import com.elietlux.shopifyapp.productlistingsection.ProductListing;
import com.elietlux.shopifyapp.requestsection.ApiClient;
import com.elietlux.shopifyapp.requestsection.ApiInterface;
import com.elietlux.shopifyapp.searchsection.AutoSearch;
import com.elietlux.shopifyapp.storagesection.LocalData;
import com.elietlux.shopifyapp.storefrontqueries.Query;
import com.elietlux.shopifyapp.storefrontresponse.AsyncResponse;
import com.elietlux.shopifyapp.storefrontresponse.Response;
import com.shopify.buy3.GraphCallResult;
import com.shopify.buy3.GraphClient;
import com.shopify.buy3.GraphResponse;
import com.shopify.buy3.QueryGraphCall;
import com.shopify.buy3.Storefront;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import java.io.UnsupportedEncodingException;
import java.math.BigDecimal;
import java.net.URLEncoder;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.Iterator;
import java.util.List;

import butterknife.BindView;
import butterknife.ButterKnife;
import okhttp3.ResponseBody;
import retrofit2.Call;

public class ProductView extends MainActivity {
    private static final String TAG = "ProductView";
    String count = "0";
    int current = 0;
    int previous = 0;
    String productID = "noid";
    String catID = "";
    int position;
    String sharingurl = "";
    GraphClient client = null;
    LocalData localData = null;
    String productHandle="";
    @Nullable
    @BindView(R.id.main)
    RelativeLayout main;
    @Nullable
    @BindView(R.id.MageNative_productimages)
    ViewPager MageNative_productimages;
    @Nullable
    @BindView(R.id.MageNative_indicator)
    CirclePageIndicator MageNative_indicator;
    @Nullable
    @BindView(R.id.MageNative_specialprice)
    TextView MageNative_specialprice;
    @Nullable
    @BindView(R.id.MageNative_normalprice)
    TextView MageNative_normalprice;
    @Nullable
    @BindView(R.id.MageNative_productname)
    TextView MageNative_productname;
    @Nullable
    @BindView(R.id.product_id)
    TextView product_id;
    @Nullable
    @BindView(R.id.optionsection)
    RelativeLayout horizontal;
    @Nullable
    @BindView(R.id.dynamic_fields_section)
    LinearLayout dynamic_fields_section;
    @Nullable
    @BindView(R.id.description)
    TextView description;
    @Nullable
    @BindView(R.id.products_details)
    WebView products_details;
    @Nullable
    @BindView(R.id.MageNative_sharetext)
    LinearLayout MageNative_sharetext;
    @Nullable
    @BindView(R.id.scrollmain)
    NestedScrollView scrollmain;

    @Nullable
    @BindView(R.id.collapsingToolbar)
    CollapsingToolbarLayout collapsingToolbar;
    @Nullable
    @BindView(R.id.similarsection)
    RelativeLayout similarsection;
    @Nullable
    @BindView(R.id.products)
    RecyclerView products;
    @Nullable
    @BindView(R.id.addtocart)
    TextView addtocart;
    @Nullable
    @BindView(R.id.MageNative_wishlistsection)
    RelativeLayout MageNative_wishlistsection;
    @Nullable
    @BindView(R.id.MageNative_wishlist)
    ImageView MageNative_wishlist;

    @Nullable
    @BindView(R.id.whatsapp1)
    ImageView whatsapp;

    @Nullable
    @BindView(R.id.fbmessenger1)
    ImageView fbmessenger;

    @Nullable
    @BindView(R.id.mail1)
    ImageView mail;

    @Nullable
    @BindView(R.id.sizechart)
    TextView sizechart;

    @Nullable
    @BindView(R.id.MageNative_vendor)
    TextView MageNative_vendor;

    @Nullable
    @BindView(R.id.outline7)
    TextView outline7;

    @Nullable
    @BindView(R.id.outline6)
    TextView outline6;

    @Nullable
    @BindView(R.id.outline5)
    TextView outline5;

    @Nullable
    @BindView(R.id.appBar)
    AppBarLayout appBar;

    //@Nullable @BindView(R.id.inventory) TextView inventory;
    String variant_id = "";
    CheckoutLineItems items = null;
    int totalvariantsize;
    boolean out_of_stock = false;
    ArrayList<String> urls;
    Storefront.ProductEdge edge = null;
    Storefront.Product product = null;
    HashMap<String, Variants> varID;
    int no_of_opt = 0;
    String variant = "";
    String v1 = "";
    String v2 = "";
    String v3 = "";
    String img = "";
    HashMap<String, String> var_image_title_src;
    HashMap<String, Integer> var_image;
    HashMap<String, String> variantArray;
    HashMap<String, String> variant_title_id_map;
    //ApiInterface apiService;
    ArrayList<String> inventoryStock = null;
    ApiInterface apiKiwiSizeChart;
    String tags = "";
    String vendor = "";
    String collections = "";
    Toolbar toolbar;
    ArrayList available_var_array;
    Dialog dialog;
    String presentmentcurrency=null;

    public static double round(double value, int places) {
        if (places < 0) throw new IllegalArgumentException();

        long factor = (long) Math.pow(10, places);
        value = value * factor;
        long tmp = Math.round(value);
        return (double) tmp / factor;
    }

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        ViewGroup content = findViewById(R.id.MageNative_frame_container);
        getLayoutInflater().inflate(R.layout.magenative_productview, content, true);
        urls = new ArrayList<String>();
        localData = new LocalData(ProductView.this);
        available_var_array = new ArrayList();
        items = new CheckoutLineItems(ProductView.this);
        varID = new HashMap<String, Variants>();
        var_image = new HashMap<String, Integer>();
        var_image_title_src = new HashMap<String, String>();
        variant_title_id_map = new HashMap<String, String>();
        variantArray = new HashMap<String, String>();
        inventoryStock = new ArrayList<String>();
        dialog = new Dialog(ProductView.this);
        mToolbar.setVisibility(View.GONE);

        toolbar = findViewById(R.id.toolbar);
        setSupportActionBar(toolbar);

        presentmentcurrency=localData.getCurrencyCode();

        toolbar.setNavigationIcon(android.support.v7.appcompat.R.drawable.abc_ic_ab_back_material);
        toolbar.setNavigationOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {

                onBackPressed();
            }
        });





        //apiService = ApiClient.getClient(ProductView.this).create(ApiInterface.class);
        if (getIntent().getSerializableExtra("productobject") != null) {
            product = (Storefront.Product) getIntent().getSerializableExtra("productobject");
        }
        if (getIntent().getSerializableExtra("object") != null) {
            edge = (Storefront.ProductEdge) getIntent().getSerializableExtra("object");
        }
        if (getIntent().getStringExtra("id") != null) {
            productID = getIntent().getStringExtra("id");

            //getResponse(call);
        }
        if (getIntent().getStringExtra("catID") != null) {
            catID = getIntent().getStringExtra("catID");
            position = getIntent().getIntExtra("position", 0);
        }
        client = ApiClient.getGraphClient(ProductView.this, true);
        apiKiwiSizeChart = ApiClient.getSizeClient(ProductView.this).create(ApiInterface.class);
        //showbackbutton();
        showTittle(" ");
        ButterKnife.bind(ProductView.this);


        whatsapp.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                PackageManager pm=getPackageManager();
                try {
                    PackageInfo info=pm.getPackageInfo("com.whatsapp", PackageManager.GET_META_DATA);

                    Intent intent = new Intent(Intent.ACTION_VIEW);
                    intent.setData(Uri.parse("http://api.whatsapp.com/send?phone="+"+13073924167"));
                    startActivity(intent);

                }catch (PackageManager.NameNotFoundException e) {
                    Toast.makeText(ProductView.this, "WhatsApp not Installed", Toast.LENGTH_SHORT)
                            .show();
                }



            }
        });
        fbmessenger.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {

                PackageManager pm=getPackageManager();
                try {
                    PackageInfo info=pm.getPackageInfo("com.facebook.orca", PackageManager.GET_META_DATA);

                    Intent intent = new Intent();
                    intent.setAction(Intent.ACTION_VIEW);
                    intent.setPackage("com.facebook.orca");
                    intent.setData(Uri.parse("https://m.me/EliteLUX.store"));
                    startActivity(intent);

                }catch (PackageManager.NameNotFoundException e) {
                    Toast.makeText(ProductView.this, "Facebook Messenger not Installed", Toast.LENGTH_SHORT)
                            .show();
                }


            }
        });
        mail.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {

                Intent i = new Intent(Intent.ACTION_SEND);
                i.setType("message/rfc822");
                i.putExtra(Intent.EXTRA_EMAIL, new String[]{"customer.service@elitelux.store"});
                try {
                    startActivity(i);
                } catch (android.content.ActivityNotFoundException ex) {
                }
            }
        });

        if (localData.getWishList() != null) {
            try {
                JSONObject object = new JSONObject(localData.getWishList());
                if (object.has(productID)) {
                    Log.i("INproduct", "IN");
                    MageNative_wishlist.setImageResource(R.drawable.wishred);
                } else {
                    MageNative_wishlist.setImageResource(R.drawable.wishlike);
                }
            } catch (Exception e) {
                e.printStackTrace();
            }
        }
        MageNative_sharetext.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                Intent shareIntent = new Intent(Intent.ACTION_SEND);
                shareIntent.setType("text/plain");
                String shareString = getResources().getString(R.string.hey) + "  " +
                        MageNative_productname.getText().toString() +
                        "  " + getResources().getString(R.string.on) + "  " + getResources().getString(R.string.app_name) + "\n" + sharingurl + "pid=" + productID;
                shareIntent.putExtra(Intent.EXTRA_SUBJECT, getResources().getString(R.string.app_name));
                shareIntent.putExtra(Intent.EXTRA_TEXT, shareString);
                startActivity(Intent.createChooser(shareIntent, getResources().getString(R.string.shareproduct)));
            }
        });
        MageNative_wishlistsection.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                processwishlist(MageNative_wishlist, productID);
            }
        });

        /**********************************************Kiwi Size Chart***********************************************************************/
       /* sizechart.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {

                dialog.show();

            }
        });*/

        /**********************************************Kiwi Size Chart***********************************************************************/

        addtocart.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                try {
                    Variants variants = varID.get(variant);
                    if (variants.vid.isEmpty()) {
                        Toast.makeText(ProductView.this, getResources().getString(R.string.selectvariant), Toast.LENGTH_LONG).show();
                    } else {
                        if (localData.getWishList() != null) {
                            JSONObject jsonObject = new JSONObject(localData.getWishList());
                            if (jsonObject.has(productID)) {
                                jsonObject.remove(productID);
                                localData.saveWishList(jsonObject);
                            }
                        }
                        if (localData.getCheckoutId() != null) {
                            localData.clearCheckoutId();
                            localData.clearCoupon();
                        }
                        if (localData.getLineItems() != null) {
                            int qty = 1;
                            JSONObject object = new JSONObject(localData.getLineItems());
                            Log.i("localdata", localData.getLineItems());
                            if (object.has(variants.vid)) {
                                qty = Integer.parseInt(object.getString(variants.vid));
                                qty = qty + 1;
                            }
                            object.put(variants.vid, qty);
                            localData.saveLineItems(object);
                        } else {
                            JSONObject object = new JSONObject();
                            object.put(variants.vid, "1");
                            localData.saveLineItems(object);
                        }
                        String message = MageNative_productname.getText().toString() + " " + getResources().getString(R.string.addedtocart);
                        Snackbar snackbar = Snackbar.make(findViewById(android.R.id.content), message, Snackbar.LENGTH_LONG);
                        snackbar.show();
                        changecount();
                    }
                } catch (Exception e) {
                    e.printStackTrace();
                }

            }
        });
        if (edge != null) {
            GraphResponse<Storefront.QueryRoot> response = null;
            processProductData(response, true);
        } else {
            if (product != null) {
                GraphResponse<Storefront.QueryRoot> response = null;
                processProductData(response, true);
            } else {
                getProductData(productID);
            }
        }

    }

    private void getProductData(String product_id) {
        try {
            QueryGraphCall call = client.queryGraph(Query.getSingleProduct(product_id));
            Response.getGraphQLResponse(call, new AsyncResponse() {
                @Override
                public void finalOutput(@NonNull Object output, @NonNull boolean error) {
                    if (error) {
                        GraphResponse<Storefront.QueryRoot> response = ((GraphCallResult.Success<Storefront.QueryRoot>) output).getResponse();
                        runOnUiThread(new Runnable() {
                            @Override
                            public void run() {
                                processProductData(response, false);
                            }
                        });
                    } else {
                        Log.i("ResponseError", "" + output.toString());
                    }
                }
            }, ProductView.this);
        } catch (Exception e) {
            e.printStackTrace();
        }
    }

    public void getProducts(String cat_id) {
        try {
            ArrayList<Storefront.CurrencyCode> currencyCodeArrayList = new ArrayList<>();
            Log.d("currencyCodeArrayList",""+localData.getCurrencyCode());
            currencyCodeArrayList.add(Storefront.CurrencyCode.valueOf(localData.getCurrencyCode()));
            Log.d("currencyCodeArrayList",""+currencyCodeArrayList);

            QueryGraphCall call = client.queryGraph(Query.getRelatedproducts(cat_id,currencyCodeArrayList));
            Response.getGraphQLResponse(call, new AsyncResponse() {
                @Override
                public void finalOutput(@NonNull Object output, @NonNull boolean error) {
                    if (error) {
                        GraphResponse<Storefront.QueryRoot> response = ((GraphCallResult.Success<Storefront.QueryRoot>) output).getResponse();
                        runOnUiThread(new Runnable() {
                            @Override
                            public void run() {
                                processProducts(response);
                            }
                        });
                    } else {
                        Log.i("ResponseError", "" + output.toString());
                    }
                }
            }, ProductView.this);
        } catch (Exception e) {
            e.printStackTrace();
        }
    }

    private void processProducts(GraphResponse<Storefront.QueryRoot> response) {
        try {
            Storefront.Collection collectionEdge = (Storefront.Collection) response.getData().getNode();
            List<Storefront.ProductEdge> edgeData = collectionEdge.getProducts().getEdges();
            if (edgeData.size() > 0) {
                Log.i("productedgelength", "data " + edgeData.size());
                if (ProductListing.productedge == null) {
                    ProductListing.productedge = edgeData;
                } else {
                    ProductListing.productedge.addAll(edgeData);
                }
                if (!(catID.isEmpty())) {
                    if (ProductListing.productedge != null) {
                        similarsection.setVisibility(View.VISIBLE);
                        products.setHasFixedSize(true);
                        products.setLayoutManager(new GridLayoutManager(this, 2));
                        products.setNestedScrollingEnabled(false);
                        List<Storefront.ProductEdge> data = new ArrayList<Storefront.ProductEdge>(ProductListing.productedge);
                        data.remove(position);
                        RelatedProducts_Adapter related = new RelatedProducts_Adapter(ProductView.this, data, catID, position,false);
                        products.setAdapter(related);
                        related.notifyDataSetChanged();
                    }
                }
            }
        } catch (Exception e) {
            e.printStackTrace();
        }
    }

    private void processProductData(GraphResponse<Storefront.QueryRoot> response, boolean flag) {
        try {
            Storefront.Product productEdge = null;
            if (flag) {
                if (product != null) {
                    productEdge = product;
                } else {
                    productEdge = edge.getNode();
                }
            } else {
                if (productID.contains("*#*")) {
                    Log.d("product","Product 1");
                    productEdge = response.getData().getShop().getProductByHandle();
                } else {
                    Log.d("product","Product 2");
                    productEdge = (Storefront.Product) response.getData().getNode();
                }
            }
            List<String> tagsList = productEdge.getTags();

            Log.d("tagss",""+tagsList);
            if(tagsList.contains("Watches")){
                sizechart.setText(getResources().getText(R.string.warranty));
            }
            if(tagsList.contains("NO_SIZE_CHART")){
                sizechart.setVisibility(View.GONE);
            }

            for (int i = 0; i < tagsList.size(); i++) {
                tags = tags + tagsList.get(i) + ",";
            }

            //tags = productEdge.getTags().toString();
            vendor = productEdge.getVendor();
            productEdge.getCollections();
            tags = tags.replace("[", "").replace("]", "");
            Log.d("vaibhav", "" + tags);
            for (int i = 0; i < productEdge.getCollections().getEdges().size(); i++) {
                collections = collections + getBase64Decode(productEdge.getCollections().getEdges().get(i).getNode().getId().toString()) + ",";
            }
            Log.d("vaibhav", "" + collections);
            //   Log.d("vaibhav",""+productEdge.getCollections());


            //size();
            sizechart.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View v) {
                    Intent intent = new Intent(ProductView.this, MainContainerWeblink.class);
                    if(tagsList.contains("Watches")){
                        Log.d("tagss",""+tagsList);
                        sizechart.setText(getResources().getText(R.string.warranty));
                        intent.putExtra("name", "");
                    }else {
                        intent.putExtra("name", getResources().getString(R.string.sizechart));
                    }

                    intent.putExtra("link", "https://www.elitelux.store/products/"+productHandle+"?view=magenative-kiwi-sizechart");
                    intent.putExtra("size",true);
                    intent.addFlags(Intent.FLAG_ACTIVITY_SINGLE_TOP);
                    startActivity(intent);
                    Animatoo.animateFade(ProductView.this);
                }
            });

            if (localData.getRecentlyViewed() == null) {
                JSONObject object = new JSONObject();
                object.put("id", productID);
                object.put("name", productEdge.getTitle());
                object.put("image", productEdge.getImages().getEdges().get(0).getNode().getTransformedSrc());
                object.put("price", CurrencyFormatter.setsymbol(productEdge.getVariants().getEdges().get(0).getNode().getPrice(), ProductView.this, localData.getMoneyFormat()));
                if (productEdge.getVariants().getEdges().get(0).getNode().getCompareAtPrice() != null) {
                    object.put("price", CurrencyFormatter.setsymbol(productEdge.getVariants().getEdges().get(0).getNode().getCompareAtPrice(), ProductView.this, localData.getMoneyFormat()));
                    object.put("special_price", CurrencyFormatter.setsymbol(productEdge.getVariants().getEdges().get(0).getNode().getPrice(), ProductView.this, localData.getMoneyFormat()));
                } else {
                    object.put("special_price", "nospecial");
                }
                JSONObject array = new JSONObject();
                array.put(productID, object);
                localData.setRecentlyViewed(array, productID);
            } else {
                JSONObject product = localData.getRecentlyViewed();
                if (product.names().length() < 50) {
                    JSONObject object = new JSONObject();
                    object.put("id", productID);
                    object.put("name", productEdge.getTitle());
                    object.put("image", productEdge.getImages().getEdges().get(0).getNode().getTransformedSrc());
                    object.put("price", CurrencyFormatter.setsymbol(productEdge.getVariants().getEdges().get(0).getNode().getPrice(), ProductView.this, localData.getMoneyFormat()));
                    if (productEdge.getVariants().getEdges().get(0).getNode().getCompareAtPrice() != null) {
                        object.put("price", CurrencyFormatter.setsymbol(productEdge.getVariants().getEdges().get(0).getNode().getCompareAtPrice(), ProductView.this, localData.getMoneyFormat()));
                        object.put("special_price", CurrencyFormatter.setsymbol(productEdge.getVariants().getEdges().get(0).getNode().getPrice(), ProductView.this, localData.getMoneyFormat()));
                    } else {
                        object.put("special_price", "nospecial");
                    }
                    product.put(productID, object);
                    localData.setRecentlyViewed(product, productID);
                } else {
                    for (int i = 0; i < 10; i++) {
                        product.remove(product.names().getString(i));
                    }
                    JSONObject object = new JSONObject();
                    object.put("id", productID);
                    object.put("name", productEdge.getTitle());
                    object.put("image", productEdge.getImages().getEdges().get(0).getNode().getTransformedSrc());
                    object.put("price", CurrencyFormatter.setsymbol(productEdge.getVariants().getEdges().get(0).getNode().getPrice(), ProductView.this, localData.getMoneyFormat()));
                    if (productEdge.getVariants().getEdges().get(0).getNode().getCompareAtPrice() != null) {
                        object.put("price", CurrencyFormatter.setsymbol(productEdge.getVariants().getEdges().get(0).getNode().getCompareAtPrice(), ProductView.this, localData.getMoneyFormat()));
                        object.put("special_price", CurrencyFormatter.setsymbol(productEdge.getVariants().getEdges().get(0).getNode().getPrice(), ProductView.this, localData.getMoneyFormat()));
                    } else {
                        object.put("special_price", "nospecial");
                    }
                    product.put(productID, object);
                    localData.setRecentlyViewed(product, productID);
                }
            }
            Storefront.ImageConnection connection = productEdge.getImages();
            Iterator imageiterator = connection.getEdges().iterator();

            /*Storefront.ProductVariantConnection variants1=productEdge.getVariants();
            List<Storefront.ProductVariantEdge> productvariantedge1 = variants1.getEdges();
            Iterator iterator1=productvariantedge1.iterator();
            Storefront.ProductVariantEdge edge1=null;
            while (iterator1.hasNext())
            {
                edge1 = (Storefront.ProductVariantEdge) iterator1.next();
                urls.add(edge1.getNode().getImage().getOriginalSrc());
            }*/

            while (imageiterator.hasNext()) {
                urls.add(((Storefront.ImageEdge) imageiterator.next()).getNode().getOriginalSrc());
            }
            ProductViewImagSlider slider = new ProductViewImagSlider(getSupportFragmentManager(), urls);
            MageNative_productimages.setAdapter(slider);
            MageNative_indicator.setViewPager(MageNative_productimages);
            MageNative_indicator.setVisibility(View.VISIBLE);

            Storefront.ProductVariantConnection variants = productEdge.getVariants();
            List<Storefront.ProductVariantEdge> productvariantedge = variants.getEdges();
            totalvariantsize = productvariantedge.size();
            if (productvariantedge != null) {
                Log.i("productvariantedge", "" + productvariantedge.size());
                Iterator iterator = productvariantedge.iterator();
                boolean first = true;
                View variantoption = null;
                ImageView image = null;
                ImageView tick_image = null;
                TextView variantid = null;
                TextView variantimage = null;
                TextView selectedoption1 = null;
                TextView selectedoption2 = null;
                TextView selectedoption3 = null;
                Storefront.ProductVariantEdge edge = null;
                List<Storefront.SelectedOption> selctedoption = null;
                Storefront.SelectedOption option = null;
                int count_i = 0;
                List<Storefront.ProductOption> productOptions = productEdge.getOptions();

                while (iterator.hasNext()) {
                    edge = (Storefront.ProductVariantEdge) iterator.next();
                    Storefront.ProductVariantPricePairEdge pedge = getEdge(edge.getNode().getPresentmentPrices().getEdges());
                    if (first) {
                        first = false;
                        if (edge.getNode().getCompareAtPrice() != null) {
                            MageNative_normalprice.setPaintFlags(Paint.STRIKE_THRU_TEXT_FLAG);
                            MageNative_specialprice.setText(NewCurrencyFormatter.setsymbol(pedge.getNode().getPrice().getAmount(), pedge.getNode().getPrice().getCurrencyCode().toString()));
                            MageNative_normalprice.setText(NewCurrencyFormatter.setsymbol(pedge.getNode().getCompareAtPrice().getAmount(),pedge.getNode().getCompareAtPrice().getCurrencyCode().toString()));
                        } else {
                            MageNative_normalprice.setText(NewCurrencyFormatter.setsymbol(pedge.getNode().getPrice().getAmount(),pedge.getNode().getPrice().getCurrencyCode().toString()));
                            MageNative_specialprice.setVisibility(View.GONE);
                        }
                    }
                    if (edge.getNode().getAvailableForSale()) {
                        available_var_array.add(edge.getNode().getTitle());
                    }
                    //if (edge.getNode().getAvailableForSale()) {
                    selctedoption = edge.getNode().getSelectedOptions();
                    if (productvariantedge.size() > 1) {
                            /*if(urls.contains(edge.getNode().getImage().getOriginalSrc()))
                            {
                                MageNative_productimages.setCurrentItem(0);
                            }
                            else
                            {
                                urls.add(edge.getNode().getImage().getOriginalSrc());
                            }*/

                        variantArray.put(edge.getNode().getTitle().trim(), pedge.getNode().getPrice().getAmount().toString());
                        if (edge.getNode().getCompareAtPrice() != null) {
                            varID.put(edge.getNode().getTitle().trim(), new Variants(edge.getNode().getId().toString(),
                                    edge.getNode().getTitle().trim(),
                                    pedge.getNode().getPrice().getCurrencyCode().toString(),
                                    pedge.getNode().getPrice().getAmount().toString(),
                                    pedge.getNode().getCompareAtPrice().getAmount().toString(),
                                    edge.getNode().getImage().getOriginalSrc()));
                        } else {
                            varID.put(edge.getNode().getTitle().trim(), new Variants(edge.getNode().getId().toString(),
                                    edge.getNode().getTitle().trim(),
                                    pedge.getNode().getPrice().getCurrencyCode().toString(),
                                    pedge.getNode().getPrice().getAmount().toString(),
                                    "",
                                    edge.getNode().getImage().getOriginalSrc()));
                        }
                        var_image_title_src.put(edge.getNode().getTitle(), edge.getNode().getImage().getOriginalSrc());
                        variant_title_id_map.put(edge.getNode().getTitle(), getBase64Decode(edge.getNode().getId().toString()));
                            /*img = edge.getNode().getImage().getOriginalSrc();
                            var_image_title_src.put(edge.getNode().getTitle(),img);
                            var_image.put(img, count_i);
                            count_i++;*/
                    } else {

                        variantArray.put(edge.getNode().getTitle().trim(), pedge.getNode().getPrice().getAmount().toString());

                        if (edge.getNode().getCompareAtPrice() != null) {
                            varID.put(edge.getNode().getTitle().trim(), new Variants(edge.getNode().getId().toString(),
                                    edge.getNode().getTitle().trim(),
                                    pedge.getNode().getPrice().getCurrencyCode().toString(),
                                    pedge.getNode().getPrice().getAmount().toString(),
                                    pedge.getNode().getCompareAtPrice().getAmount().toString(),
                                    edge.getNode().getImage().getOriginalSrc()));
                        } else {
                            varID.put(edge.getNode().getTitle().trim(), new Variants(edge.getNode().getId().toString(),
                                    edge.getNode().getTitle().trim(),
                                    pedge.getNode().getPrice().getCurrencyCode().toString(),
                                    pedge.getNode().getPrice().getAmount().toString(),
                                    "",
                                    edge.getNode().getImage().getOriginalSrc()));
                        }

                        var_image_title_src.put(edge.getNode().getTitle(), edge.getNode().getImage().getOriginalSrc());
                        variant_title_id_map.put(edge.getNode().getTitle(), getBase64Decode(edge.getNode().getId().toString()));
                            /*img = edge.getNode().getImage().getOriginalSrc();
                            var_image_title_src.put(edge.getNode().getTitle(),img);
                            var_image.put(img, count_i);*/

                        count_i++;
                    }

                    //} else { if (totalvariantsize == 1) { out_of_stock = true;addtocart.setText(getResources().getString(R.string.outofstock));addtocart.setEnabled(false); } }
                }

                /*TextView opt;
                int count = 0;
                no_of_opt = productOptions.size();
                LinearLayout variantoptionsection;
                View variant_options;
                Spinner pallet;
                ArrayList variantavailable=new ArrayList();
                for (int i = 0; i < productOptions.size(); i++) {
                    count = i;
                    Log.i("REpo","Variants"+productOptions.get(i).getName());
                    variant_options = View.inflate(ProductView.this, R.layout.variantoption, null);
                    variantoptionsection=(LinearLayout)variant_options.findViewById(R.id.variantoptionsection);
                    pallet=(Spinner)variant_options.findViewById(R.id.pallet);

                    LinearLayout.LayoutParams childParam = new LinearLayout.LayoutParams(0,LinearLayout.LayoutParams.WRAP_CONTENT,1);
                    childParam.setMargins(5,0,5,5);
                    variant_options.setLayoutParams(childParam);

                    opt=(TextView)variant_options.findViewById(R.id.opt_text);
                    opt.setText(productOptions.get(i).getName());


                    variantavailable.add(productEdge.getVariants().getEdges().get(i).getNode().getTitle());


                    ArrayList<String> optvalues=new ArrayList<String>();
                    for (int j = 0; j < productOptions.get(i).getValues().size(); j++) {
                        optvalues.add(productOptions.get(i).getValues().get(j));
                    }
                    Log.i("productEdgeproductEdge",""+optvalues);
                    ArrayAdapter<String> couponAdpt=new ArrayAdapter<String>(getApplicationContext(),R.layout.spinnertext, optvalues);
                    pallet.setAdapter(couponAdpt);

                    int finalCount1 = count;
                    Spinner finalPallet = pallet;
                    pallet.setOnItemSelectedListener(new AdapterView.OnItemSelectedListener() {
                        @Override
                        public void onItemSelected(AdapterView<?> parent, View view, int position, long id) {
                            String vname= finalPallet.getSelectedItem().toString();
                            Log.i("v_name",vname);
                            checkVariantArray(vname, finalCount1,variantavailable,position);
                        }

                        @Override
                        public void onNothingSelected(AdapterView<?> parent) {

                        }
                    });

                    dynamic_fields_section.addView(variant_options);
                }*/

                Storefront.Product finalProductEdge = productEdge;

                TextView opt;
                int count = 0;
                no_of_opt = productOptions.size();
                LinearLayout variantoptionsection;
                View variant_options;
                Spinner pallet;
                ArrayList variantavailable = new ArrayList();
                for (int i = 0; i < productOptions.size(); i++) {
                    count = i;
                    Log.i("REpo", "Variants" + productOptions.get(i).getName());
                    variant_options = View.inflate(ProductView.this, R.layout.variantoption, null);
                    variantoptionsection = variant_options.findViewById(R.id.variantoptionsection);
                    pallet = variant_options.findViewById(R.id.pallet);

                    LinearLayout.LayoutParams childParam = new LinearLayout.LayoutParams(LinearLayout.LayoutParams.MATCH_PARENT, LinearLayout.LayoutParams.WRAP_CONTENT, 1);
                    childParam.setMargins(5, 0, 5, 5);
                    variant_options.setLayoutParams(childParam);

                    opt = variant_options.findViewById(R.id.opt_text);
                    opt.setText(productOptions.get(i).getName());
                    try {
                        Log.d("variantavailable", variantavailable + "");
                        Log.d("variantavailable", finalProductEdge.getVariants().getEdges().get(i).getNode().getTitle() + "");
                        variantavailable.add(finalProductEdge.getVariants().getEdges().get(i).getNode().getTitle());
                    } catch (Exception r) {
                        r.printStackTrace();
                    }

                    ArrayList<String> optvalues = new ArrayList<String>();
                    for (int j = 0; j < productOptions.get(i).getValues().size(); j++) {
                        optvalues.add(productOptions.get(i).getValues().get(j));
                    }
                    Log.i("productEdgeproductEdge", "" + optvalues);
                 //   pallet.setBackground(getResources().getDrawable(R.drawable.border2));
                    ArrayAdapter<String> couponAdpt = new ArrayAdapter<String>(getApplicationContext(), R.layout.spinnertext, optvalues);
                    pallet.setAdapter(couponAdpt);
                    Log.i("productEdgeproductEdge", "2 " + optvalues);
                    int finalCount1 = count;
                    Spinner finalPallet = pallet;
                    Storefront.ProductVariantEdge finalEdge = edge;

                    /************************************ Variant selection on page load in case of Default title Spinner is hidden (spineer item selected does't execute in this case)********************************************************/
                    String vname = finalPallet.getSelectedItem().toString();
                    checkVariantArray(vname, finalCount1, variantavailable);
                    /************************************ Variant selection on page load in case of Default title Spinner is hidden (spineer item selected does't execute in this case)********************************************************/

                    pallet.setOnItemSelectedListener(new AdapterView.OnItemSelectedListener() {
                        @Override
                        public void onItemSelected(AdapterView<?> parent, View view, int position, long id) {
                            try {
                                String vname = finalPallet.getSelectedItem().toString();
                                Log.i("v_name", vname);
                                Log.e("priceee", "" + finalEdge.getNode().getPrice());
                                Log.e("priceee", "" + finalEdge.getNode().getCompareAtPrice());
                                checkVariantArray(vname, finalCount1, variantavailable);

                                if (varID.containsKey(variant)) {
                                    Variants variants = varID.get(variant);
                                    if (urls.contains(variants.getVimage())) {
                                        MageNative_productimages.setCurrentItem(urls.indexOf(variants.getVimage()), true);
                                    } else {
                                        urls.add(variants.getVimage());
                                        ProductViewImagSlider slider = new ProductViewImagSlider(getSupportFragmentManager(), urls);
                                        MageNative_productimages.setAdapter(slider);
                                        MageNative_indicator.setViewPager(MageNative_productimages);
                                    }

                                    MageNative_productimages.requestFocus();
                                    scrollmain.scrollTo(0, 0);
                                    // Log.i("option", "variant-available : " + variant);
                                }
                                                /*if(finalEdge.getNode().getCompareAtPrice()!=null)
                                                {
                                                    if(finalEdge.getNode().getCompareAtPrice().compareTo(finalEdge.getNode().getPrice())==1)
                                                    {
                                                        MageNative_normalprice.setPaintFlags(Paint.STRIKE_THRU_TEXT_FLAG);
                                                        MageNative_specialprice.setText(CurrencyFormatter.setsymbol(finalEdge.getNode().getPrice(),ProductView.this,localData.getMoneyFormat()));
                                                        MageNative_normalprice.setText(CurrencyFormatter.setsymbol(finalEdge.getNode().getCompareAtPrice(),ProductView.this,localData.getMoneyFormat()));
                                                        Log.e("priceee",""+finalEdge.getNode().getPrice());
                                                        Log.e("priceee",""+finalEdge.getNode().getCompareAtPrice());
                                                    }
                                                    else
                                                    {
                                                        Log.e("priceee",""+finalEdge.getNode().getPrice());
                                                        MageNative_normalprice.setText(CurrencyFormatter.setsymbol(finalEdge.getNode().getPrice(),ProductView.this,localData.getMoneyFormat()));
                                                        //MageNative_specialprice.setVisibility(View.GONE);
                                                    }

                                                }
                                                else
                                                {
                                                    Log.e("priceee",""+finalEdge.getNode().getPrice());
                                                    MageNative_normalprice.setText(CurrencyFormatter.setsymbol(finalEdge.getNode().getPrice(),ProductView.this,localData.getMoneyFormat()));
                                                    MageNative_specialprice.setVisibility(View.GONE);
                                                }*/


                            } catch (JSONException e) {
                                e.printStackTrace();
                            }
                        }

                        @Override
                        public void onNothingSelected(AdapterView<?> parent) {

                        }
                    });
                    dynamic_fields_section.addView(variant_options);

                    if (optvalues.size() == 1) {
                        if (!optvalues.get(0).equalsIgnoreCase("Default Title")){
                            dynamic_fields_section.setVisibility(View.VISIBLE);
                        }else {
                            dynamic_fields_section.setVisibility(View.GONE);
                        }
                    }
                }


                if (dynamic_fields_section.getChildCount() > 0) {
                    horizontal.setVisibility(View.VISIBLE);
                }else {
                    outline5.setVisibility(View.GONE);
                }
            }


            // MageNative_productname.setText(productEdge.getTitle());
            Log.d("titleee", "" + productEdge.getTitle());
            MageNative_vendor.setText(productEdge.getVendor());
            productHandle = productEdge.getHandle();
            //collapsingToolbar.setTitle(productEdge.getTitle());
            collapsingToolbar.setVisibility(View.VISIBLE);
            setUpCollapsingToolBar(productEdge.getTitle());
            collapsingToolbar.setExpandedTitleTextAppearance(R.style.ExpandedAppBar);
            collapsingToolbar.setCollapsedTitleTextAppearance(R.style.CollapsedAppBar);

            product_id.setText(productID);
            Log.i("Description", "" + productEdge.getDescriptionHtml());
            if (productEdge.getDescriptionHtml() != null) {
                if (!(productEdge.getDescription().isEmpty())) {
                    description.setVisibility(View.VISIBLE);
                    products_details.setVisibility(View.VISIBLE);
                    products_details.getSettings().setLoadsImagesAutomatically(true);
                    products_details.getSettings().setJavaScriptEnabled(true);
                    products_details.getSettings().setDomStorageEnabled(true);
                    String description = "<style>img{width:100%;margin-top:10px;}</style>" + productEdge.getDescriptionHtml().replace("//cdn", "https://cdn");
                    products_details.loadData(description.trim(), "text/html", "utf-8");

                }
            }
            sharingurl = productEdge.getOnlineStoreUrl();

            if (!(catID.isEmpty())) {
                if (ProductListing.productedge != null) {
                    similarsection.setVisibility(View.VISIBLE);
                    products.setHasFixedSize(true);
                    products.setLayoutManager(new GridLayoutManager(this, 2));
                    products.setNestedScrollingEnabled(false);
                    List<Storefront.ProductEdge> data = new ArrayList<Storefront.ProductEdge>(ProductListing.productedge);
                    // data.remove(position);
                    RelatedProducts_Adapter related = new RelatedProducts_Adapter(ProductView.this, data, catID, position,true);
                    products.setAdapter(related);
                    related.notifyDataSetChanged();
                }
            } else {
                if (productEdge.getCollections() != null) {
                    Log.i("connectionquery", "" + productEdge.getCollections().getEdges().size());
                    catID = productEdge.getCollections().getEdges().get(0).getNode().getId().toString();
                    getProducts(catID);
                }
            }
            main.setVisibility(View.VISIBLE);
            new Handler().postDelayed(new Runnable() {
                @Override
                public void run() {
                    MageNative_productimages.requestFocus();
                    scrollmain.scrollTo(0, 0);
                }
            }, 100);

        } catch (Exception e) {
            e.printStackTrace();
        }
    }

    private void setUpCollapsingToolBar(String title) {
        appBar.addOnOffsetChangedListener(new AppBarLayout.OnOffsetChangedListener() {
            boolean isShow = true;
            int scrollRange = -1;

            @Override
            public void onOffsetChanged(AppBarLayout appBarLayout, int verticalOffset) {
                Log.d("scrollRange", scrollRange + " -- " + verticalOffset);
                if (scrollRange == -1) {
                    scrollRange = appBarLayout.getTotalScrollRange();
                }
                if (scrollRange + verticalOffset == 0) {
                    collapsingToolbar.setTitle(title);
                    isShow = true;
                } else if (verticalOffset == 0) {
                    collapsingToolbar.setTitle(title);
                    isShow = true;
                } else if (isShow) {
                    collapsingToolbar.setTitle(" ");//careful there should a space between double quote otherwise it wont work
                    isShow = false;
                }
            }
        });
    }

    @Override
    public void onResume() {
        LoadLanguage.setLocale(localData.getLangCode(), ProductView.this);
        if (localData.getLineItems() != null) {
            count = String.valueOf(items.getItemcounts());
        } else {
            count = "0";
        }
        invalidateOptionsMenu();
        super.onResume();
    }

    @Override
    public boolean onCreateOptionsMenu(@NonNull Menu menu) {
        getMenuInflater().inflate(R.menu.menu_product, menu);

        MenuItem cityitem =menu.findItem(R.id.MageNative_action_city);
        cityitem.setActionView(R.layout.magenative_feed_update_city);
        View cityrtext = cityitem.getActionView();
        if (localData.getCurrencyCode()!=null){
            textView1_city = cityrtext.findViewById(R.id.city);
            textView1_city.setText(localData.getCurrencyCode());
        }
        textView1_city.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                showAppSetting();
            }
        });

        MenuItem item = menu.findItem(R.id.MageNative_action_cart);
        item.setActionView(R.layout.magenative_feed_update_count);
        View notifCount = item.getActionView();
        TextView textView = notifCount.findViewById(R.id.MageNative_hotlist_hot);
        RelativeLayout red_circle = notifCount.findViewById(R.id.red_circle);

        if (!count.equals("0")) {
            red_circle.setVisibility(View.VISIBLE);
            textView.setText(count);
        } else {
            red_circle.setVisibility(View.GONE);
        }

        notifCount.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                Intent intent = new Intent(getApplicationContext(), CartListing.class);
                intent.setFlags(Intent.FLAG_ACTIVITY_SINGLE_TOP);
                startActivity(intent);
                overridePendingTransition(R.anim.magenative_slide_in, R.anim.magenative_slide_out);
            }
        });



        return true;
    }

    @Override
    public boolean onOptionsItemSelected(@NonNull MenuItem item) {
        switch (item.getItemId()) {
            case android.R.id.home:
                this.finish();
                return true;
            case R.id.MageNative_action_search:
                Intent searchintent = new Intent(ProductView.this, AutoSearch.class);
                startActivity(searchintent);
                overridePendingTransition(R.anim.magenative_slide_in, R.anim.magenative_slide_out);

            default:
                return super.onOptionsItemSelected(item);
        }

    }

    public void changecount() {
        if (localData.getLineItems() != null) {
            count = String.valueOf(items.getItemcounts());
        } else {
            count = "0";
        }
        invalidateOptionsMenu();
    }

    private void processwishlist(ImageView wishlist, String s) {
        try {
            Variants variants = varID.get(variant);
            if (localData.getWishList() != null) {
                JSONObject object = new JSONObject((localData.getWishList()));

                if (object.has(s)) {
                    Log.i("InRemoval", "In");
                    object.remove(s);
                    localData.saveWishList(object);
                    wishlist.setImageResource(R.drawable.wishlike);
                } else {
                    JSONObject subobject = new JSONObject();
                    subobject.put("product_id", productID);
                    subobject.put("product_name", MageNative_productname.getText().toString());
                    subobject.put("image", urls.get(0));
                    subobject.put("varinats", totalvariantsize);
                    subobject.put("varinatid", variants.vid);
                    object.put(productID, subobject);
                    localData.saveWishList(object);
                    wishlist.setImageResource(R.drawable.wishred);
                }
            } else {
                JSONObject object = new JSONObject();
                JSONObject subobject = new JSONObject();
                subobject.put("product_id", productID);
                subobject.put("product_name", MageNative_productname.getText().toString());
                subobject.put("varinats", totalvariantsize);
                subobject.put("varinatid", variants.vid);
                subobject.put("image", urls.get(0));
                object.put(productID, subobject);
                localData.saveWishList(object);
                wishlist.setImageResource(R.drawable.wishred);
            }
        } catch (Exception e) {
            e.printStackTrace();
        }
    }

    private void checkVariantArray(String text, int i, ArrayList variantsavailable) throws JSONException {

        Log.i("option", "opt : " + text + " i : " + i);
        Log.i("optionopt", "" + variantsavailable);
        if (i == 0)
            v1 = text;
        else if (i == 1)
            v2 = text;
        else if (i == 2)
            v3 = text;

        if (no_of_opt == 1)
            variant = v1;
        else if (no_of_opt == 2)
            variant = v1 + " / " + v2;
        else if (no_of_opt == 3)
            variant = v1 + " / " + v2 + " / " + v3;
        Log.i("optionopt", "" + variant);

        Variants variants = varID.get(variant);
        String sp = null;
        if (variants != null) {
            if (variants.special_price != null) {
                if (!variants.special_price.isEmpty())
                    sp = Double.parseDouble(variants.special_price)+"";
            }
            String rp = Double.parseDouble(variants.regular_price)+"";
            if (!variants.special_price.isEmpty()) {
                if (sp.compareTo(rp) == 1) {
                    Log.d("checkkk 1","if");

                    MageNative_normalprice.setPaintFlags(Paint.STRIKE_THRU_TEXT_FLAG);
                    MageNative_specialprice.setText(NewCurrencyFormatter.setsymbol(rp, variants.currency));
                    MageNative_normalprice.setText(NewCurrencyFormatter.setsymbol(sp, variants.currency));
                } else {
                    Log.d("checkkk 1","else");
                    MageNative_specialprice.setText(NewCurrencyFormatter.setsymbol(rp,  variants.currency));
                    MageNative_normalprice.setText(NewCurrencyFormatter.setsymbol(sp,variants.currency));
                    // MageNative_specialprice.setVisibility(View.GONE);
                }

            } else {

                MageNative_normalprice.setText(NewCurrencyFormatter.setsymbol(rp,  variants.currency));
                MageNative_specialprice.setVisibility(View.GONE);
            }
        }


        if (!available_var_array.contains(variant)) {
            //isoutofstock.setVisibility(View.VISIBLE);
            out_of_stock = true;
            addtocart.setText(getResources().getString(R.string.outofstock));
            addtocart.setBackgroundColor(getResources().getColor(R.color.red));
            addtocart.setEnabled(false);
            //buunow.setVisibility(View.GONE);
        } else {
            //isoutofstock.setVisibility(View.GONE);
            out_of_stock = false;
            addtocart.setText(getResources().getString(R.string.add_to_cart));
            addtocart.setBackgroundColor(getResources().getColor(R.color.black));
            addtocart.setEnabled(true);
            //buunow.setVisibility(View.VISIBLE);
        }

        if (variant_title_id_map.containsKey(variant)) {
            /*if (hashMap.get(variant_title_id_map.get(variant)).equals("0")) {
                out_of_stock = true;
                addtocart.setBackgroundColor(getResources().getColor(R.color.light_pink));
                addtocart.setText(getResources().getString(R.string.outofstock));
                addtocart.setEnabled(false);
            } else {
                out_of_stock = false;
                addtocart.setBackgroundColor(getResources().getColor(R.color.pink));
                addtocart.setText(getResources().getString(R.string.add_to_cart));
                addtocart.setEnabled(true);
            }
            inventory.setText("Sisa Stok: " + hashMap.get(variant_title_id_map.get(variant)));*/
            MageNative_productimages.setCurrentItem(urls.indexOf(var_image_title_src.get(variant)), true);
            MageNative_productimages.requestFocus();
        }

    }

    private void checkVariantArray(String text, int i) {
        Log.i("option", "opt : " + text + " i : " + i);

        if (i == 0)
            v1 = text;
        else if (i == 1)
            v2 = text;
        else if (i == 2)
            v3 = text;

        if (no_of_opt == 1)
            variant = v1;
        else if (no_of_opt == 2)
            variant = v1 + " / " + v2;
        else if (no_of_opt == 3)
            variant = v1 + " / " + v2 + " / " + v3;

        if (varID.containsKey(variant)) {
            Log.i("option", "variant-available : " + variant);
        }
    }

    /******************************************************** Size Chart ***************************************************************************/
    private void size() throws UnsupportedEncodingException {

        String url = "details?shop=" + getResources().getString(R.string.shopdomain) + "&product_id=" + getBase64Decode(productID) + "&tags=" + URLEncoder.encode(tags, "utf-8") + "&vendor=" + vendor + "&collections=" + URLEncoder.encode(collections, "utf-8");
        Call<ResponseBody> call = apiKiwiSizeChart.details(url);
        // Call<ResponseBody> call = apiKiwiSizeChart.details(getResources().getString(R.string.shopdomain), getBase64Decode(productID),tags,vendor,collections);
        Response.getRetrofitResponse(call, new AsyncResponse() {
            @Override
            public void finalOutput(Object output, boolean error) throws Exception {
                if (error) {
                    try {

                        dialog.setContentView(R.layout.magenative_sizechart);
                        LinearLayout l1 = dialog.findViewById(R.id.relative_sizechart);
                        Window window = dialog.getWindow();
                        window.setLayout(ViewGroup.LayoutParams.MATCH_PARENT, ViewGroup.LayoutParams.WRAP_CONTENT);

                        JSONObject jsonObject = new JSONObject(output.toString());
                        JSONObject jsonObjectData = jsonObject.getJSONObject("data");
                        JSONArray jsonArray = jsonObjectData.getJSONArray("sizings");
                        if (jsonArray.length() > 0) {
                            Iterator jsonObjectTables = jsonArray.getJSONObject(0).getJSONObject("tables").keys();
                            String key = "";
                            while (jsonObjectTables.hasNext()) {
                                key = jsonObjectTables.next().toString();
                            }
                            JSONObject tables = jsonArray.getJSONObject(0).getJSONObject("tables").getJSONObject(key);

                            JSONArray jsonArrayData = tables.getJSONArray("data");
                            TextView tv1 = dialog.findViewById(R.id.tv1);
                            TextView inches = dialog.findViewById(R.id.inches);
                            TextView cm = dialog.findViewById(R.id.cm);
                            tv1.setText(jsonArray.getJSONObject(0).getString("name"));
                            Log.e("jsonArrayData", jsonArrayData.toString());
                            View view = null;
                            TextView label_name = null;
                            LinearLayout linearLayout = null;
                            LinearLayout lin_parent = null;

                            layoutSet(jsonArrayData, linearLayout, view, label_name, l1, 0);
                            cm.setTextColor(getResources().getColor(R.color.black));
                            inches.setTextColor(getResources().getColor(R.color.main_color_gray));
                            cm.setOnClickListener(new View.OnClickListener() {
                                @Override
                                public void onClick(View v) {
                                    cm.setTextColor(getResources().getColor(R.color.black));
                                    inches.setTextColor(getResources().getColor(R.color.main_color_gray));
                                    layoutSet(jsonArrayData, linearLayout, view, label_name, l1, 0);
                                }
                            });

                            inches.setOnClickListener(new View.OnClickListener() {
                                @Override
                                public void onClick(View v) {
                                    inches.setTextColor(getResources().getColor(R.color.black));
                                    cm.setTextColor(getResources().getColor(R.color.main_color_gray));
                                    layoutSet(jsonArrayData, linearLayout, view, label_name, l1, 1);
                                }
                            });

                            sizechart.setVisibility(View.VISIBLE);
                            outline7.setVisibility(View.VISIBLE);
                        }else {
                            outline6.setVisibility(View.GONE);
                            sizechart.setVisibility(View.GONE);
                            outline7.setVisibility(View.GONE);
                        }


                    } catch (Exception e) {
                        e.printStackTrace();
                    }
                }
            }
        }, ProductView.this);
    }

    private void layoutSet(JSONArray jsonArrayData, LinearLayout linearLayout, View view, TextView label_name, LinearLayout l1, int check) {
        l1.removeAllViews();
        for (int i = 0; i < jsonArrayData.length(); i++) {
            JSONArray items = null;
            try {
                items = jsonArrayData.getJSONArray(i);
                linearLayout = new LinearLayout(ProductView.this);
                linearLayout.setOrientation(LinearLayout.HORIZONTAL);
                //  for (int j = items.length() - 1; j >= 0; j--) {
                for (int j = 0; j < items.length(); j++) {

                    view = LayoutInflater.from(ProductView.this).inflate(R.layout.size_chart_item, null, false);
                    label_name = view.findViewById(R.id.label_name);
                    Log.d(TAG, "finalOutput: " + items.getJSONObject(j).getString("value"));
                    String type = items.getJSONObject(j).getString("type");
                    if (type.equalsIgnoreCase("header")) {
                        label_name.setTextColor(getResources().getColor(R.color.black));
                    } else {
                        label_name.setTextColor(getResources().getColor(R.color.main_color_gray));
                    }
                    String value = items.getJSONObject(j).getString("value");
                    if (!type.equalsIgnoreCase("header") && check == 1) {
                        if (value.contains("/")) {
                            String[] parts = value.split("/");
                            String part_one = String.valueOf(round(Integer.parseInt(parts[0]) * 0.393701, 1));
                            String part_two = String.valueOf(round(Integer.parseInt(parts[1]) * 0.393701, 1));
                            label_name.setText(part_one + "/" + part_two);
                        } else {
                            label_name.setText(String.valueOf(round(items.getJSONObject(j).getInt("value") * 0.393701, 1)));
                        }
                    } else {
                        label_name.setText(value);
                    }
                    linearLayout.addView(view);
                }
                l1.addView(linearLayout);
            } catch (JSONException e) {
                e.printStackTrace();
            }

        }
    }

    private Storefront.ProductVariantPricePairEdge getEdge(List<Storefront.ProductVariantPricePairEdge> edges) {
        Storefront.ProductVariantPricePairEdge pairEdge = null;
        try {
            for (int i = 0; i < edges.size(); i++) {
                Log.i("presentment", edges.get(i).getNode().getPrice().getCurrencyCode().toString());
                Log.i("presentment", presentmentcurrency);

                if (edges.get(i).getNode().getPrice().getCurrencyCode().toString().equals(presentmentcurrency)) {
                    pairEdge = edges.get(i);
                    break;
                }
            }
        } catch (Exception e) {
            e.printStackTrace();
        }
        return  pairEdge;
    }


    public static class Variants {
        public String vid, title, regular_price, special_price, vimage,currency;

        public Variants(String vid, String title,String currency ,String regualr_price, String special_price, String vimage) {
            this.vid = vid;
            this.title = title;
            this.regular_price = regualr_price;
            this.special_price = special_price;
            this.vimage = vimage;
            this.currency = currency;
        }

        public String getVid() {
            return vid;
        }

        public String getTitle() {
            return title;
        }

        public String getPrice() {
            return regular_price;
        }

        public String getVimage() {
            return vimage;
        }

        public String getCurrency() {
            return currency;
        }
    }

    /******************************************************** Size Chart ***************************************************************************/

}
