package com.elietlux.shopifyapp.productlistingsection;

import android.annotation.SuppressLint;
import android.app.Activity;
import android.app.Dialog;
import android.content.Context;
import android.content.Intent;
import android.os.Build;
import android.os.Bundle;
import android.support.annotation.NonNull;
import android.support.annotation.Nullable;
import android.support.annotation.RequiresApi;
import android.text.Html;
import android.util.Base64;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.AbsListView;
import android.widget.AdapterView;
import android.widget.GridView;
import android.widget.ImageView;
import android.widget.ListView;
import android.widget.RelativeLayout;
import android.widget.TextView;
import android.widget.Toast;

import com.blogspot.atifsoftwares.animatoolib.Animatoo;
import com.elietlux.shopifyapp.R;
import com.elietlux.shopifyapp.adaptersection.Product_Adapter;
import com.elietlux.shopifyapp.adaptersection.Product_Adapter_Tags;
import com.elietlux.shopifyapp.adaptersection.Sort_Adapter;
import com.elietlux.shopifyapp.datasection.Data;
import com.elietlux.shopifyapp.loadersection.CustomProgressDialog;
import com.elietlux.shopifyapp.loadersection.Loader;
import com.elietlux.shopifyapp.maincontainer.MainActivity;
import com.elietlux.shopifyapp.productviewsection.ProductView;
import com.elietlux.shopifyapp.requestsection.ApiClient;
import com.elietlux.shopifyapp.requestsection.ApiInterface;
import com.elietlux.shopifyapp.searchsection.SearchView;
import com.elietlux.shopifyapp.storagesection.LocalData;
import com.elietlux.shopifyapp.storefrontqueries.Query;
import com.elietlux.shopifyapp.storefrontresponse.AsyncResponse;
import com.elietlux.shopifyapp.storefrontresponse.Response;
import com.shopify.buy3.GraphCallResult;
import com.shopify.buy3.GraphClient;
import com.shopify.buy3.GraphResponse;
import com.shopify.buy3.QueryGraphCall;
import com.shopify.buy3.Storefront;
import com.shopify.graphql.support.ID;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import java.io.Serializable;
import java.io.UnsupportedEncodingException;
import java.nio.charset.StandardCharsets;
import java.util.ArrayList;
import java.util.List;
import java.util.Objects;

import butterknife.BindView;
import butterknife.ButterKnife;
import io.reactivex.Observable;
import io.reactivex.SingleObserver;
import io.reactivex.android.schedulers.AndroidSchedulers;
import io.reactivex.disposables.Disposable;
import io.reactivex.schedulers.Schedulers;
import okhttp3.ResponseBody;
import retrofit2.Call;

/**
 * Created by cedcoss on 27/4/18.
 */
public class ProductListing extends MainActivity {
    public static List<Storefront.ProductEdge> productedge = null;
    public static List<Storefront.Product> productedge_filter = null;
    public static List<Storefront.Product> _productedge_filter = null;
    int pagee = 1;
    public JSONObject filter_id;
    String isFrombrands = "false";
    @Nullable
    @BindView(R.id.grid)
    GridView grid;
    @Nullable
    @BindView(R.id.MageNative_main)
    RelativeLayout MageNative_main = null;
    @Nullable
    @BindView(R.id.oops)
    TextView oops = null;
    @Nullable
    @BindView(R.id.MageNative_sortsection)
    RelativeLayout MageNative_sortsection = null;
    @Nullable
    @BindView(R.id.MageNative_filtersection)
    RelativeLayout MageNative_filtersection = null;
    GraphClient client = null;
    String cursor = "nocursor";
    String cursor_search = "nocursor";
    boolean hasNextPage = false;
    boolean hasNextPageFilter = false;
    boolean reverse = false;
    ApiInterface apiService;
    Storefront.ProductCollectionSortKeys sort_key = Storefront.ProductCollectionSortKeys.BEST_SELLING;
    int sortvalue = -1;
    String cat_id = "";
    String cat_id_decoded = "";
    LocalData localData = null;
    String currency_symbol = "";
    Loader loader = null;
    String filterString = "";
    JSONArray products;
    JSONArray _products;
    List<Storefront.ProductEdge> productEdgesFilter;
    Product_Adapter_Tags adapter;
    JSONArray previousproducts;
    String sort_by_filter = "title-ascending";
    Storefront.ProductSortKeys sort_key_search = Storefront.ProductSortKeys.BEST_SELLING;
    boolean stopfilter = false;
    ViewGroup content;
    ArrayList<ID> filer_product_list;
    Intent starterIntent;
    Boolean isProductsFound = true;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        try {
            super.onCreate(savedInstanceState);
            content = findViewById(R.id.MageNative_frame_container);
            getLayoutInflater().inflate(R.layout.magenative_gridview, content, true);
            localData = new LocalData(ProductListing.this);
            filer_product_list = new ArrayList<>();
            products = new JSONArray();
            _products = new JSONArray();
            productEdgesFilter = new ArrayList<>();
            loader = new Loader(ProductListing.this);
            Log.d("loaderr", "1");
            starterIntent = getIntent();
            //loader.show();
            if (localData.getMoneyFormat() != null) {
                currency_symbol = localData.getMoneyFormat();
            }
            color_bank = new JSONObject();
            filter_id = new JSONObject();
            productedge = new ArrayList<Storefront.ProductEdge>();
            productedge_filter = new ArrayList<Storefront.Product>();
            _productedge_filter = new ArrayList<Storefront.Product>();
            ButterKnife.bind(ProductListing.this);
            //showbackbutton();
            cat_id = getIntent().getStringExtra("cat_id");
            Log.i("category_id", "" + cat_id);

            if (!cat_id.contains(" ") && !cat_id.contains("=")) {
                Log.d("Aerin", "if");
                cat_id_decoded = cat_id;
            } else {
                Log.d("Aerin", "else");
                if (isFrombrands.equalsIgnoreCase("false")) {
                    cat_id_decoded = getBase64Decode(cat_id);
                }
            }

            client = ApiClient.getGraphClient(ProductListing.this, true);
            Log.i("category_id", "" + cat_id);
            Log.i("category_id", "" + cat_id_decoded);

            isFrombrands = getIntent().getStringExtra("isFrombrands");
            Log.d("isFrombrands", "" + isFrombrands);

            if (getIntent().getStringExtra("cat_name") != null) {
                showTittle(getIntent().getStringExtra("cat_name"));
            } else {
                showTittle(" ");
            }

            if (isFrombrands.equalsIgnoreCase("true")) {
                MageNative_sortsection.setVisibility(View.GONE);
                // loader.show();
                getProductsSearch(cat_id, cursor_search, sort_key_search, reverse, "firsttime");
                Log.d("Tessssttt", "1");
                grid.setOnScrollListener(new AbsListView.OnScrollListener() {
                    @Override
                    public void onScrollStateChanged(AbsListView absListView, int i) {

                    }

                    @Override
                    public void onScroll(AbsListView absListView, int firstVisibleItem, int visibleItemCount, int totalItemCount) {
                        Log.d("isFrombrandscheck", "1 " + filterString);

                        if ((firstVisibleItem + visibleItemCount) != 0) {
                            if (((firstVisibleItem + visibleItemCount) >= totalItemCount) && hasNextPage) { //
                                if (!stopfilter) {
                                    if (filterString.isEmpty()) {
                                        hasNextPage = false;
                                        hasNextPageFilter = false;
                                        Log.d("isFilterscroll", "if " + filterString);
                                        Log.d("loaderr", "2");
                                        getProductsSearch(cat_id, cursor_search, sort_key_search, reverse, "scroll");
                                    } else {
                                        try {
                                            if (hasNextPageFilter) {
                                                pagee++;
                                                Log.d("isgetFilterscroll", "1- " + filterString);
                                                getFilters(filterString, pagee, "Source 1");
                                            }

                                        } catch (JSONException e) {
                                            e.printStackTrace();
                                        }
                                    }
                                }
                            }
                        }
                    }
                });
                grid.setOnItemClickListener(new AdapterView.OnItemClickListener() {
                    @Override
                    public void onItemClick(AdapterView<?> adapterView, View view, int i, long l) {
                        TextView id = view.findViewById(R.id.product_id);
                        Intent intent = new Intent(ProductListing.this, ProductView.class);
                        intent.putExtra("id", id.getText().toString());
                        //intent.putExtra("object",(Serializable)productedge.get(i));
                        startActivity(intent);
                        Animatoo.animateZoom(ProductListing.this);
                        // overridePendingTransition(R.anim.magenative_slide_in, R.anim.magenative_slide_out);
                    }
                });

            } else {

                getProducts(cat_id, cursor, sort_key, reverse, "firsttime");
                grid.setOnScrollListener(new AbsListView.OnScrollListener() {
                    @Override
                    public void onScrollStateChanged(AbsListView absListView, int i) {

                    }

                    @Override
                    public void onScroll(AbsListView absListView, int firstVisibleItem, int visibleItemCount, int totalItemCount) {
                        if ((firstVisibleItem + visibleItemCount) != 0) {
                            Log.d("isFrombrandscheck", "2" + filterString + "  " + hasNextPage);
                            if ((((firstVisibleItem + visibleItemCount) == totalItemCount) && hasNextPage)) { //((firstVisibleItem + visibleItemCount) >= totalItemCount / 2)

                                if (filterString.isEmpty()) {
                                    Log.d("checkk", "if");
                                    hasNextPage = false;
                                    hasNextPageFilter = false;
                                    Log.d("isFilterscroll", "if " + filterString);
                                    getProducts(cat_id, cursor, sort_key, reverse, "scroll");
                                } else {
                                    Log.d("checkk", "else");
                                    try {
                                        Log.d("checkk", "else " + hasNextPageFilter);
                                        if (hasNextPageFilter) {
                                            Log.d("checkk", "else " + hasNextPageFilter + " " + loader.isShowing());
                                            // if (!loader.isShowing()) { // for checking if request is still in progress
                                            pagee++;
                                            Log.d("isgetFilterscroll", "1- " + filterString);
                                            //  loader.show();
                                            getFilters(filterString, pagee, "Source 2");
                                            //  }
                                        }

                                    } catch (JSONException e) {
                                        e.printStackTrace();
                                        loader.dismiss();
                                    }
                                }

                            }
                        }
                    }
                });
                MageNative_sortsection.setOnClickListener(new View.OnClickListener() {
                    @Override
                    public void onClick(View view) {
                        Sort();
                    }
                });
                grid.setOnItemClickListener(new AdapterView.OnItemClickListener() {
                    @Override
                    public void onItemClick(AdapterView<?> adapterView, View view, int i, long l) {
                        TextView id = view.findViewById(R.id.product_id);
                        Intent intent = new Intent(ProductListing.this, ProductView.class);
                        intent.putExtra("id", id.getText().toString());
                        // intent.putExtra("object", productedge.get(i));
                        intent.putExtra("catID", cat_id);
                        intent.putExtra("position", i);
                        startActivity(intent);
                        Animatoo.animateZoom(ProductListing.this);
                        overridePendingTransition(R.anim.magenative_slide_in, R.anim.magenative_slide_out);
                    }
                });
                Log.d("isFilterscroll", "2 " + filterString);
                // moved here so that we get filters data first
            }
            /********************************************************** Filter Request ******************************************************************/
            isFilteredApiInstalled();
            /********************************************************** Filter Request ******************************************************************/

        } catch (Exception e) {
            e.printStackTrace();
        }


    }

    private void isFilteredApiInstalled() {
        apiService = ApiClient.getFilterClient(ProductListing.this).create(ApiInterface.class);
        Call<ResponseBody> call = apiService.getFilterInstalledStatus(getResources().getString(R.string.mid));
        Response.getRetrofitResponse(call, new AsyncResponse() {
            @Override
            public void finalOutput(@NonNull Object output, @NonNull boolean error) throws JSONException {
                if (error) {
//                    if (CustomProgressDialog.mDialog != null) {
//                        CustomProgressDialog.mDialog.dismiss();
//                        CustomProgressDialog.mDialog = null;
//                    }
                    JSONObject jsonObject = new JSONObject(output.toString());
                    if (jsonObject.getBoolean("is_installed")) {
                        if (jsonObject.getBoolean("success")) {
                            //color_bank = jsonObject.getJSONObject("data").getJSONObject("swatches").getJSONObject("tag");
                            filter_id = jsonObject.getJSONObject("new_filter_data");
                            Log.d("color_bank", "" + color_bank);
                            Log.d("color_bank", "" + filter_id);
                            Log.d("isgetFilterscroll", "2- " + filterString);
                            pagee = 1;
                            getFilters("", pagee, "Source 3");
                        }
                    }
                } else {
                    Log.i("ErrorHomePage", "" + output.toString());
                }
            }
        }, ProductListing.this);
    }

    private void getFilters(String isBackFromFilters, int page, String source) throws JSONException {
        Log.d("pageee", source + " - " + page);
        String filter = "";
        if (!loader.isShowing()) {
            Log.d("loaderr", "4");
            loader.show();
        }

        if (filter_id.has(cat_id_decoded)) {
            filter = filter_id.getString(cat_id_decoded);
        } else if (filter_id.has("1")) {
            filter = filter_id.getString("1");
        } else if (filter_id.has("2")) {
            filter = filter_id.getString("2");
        } else {
            filter = filter_id.getString("default");
        }

        Log.d("filter_id", "" + filter);
        String filterURL = "";
        if (!isBackFromFilters.isEmpty()) {
            if (isFrombrands.equalsIgnoreCase("true")) {
                filterURL += "globosmartproductfilterapi/filter?" + "filter_id=" + filter + "&shop=" + getResources().getString(R.string.shopdomain) + "&collection=" + "false" + isBackFromFilters + "&limit=24" + "&page=" + page + "&current_tags[]=" + cat_id.replace("&", "%26") + "&sort=" + sort_by_filter;
            } else {
                filterURL += "globosmartproductfilterapi/filter?" + "filter_id=" + filter + "&shop=" + getResources().getString(R.string.shopdomain) + "&collection=" + cat_id_decoded + isBackFromFilters + "&limit=24" + "&page=" + page + "&sort=" + sort_by_filter;
            }
        } else {
            if (isFrombrands.equalsIgnoreCase("true")) {
                filterURL += "globosmartproductfilterapi/filter?" + "filter_id=" + filter + "&shop=" + getResources().getString(R.string.shopdomain) + "&collection=" + "false" + "&limit=24" + "&page=" + page + "&current_tags[]=" + cat_id.replace("&", "%26") + "&sort=" + sort_by_filter;
            } else {
                filterURL += "globosmartproductfilterapi/filter?" + "filter_id=" + filter + "&shop=" + getResources().getString(R.string.shopdomain) + "&collection=" + cat_id_decoded + "&limit=24" + "&page=" + page + "&sort=" + sort_by_filter;

            }
        }
        int length = 0;
        apiService = ApiClient.getFilterClient(ProductListing.this).create(ApiInterface.class);
        Log.d("filterURL", "" + filterURL);
        Call<ResponseBody> call = apiService.getFilters(filterURL);
        Response.getRetrofitResponse(call, new AsyncResponse() {
            @Override
            public void finalOutput(@NonNull Object output, @NonNull boolean error) {
                Log.d("vaibhav getGiftProduct", error + " -- " + output);
                if (error) {
                    System.out.println("output=Filter= " + output.toString());
//                    if (CustomProgressDialog.mDialog != null) {
//                        CustomProgressDialog.mDialog.dismiss();
//                        CustomProgressDialog.mDialog = null;
//                    }
                    loader.dismiss();
                    try {
                        filterJson = new JSONObject(output.toString());
                        Log.d(" ", "" + filterJson);
                        MageNative_filtersection.setVisibility(View.VISIBLE);
                        loader.dismiss();
                        /**************************************************************************************************************/
                        if (!isBackFromFilters.isEmpty()) {
                            products = filterJson.getJSONObject("data").getJSONArray("products");
                            productedge_filter = new ArrayList<>();
                            hasNextPageFilter = filterJson.getJSONObject("data").getJSONObject("pagination").getBoolean("hasMorePages");
                            hasNextPage = filterJson.getJSONObject("data").getJSONObject("pagination").getBoolean("hasMorePages");
                            Log.d("products", "page " + page);
                            Log.d("products", "hasMorePages " + filterJson.getJSONObject("data").getJSONObject("pagination").getBoolean("hasMorePages"));
                            JSONObject jsonObject;
                            String s1 = "";
                            Storefront.ProductEdge edge;
                            final int[] length = {products.length()};
                            if (products.length() > 0) {
                                hideNoProducts();
                                Log.d("products", "size_2-" + products.length());
                                // hasNextPageFilter = true;
                                for (int i = 0; i < products.length(); i++) {

                                    _products.put(products.getJSONObject(i));
                                    s1 = "gid://shopify/Product/" + products.getJSONObject(i).getString("id");

                                    Log.d("product_idd", "" + getBase64Encode(s1));
                                    filer_product_list.add(new ID(getBase64Encode(s1)));

                                    if (getBase64Encode(s1) != null) {
                                        QueryGraphCall call = client.queryGraph(Query.getSingleProduct(getBase64Encode(s1)));
                                        Response.getGraphQLResponse(call, new AsyncResponse() {
                                            @Override
                                            public void finalOutput(@NonNull Object output, @NonNull boolean error) {
                                                if (error) {
                                                    GraphResponse<Storefront.QueryRoot> response = ((GraphCallResult.Success<Storefront.QueryRoot>) output).getResponse();
                                                    runOnUiThread(new Runnable() {
                                                        @Override
                                                        public void run() {

                                                            if (response.getData().getNode() != null) {
                                                                productedge_filter.add((Storefront.Product) response.getData().getNode());
                                                            } else {
                                                                length[0] = length[0] - 1;
                                                            }
                                                            Log.d("product_idd", "" + productedge_filter.size());
                                                            Log.d("product_idd", "" + length[0]);

                                                            if (length[0] == productedge_filter.size()) {
                                                                _productedge_filter.addAll(productedge_filter);
                                                                if (pagee == 1) {
                                                                    Log.d("adapter", "if");
                                                                    adapter = new Product_Adapter_Tags(ProductListing.this, _productedge_filter, currency_symbol, grid);
                                                                    grid.setAdapter(adapter);
                                                                    int cp = grid.getFirstVisiblePosition();
                                                                    grid.setSelection(cp + 1);

                                                                } else {
                                                                    Log.d("adapter", "else " + pagee);
                                                                    int cp = grid.getFirstVisiblePosition();
                                                                    grid.setSelection(cp + 1);
                                                                    adapter.notifyDataSetChanged();
                                                                }


                                                                if (productedge_filter.size() == 0) {
                                                                    isProductsFound = false;
                                                                    // getLayoutInflater().inflate(R.layout.emptywishlist, content, true);
                                                                    showNoProducts();

                                                                }
                                                                new Thread(new Runnable() {
                                                                    @Override
                                                                    public void run() {
                                                                        runOnUiThread(new Runnable() {
                                                                            @Override
                                                                            public void run() {
                                                                                loader.dismiss();
                                                                            }
                                                                        });
                                                                    }
                                                                }).start();


                                                            }
                                                        }
                                                    });
                                                } else {
                                                    Log.i("ResponseError", " " + output.toString());
                                                    new Thread(new Runnable() {
                                                        @Override
                                                        public void run() {
                                                            runOnUiThread(new Runnable() {
                                                                @Override
                                                                public void run() {
                                                                    loader.dismiss();
                                                                }
                                                            });
                                                        }
                                                    }).start();
                                                }
                                            }
                                        }, ProductListing.this);
                                    }
                                }

                            } else {
                                hasNextPageFilter = false;
                                filterString = "";
                                stopfilter = true;
                                loader.hide();
                                Log.d("isgetFilterscroll", "3- " + filterString);
                                //getFilters(filterString, 1);
                                if (_products.length() == 0) {
                                    if (products.length() == 0) {
                                        isProductsFound = false;
                                        showNoProducts();
                                        //getLayoutInflater().inflate(R.layout.emptywishlist, content, true);
                                        // finish();
                                    } else {
                                        showNoProducts();
                                        isProductsFound = false;
                                        //getLayoutInflater().inflate(R.layout.emptywishlist, content, true);
                                    }
                                }
                            }
                        }
                        /**************************************************************************************************************/
                        /******************************************************************************************************************/
                        MageNative_filtersection.setOnClickListener(new View.OnClickListener() {
                            @Override
                            public void onClick(View view) {
                                Intent filteractivity = new Intent(ProductListing.this, FilterActivity.class);
                                // filteractivity.putExtra("filterData", ""+filterJson);
                                startActivityForResult(filteractivity, 1);
                            }
                        });
                        /******************************************************************************************************************/

                    } catch (JSONException e) {
                        e.printStackTrace();
                    }
                }
            }
        }, ProductListing.this);
    }

    public void Sort() {
        try {
            final Dialog listDialog = new Dialog(this, R.style.PauseDialog);
            ((ViewGroup) ((ViewGroup) Objects.requireNonNull(listDialog.getWindow()).getDecorView()).getChildAt(0))
                    .getChildAt(1)
                    .setBackgroundColor(getResources().getColor(R.color.black));
            listDialog.setTitle(Html.fromHtml("<center><font color='#ffffff'>" + getResources().getString(R.string.sortbyitems) + "</font></center>"));
            LayoutInflater li = (LayoutInflater) this.getSystemService(Context.LAYOUT_INFLATER_SERVICE);
            @SuppressLint("InflateParams") View v = Objects.requireNonNull(li).inflate(R.layout.magenative_sortitemlist, null, false);
            listDialog.setContentView(v);
            listDialog.setCancelable(true);
            ListView list1 = listDialog.findViewById(R.id.MageNative_sortlistview);
            list1.setOnItemClickListener(new AdapterView.OnItemClickListener() {
                @Override
                public void onItemClick(AdapterView<?> adapterView, @NonNull View view, int i, long l) {
                    listDialog.cancel();
                    listDialog.dismiss();
                    sortvalue = i;
                    setSortKeys(i);
                    cursor = "nocursor";
                    hasNextPage = false;
                    productedge = null;
                    Log.d("loaderr", "5");

                    if (filterString.isEmpty()) {
                        Log.d("isFilterscroll", "3 " + filterString);
                        getProducts(cat_id, cursor, sort_key, reverse, "firsttime");
                    } else {
                        try {
                            pagee = 1;
                            _products = new JSONArray();
                            _productedge_filter = new ArrayList<>();
                            Log.d("isgetFilterscroll", "4- " + filterString);
                            getFilters(filterString, pagee, "Source 4");
                        } catch (JSONException e) {
                            e.printStackTrace();
                        }

                    }
                }
            });
            Sort_Adapter adapter = new Sort_Adapter(this, Data.getSortOptions(ProductListing.this), sortvalue);
            list1.setAdapter(adapter);
            listDialog.show();
        } catch (Exception e) {
            e.printStackTrace();
        }

    }

    private void setSortKeys(int i) {
        switch (i) {
            case 0:
                sort_key = Storefront.ProductCollectionSortKeys.TITLE;
                sort_by_filter = "title-ascending";
                reverse = false;
                break;

            case 1:
                sort_key = Storefront.ProductCollectionSortKeys.TITLE;
                sort_by_filter = "title-decending";
                reverse = true;
                break;

            case 2:
                sort_key = Storefront.ProductCollectionSortKeys.PRICE;
                sort_by_filter = "price-decending";
                reverse = true;
                break;

            case 3:
                sort_key = Storefront.ProductCollectionSortKeys.PRICE;
                sort_by_filter = "price-ascending";
                reverse = false;
                break;

            case 4:
                sort_key = Storefront.ProductCollectionSortKeys.BEST_SELLING;
                reverse = false;
                sort_by_filter = "best-selling";
                break;

            case 5:
                sort_key = Storefront.ProductCollectionSortKeys.CREATED;
                sort_by_filter = "created";
                reverse = false;
                break;
        }
    }

    public void getProducts(String cat_id, String cursor, Storefront.ProductCollectionSortKeys keys, boolean reverse, String origin) {
        try {
            Log.i("Cursor", "" + cursor);
            ArrayList<Storefront.CurrencyCode> currencyCodeArrayList = new ArrayList<>();
            Log.d("currencyCodeArrayList", "" + localData.getCurrencyCode());
            currencyCodeArrayList.add(Storefront.CurrencyCode.valueOf(localData.getCurrencyCode()));
            Log.d("currencyCodeArrayList", "" + currencyCodeArrayList);
//            if (!loader.isShowing()) {
//                loader.show();
//            }

            QueryGraphCall call = client.queryGraph(Query.getProductListing(cat_id, cursor, keys, reverse, currencyCodeArrayList));
            Response.getGraphQLResponse(call, new AsyncResponse() {
                @Override
                public void finalOutput(@NonNull Object output, @NonNull boolean error) {
                    if (error) {
                        GraphResponse<Storefront.QueryRoot> response = ((GraphCallResult.Success<Storefront.QueryRoot>) output).getResponse();

                        new Thread(new Runnable() {
                            @Override
                            public void run() {
                                runOnUiThread(new Runnable() {
                                    @Override
                                    public void run() {
                                        loader.dismiss();
                                        processProducts(response, origin);
                                    }
                                });
                            }
                        }).start();
                    } else {
                        Log.i("ResponseError GL", "" + output.toString());
                        new Thread(new Runnable() {
                            @Override
                            public void run() {
                                runOnUiThread(new Runnable() {
                                    @Override
                                    public void run() {
                                        loader.dismiss();
                                    }
                                });
                            }
                        }).start();

                        hasNextPage = false;
                    }
                }
            }, ProductListing.this);
        } catch (Exception e) {
            e.printStackTrace();
            loader.dismiss();
        }
    }

    private void showNoProducts() {
        oops.setVisibility(View.VISIBLE);
        grid.setVisibility(View.GONE);
    }

    private void hideNoProducts() {
        oops.setVisibility(View.GONE);
        grid.setVisibility(View.VISIBLE);
    }

    private void processProducts(GraphResponse<Storefront.QueryRoot> response, String origin) {
        try {
            loader.dismiss();
            Storefront.Collection collectionEdge = null;
            if (cat_id.contains("*#*")) {
                Log.i("Inhandle", "2");
                collectionEdge = response.getData().getShop().getCollectionByHandle();
            } else {
                Log.i("Inhandle", "1");
                collectionEdge = (Storefront.Collection) response.getData().getNode();
            }
            hasNextPage = collectionEdge.getProducts().getPageInfo().getHasNextPage();
            Log.i("hasNextPage", "" + hasNextPage);
            List<Storefront.ProductEdge> data = collectionEdge.getProducts().getEdges();

            if (data.size() > 0) {
                hideNoProducts();
                Observable.fromIterable(data).subscribeOn(Schedulers.io())
                        .filter(x -> x.getNode().getAvailableForSale())
                        .toList()
                        .observeOn(AndroidSchedulers.mainThread())
                        .subscribe(new SingleObserver<List<Storefront.ProductEdge>>() {
                            @Override
                            public void onSubscribe(Disposable d) {

                            }

                            @Override
                            public void onSuccess(List<Storefront.ProductEdge> productEdges) {
                                if (productedge == null) {
                                    productedge = productEdges;
                                } else {
                                    productedge.addAll(productEdges);
                                }
                                cursor = productedge.get(productedge.size() - 1).getCursor();
                                onResume();

                            }

                            @Override
                            public void onError(Throwable e) {
                                loader.dismiss();
                            }
                        });//done
//                if (productedge == null) {
//                    productedge = data;
//                } else {
//                    productedge.addAll(data);
//                }
//                cursor = productedge.get(productedge.size() - 1).getCursor();


            } else {
                if (origin.equals("firsttime")) {
                    isProductsFound = false;
                    // getLayoutInflater().inflate(R.layout.emptywishlist, content, true);
                    showNoProducts();
                    loader.dismiss();
                    //Toast.makeText(ProductListing.this, getResources().getString(R.string.noproducts), Toast.LENGTH_LONG).show();
                    //finish();
                }
            }
        } catch (Exception e) {
            e.printStackTrace();
            loader.dismiss();
        }
    }

    @Override
    public void onResume() {
        try {
            super.onResume();
            //Log.d("Resumee",""+productedge_filter.size());

            Log.d("Resumeeproducts", "" + products);
            Log.d("Resumeeproducts", "-" + filterString);
            Log.d("productedge", "-" + _productedge_filter);
            Log.d("isProductsFound ", "-" + isProductsFound);

            if (!filterString.isEmpty() && isProductsFound) {
                adapter = new Product_Adapter_Tags(ProductListing.this, _productedge_filter, currency_symbol, grid);
                int cp = grid.getFirstVisiblePosition();
                grid.setAdapter(adapter);
                grid.setSelection(cp + 1);
                adapter.notifyDataSetChanged();

            } else {
                if (productedge != null && isProductsFound) {
                    Product_Adapter adapter = new Product_Adapter(ProductListing.this, productedge, currency_symbol, grid);
                    int cp = grid.getFirstVisiblePosition();
                    grid.setAdapter(adapter);
                    grid.setSelection(cp + 1);
                    adapter.notifyDataSetChanged();

                }
            }

            //loader.hide();

        } catch (Exception e) {
            e.printStackTrace();
            loader.dismiss();
        }

    }

    @Override
    public void onBackPressed() {
        super.onBackPressed();
    }

    @Override
    protected void onActivityResult(int requestCode, int resultCode, Intent data) {
        try {
            Log.d("resultCode", "" + resultCode);
            if (requestCode == 1) {
                Log.d("isgetFilterscroll", "5- " + loader.isShowing());
                Log.d("loaderr", "6");
                Log.d("isgetFilterscroll", "5- " + loader.isShowing());
                hideNoProducts();
                if (resultCode == Activity.RESULT_OK) {
                    // loader.show();
                    Log.d("onActivityResult", data.getStringExtra("filterString"));
//                    grid.removeAllViews();
                    filterString = data.getStringExtra("filterString");
                    reverse = data.getBooleanExtra("reverse", false);
                    int sortkey = data.getIntExtra("sort_key", 0);
                    setSortKeys(sortkey);

                    Log.d("filterStringreturn", reverse + " --- " + sortkey);
                    if (filterString.isEmpty()) {
                        Log.d("isFilterscroll", "4 " + filterString);
                        getProducts(cat_id, cursor, sort_key, reverse, "firsttime");
                    } else {
                        Log.d("isgetFilterscroll", "5- " + filterString);
                        Log.d("loader_check", "5- " + loader.isShowing());
                        _products = new JSONArray();
                        _productedge_filter = new ArrayList<>();
                        pagee = 1;

                        Log.d("loader_check", "5- " + loader.isShowing());
                        getFilters(filterString, pagee, "Source 5");
                    }
                }

                if (resultCode == 2) {
                    Log.d("resultCode", "" + resultCode);
                    finish();
                    startActivity(starterIntent);
                    Animatoo.animateZoom(ProductListing.this);
                }
            }


        } catch (Exception e) {
            e.printStackTrace();
        }
    }


    public String getBase64Encode(String id) {
        byte[] data = Base64.encode(id.getBytes(), Base64.DEFAULT);
        id = new String(data, StandardCharsets.UTF_8).trim();
        return id;
    }

    /**************************************************** Search Code **********************************************/
    public void getProductsSearch(String cat_id, String cursor, Storefront.ProductSortKeys keys, boolean reverse, String origin) {
        try {
            Log.i("Cursor", "" + cursor_search);
            ArrayList<Storefront.CurrencyCode> currencyCodeArrayList = new ArrayList<>();
            Log.d("currencyCodeArrayList", "" + localData.getCurrencyCode());
            currencyCodeArrayList.add(Storefront.CurrencyCode.valueOf(localData.getCurrencyCode()));
            Log.d("currencyCodeArrayList", "" + currencyCodeArrayList);
            QueryGraphCall call = client.queryGraph(Query.getAutoSearchProducts(cursor_search, cat_id, keys, reverse, currencyCodeArrayList));
            Response.getGraphQLResponse(call, new AsyncResponse() {
                @Override
                public void finalOutput(@NonNull Object output, @NonNull boolean error) {
                    if (error) {
                        GraphResponse<Storefront.QueryRoot> response = ((GraphCallResult.Success<Storefront.QueryRoot>) output).getResponse();
                        runOnUiThread(new Runnable() {
                            @Override
                            public void run() {
                                Log.d("Tessssttt", "2");
                                if (loader.isShowing()) {
                                    loader.dismiss();
                                }
                                processProductsSearch(response, origin);
                            }
                        });
                    } else {
                        Log.i("ResponseError", "" + output.toString());
                        hasNextPage = false;
                        new Thread(new Runnable() {
                            @Override
                            public void run() {
                                runOnUiThread(new Runnable() {
                                    @Override
                                    public void run() {
                                        loader.dismiss();
                                    }
                                });
                            }
                        }).start();

                    }
                }
            }, ProductListing.this);
        } catch (Exception e) {
            e.printStackTrace();
        }
    }

    private void processProductsSearch(GraphResponse<Storefront.QueryRoot> response, String origin) {
        try {
            hasNextPage = response.getData().getShop().getProducts().getPageInfo().getHasNextPage();
            Log.i("hasNextPage", "" + hasNextPage);
            List<Storefront.ProductEdge> data = response.getData().getShop().getProducts().getEdges();
            if (data.size() > 0) {
                hideNoProducts();
                Log.i("hasNextPage", "" + cat_id);
                Log.i("hasNextPage", "" + data.get(0).getNode().getVendor());
                Observable.fromIterable(data).subscribeOn(Schedulers.io())
                        .filter(x -> x.getNode().getAvailableForSale())
                        .filter(productEdge -> productEdge.getNode().getVendor().toLowerCase()
                                .contains(cat_id.toLowerCase()))
                        .toList()
                        .observeOn(AndroidSchedulers.mainThread())
                        .subscribe(new SingleObserver<List<Storefront.ProductEdge>>() {
                            @Override
                            public void onSubscribe(Disposable d) {

                            }

                            @Override
                            public void onSuccess(List<Storefront.ProductEdge> productEdges) {
                                if (productedge == null) {
                                    productedge = productEdges;
                                } else {
                                    productedge.addAll(productEdges);
                                }
                                Log.d("cursor_search", "" + productedge.size());
                                if (productEdges.size() > 0) {
                                    hideNoProducts();
                                    cursor_search = productedge.get(productedge.size() - 1).getCursor();
                                    Log.d("cursor_search", "" + cursor_search);
                                    onResume();
                                    loader.dismiss();
                                } else {
                                    MageNative_filtersection.setVisibility(View.GONE);
                                    loader.dismiss();
                                    isProductsFound = false;
                                    showNoProducts();
                                    // getLayoutInflater().inflate(R.layout.emptywishlist, content, true);
                                }

                            }

                            @Override
                            public void onError(Throwable e) {
                                loader.dismiss();
                            }
                        });

                /*if(productedge==null)
                {
                    productedge=data;
                }
                else
                {
                    productedge.addAll(data);
                }
                cursor_search=productedge.get(productedge.size()-1).getCursor();
                Log.d("cursor_search",""+cursor_search);
                onResume();*/
            } else {
                if (origin.equals("firsttime")) {
                    isProductsFound = false;
                    // getLayoutInflater().inflate(R.layout.emptywishlist, content, true);
                    showNoProducts();

                    // Toast.makeText(ProductListing.this, getResources().getString(R.string.noproducts), Toast.LENGTH_LONG).show();
                    //finish();
                }
            }

        } catch (Exception e) {
            e.printStackTrace();
        }
    }

    /**************************************************** Search Code **********************************************/

    private void getProductDataByID(String product_id) {
        try {
            QueryGraphCall call = client.queryGraph(Query.getSingleProduct(product_id));
            Response.getGraphQLResponse(call, new AsyncResponse() {
                @Override
                public void finalOutput(@NonNull Object output, @NonNull boolean error) {
                    if (error) {
                        GraphResponse<Storefront.QueryRoot> response = ((GraphCallResult.Success<Storefront.QueryRoot>) output).getResponse();
                        runOnUiThread(new Runnable() {
                            @Override
                            public void run() {
                                //  processProductData(response);
                            }
                        });
                    } else {
                        Log.i("ResponseError", " " + output.toString());
                        loader.dismiss();
                    }
                }
            }, ProductListing.this);
        } catch (Exception e) {
            e.printStackTrace();
        }
    }

}