package com.elietlux.shopifyapp.productlistingsection;

import android.annotation.SuppressLint;
import android.app.Activity;
import android.app.Dialog;
import android.content.Context;
import android.content.Intent;
import android.os.Bundle;
import android.support.annotation.NonNull;
import android.support.annotation.Nullable;
import android.text.Html;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.MenuItem;
import android.view.View;
import android.view.ViewGroup;
import android.widget.AdapterView;
import android.widget.Button;
import android.widget.CheckBox;
import android.widget.CompoundButton;
import android.widget.LinearLayout;
import android.widget.ListView;
import android.widget.RadioButton;
import android.widget.RadioGroup;
import android.widget.TextView;

import com.shopify.buy3.Storefront;
import com.elietlux.shopifyapp.R;
import com.elietlux.shopifyapp.adaptersection.Sort_Adapter;
import com.elietlux.shopifyapp.datasection.Data;
import com.elietlux.shopifyapp.maincontainer.MainActivity;
import com.elietlux.shopifyapp.requestsection.ApiInterface;
import com.yahoo.mobile.client.android.util.rangeseekbar.RangeSeekBar;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.Iterator;
import java.util.Map;
import java.util.Objects;

import butterknife.BindView;
import butterknife.ButterKnife;

public class FilterActivity extends MainActivity {

    @Nullable
    @BindView(R.id.lienar_main_cat_filter)
    LinearLayout lienar_main_cat_filter;

    @Nullable
    @BindView(R.id.linear_sortSection)
    RadioGroup linear_sortSection;

    @Nullable
    @BindView(R.id.btn_apply)
    Button applyFilter;

    @Nullable
    @BindView(R.id.clear_filter)
    Button clear_filter;

    boolean reverse=false;
    int sort_key=0;

    HashMap<String, ArrayList<String>> selectedValueMap;
    HashMap<String, ArrayList<String>> selectedValueMapRange;



    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        ViewGroup content = findViewById(R.id.MageNative_frame_container);
        getLayoutInflater().inflate(R.layout.activity_filter2, content, true);
        ButterKnife.bind(FilterActivity.this);
        selectedValueMap = new HashMap<String, ArrayList<String>>();
        selectedValueMapRange = new HashMap<String, ArrayList<String>>();
        getSupportActionBar().setDisplayHomeAsUpEnabled(true);
        renderSort();
        if(!filterJson.toString().isEmpty()){
            Log.d("filterData",filterJson+"");
            try {
                JSONObject main = filterJson;
                JSONArray filters = main.getJSONObject("data").getJSONArray("filters");
                Log.d("filterarray",""+filters);

                View filersectionsingleitem=null;
                LinearLayout filteroptions;
                JSONObject innerjson;
                JSONArray values;

                TextView filtertitle;
                for (int i=0;i<filters.length();i++){
                    innerjson = filters.getJSONObject(i);
                    filersectionsingleitem = getLayoutInflater().inflate(R.layout.filersectionsingleitem,null);
                    filtertitle = (TextView) filersectionsingleitem.findViewById(R.id.filtertitlee);
                    filteroptions =(LinearLayout) filersectionsingleitem.findViewById(R.id.filteroptions);
                    filtertitle.setText(innerjson.getString("label"));
                    String id = innerjson.getString("id");


                    switch (innerjson.getString("style")){
                        case"Swatch - Text":
                        case "Checkbox" :
                            values = innerjson.getJSONArray("values");
                            filersectionsingleitem.setTag("Checkbox");
                            generateFilterOptionsCheckbox(filteroptions,values,id);

                            break;
                        case "Slider":
                            JSONObject values1 = innerjson.getJSONObject("values");
                            JSONObject ranges = innerjson.getJSONObject("ranges");
                            Log.d("ranges",""+values1);
                            filersectionsingleitem.setTag("Slider");
                            generateFilterOptionsSlider(filteroptions,values1,ranges,id);
                            break;
                    }

                    lienar_main_cat_filter.addView(filersectionsingleitem);
                }
            }catch (Exception e){
                e.printStackTrace();
            }
        }

        clear_filter.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                setResult(2);
                finish();
            }
        });


        applyFilter.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                String filterString="";
                Iterator it = selectedValueMap.entrySet().iterator();
                while (it.hasNext()) {
                    Map.Entry pair = (Map.Entry)it.next();

                    ArrayList<String> arrayList = (ArrayList<String>) pair.getValue();
                    for (int i = 0;i<arrayList.size();i++){
                        filterString+="&"+pair.getKey() + "=" + arrayList.get(i).replace("&","%26");
                    }

                  //  filterString+="&"+pair.getKey() + "=" + pair.getValue().toString().replace("[","").replace("]","");

                    it.remove(); // avoids a ConcurrentModificationException
                }

                Iterator itt = selectedValueMapRange.entrySet().iterator();
                while (itt.hasNext()) {
                    Map.Entry pair = (Map.Entry)itt.next();
                    filterString+="&"+pair.getKey() + "=" + pair.getValue().toString().replace(",",":");
                    itt.remove(); // avoids a ConcurrentModificationException
                }

                Log.d("filterString",""+filterString);

                Intent returnIntent = new Intent();
                returnIntent.putExtra("filterString",filterString);
                returnIntent.putExtra("sort_key",sort_key);
                returnIntent.putExtra("reverse",reverse);
                setResult(Activity.RESULT_OK,returnIntent);

                finish();


            }
        });

    }

    private void generateFilterOptionsSlider(LinearLayout filteroptions, JSONObject values1, JSONObject ranges, String id) {
        try {
            View seekbarlayout;
            RangeSeekBar seekBar;
            TextView minimunValue;
            TextView maximumValue;
            JSONObject jsonObject;
                seekbarlayout = getLayoutInflater().inflate(R.layout.seekbar,null);
                seekBar = seekbarlayout.findViewById(R.id.seek);
                minimunValue = seekbarlayout.findViewById(R.id.minvlaue);
                maximumValue = seekbarlayout.findViewById(R.id.maxvlaue);

                /************************************** Setting Range Of SeekBar***************************************************/
                Log.d("Seekbar","----"+ranges);
                int min = ranges.getInt("min");
                int max = ranges.getInt("max");
                Log.d("Seekbar-Ranges",min+"----"+max);
                minimunValue.setText(min+"");
                maximumValue.setText(max+"");
                seekBar.setSelectedMinValue(min);
                seekBar.setSelectedMaxValue(max);
                minimunValue.setText(min+"");
                maximumValue.setText(max+"");


            /************************************** Setting Range Of SeekBar***************************************************/

            /************************************** Setting Selected Value Of SeekBar***************************************************/
            Log.d("Seekbar","----"+values1);
            int selectedmin = values1.getInt("min");
            int selectedmax = values1.getInt("max");
            Log.d("Seekbar-Values",min+"----"+max);
            seekBar.setRangeValues(min,max);
            /************************************** Setting Selected Value Of SeekBar***************************************************/
            String hashid = "filter["+id+"][]";
            ArrayList<String> list = new ArrayList<>();
            list.add(selectedmin+"");
            list.add(selectedmax+"");
           // selectedValueMapRange.put(hashid,list);

                seekBar.setOnRangeSeekBarChangeListener(new RangeSeekBar.OnRangeSeekBarChangeListener() {
                    @Override
                    public void onRangeSeekBarValuesChanged(RangeSeekBar bar, Object minValue, Object maxValue) {
                        minimunValue.setText(minValue.toString());
                        maximumValue.setText(maxValue.toString());

                        ArrayList<String> list =new ArrayList<>();
                        list.add(minimunValue.getText().toString());
                        list.add(maximumValue.getText().toString());
                        selectedValueMapRange.put(hashid,list);

                        Log.d("selectedValueMap",selectedValueMap+"");

                    }
                });

                seekbarlayout.setTag("Seekbar");
                filteroptions.addView(seekbarlayout);

        }
        catch (Exception e){
            e.printStackTrace();
        }
    }

    private void generateFilterOptionsCheckbox(LinearLayout filteroptions, JSONArray values, String id) {
       try {
           View checkboxlayout;
           CheckBox checkBox;
           JSONObject jsonObject;
           String hashid = "filter["+id+"][]";

           for (int i =0;i<values.length();i++){
               jsonObject = values.getJSONObject(i);
               checkboxlayout = getLayoutInflater().inflate(R.layout.checkoxview,null);
               checkBox = (CheckBox) checkboxlayout.findViewById(R.id.checkbox);
               checkBox.setText(jsonObject.getString("label"));
               checkBox.setTag(jsonObject.getString("value"));
               CheckBox finalCheckBox = checkBox;
               JSONObject finalJsonObject = jsonObject;

               /********************************************** Selection Case ****************************************************************/
               Log.d("selected",finalJsonObject.getBoolean("selected")+"");
               if(finalJsonObject.getBoolean("selected")){
                   ArrayList<String> list = new ArrayList<String>();
                   list.add(finalJsonObject.getString("value").toString()); //.replace(" ","")
                   selectedValueMap.put(hashid,list);
                   checkBox.setChecked(true);
               }
               /********************************************** Selection Case ****************************************************************/

               try {
                   checkBox.setOnCheckedChangeListener(new CompoundButton.OnCheckedChangeListener() {
                       @Override
                       public void onCheckedChanged(CompoundButton compoundButton, boolean b) {
                           if (b){
                               if (selectedValueMap.containsKey(hashid)){
                                   ArrayList<String> list = selectedValueMap.get(hashid);
                                   try {
                                       list.add(finalJsonObject.getString("value"));
                                   } catch (JSONException e) {
                                       e.printStackTrace();
                                   }
                                   selectedValueMap.put(hashid,list);
                                   Log.d("selectedValueMap",""+selectedValueMap);
                                   list =null;
                               }
                               else {
                                   ArrayList<String> list = new ArrayList<String>();
                                   try {
                                       list.add(finalJsonObject.getString("value"));
                                   } catch (JSONException e) {
                                       e.printStackTrace();
                                   }
                                   selectedValueMap.put(hashid, list);
                                   Log.d("selectedValueMap",""+selectedValueMap);
                                   list =null;
                               }
                           }else {
                               ArrayList<String> list = selectedValueMap.get(hashid);
                               try {
                                   if (list.contains(finalJsonObject.getString("value")))
                                   list.remove(finalJsonObject.getString("value"));
                               } catch (JSONException e) {
                                   e.printStackTrace();
                               }
                               if (list.size()==0){
                                   selectedValueMap.remove(hashid);
                               }
                               list =null;

                           }
                       }
                   });

               }
               catch (Exception e){
                   e.printStackTrace();
               }
               checkboxlayout.setTag("CheckBox");
               filteroptions.addView(checkboxlayout);
           }
       }
       catch (Exception e){
           e.printStackTrace();
       }
    }

    private void renderSort() {
        try {
            JSONArray sortOptions = Data.getSortOptions(FilterActivity.this);

            View radioButton = null;
            RadioButton sortRadioButton = null;
            for (int i=0;i<sortOptions.length();i++){
                radioButton = getLayoutInflater().inflate(R.layout.radiobutton,null);
                sortRadioButton = (RadioButton) radioButton.findViewById(R.id.sortRadioButton);
                sortRadioButton.setText(sortOptions.getString(i));
                Log.d("sortOptions",i+" -- "+sortOptions.getString(i));
                Log.d("sortOptions",i+" -- "+sortOptions.length());
                sortRadioButton.setId(i);
                if (i==0){
                    sortRadioButton.setSelected(true);
                }

                RadioButton finalSortRadioButton = sortRadioButton;
                sortRadioButton.setOnClickListener(new View.OnClickListener() {
                    @Override
                    public void onClick(View v) {
                        switch (finalSortRadioButton.getId())
                        {
                            case 0:
                                sort_key = 0;
                                reverse = false;
                                break;

                            case 1:
                                sort_key = 1;
                                reverse = true;
                                break;

                            case 2:
                                sort_key = 2;
                                reverse = true;
                                break;

                            case 3:
                                sort_key = 3;
                                reverse = false;
                                break;

                            case 4:
                                sort_key = 4;
                                reverse = false;
                                break;

                            case 5:
                                sort_key = 5;
                                reverse = false;
                                break;
                        }
                    }
                });

                linear_sortSection.addView(sortRadioButton);
            }

           /* final Dialog listDialog = new Dialog(this, R.style.PauseDialog);
            ((ViewGroup) ((ViewGroup) Objects.requireNonNull(listDialog.getWindow()).getDecorView()).getChildAt(0))
                    .getChildAt(1)
                    .setBackgroundColor(getResources().getColor(R.color.black));
            listDialog.setTitle(Html.fromHtml("<center><font color='#ffffff'>" + getResources().getString(R.string.sortbyitems) + "</font></center>"));
            LayoutInflater li = (LayoutInflater) this.getSystemService(Context.LAYOUT_INFLATER_SERVICE);
            @SuppressLint("InflateParams") View v = Objects.requireNonNull(li).inflate(R.layout.magenative_sortitemlist, null, false);
            listDialog.setContentView(v);
            listDialog.setCancelable(true);
            ListView list1 = listDialog.findViewById(R.id.MageNative_sortlistview);
          *//*  list1.setOnItemClickListener(new AdapterView.OnItemClickListener()
            {
                @Override
                public void onItemClick(AdapterView<?> adapterView, @NonNull View view, int i, long l)
                {
                    listDialog.cancel();
                    listDialog.dismiss();
                    sortvalue=i;
                    switch (i)
                    {
                        case 0:
                            sort_key = Storefront.ProductCollectionSortKeys.TITLE;
                            reverse = false;
                            break;

                        case 1:
                            sort_key = Storefront.ProductCollectionSortKeys.TITLE;
                            reverse = true;
                            break;

                        case 2:
                            sort_key = Storefront.ProductCollectionSortKeys.PRICE;
                            reverse = true;
                            break;

                        case 3:
                            sort_key = Storefront.ProductCollectionSortKeys.PRICE;
                            reverse = false;
                            break;

                        case 4:
                            sort_key = Storefront.ProductCollectionSortKeys.BEST_SELLING;
                            reverse = false;
                            break;

                        case 5:
                            sort_key = Storefront.ProductCollectionSortKeys.CREATED;
                            reverse = false;
                            break;
                    }
                    cursor="nocursor";
                    hasNextPage=false;
                    productedge=null;
                    getProducts(cat_id,cursor,sort_key,reverse,"firsttime");

                }
            });*//*
            Sort_Adapter adapter = new Sort_Adapter(this, Data.getSortOptions(ProductListing.this), sortvalue);
            list1.setAdapter(adapter);
            listDialog.show();*/
        }
        catch (Exception e)
        {
            e.printStackTrace();
        }

    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        switch (item.getItemId()) {
            case android.R.id.home:
                onBackPressed();
                return true;
        }
        return super.onOptionsItemSelected(item);
    }
}
