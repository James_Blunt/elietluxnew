/*
 * Copyright/**
 *          * CedCommerce
 *           *
 *           * NOTICE OF LICENSE
 *           *
 *           * This source file is subject to the End User License Agreement (EULA)
 *           * that is bundled with this package in the file LICENSE.txt.
 *           * It is also available through the world-wide-web at this URL:
 *           * http://cedcommerce.com/license-agreement.txt
 *           *
 *           * @category  Ced
 *           * @package   MageNative
 *           * @author    CedCommerce Core Team <connect@cedcommerce.com >
 *           * @copyright Copyright CEDCOMMERCE (http://cedcommerce.com/)
 *           * @license      http://cedcommerce.com/license-agreement.txt
 *
 */
package com.elietlux.shopifyapp.searchsection;
import android.content.Intent;
import android.os.Bundle;
import android.os.Handler;
import android.support.annotation.NonNull;
import android.support.annotation.Nullable;
import android.text.Editable;
import android.text.TextWatcher;
import android.util.Log;
import android.view.KeyEvent;
import android.view.Menu;
import android.view.View;
import android.view.ViewGroup;
import android.view.inputmethod.EditorInfo;
import android.widget.AdapterView;
import android.widget.AutoCompleteTextView;
import android.widget.TextView;
import android.widget.Toast;

import com.blogspot.atifsoftwares.animatoolib.Animatoo;
import com.elietlux.shopifyapp.storagesection.LocalData;
import com.shopify.buy3.GraphCallResult;
import com.shopify.buy3.GraphClient;
import com.shopify.buy3.GraphResponse;
import com.shopify.buy3.QueryGraphCall;
import com.shopify.buy3.Storefront;
import com.elietlux.shopifyapp.R;
import com.elietlux.shopifyapp.homesection.SearchCategories;
import com.elietlux.shopifyapp.maincontainer.MainActivity;
import com.elietlux.shopifyapp.productviewsection.ProductView;
import com.elietlux.shopifyapp.requestsection.ApiClient;
import com.elietlux.shopifyapp.storefrontqueries.Query;
import com.elietlux.shopifyapp.storefrontresponse.AsyncResponse;
import com.elietlux.shopifyapp.storefrontresponse.Response;

import java.io.Serializable;
import java.util.ArrayList;
import java.util.Collections;
import java.util.HashMap;
import java.util.Iterator;
import java.util.List;
import java.util.Objects;

import butterknife.BindView;
import butterknife.ButterKnife;
public class AutoSearch extends MainActivity
{
    GraphClient client;
    private ArrayList<String> suggestions;
    private HashMap<String,String> name_id;
    String cursor="nocursor";
    @Nullable @BindView(R.id.search) AutoCompleteTextView search;
    @Nullable @BindView(R.id.category) TextView category;
    public  List<Storefront.ProductEdge> productedge=null;
    LocalData data;
    @Override
    protected void onCreate(Bundle savedInstanceState)
    {
        super.onCreate(savedInstanceState);
        ViewGroup content = findViewById(R.id.MageNative_frame_container);
        getLayoutInflater().inflate(R.layout.magenative_searchpage, content, true);
        ButterKnife.bind(AutoSearch.this);
        //showbackbutton();
        showTittle(getResources().getString(R.string.SearchYourProducts));
        suggestions=new ArrayList<String>();
        name_id = new HashMap<>();
        client= ApiClient.getGraphClient(AutoSearch.this,true);
        data = new LocalData(this);

        category.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                Intent in = new Intent(AutoSearch.this, SearchCategories.class);
                startActivity(in);
                overridePendingTransition(R.anim.magenative_slide_in, R.anim.magenative_slide_out);

            }
        });

        search.setOnEditorActionListener(new TextView.OnEditorActionListener()
        {
            @Override
            public boolean onEditorAction(TextView v, int actionId, KeyEvent event)
            {
                if (actionId == EditorInfo.IME_ACTION_SEARCH)
                {
                    Intent intent = new Intent(getApplicationContext(), SearchView.class);
                    intent.putExtra("cat_id", search.getText().toString());
                    intent.addFlags(Intent.FLAG_ACTIVITY_CLEAR_TOP);
                    startActivity(intent);
                    overridePendingTransition(R.anim.magenative_slide_in, R.anim.magenative_slide_out);
                    return true;
                }
                return false;
            }
        });
        search.addTextChangedListener(new TextWatcher()
        {
            @Override
            public void onTextChanged(@NonNull final CharSequence s, int start, int before, int count)
            {
                Log.i("Keyword",""+s.toString());
                if (s.length() >= 4) {
                    searchResult(s.toString());
                }
                popuplate();
            }
            @Override
            public void beforeTextChanged(CharSequence s, int start, int count, int after) {

            }
            @Override
            public void afterTextChanged(Editable s) {

            }
        });

    }

    private void searchResult(String keyword)
    {
        try
        {
            ArrayList<Storefront.CurrencyCode> currencyCodeArrayList = new ArrayList<>();
            Log.d("currencyCodeArrayList",""+data.getCurrencyCode());
            currencyCodeArrayList.add(Storefront.CurrencyCode.valueOf(data.getCurrencyCode()));
            Log.d("currencyCodeArrayList",""+currencyCodeArrayList);

            QueryGraphCall call = client.queryGraph(Query.getAutoSearchProducts(cursor,keyword, Storefront.ProductSortKeys.CREATED_AT,false,currencyCodeArrayList));
            Response.getGraphQLResponse(call,new AsyncResponse()
            {
                @Override
                public void finalOutput(@NonNull Object output,@NonNull boolean error )
                {
                    if(error)
                    {
                        GraphResponse<Storefront.QueryRoot> response  = ((GraphCallResult.Success<Storefront.QueryRoot>) output).getResponse();
                        runOnUiThread(new Runnable()
                        {
                            @Override
                            public void run()
                            {
                                attachDataToTextview(response);
                            }
                        });
                    }
                    else
                    {
                        Log.i("ResponseError",""+output.toString());
                    }
                }
            },AutoSearch.this);
        }
        catch (Exception e)
        {
            e.printStackTrace();
        }
    }

    private void attachDataToTextview(GraphResponse<Storefront.QueryRoot> response)
    {
        try
        {
            List<Storefront.ProductEdge> data =response.getData().getShop().getProducts().getEdges();
            if(data.size()>0)
            {
                if(productedge==null)
                {
                    productedge=data;
                }
                else
                {
                    productedge.addAll(data);
                }
                Iterator<Storefront.ProductEdge> iterator=productedge.iterator();
                Storefront.ProductEdge edge=null;
                while (iterator.hasNext())
                {
                    edge=iterator.next();
                    String finalstring=edge.getNode().getTitle() + "#" + edge.getNode().getImages().getEdges().get(0).getNode().getTransformedSrc()+"#"+edge.getNode().getId();
                    if (suggestions.size() > 0)
                    {
                        if (!(suggestions.contains(finalstring)))
                        {
                            suggestions.add(finalstring);
                        }
                    }
                    else
                    {
                        suggestions.add(finalstring);
                    }
                }
            }
            if(suggestions.size()>0)
            {
                popuplate();
            }
            else
            {
                Toast.makeText(AutoSearch.this,getResources().getString(R.string.noresult),Toast.LENGTH_LONG).show();
            }

        }
        catch (Exception e)
        {
            e.printStackTrace();
        }
    }

    private void popuplate()
    {
        try
        {
            if(suggestions.size()>0)
            {
                Collections.reverse(suggestions);
                final CustomListAdapter adapter = new CustomListAdapter(getApplicationContext(), R.layout.search, suggestions);
                search.setOnItemClickListener(new AdapterView.OnItemClickListener()
                {
                    @Override
                    public void onItemClick(@NonNull AdapterView<?> parent, View view, int position, long id)
                    {
                        TextView  name=view.findViewById(R.id.textView);
                        TextView  product_id=view.findViewById(R.id.product_id);
                        String parts[] = parent.getItemAtPosition(position).toString().split("#");
                        search.setText(name.getText().toString());
                        Intent intent = new Intent(getApplicationContext(), ProductView.class);
                        intent.putExtra("id", product_id.getText().toString());
                        //intent.putExtra("object",(Serializable)productedge.get(position));
                        intent.addFlags(Intent.FLAG_ACTIVITY_CLEAR_TOP);
                        startActivity(intent);
                        Animatoo.animateZoom(AutoSearch.this);
                      //  overridePendingTransition(R.anim.magenative_slide_in, R.anim.magenative_slide_out);
                    }
                });
                search.setThreshold(1);
                search.setAdapter(adapter);
                search.showDropDown();
                search.setSelection(search.getText().length());
            }

        }
        catch (Exception e) {
            e.printStackTrace();
        }
    }

    @Override
    public boolean onCreateOptionsMenu(@NonNull Menu menu) {
        return false;
    }
}
