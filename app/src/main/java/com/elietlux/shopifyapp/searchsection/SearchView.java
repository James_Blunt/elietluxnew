package com.elietlux.shopifyapp.searchsection;

import android.annotation.SuppressLint;
import android.app.Dialog;
import android.content.Context;
import android.content.Intent;
import android.os.Bundle;
import android.support.annotation.NonNull;
import android.support.annotation.Nullable;
import android.text.Html;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.Menu;
import android.view.View;
import android.view.ViewGroup;
import android.widget.AbsListView;
import android.widget.AdapterView;
import android.widget.GridView;
import android.widget.LinearLayout;
import android.widget.ListView;
import android.widget.RelativeLayout;
import android.widget.TextView;
import android.widget.Toast;

import com.blogspot.atifsoftwares.animatoolib.Animatoo;
import com.shopify.buy3.GraphCallResult;
import com.shopify.buy3.GraphClient;
import com.shopify.buy3.GraphResponse;
import com.shopify.buy3.QueryGraphCall;
import com.shopify.buy3.Storefront;
import com.elietlux.shopifyapp.R;
import com.elietlux.shopifyapp.adaptersection.Product_Adapter;
import com.elietlux.shopifyapp.adaptersection.Sort_Adapter;
import com.elietlux.shopifyapp.datasection.Data;
import com.elietlux.shopifyapp.maincontainer.MainActivity;
import com.elietlux.shopifyapp.productlistingsection.ProductListing;
import com.elietlux.shopifyapp.productviewsection.ProductView;
import com.elietlux.shopifyapp.requestsection.ApiClient;
import com.elietlux.shopifyapp.storagesection.LocalData;
import com.elietlux.shopifyapp.storefrontqueries.Query;
import com.elietlux.shopifyapp.storefrontresponse.AsyncResponse;
import com.elietlux.shopifyapp.storefrontresponse.Response;

import java.io.Serializable;
import java.util.ArrayList;
import java.util.List;
import java.util.Objects;

import butterknife.BindView;
import butterknife.ButterKnife;
import io.reactivex.Observable;
import io.reactivex.SingleObserver;
import io.reactivex.android.schedulers.AndroidSchedulers;
import io.reactivex.disposables.Disposable;
import io.reactivex.schedulers.Schedulers;

/**
 * Created by cedcoss on 27/4/18.
 */
public class SearchView extends MainActivity
{
    @Nullable @BindView(R.id.grid)  GridView grid;
    @Nullable @BindView(R.id.MageNative_main)  RelativeLayout MageNative_main = null;
    @Nullable @BindView(R.id.MageNative_sortsection)  RelativeLayout MageNative_sortsection = null;
    @Nullable @BindView(R.id.MageNative_sortingsection) LinearLayout MageNative_sortingsection = null;
    GraphClient client=null;
    String cursor="nocursor";
    boolean hasNextPage=false;
    boolean reverse=false;
    Storefront.ProductSortKeys sort_key=Storefront.ProductSortKeys.CREATED_AT;
    int sortvalue=0;
    String cat_id="";
    LocalData localData=null;
    String currency_symbol="";
    public static  List<Storefront.ProductEdge> productedge=null;
    @Override
    protected void onCreate(Bundle savedInstanceState)
    {
        try
        {
            super.onCreate(savedInstanceState);
            ViewGroup content = findViewById(R.id.MageNative_frame_container);
            getLayoutInflater().inflate(R.layout.magenative_gridview, content, true);
            localData=new LocalData(SearchView.this);
            if(localData.getMoneyFormat()!=null)
            {
                currency_symbol=localData.getMoneyFormat();
            }
            productedge=new ArrayList<Storefront.ProductEdge>();
            ButterKnife.bind(SearchView.this);
            //showbackbutton();
            MageNative_sortingsection.setVisibility(View.GONE);
            cat_id=getIntent().getStringExtra("cat_id");
            Log.i("category_id",""+cat_id);
            String Tittle=getResources().getString(R.string.search)+" "+cat_id;
            showTittle(Tittle);
            client= ApiClient.getGraphClient(SearchView.this,true);
            grid.setOnScrollListener(new AbsListView.OnScrollListener()
            {
                @Override
                public void onScrollStateChanged(AbsListView absListView, int i) {

                }

                @Override
                public void onScroll(AbsListView absListView, int firstVisibleItem, int visibleItemCount, int totalItemCount)
                {
                    if ((firstVisibleItem + visibleItemCount) != 0) {
                        if (((firstVisibleItem + visibleItemCount) == totalItemCount) && hasNextPage)
                        {
                            hasNextPage = false;
                            getProducts(cat_id,cursor,sort_key,reverse,"scroll");
                        }
                    }
                }
            });
            grid.setOnItemClickListener(new AdapterView.OnItemClickListener()
            {
                @Override
                public void onItemClick(AdapterView<?> adapterView, View view, int i, long l)
                {
                    TextView  id=view.findViewById(R.id.product_id);
                    Intent intent=new Intent(SearchView.this, ProductView.class);
                    intent.putExtra("id",id.getText().toString());
                    intent.putExtra("object",(Serializable)productedge.get(i));
                    startActivity(intent);
                    overridePendingTransition(R.anim.magenative_slide_in, R.anim.magenative_slide_out);
                }
            });
            getProducts(cat_id,cursor,sort_key,reverse,"firsttime");
        }
        catch (Exception e)
        {
            e.printStackTrace();
        }
    }
    public void getProducts(String cat_id,String cursor,Storefront.ProductSortKeys keys,boolean reverse,String origin)
    {
        try
        {
            Log.i("Cursor",""+cursor);
            ArrayList<Storefront.CurrencyCode> currencyCodeArrayList = new ArrayList<>();
            Log.d("currencyCodeArrayList",""+localData.getCurrencyCode());
            currencyCodeArrayList.add(Storefront.CurrencyCode.valueOf(localData.getCurrencyCode()));
            Log.d("currencyCodeArrayList",""+currencyCodeArrayList);

            QueryGraphCall call = client.queryGraph(Query.getAutoSearchProducts(cursor,cat_id,keys,reverse,currencyCodeArrayList));
            Response.getGraphQLResponse(call,new AsyncResponse()
            {
                @Override
                public void finalOutput(@NonNull Object output,@NonNull boolean error )
                {
                    if(error)
                    {
                        GraphResponse<Storefront.QueryRoot> response  = ((GraphCallResult.Success<Storefront.QueryRoot>) output).getResponse();
                        runOnUiThread(new Runnable()
                        {
                            @Override
                            public void run()
                            {
                                processProducts(response,origin);
                            }
                        });
                    }
                    else
                    {
                        Log.i("ResponseError",""+output.toString());
                        hasNextPage=false;
                    }
                }
            },SearchView.this);
        }
        catch (Exception e)
        {
            e.printStackTrace();
        }
    }
    private void processProducts(GraphResponse<Storefront.QueryRoot> response,String origin)
    {
        try
        {

            hasNextPage=response.getData().getShop().getProducts().getPageInfo().getHasNextPage();
            Log.i("hasNextPage", "" + hasNextPage);
            List<Storefront.ProductEdge> data = response.getData().getShop().getProducts().getEdges();
            if(data.size()>0)
            {
                if(productedge==null)
                {
                    productedge=data;
                }
                else
                {
                    productedge.addAll(data);
                }
                cursor=productedge.get(productedge.size()-1).getCursor();
                onResume();
            }
            else
            {
                if (origin.equals("firsttime"))
                {
                    Toast.makeText(SearchView.this, getResources().getString(R.string.noproducts), Toast.LENGTH_LONG).show();
                    finish();
                }
            }
        }
        catch (Exception e)
        {
            e.printStackTrace();
        }
    }
    @Override
    public void onResume()
    {
        try
        {
            if(productedge!=null)
            {
                Product_Adapter adapter = new Product_Adapter(SearchView.this, productedge,currency_symbol,grid);
                int cp = grid.getFirstVisiblePosition();
                grid.setAdapter(adapter);
                grid.setSelection(cp + 1);
                adapter.notifyDataSetChanged();
            }
        }
        catch (Exception e)
        {
            e.printStackTrace();
        }
        super.onResume();
    }
    //@Override
    //public void onBackPressed()
    //{
    // super.onBackPressed();
    //}

    @Override
    public boolean onCreateOptionsMenu(@NonNull Menu menu) {
        return false;
    }
}