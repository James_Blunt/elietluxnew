package com.elietlux.shopifyapp.requestsection;
import okhttp3.ResponseBody;
import retrofit2.Call;
import retrofit2.http.FormUrlEncoded;
import retrofit2.http.GET;
import retrofit2.http.Query;
import retrofit2.http.Url;

public interface ApiInterface
{
    @GET("getstatus")
    Call<ResponseBody> getStatus(@Query("mid") String mid, @Query("device_type") String device_type);
    @GET("getcategorymenus")
    Call<ResponseBody> getCategoryMenus(@Query("mid") String mid,@Query("handle") String handle);
    @GET("appdata")
    Call<ResponseBody> getAppdata(@Query("mid") String mid);
    @GET("homepagedata")
    Call<ResponseBody> getHomePage(@Query("mid") String mid);
    @GET("setorder")
    Call<ResponseBody> setOrder(@Query("mid") String mid,@Query("checkout_token") String checkout_token);
    @GET("setdevices")
    Call<ResponseBody> setDevices(@Query("mid") String mid,@Query("device_id") String device_id,@Query("email") String email,@Query("type") String type,@Query("unique_id") String unique_id);
    @GET("globosmartproductfilterapi/installedstatus")
    Call<ResponseBody> getFilterInstalledStatus(@Query("mid")String string);

    @GET
    Call<ResponseBody> getFilters( @Url String url);

    @GET("sociologincustomer")
    Call<ResponseBody> getuserLogin(@Query("mid") String mid, @Query("email") String email);
    @GET("getcollectionproperties")
    Call<ResponseBody> getcollectionproperties(@Query("mid") String mid, @Query("prop") String prop);
    @GET
    Call<ResponseBody> getCountry(@Url String url);
    @GET("getcurrencyrates")
    Call<ResponseBody> getcurrencyrates(@Query("shop") String shop,@Query("code") String code);

    /*@GET("details")
    Call<ResponseBody> details(@Query("shop") String shop, @Query("product_id") String product_id,@Query("tags") String tags,@Query("vendor") String vendor,@Query("collections") String collections);*/

    @GET
    Call<ResponseBody> details(@Url String url);

}
