package com.elietlux.shopifyapp.storefrontqueries;
import com.elietlux.shopifyapp.orderssection.OrderListing;
import com.shopify.buy3.Storefront;
import com.shopify.graphql.support.ID;

import java.util.ArrayList;
import java.util.List;

import timber.log.Timber;
public  class Query
{
    public static Storefront.QueryRootQuery getCollections(String cursor)
    {
        Storefront.QueryRootQuery.CollectionsArgumentsDefinition definition;
        if(cursor.equals("nocursor"))
        {
            definition=args -> args.first(50);
        }
        else
        {
            definition=args -> args.first(50).after(cursor);
        }

        return Storefront.query(root->root
                .collections(definition, collect->collect.
                        edges(edge->edge.cursor()
                                .node(node->node
                                        .title()
                                        .image(Storefront.ImageQuery::originalSrc)))
                        .pageInfo(Storefront.PageInfoQuery::hasNextPage))
        );
    }
    private static Storefront.ProductConnectionQueryDefinition getProductDefinition(ArrayList<Storefront.CurrencyCode> currencyCodeArrayList)
    {

        Storefront.ProductVariantQuery.PresentmentPricesArgumentsDefinition definition;

        return productdata -> productdata
                .edges(edges -> edges
                        .cursor()
                        .node(node -> node
                                .title()
                                .handle()
                                .vendor()
                                .tags()
                                .collections(getconn->getconn.first(100),conn -> conn
                                        .edges(edgeconn -> edgeconn
                                                .node(nodeconn -> nodeconn
                                                        .title()
                                                )
                                        )
                                )
                                .images(img->img.first(10),imag->imag.
                                        edges(imgedge->imgedge
                                                .node(imgnode->imgnode
                                                        .originalSrc()
                                                        .transformedSrc( /*t->t
                                                                .maxWidth(600)
                                                                .maxHeight(600)*/
                                                        )
                                                )
                                        )
                                )
                                .availableForSale()
                                .descriptionHtml()
                                .description()
                                .variants(args -> args
                                        .first(120), variant -> variant
                                        .edges(variantEdgeQuery -> variantEdgeQuery
                                                .node(productVariantQuery -> productVariantQuery
                                                        .priceV2(price -> price.amount().currencyCode())
                                                        .presentmentPrices( arg -> arg.first(25).presentmentCurrencies(currencyCodeArrayList) , price -> price.edges( e -> e.cursor().node(n -> n.price( p -> p.amount().currencyCode() ).compareAtPrice( cp -> cp.amount().currencyCode() ) ) ) )
                                                        .price()
                                                        .title()
                                                        .selectedOptions(select->select.name().value())
                                                        .compareAtPriceV2(compare->compare.amount().currencyCode())
                                                        .compareAtPrice()
                                                        .image(Storefront.ImageQuery::originalSrc)
                                                        .availableForSale()
                                                )
                                        )
                                )
                                .onlineStoreUrl()
                                .options(op->op.
                                        name()
                                        .values()
                                ))
                )
                .pageInfo(Storefront.PageInfoQuery::hasNextPage
                );
    }
    public static Storefront.QueryRootQuery getProducts(String cat_id, String cursor,Storefront.ProductCollectionSortKeys sortby_key,boolean direction,int number, ArrayList<Storefront.CurrencyCode> currencyCodeArrayList)
    {
        Storefront.CollectionQuery.ProductsArgumentsDefinition definition;
        if(cursor.equals("nocursor"))
        {
            definition=args -> args.first(number).sortKey(sortby_key).reverse(direction);
        }
        else
        {
            definition=args -> args.first(number).after(cursor).sortKey(sortby_key).reverse(direction);
        }
        Storefront.QueryRootQuery productsquery=null;
        if(cat_id.contains("*#*"))
        {
            Timber.tag("Inhandle").i("1");

            productsquery=Storefront.query(root->root
                    .collectionByHandle(cat_id.replace("*#*","").trim(),collect->collect
                            .products(definition,getProductDefinition(currencyCodeArrayList))))
                    .shop(shop->shop.collectionByHandle(cat_id.replace("*#*","").trim(),collect->collect
                            .products(definition,getProductDefinition(currencyCodeArrayList))));
        }
        else
        {
            productsquery = Storefront.query(root ->root
                    .node(new ID(cat_id),
                            rootnode -> rootnode.
                                    onCollection(oncollection -> oncollection
                                            .image(image->image
                                                    .originalSrc()
                                                    .transformedSrc(/*tr->tr
                                                            .maxHeight(300)
                                                            .maxWidth(700)*/
                                                    )
                                            )
                                            .products(definition, getProductDefinition(currencyCodeArrayList)
                                            )
                                    )
                    )
            );
        }
        return productsquery;
    }
    public static Storefront.QueryRootQuery getProductListing(String cat_id, String cursor, Storefront.ProductCollectionSortKeys sortby_key, boolean direction, ArrayList<Storefront.CurrencyCode> currencyCodeArrayList)
    {
        Storefront.CollectionQuery.ProductsArgumentsDefinition definition;
        if(cursor.equals("nocursor"))
        {
            definition=args -> args.first(50).sortKey(sortby_key).reverse(direction);
        }
        else
        {
            definition=args -> args.first(50).after(cursor).sortKey(sortby_key).reverse(direction);
        }
        Storefront.QueryRootQuery productsquery=null;
        if(cat_id.contains("*#*"))
        {
            Timber.tag("Inhandle").i("1");

            productsquery=Storefront.query(root->root
                    .collectionByHandle(cat_id.replace("*#*","").trim(),collect->collect
                            .products(definition,getProductDefinition(currencyCodeArrayList))))
                    .shop(shop->shop.collectionByHandle(cat_id.replace("*#*","").trim(),collect->collect
                            .products(definition,getProductDefinition(currencyCodeArrayList))));
        }
        else
        {
            productsquery = Storefront.query(root ->root
                    .node(new ID(cat_id),
                            rootnode -> rootnode.
                                    onCollection(oncollection -> oncollection
                                            .image(image->image
                                                    .originalSrc()
                                                    .transformedSrc()
                                            )
                                            .products(definition, getProductDefinition(currencyCodeArrayList)
                                            )
                                    )
                    )
            );
        }
        return productsquery;
    }
    public static Storefront.QueryRootQuery getRelatedproducts(String cat_id, ArrayList<Storefront.CurrencyCode> currencyCodeArrayList)
    {
        Storefront.CollectionQuery.ProductsArgumentsDefinition definition=args -> args.first(30);
        Storefront.QueryRootQuery productsquery= Storefront.query(root ->root
                .node(new ID(cat_id),
                        rootnode -> rootnode.
                                onCollection(oncollection -> oncollection
                                        .image(image->image
                                                .originalSrc()
                                                .transformedSrc()
                                        )
                                        .products(definition, getProductDefinition(currencyCodeArrayList)
                                        )
                                )
                )
        );

        return productsquery;
    }
    public static Storefront.QueryRootQuery getShopDetails()
    {

        return Storefront.query(root->root
                .shop(shop->shop
                        .moneyFormat()
                        .paymentSettings(pay->pay.currencyCode()
                        .enabledPresentmentCurrencies().currencyCode())
                        .privacyPolicy(privacy->privacy
                                .body()
                                .title()
                                .url())
                        .refundPolicy(refund->refund
                                .body()
                                .url()
                                .title())
                        .termsOfService(term->term
                                .title()
                                .url()
                                .body())
                        .articles(article->article
                                .edges(edge->edge
                                .node(node->node
                                        .title()
                                        .url()))
                        )
                )
        );
    }
    public  static Storefront.QueryRootQuery getSingleProduct(String product_id)
    {
        Storefront.QueryRootQuery productquery=null;
        if(product_id.contains("*#*"))
        {
            productquery=Storefront.query(root->root
                    .productByHandle(product_id.replace("*#*","").trim(),getProductQuery()));
        }
        else
        {
            productquery = Storefront.query(root ->root
                    .node(new ID(product_id),
                            rootnode -> rootnode.
                                    onProduct(getProductQuery()
                                    )));
        }
        return productquery;
    }
    private static Storefront.ProductQueryDefinition getProductQuery()
    {

        return product->product
                .title()
                .handle()
                .vendor()
                .tags()
                .collections(getconn->getconn.first(100),conn -> conn
                        .edges(edgeconn -> edgeconn
                                .node(nodeconn -> nodeconn
                                        .title()
                                )
                        )
                )
                .images(img->img.
                        first(10),imag->imag.
                        edges(imgedge->imgedge.
                                node(imgnode->imgnode.originalSrc()
                                        .transformedSrc(tr->tr.maxWidth(600).maxHeight(600))
                                )
                        )
                )
                .availableForSale()
                .descriptionHtml()
                .description()
                .variants(args -> args
                        .first(120), variant -> variant
                        .edges(variantEdgeQuery -> variantEdgeQuery
                                .node(productVariantQuery -> productVariantQuery
                                        .price()
                                        .title()
                                        .priceV2(p->p.amount().currencyCode())
                                        .presentmentPrices( arg -> arg.first(30) , price -> price.edges( e -> e.cursor().node(n -> n.price( p -> p.amount().currencyCode() ).compareAtPrice( cp -> cp.amount().currencyCode() ) ) ) )
                                        .selectedOptions(select->select.name().value())
                                        .compareAtPrice()
                                        .compareAtPriceV2(c->c.amount().currencyCode())
                                        .image(Storefront.ImageQuery::originalSrc)
                                        .availableForSale()
                                )
                        )
                )
                .onlineStoreUrl()
                .options(op->op.
                        name()
                        .values()
                );
    }
    public static Storefront.QueryRootQuery getCustomerDetails(String customeraccestoken)
    {

        return Storefront.query(root->root
                .customer(customeraccestoken,
                        customerQuery->customerQuery
                                .firstName()
                                .lastName()
                )
        );
    }
    public static Storefront.QueryRootQuery getAutoSearchProducts(String cursor, String keyword, Storefront.ProductSortKeys sortby_key,boolean direction, ArrayList<Storefront.CurrencyCode> currencyCodeArrayList)
    {
        Storefront.QueryRootQuery.ProductsArgumentsDefinition definition;
        if(cursor.equals("nocursor"))
        {
            definition=args -> args.query(keyword).first(50).sortKey(sortby_key).reverse(direction);
        }
        else
        {
            definition=args -> args.query(keyword).first(50).after(cursor).sortKey(sortby_key).reverse(direction);
        }
        Storefront.ShopQuery.ProductsArgumentsDefinition shoppro;
        if(cursor.equals("nocursor"))
        {
            shoppro=args -> args.query(keyword).first(50).sortKey(sortby_key).reverse(direction);
        }
        else
        {
            shoppro=args -> args.query(keyword).first(50).after(cursor).sortKey(sortby_key).reverse(direction);
        }
        return Storefront.query(root->root
                .shop(shop->shop
                        .products(shoppro,getProductDefinition(currencyCodeArrayList)
                        )
                )
                .products(definition,getProductDefinition(currencyCodeArrayList))
        );
    }
    public static Storefront.QueryRootQuery getAddressList(String accesstoken,String cursor)
    {
        Storefront.CustomerQuery.AddressesArgumentsDefinition definitions;
        if(cursor.equals("nocursor"))
            definitions = args -> args.first(10);
        else
            definitions = args -> args.first(10).after(cursor);

        return Storefront.query(root->root
                .customer(accesstoken,customer->customer
                        .addresses(definitions, address->address
                                .edges(edge->edge
                                        .cursor()
                                        .node(node->node
                                                .firstName()
                                                .lastName()
                                                .company()
                                                .address1()
                                                .address2()
                                                .city()
                                                .country()
                                                .province()
                                                .phone()
                                                .zip()
                                                .formattedArea()
                                        )
                                )
                                .pageInfo(Storefront.PageInfoQuery::hasNextPage))));
    }
    public static Storefront.QueryRootQuery getOrderList(String accesstoken,String cursor)
    {
        Storefront.CustomerQuery.OrdersArgumentsDefinition definition;
        if(cursor.equals("nocursor"))
        {
            definition=args->args.first(30).sortKey(Storefront.OrderSortKeys.ID).reverse(true);
        }
        else
        {
            definition=args->args.first(30).after(cursor).sortKey(Storefront.OrderSortKeys.ID).reverse(true);
        }

        return Storefront.query(root->root
                .customer(accesstoken,customer->customer
                        .orders(definition, order->order
                                .edges(edge->edge
                                        .cursor()
                                        .node(ordernode->ordernode
                                                .customerUrl()
                                                .statusUrl()
                                                .processedAt()
                                                .orderNumber()
                                                .lineItems(arg->arg.first(150),item->item
                                                        .edges(itemedge->itemedge.node(Storefront.OrderLineItemQuery::title))
                                                        .edges(itemedge->itemedge.node(p->p.title().quantity().variant(v->v.image(i->i.originalSrc())))))
                                                .totalPriceV2(tp->tp.amount().currencyCode())
                                                .totalPrice()
                                                .fulfillmentStatus()

                                        )
                                )
                                .pageInfo(Storefront.PageInfoQuery::hasNextPage
                                )
                        )
                )
        );
    }

    public static Storefront.QueryRootQuery getAllProducts( String cursor,Storefront.ProductSortKeys sortby_key,boolean direction,int number,ArrayList<Storefront.CurrencyCode> currencyCodeArrayList)
    {
        Storefront.ShopQuery.ProductsArgumentsDefinition definition;
        if(cursor.equals("nocursor"))
        {
            definition=args -> args.first(number).sortKey(sortby_key).reverse(direction);
        }
        else
        {
            definition=args -> args.first(number).after(cursor).sortKey(sortby_key).reverse(direction);
        }
        Storefront.QueryRootQuery.ProductsArgumentsDefinition shoppro;
        if(cursor.equals("nocursor"))
        {
            shoppro=args -> args.first(number).sortKey(sortby_key).reverse(direction);
        }
        else
        {
            shoppro=args -> args.first(number).after(cursor).sortKey(sortby_key).reverse(direction);
        }
        return Storefront.query(root ->root
                .shop(rootnode -> rootnode
                                .products(definition, getProductDefinition(currencyCodeArrayList)))
                .products(shoppro,getProductDefinition(currencyCodeArrayList))

        );
    }
    public static Storefront.QueryRootQuery getproductfromlist(List<ID> ids) {
        String cursor="nocursor";
        Storefront.QueryRootQuery.ProductsArgumentsDefinition shoppro;
        if(cursor.equals("nocursor"))
        {
            shoppro=args -> args.first(10);
        }
        else
        {
            shoppro=args -> args.first(10);
        }

        Storefront.CollectionQuery.ProductsArgumentsDefinition definition;
        definition=args -> args.first(20);
        return Storefront.query(root->root
                .nodes(ids, n-> n.onProduct(p->p.title()
                                .handle()
                                .vendor()
                                .tags()
                                .collections(getconn->getconn.first(100),conn -> conn
                                        .edges(edgeconn -> edgeconn
                                                .node(nodeconn -> nodeconn
                                                        .title()
                                                )
                                        )
                                )
                                .images(img->img.first(10),imag->imag.
                                        edges(imgedge->imgedge
                                                .node(imgnode->imgnode
                                                        .originalSrc()
                                                        .transformedSrc( /*t->t
                                                                .maxWidth(600)
                                                                .maxHeight(600)*/
                                                        )
                                                )
                                        )
                                )
                                .availableForSale()
                                .descriptionHtml()
                                .description()
                                .variants(args -> args
                                        .first(120), variant -> variant
                                        .edges(variantEdgeQuery -> variantEdgeQuery
                                                .node(productVariantQuery -> productVariantQuery
                                                        .priceV2(price -> price.amount().currencyCode())
                                                        .presentmentPrices( arg -> arg.first(25) , price -> price.edges( e -> e.cursor().node(na -> na.price( pr -> pr.amount().currencyCode() ).compareAtPrice( cp -> cp.amount().currencyCode() ) ) ) )
                                                        .price()
                                                        .title()
                                                        .selectedOptions(select->select.name().value())
                                                        .compareAtPriceV2(compare->compare.amount().currencyCode())
                                                        .compareAtPrice()
                                                        .image(Storefront.ImageQuery::originalSrc)
                                                        .availableForSale()
                                                )
                                        )
                                )
                               )

                ));

//





    }

    /*private static Storefront.ProductQueryDefinition getProductQuery2() {
        return product->product
                .title()
                .vendor()
                .tags()
                .collections(getconn->getconn.first(100),conn -> conn
                        .edges(edgeconn -> edgeconn
                                .node(nodeconn -> nodeconn
                                        .title()
                                        .products(p->
                                                p.edges())
                                )
                        )
                )
                .images(img->img.
                        first(10),imag->imag.
                        edges(imgedge->imgedge.
                                node(imgnode->imgnode.originalSrc()
                                        .transformedSrc(tr->tr.maxWidth(600).maxHeight(600))
                                )
                        )
                )
                .availableForSale()
                .descriptionHtml()
                .description()
                .variants(args -> args
                        .first(120), variant -> variant
                        .edges(variantEdgeQuery -> variantEdgeQuery
                                .node(productVariantQuery -> productVariantQuery
                                        .price()
                                        .title()
                                        .priceV2(p->p.amount().currencyCode())
                                        .presentmentPrices(_queryBuilder -> _queryBuilder.edges(e->e.cursor().node(n->n.price(p->p.amount().currencyCode()).compareAtPrice(c->c.amount().currencyCode()))))
                                        .selectedOptions(select->select.name().value())
                                        .compareAtPrice()
                                        .compareAtPriceV2(c->c.amount().currencyCode())
                                        .image(Storefront.ImageQuery::originalSrc)
                                        .availableForSale()
                                )
                        )
                )
                .onlineStoreUrl()
                .options(op->op.
                        name()
                        .values()
                );
    }*/


}
