package com.elietlux.shopifyapp.homesection;

import android.content.Intent;
import android.support.annotation.Nullable;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.view.View;
import android.view.ViewGroup;
import android.widget.TextView;

import com.blogspot.atifsoftwares.animatoolib.Animatoo;
import com.elietlux.shopifyapp.R;
import com.elietlux.shopifyapp.maincontainer.MainActivity;
import com.elietlux.shopifyapp.productlistingsection.ProductListing;
import com.elietlux.shopifyapp.searchsection.AutoSearch;

import butterknife.BindView;
import butterknife.ButterKnife;

public class SearchCategoryStativActivity extends MainActivity {

    @Nullable
    @BindView(R.id.searchBar)
    TextView searchBar;
    @Nullable
    @BindView(R.id.categorytitle)
    TextView categorytitle;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        ViewGroup content = findViewById(R.id.MageNative_frame_container);
        getLayoutInflater().inflate(R.layout.activity_search_category_stativ, content, true);
        ButterKnife.bind(SearchCategoryStativActivity.this);

        searchBar.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {

                Intent in = new Intent(SearchCategoryStativActivity.this, AutoSearch.class);
                startActivity(in);
                overridePendingTransition(R.anim.magenative_slide_in, R.anim.magenative_slide_out);
                Animatoo.animateZoom(SearchCategoryStativActivity.this);
            }
        });
        categorytitle.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {

                selectCategory();
                Intent in = new Intent(SearchCategoryStativActivity.this, SearchCategories.class);
                in.setFlags(Intent.FLAG_ACTIVITY_SINGLE_TOP);
                startActivity(in);
                Animatoo.animateFade(SearchCategoryStativActivity.this);
            }
        });
    }
}