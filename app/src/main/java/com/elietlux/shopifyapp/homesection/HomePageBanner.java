/*
 * Copyright/**
 *          * CedCommerce
 *           *
 *           * NOTICE OF LICENSE
 *           *
 *           * This source file is subject to the End User License Agreement (EULA)
 *           * that is bundled with this package in the file LICENSE.txt.
 *           * It is also available through the world-wide-web at this URL:
 *           * http://cedcommerce.com/license-agreement.txt
 *           *
 *           * @category  Ced
 *           * @package   MageNative
 *           * @author    CedCommerce Core Team <connect@cedcommerce.com >
 *           * @copyright Copyright CEDCOMMERCE (http://cedcommerce.com/)
 *           * @license      http://cedcommerce.com/license-agreement.txt
 *
 */
package com.elietlux.shopifyapp.homesection;

import android.content.Context;
import android.content.Intent;
import android.os.Bundle;
import android.support.annotation.NonNull;
import android.support.v4.app.Fragment;
import android.support.v4.app.FragmentManager;
import android.support.v4.app.FragmentPagerAdapter;
import android.support.v4.app.FragmentStatePagerAdapter;
import android.support.v4.view.PagerAdapter;
import android.util.Base64;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.TextView;

import com.blogspot.atifsoftwares.animatoolib.Animatoo;
import com.bumptech.glide.Glide;
import com.bumptech.glide.request.RequestOptions;
import com.elietlux.shopifyapp.R;
import com.elietlux.shopifyapp.productlistingsection.ProductListing;
import com.elietlux.shopifyapp.productviewsection.ProductView;

import org.jetbrains.annotations.NotNull;
import org.json.JSONArray;

import java.io.UnsupportedEncodingException;

public class HomePageBanner extends PagerAdapter {
    @Override
    public int getCount() {
        return stringArray.length();
    }

    @Override
    public boolean isViewFromObject(@NonNull @NotNull View view, @NonNull @NotNull Object o) {
        return view == o;
    }

    @Override
    public void destroyItem(@NonNull @NotNull ViewGroup container, int position, @NonNull @NotNull Object object) {
        container.removeView((View) object);
    }

    @NonNull
    @NotNull
    @Override
    public Object instantiateItem(@NonNull @NotNull ViewGroup container, int position) {
        View banner = null;
        try {
            banner = LayoutInflater.from(container.getContext()).inflate(R.layout.magenative_banner_layout, null);
            final ImageView bannerimage = (ImageView) banner.findViewById(R.id.MageNative_bannerimage);
            final TextView banner_link_to = (TextView) banner.findViewById(R.id.link_to);
            banner_link_to.setText(stringArray.getJSONObject(position).getString("link_to"));
            final TextView banner_id = (TextView) banner.findViewById(R.id.id);
            banner_id.setText(stringArray.getJSONObject(position).getString("id"));
            Glide.with(context)
                    .load(stringArray.getJSONObject(position).getString("url"))
                    .apply(new RequestOptions().placeholder(R.drawable.bannerplaceholder).error(R.drawable.bannerplaceholder).dontTransform())
                    .into(bannerimage);
            bannerimage.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View v) {

                    if (banner_link_to.getText().toString().equals("collection")) {
                        String s1 = "gid://shopify/Collection/" + banner_id.getText().toString();
                        Intent intent = new Intent(context, ProductListing.class);
                        intent.putExtra("cat_id", getBase64Encode(s1));
                        intent.putExtra("isFrombrands", "false");
                        context.startActivity(intent);
                        ((HomePage) context).overridePendingTransition(R.anim.magenative_slide_in, R.anim.magenative_slide_out);
                    }
                    if (banner_link_to.getText().toString().equals("product")) {
                        String s1 = "gid://shopify/Product/" + banner_id.getText().toString();
                        Intent prod_link = new Intent(context, ProductView.class);
                        prod_link.putExtra("id", getBase64Encode(s1));
                        context.startActivity(prod_link);
                        Animatoo.animateZoom(context);
                        //getActivity().overridePendingTransition(R.anim.magenative_slide_in, R.anim.magenative_slide_out);
                    }
                    if (banner_link_to.getText().toString().equals("web_address")) {
                        Intent weblink = new Intent(context, HomeWeblink.class);
                        weblink.putExtra("link", banner_id.getText().toString());
                        context.startActivity(weblink);
                        ((HomePage) context).overridePendingTransition(R.anim.magenative_slide_in, R.anim.magenative_slide_out);
                    }
                }
            });
        } catch (Exception e) {
            e.printStackTrace();
        }
        container.addView(banner);
        return banner;
    }

    JSONArray stringArray;
    Context context;

    public HomePageBanner(Context context, JSONArray img) {
        stringArray = img;
        this.context = context;
    }

    public String getBase64Encode(String id) {
        byte[] data = Base64.encode(id.getBytes(), Base64.DEFAULT);
        try {
            id = new String(data, "UTF-8").trim();
        } catch (UnsupportedEncodingException e) {
            e.printStackTrace();
        }
        return id;
    }
//    @Override
//    public Fragment getItem(int position)
//    {
//        final MainBannerFragment f1=new MainBannerFragment();
//        final Bundle bundle=new Bundle();
//        try
//        {
//            bundle.putString("banner_image", stringArray.getJSONObject(position).getString("url"));
//            bundle.putString("link_to", stringArray.getJSONObject(position).getString("link_to"));
//            bundle.putString("id", stringArray.getJSONObject(position).getString("id"));
//        }
//        catch (Exception e)
//        {
//            e.printStackTrace();
//        }
//        f1.setArguments(bundle);
//        return f1;
//    }
//    @Override
//    public int getCount()
//    {
//        return stringArray.length();
//    }
}
