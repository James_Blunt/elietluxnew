package com.elietlux.shopifyapp.homesection;
import android.content.Intent;
import android.os.Bundle;
import android.support.annotation.NonNull;
import android.support.annotation.Nullable;
import android.support.design.widget.TabLayout;
import android.support.v4.app.Fragment;
import android.support.v4.app.FragmentManager;
import android.support.v4.app.FragmentPagerAdapter;
import android.support.v4.view.ViewPager;
import android.util.Base64;
import android.util.Log;
import android.view.Menu;
import android.view.MenuItem;
import android.view.View;
import android.view.ViewGroup;
import android.widget.TextView;
import android.widget.Toast;

import com.elietlux.shopifyapp.R;
import com.elietlux.shopifyapp.maincontainer.MainActivity;
import com.elietlux.shopifyapp.productlistingsection.AllProductListing;
import com.elietlux.shopifyapp.requestsection.ApiClient;
import com.elietlux.shopifyapp.searchsection.AutoSearch;
import com.elietlux.shopifyapp.storagesection.LocalData;
import com.elietlux.shopifyapp.storefrontqueries.Query;
import com.elietlux.shopifyapp.storefrontresponse.AsyncResponse;
import com.elietlux.shopifyapp.storefrontresponse.Response;
import com.shopify.buy3.GraphCallResult;
import com.shopify.buy3.GraphClient;
import com.shopify.buy3.GraphResponse;
import com.shopify.buy3.QueryGraphCall;
import com.shopify.buy3.Storefront;
import com.shopify.graphql.support.ID;
import com.smartlook.sdk.smartlook.Smartlook;

import java.util.ArrayList;
import java.util.List;
import java.util.Objects;
import butterknife.BindView;
import butterknife.ButterKnife;

public class HomePage extends MainActivity
{
    private String[] tittles;
    @Nullable
    @BindView(R.id.MageNative_pager) ViewPager pager;
    @Nullable
    @BindView(R.id.tabs) TabLayout tabLayout;
    @Nullable
    @BindView(R.id.searchBar) TextView searchBar;
    @Nullable
    @BindView(R.id.country) TextView country;
    ArrayList<ID> list ;
    GraphClient client=null;
    LocalData localData;

    @Override
    protected void onDestroy() {
        super.onDestroy();
    }

    @Override
    protected void onCreate(Bundle savedInstanceState)
    {
        super.onCreate(savedInstanceState);
        ViewGroup content = findViewById(R.id.MageNative_frame_container);
        getLayoutInflater().inflate(R.layout.magenative_home_page, content, true);
        ButterKnife.bind(HomePage.this);
        tittles = new String[3];
        client= ApiClient.getGraphClient(HomePage.this,true);
        list = new ArrayList<>();
        localData = new LocalData(this);

/*
        if (getIntent().getStringExtra("country")!=null)
*/
       // Toast.makeText(this, ""+getIntent().getStringExtra("country"), Toast.LENGTH_SHORT).show();
    //    Log.d("countrycheck",getIntent().getStringExtra("country"));
      //  country.setText(getIntent().getStringExtra("country"));

  /*    try {
          String s1 = "gid://shopify/Product/" +"14628133601340";
          byte[] data = Base64.encode(s1.getBytes(), Base64.DEFAULT);
          s1 = new String(data, "UTF-8").trim();

          String s2 = "gid://shopify/Product/" +"14628145332284";
          byte[] data2 = Base64.encode(s1.getBytes(), Base64.DEFAULT);
          s2 = new String(data2, "UTF-8").trim();

          String s3 = "gid://shopify/Product/" +"32244535590972";
          byte[] data3 = Base64.encode(s1.getBytes(), Base64.DEFAULT);
          s3 = new String(data3, "UTF-8").trim();

          list.add( new ID("Z2lkOi8vc2hvcGlmeS9Qcm9kdWN0LzQ2MjI1Njk3MzQyMDQ=") );
          list.add(new ID("Z2lkOi8vc2hvcGlmeS9Qcm9kdWN0LzQ2ODc5NDM5OTEzNTY="));
          list.add(new ID("Z2lkOi8vc2hvcGlmeS9Qcm9kdWN0LzQ2ODc5NDM4NjAyODQ="));

         // Log.d("moneyformat",localData.getMoneyFormat());

         // hashFromSHA1("66:DB:57:27:6D:5D:D3:C5:EB:DC:0F:DC:26:A2:FA:FE:17:E9:1E:B9");

      }catch (Exception e){
          e.printStackTrace();
      }



        tittles[0] = getResources().getString(R.string.women);
        tittles[1] = getResources().getString(R.string.men);
        tittles[2] = getResources().getString(R.string.brands);*/



        MyPagerAdapter adapter = new MyPagerAdapter(getSupportFragmentManager());
        pager.setAdapter(adapter);
        pager.setOffscreenPageLimit(3);
        tabLayout.setupWithViewPager(pager);
        tabLayout.setSelectedTabIndicatorColor(getResources().getColor(R.color.selectionColor));
        setupTabIcons();
        searchBar.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {

                Intent in = new Intent(HomePage.this, AutoSearch.class);
                startActivity(in);
                overridePendingTransition(R.anim.magenative_slide_in, R.anim.magenative_slide_out);
            }
        });
        searchBar.setVisibility(View.GONE);
        tabLayout.addOnTabSelectedListener(new TabLayout.OnTabSelectedListener()
        {
            @Override
            public void onTabSelected(@NonNull TabLayout.Tab tab) {
                Log.i("tab.getPosition(1)", "" + tab.getPosition());
                TextView selectedtab = Objects.requireNonNull(tab.getCustomView()).findViewById(R.id.tab);
                selectedtab.setTextColor(getResources().getColor(R.color.selectionColor));
                 pager.setCurrentItem(tab.getPosition());
            }

            @Override
            public void onTabUnselected(@NonNull TabLayout.Tab tab) {
                Log.i("tab.getPosition(2)", "" + tab.getPosition());
                TextView selectedtab = Objects.requireNonNull(tab.getCustomView()).findViewById(R.id.tab);
                selectedtab.setTextColor(getResources().getColor(R.color.black));
            }

            @Override
            public void onTabReselected(@NonNull TabLayout.Tab tab) {
                Log.i("tab.getPosition(3)", "" + tab.getPosition());
                pager.setCurrentItem(tab.getPosition());
            }
        });
        if(getIntent().getStringExtra("collection-all")!=null)
        {
            Log.i("Collection-All","IN");
            //pager.setCurrentItem(1);
        }

        try
        {

            QueryGraphCall call = client.queryGraph(Query.getproductfromlist(list));
            Response.getGraphQLResponse(call,new AsyncResponse()
            {
                @Override
                public void finalOutput(@NonNull Object output,@NonNull boolean error )
                {
                    if(error)
                    {
                        GraphResponse<Storefront.QueryRoot> response  = ((GraphCallResult.Success<Storefront.QueryRoot>) output).getResponse();
                        runOnUiThread(new Runnable()
                        {
                            @Override
                            public void run()
                            {
                                Log.d("MultiProducts",""+response.getData().getNodes());
                            }
                        });
                    }
                    else
                    {
                        Log.i("ResponseError",""+output.toString());

                    }
                }
            },HomePage.this);
        }
        catch (Exception e)
        {
            e.printStackTrace();
        }
    }

    private void setupTabIcons()
    {
        View view1 = View.inflate(HomePage.this, R.layout.custom_tab, null);
        TextView tab1 = view1.findViewById(R.id.tab);
        tab1.setText(getResources().getString(R.string.women));
        tab1.setTextColor(getResources().getColor(R.color.selectionColor));
        Objects.requireNonNull(tabLayout.getTabAt(0)).setCustomView(view1);

        View view2 = View.inflate(HomePage.this, R.layout.custom_tab, null);
        TextView tab2 = view2.findViewById(R.id.tab);
        tab2.setText(getResources().getString(R.string.men));
        tab2.setTextColor(getResources().getColor(R.color.black));
        Objects.requireNonNull(tabLayout.getTabAt(1)).setCustomView(view2);

        View view3 = View.inflate(HomePage.this, R.layout.custom_tab, null);
        TextView tab3 = view3.findViewById(R.id.tab);
        tab3.setText(getResources().getString(R.string.brands));
        tab3.setTextColor(getResources().getColor(R.color.black));
        Objects.requireNonNull(tabLayout.getTabAt(2)).setCustomView(view3);
    }

    @Override
    public void onResume() {
        selectHome();
        invalidateOptionsMenu();
        super.onResume();
    }
    class MyPagerAdapter extends FragmentPagerAdapter
    {

        MyPagerAdapter(FragmentManager fm) {
            super(fm);
        }
        @Override
        public CharSequence getPageTitle(int position) {
            return tittles[position];
        }
        @Override
        public int getCount() {
            return tittles.length;
        }
        @Nullable
        @Override
        public Fragment getItem(int position)
        {
            switch (position)
            {
                case 0:
                    MenWomenFragment womenFragment = new MenWomenFragment();
                    Bundle womenBundle  = new Bundle();
                    womenBundle.putInt("key",0);
                    womenFragment.setArguments(womenBundle);
                    return  womenFragment;
                case 1:
                    MenWomenFragment menFragment = new MenWomenFragment();
                    Bundle menBundle  = new Bundle();
                    menBundle.putInt("key",1);
                    menFragment.setArguments(menBundle);
                    return  menFragment;
                case 2:
                    return new BrandsFragment();
            }
            return null;
        }
    }



}
