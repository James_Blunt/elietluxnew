package com.elietlux.shopifyapp.homesection;

import android.content.Intent;
import android.os.Bundle;
import android.support.annotation.Nullable;
import android.support.v7.widget.LinearLayoutCompat;
import android.util.Base64;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.TextView;

import com.elietlux.shopifyapp.R;
import com.elietlux.shopifyapp.maincontainer.MainActivity;
import com.elietlux.shopifyapp.productlistingsection.ProductListing;
import com.elietlux.shopifyapp.requestsection.ApiInterface;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;
import java.io.UnsupportedEncodingException;
import butterknife.BindView;
import butterknife.ButterKnife;

public class Subcats extends MainActivity {

    @Nullable
    @BindView(R.id.mainLinear)
    LinearLayoutCompat mainLinear;
    JSONArray menus ;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        ViewGroup content = findViewById(R.id.MageNative_frame_container);
        getLayoutInflater().inflate(R.layout.new_layout_hompagecategory, content, true);
        ButterKnife.bind(Subcats.this);
        if (getIntent().getStringExtra("menus")!=null){
            try {
                menus = new JSONArray(getIntent().getStringExtra("menus"));
            } catch (JSONException e) {
                e.printStackTrace();
            }
        }
        processCategories(menus);


    }

    private void processCategories(JSONArray data) {
        try {
            View category = null;
            TextView id =null;
            ImageView arrow =null;
            TextView categorytitle =null;
            for (int i=0;i<data.length();i++){
                JSONObject inner = data.getJSONObject(i);
                category = getLayoutInflater().inflate(R.layout.category,null);
                id=category.findViewById(R.id.id);
                categorytitle=category.findViewById(R.id.categorytitle);
                arrow=category.findViewById(R.id.arrow);

                if (inner.has("id")){
                    id.setText(inner.getString("id").toString());
                }

                if (inner.has("menus")){
                    arrow.setVisibility(View.VISIBLE);
                }else {
                     arrow.setVisibility(View.GONE);
                }
                categorytitle.setText(inner.getString("title"));

                category.setOnClickListener(new View.OnClickListener() {
                    @Override
                    public void onClick(View v) {

                        try {
                            if (inner.has("menus")) {
                                Intent in = new Intent(Subcats.this, Subcats.class);
                                in.putExtra("menus", inner.getString("menus"));
                                startActivity(in);
                                overridePendingTransition(R.anim.magenative_slide_in, R.anim.magenative_slide_out);

                            }else{

                                String s1 = "gid://shopify/Collection/" +  inner.getString("id");
                                Intent intent=new Intent(Subcats.this, ProductListing.class);
                                intent.putExtra("cat_id",getBase64Encode(s1));
                                intent.putExtra("cat_name",inner.getString("title"));
                                intent.putExtra("isFrombrands","false");
                                startActivity(intent);
                                overridePendingTransition(R.anim.magenative_slide_in, R.anim.magenative_slide_out);
                            }

                        } catch (JSONException e) {
                            e.printStackTrace();
                        }
                    }
                });

                mainLinear.addView(category);



            }

        } catch (JSONException e) {
            e.printStackTrace();
        }

    }

    public String getBase64Encode(String id) {
        byte[] data = Base64.encode(id.getBytes(), Base64.DEFAULT);
        try {
            id = new String(data, "UTF-8").trim();
        } catch (UnsupportedEncodingException e) {
            e.printStackTrace();
        }
        return id;
    }
}
