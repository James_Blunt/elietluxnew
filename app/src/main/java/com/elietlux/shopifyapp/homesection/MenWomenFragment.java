package com.elietlux.shopifyapp.homesection;

import android.content.Intent;
import android.os.Bundle;
import android.os.Handler;
import android.support.annotation.NonNull;
import android.support.annotation.Nullable;
import android.support.v4.app.Fragment;
import android.support.v4.view.ViewPager;
import android.support.v7.widget.GridLayoutManager;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.text.Html;
import android.util.Base64;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.Button;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.RelativeLayout;
import android.widget.ScrollView;
import android.widget.TextView;
import android.widget.Toast;

import com.blogspot.atifsoftwares.animatoolib.Animatoo;
import com.bumptech.glide.Glide;
import com.bumptech.glide.request.RequestOptions;
import com.shopify.buy3.GraphCallResult;
import com.shopify.buy3.GraphClient;
import com.shopify.buy3.GraphResponse;
import com.shopify.buy3.QueryGraphCall;
import com.shopify.buy3.Storefront;
import com.elietlux.shopifyapp.R;
import com.elietlux.shopifyapp.adaptersection.RecentlyViewedProducts_Adapter;
import com.elietlux.shopifyapp.adaptersection.RelatedProducts_Adapter;
import com.elietlux.shopifyapp.loadersection.CustomProgressDialog;
import com.elietlux.shopifyapp.loadersection.Loader;
import com.elietlux.shopifyapp.maincontainer.MainContainerWeblink;
import com.elietlux.shopifyapp.productlistingsection.AllProductListing;
import com.elietlux.shopifyapp.productlistingsection.ProductListing;
import com.elietlux.shopifyapp.productviewsection.CirclePageIndicator;
import com.elietlux.shopifyapp.productviewsection.ProductView;
import com.elietlux.shopifyapp.requestsection.ApiClient;
import com.elietlux.shopifyapp.requestsection.ApiInterface;
import com.elietlux.shopifyapp.storagesection.LocalData;
import com.elietlux.shopifyapp.storefrontqueries.Query;
import com.elietlux.shopifyapp.storefrontresponse.AsyncResponse;
import com.elietlux.shopifyapp.storefrontresponse.Response;

import org.json.JSONArray;
import org.json.JSONObject;

import java.io.Serializable;
import java.io.UnsupportedEncodingException;
import java.util.ArrayList;
import java.util.Iterator;
import java.util.List;
import java.util.Objects;
import java.util.Timer;
import java.util.TimerTask;

import butterknife.BindView;
import butterknife.ButterKnife;
import butterknife.Unbinder;
import io.reactivex.Observable;
import io.reactivex.SingleObserver;
import io.reactivex.android.schedulers.AndroidSchedulers;
import io.reactivex.disposables.Disposable;
import io.reactivex.schedulers.Schedulers;
import okhttp3.ResponseBody;
import retrofit2.Call;

public class MenWomenFragment extends Fragment {
    @Nullable
    Unbinder unbinder;
    ApiInterface apiService;
    @Nullable
    @BindView(R.id.MageNative_homepagebanner)
    ViewPager MageNative_homepagebanner;
    @Nullable
    @BindView(R.id.MageNative_indicator2)
    CirclePageIndicator MageNative_indicator2;
    @Nullable
    @BindView(R.id.cat1image)
    ImageView cat1image;
    @Nullable
    @BindView(R.id.cat1id)
    TextView cat1id;
    @Nullable
    @BindView(R.id.cat1section)
    RelativeLayout cat1section;
    @Nullable
    @BindView(R.id.cat2image)
    ImageView cat2image;
    @Nullable
    @BindView(R.id.cat2id)
    TextView cat2id;
    @Nullable
    @BindView(R.id.cat2section)
    RelativeLayout cat2section;
    @Nullable
    @BindView(R.id.cat3image)
    ImageView cat3image;
    @Nullable
    @BindView(R.id.cat3id)
    TextView cat3id;
    @Nullable
    @BindView(R.id.cat3section)
    RelativeLayout cat3section;
    @Nullable
    @BindView(R.id.cat4image)
    ImageView cat4image;
    @Nullable
    @BindView(R.id.cat4id)
    TextView cat4id;
    @Nullable
    @BindView(R.id.cat4section)
    RelativeLayout cat4section;
    @Nullable
    @BindView(R.id.cat5image)
    ImageView cat5image;
    @Nullable
    @BindView(R.id.cat5id)
    TextView cat5id;
    @Nullable
    @BindView(R.id.cat5section)
    RelativeLayout cat5section;
    @Nullable
    @BindView(R.id.cat6image)
    ImageView cat6image;
    @Nullable
    @BindView(R.id.cat6id)
    TextView cat6id;
    @Nullable
    @BindView(R.id.cat6section)
    RelativeLayout cat6section;
    @Nullable
    @BindView(R.id.cat7image)
    ImageView cat7image;
    @Nullable
    @BindView(R.id.cat7id)
    TextView cat7id;
    @Nullable
    @BindView(R.id.cat7section)
    RelativeLayout cat7section;
    @Nullable
    @BindView(R.id.cat8image)
    ImageView cat8image;
    @Nullable
    @BindView(R.id.cat8id)
    TextView cat8id;
    @Nullable
    @BindView(R.id.cat8section)
    RelativeLayout cat8section;
    @Nullable
    @BindView(R.id.cat9image)
    ImageView cat9image;
    @Nullable
    @BindView(R.id.cat9id)
    TextView cat9id;
    @Nullable
    @BindView(R.id.cat9section)
    RelativeLayout cat9section;
    @Nullable
    @BindView(R.id.grid9imagesection)
    RelativeLayout grid9imagesection;
    @Nullable
    @BindView(R.id.tittle)
    TextView grid9tittle;
    @Nullable
    @BindView(R.id.catid)
    TextView catid;
    @Nullable
    @BindView(R.id.viewcats)
    ImageView catviewallcard;
    @Nullable
    @BindView(R.id.main)
    ScrollView main;
    @Nullable
    @BindView(R.id.staggaredgridsection)
    RelativeLayout staggaredgridsection;
    @Nullable
    @BindView(R.id.staggeredcatname)
    TextView staggeredcatname;
    @Nullable
    @BindView(R.id.staggeredcatid)
    TextView staggeredcatid;
    @Nullable
    @BindView(R.id.staggeredproducts)
    RecyclerView staggeredproducts;
    @Nullable
    @BindView(R.id.sectionviewall)
    Button sectionviewall;
    @Nullable
    @BindView(R.id.thirdcollections)
    RelativeLayout thirdcollections;
    @Nullable
    @BindView(R.id.thirdcatname)
    TextView thirdcatname;
    @Nullable
    @BindView(R.id.thirdcatid)
    TextView thirdcatid;
    @Nullable
    @BindView(R.id.products)
    RecyclerView products;
    @Nullable
    @BindView(R.id.viewall)
    Button viewall;

    @Nullable
    @BindView(R.id.secondcollections)
    RelativeLayout secondcollections;
    @Nullable
    @BindView(R.id.secondcatname)
    TextView secondcatname;
    @Nullable
    @BindView(R.id.secondcatid)
    TextView secondcatid;
    @Nullable
    @BindView(R.id.secondproducts)
    RecyclerView secondproducts;
    @Nullable
    @BindView(R.id.secondviewall)
    Button secondviewall;

    @Nullable
    @BindView(R.id.fourthcollections)
    RelativeLayout fourthcollections;
    @Nullable
    @BindView(R.id.fourthcatname)
    TextView fourthcatname;
    @Nullable
    @BindView(R.id.fourthcatid)
    TextView fourthcatid;
    @Nullable
    @BindView(R.id.fourthproducts)
    RecyclerView fourthproducts;
    @Nullable
    @BindView(R.id.fourthviewall)
    Button fourthviewall;
    @Nullable
    @BindView(R.id.fivecollections)
    RelativeLayout fivecollections;
    @Nullable
    @BindView(R.id.fivecatname)
    TextView fivecatname;
    @Nullable
    @BindView(R.id.fivecatid)
    TextView fivecatid;
    @Nullable
    @BindView(R.id.fiveproducts)
    RecyclerView fiveproducts;
    @Nullable
    @BindView(R.id.fiveviewall)
    Button fiveviewall;
    @Nullable
    @BindView(R.id.sixcollections)
    RelativeLayout sixcollections;
    @Nullable
    @BindView(R.id.sixcatname)
    TextView sixcatname;
    @Nullable
    @BindView(R.id.sixcatid)
    TextView sixcatid;
    @Nullable
    @BindView(R.id.sixproducts)
    RecyclerView sixproducts;
    @Nullable
    @BindView(R.id.sixviewall)
    Button sixviewall;
    @Nullable
    @BindView(R.id.middlebanner)
    ViewPager middlebanner;
    @Nullable
    @BindView(R.id.bannersection)
    LinearLayout bannersection;
    @Nullable
    @BindView(R.id.recentlyviewedsection)
    RelativeLayout recentlyviewedsection;
    @Nullable
    @BindView(R.id.recentsproducts)
    RecyclerView recentsproducts;

    @Nullable
    @BindView(R.id.allcatslinear)
    LinearLayout allcatslinear;
    @Nullable
    @BindView(R.id.allcategorysection)
    RelativeLayout allcategorysection;

    List<Storefront.ProductEdge> grid9data;
    Timer timer;
    Timer timer2;
    int page = 0;
    int page2 = 0;
    JSONArray banners_widget;
    JSONArray collection_widget = null;
    JSONArray banners_widget2;
    GraphClient client;
    LocalData data = null;
    Loader loader = null;
    Call<ResponseBody> callCollectionData = null;
    int key = 0;
    View view = null;

    @Override
    public View onCreateView(@NonNull LayoutInflater inflater, ViewGroup container, Bundle savedInstanceState) {
        view = inflater.inflate(R.layout.new_layout_hompage, container, false);
        unbinder = ButterKnife.bind(this, view);
        apiService = ApiClient.getClient(getActivity()).create(ApiInterface.class);
        client = ApiClient.getGraphClient(getActivity(), true);
        Call<ResponseBody> call = apiService.getHomePage(getResources().getString(R.string.mid));

        data = new LocalData(getActivity());
        loader = new Loader(getActivity());
        loader.show();
        if (!getArguments().isEmpty()) {
            key = getArguments().getInt("key");
            Log.d("Arguments", getArguments().getInt("key") + "");
            switch (key) {
                case 0:
                    callCollectionData = apiService.getCategoryMenus(getResources().getString(R.string.mid), "women-collection-app-main");
                    break;
                case 1:
                    callCollectionData = apiService.getCategoryMenus(getResources().getString(R.string.mid), "men-collection-app-main");
                    break;
            }
        }
        getResponseBanners(call);
        // bindCategories();
        return view;
    }


    private void bindCategories() {
        Log.i("bindCategories", "" + data.getMenus());
        try {
            if (data.getMenus() != null) {
                JSONObject object = new JSONObject(data.getMenus());
                if (object.getBoolean("success")) {
                    if (object.has("data")) {
                        JSONArray array = object.getJSONArray("data");
                        if (array.length() > 0) {
                            generateMenus(array);
                        }
                    }
                }
            }
        } catch (Exception e) {
            e.printStackTrace();
        }
    }

    void generateMenus(JSONArray data) {
        try {
            allcategorysection.setVisibility(View.VISIBLE);
            View cat_View = null;
            TextView title;
            TextView id;
            TextView handle;
            TextView url;
            TextView type;
            JSONObject objectouter, object = null;
            JSONArray menus;
            LinearLayout.LayoutParams layoutParams = new LinearLayout.LayoutParams(ViewGroup.LayoutParams.WRAP_CONTENT, ViewGroup.LayoutParams.WRAP_CONTENT);
            layoutParams.setMargins(5, 5, 5, 5);
            for (int i = 0; i < data.length(); i++) {
                objectouter = data.getJSONObject(i);
                if (objectouter.has("menus")) {
                    menus = objectouter.getJSONArray("menus");
                    for (int j = 0; j < menus.length(); j++) {
                        cat_View = getLayoutInflater().inflate(R.layout.circularcategoryitem, null);
                        title = cat_View.findViewById(R.id.cat_title);
                        id = cat_View.findViewById(R.id.id);
                        type = cat_View.findViewById(R.id.type);
                        url = cat_View.findViewById(R.id.url);
                        handle = cat_View.findViewById(R.id.handle);
                        cat_View.setLayoutParams(layoutParams);
                        object = menus.getJSONObject(j);
                        if (object.has("type")) {
                            String typedata = object.getString("type");
                            type.setText(typedata);
                        }
                        if (object.has("title")) {
                            String upperString = object.getString("title").substring(0, 1).toUpperCase() + object.getString("title").substring(1).toLowerCase();
                            title.setText(objectouter.getString("title") + " " + upperString);
                        }
                        if (object.has("id")) {
                            id.setText(object.getString("id"));
                        }
                        if (object.has("handle")) {
                            handle.setText(object.getString("handle"));
                        }
                        if (object.has("url")) {
                            url.setText(object.getString("url"));
                        }

                        TextView finalType = type;
                        TextView finalId = id;
                        TextView finalHandle = handle;
                        TextView finalTitle = title;
                        TextView finalUrl = url;
                        cat_View.setOnClickListener(new View.OnClickListener() {
                            @Override
                            public void onClick(View view) {
                                if (finalType.getText().toString().equals("collection")) {
                                    try {
                                        String s1;
                                        if (finalId.getText().toString().isEmpty()) {
                                            s1 = finalHandle.getText().toString() + "*#*";
                                        } else {
                                            s1 = "gid://shopify/Collection/" + finalId.getText().toString();
                                            byte[] data = Base64.encode(s1.getBytes(), Base64.DEFAULT);
                                            s1 = new String(data, "UTF-8").trim();
                                        }
                                        Intent intent = new Intent(getActivity(), ProductListing.class);
                                        intent.addFlags(Intent.FLAG_ACTIVITY_SINGLE_TOP);
                                        intent.putExtra("cat_id", s1);
                                        intent.putExtra("cat_name", finalTitle.getText().toString());
                                        intent.putExtra("isFrombrands", "false");
                                        startActivity(intent);
                                        getActivity().overridePendingTransition(R.anim.magenative_slide_in, R.anim.magenative_slide_out);

                                    } catch (Exception e) {
                                        e.printStackTrace();
                                    }
                                } else {
                                    if (finalType.getText().toString().equals("product")) {
                                        try {
                                            String s1;
                                            if (finalId.getText().toString().isEmpty()) {
                                                s1 = finalHandle.getText().toString() + "*#*";
                                            } else {
                                                s1 = "gid://shopify/Product/" + finalId.getText().toString();
                                                byte[] data = Base64.encode(s1.getBytes(), Base64.DEFAULT);
                                                s1 = new String(data, "UTF-8").trim();
                                            }
                                            Intent intent = new Intent(getActivity(), ProductView.class);
                                            intent.addFlags(Intent.FLAG_ACTIVITY_SINGLE_TOP);
                                            intent.putExtra("id", s1);
                                            startActivity(intent);
                                            Animatoo.animateZoom(getActivity());
                                            //getActivity().overridePendingTransition(R.anim.magenative_slide_in, R.anim.magenative_slide_out);

                                        } catch (Exception e) {
                                            e.printStackTrace();
                                        }
                                    } else {
                                        if (finalType.getText().toString().equals("product-all")) {
                                            try {
                                                Intent intent = new Intent(getActivity(), AllProductListing.class);
                                                intent.putExtra("cat_name", finalTitle.getText().toString());
                                                intent.putExtra("isFrombrands", "false");
                                                intent.addFlags(Intent.FLAG_ACTIVITY_SINGLE_TOP);
                                                startActivity(intent);
                                                getActivity().overridePendingTransition(R.anim.magenative_slide_in, R.anim.magenative_slide_out);

                                            } catch (Exception e) {
                                                e.printStackTrace();
                                            }
                                        } else {
                                            if (finalType.getText().toString().equals("collection-all")) {
                                                try {
                                                    Intent intent = new Intent(getActivity(), HomePage.class);
                                                    intent.putExtra("collection-all", finalTitle.getText().toString());
                                                    intent.addFlags(Intent.FLAG_ACTIVITY_SINGLE_TOP);
                                                    intent.addFlags(Intent.FLAG_ACTIVITY_CLEAR_TASK);
                                                    intent.addFlags(Intent.FLAG_ACTIVITY_NEW_TASK);

                                                    startActivity(intent);
                                                    getActivity().overridePendingTransition(R.anim.magenative_slide_in, R.anim.magenative_slide_out);

                                                } catch (Exception e) {
                                                    e.printStackTrace();
                                                }
                                            } else {
                                                if (finalType.getText().toString().equals("page") || finalType.getText().toString().equals("blog")) {
                                                    try {
                                                        Intent intent = new Intent(getActivity(), MainContainerWeblink.class);
                                                        intent.putExtra("name", finalTitle.getText().toString());
                                                        intent.putExtra("link", finalUrl.getText().toString());
                                                        intent.addFlags(Intent.FLAG_ACTIVITY_SINGLE_TOP);
                                                        startActivity(intent);
                                                        getActivity().overridePendingTransition(R.anim.magenative_slide_in, R.anim.magenative_slide_out);

                                                    } catch (Exception e) {
                                                        e.printStackTrace();
                                                    }
                                                } else {
                                                    Toast.makeText(getActivity(), finalTitle.getText().toString(), Toast.LENGTH_LONG).show();
                                                }
                                            }
                                        }
                                    }
                                }
                            }
                        });

                        allcatslinear.addView(cat_View);
                    }
                }
            }
        } catch (Exception e) {
            e.printStackTrace();
        }
    }

    private void getResponseBanners(Call<ResponseBody> call) {
        try {
            Response.getRetrofitResponse(call, new AsyncResponse() {
                @Override
                public void finalOutput(@NonNull Object output, @NonNull boolean error) {
                    if (error) {
                        if (CustomProgressDialog.mDialog != null) {
                            CustomProgressDialog.mDialog.dismiss();
                            CustomProgressDialog.mDialog = null;
                        }
                        creatHomePage(output.toString());
                        getResponseCollectionsData(callCollectionData);
                    } else {
                        Log.i("ErrorHomePage", "" + output.toString());
                    }
                }
            }, getActivity());
        } catch (Exception e) {
            e.printStackTrace();
        }
    }

    private void getResponseCollectionsData(Call<ResponseBody> call) {
        try {
            com.elietlux.shopifyapp.storefrontresponse.Response.getRetrofitResponse(call, new AsyncResponse() {
                @Override
                public void finalOutput(@NonNull Object output, @NonNull boolean error) {
                    if (error) {
                        /*processMenus(output.toString());*/
                        processCollectionsData(output.toString());
                        Log.d("getResponseCollections", "" + output);
                    } else {
                        Log.i("ErrorHomePage", "" + output.toString());
                    }
                }
            }, getActivity());
        } catch (Exception e) {
            e.printStackTrace();
        }
    }

    private void processCollectionsData(String toString) {
        try {
            JSONObject main = new JSONObject(toString);
            JSONArray data = main.getJSONArray("data");
            fetchCollectionData(data.getJSONObject(0).getString("id"), "staggeredgrid", 10, data.getJSONObject(0).getString("title"));
            fetchCollectionData(data.getJSONObject(1).getString("id"), "secondcollection", 10, data.getJSONObject(1).getString("title"));
            fetchCollectionData(data.getJSONObject(2).getString("id"), "thirdcollection", 10, data.getJSONObject(2).getString("title"));
            fetchCollectionData(data.getJSONObject(3).getString("id"), "fourthcollection", 10, data.getJSONObject(3).getString("title"));
            fetchCollectionData(data.getJSONObject(4).getString("id"), "fifthcollection", 10, data.getJSONObject(4).getString("title"));
            fetchCollectionData(data.getJSONObject(5).getString("id"), "sixthcollection", 10, data.getJSONObject(5).getString("title"));

        } catch (Exception e) {
            e.fillInStackTrace();
        }


    }


    private void creatHomePage(String result) {
        try {
            JSONObject object = new JSONObject(result);
            String success = object.getString("success");
            if (success.equals("true")) {
                if (key == 0) {
                    banners_widget = object.getJSONArray("banners");
                    collection_widget = object.getJSONArray("collections");
                    Log.i("collection_widget", "" + object.getJSONArray("collections"));
                    MageNative_homepagebanner.setAdapter(new HomePageBanner(getActivity(), object.getJSONArray("banners")));
                    pageSwitcher(10);
                    MageNative_indicator2.setViewPager(this.MageNative_homepagebanner);
                } else {
                    JSONObject banners_widget = object.getJSONObject("bannersAdditional");
                    collection_widget = banners_widget.getJSONArray("middle");
                    MageNative_homepagebanner = view.findViewById(R.id.MageNative_homepagebanner);
                    Log.i("collection_widget", "" + banners_widget.getJSONArray("middle"));
                    if (collection_widget.length() > 0) {
                        MageNative_homepagebanner.setAdapter(new HomePageBanner(getActivity(), collection_widget));
                        pageSwitcher(10);
                        MageNative_indicator2.setViewPager(MageNative_homepagebanner);
                    } else {
                        MageNative_homepagebanner.setVisibility(View.GONE);
                    }

                }



                /*fetchCollectionData(object.getJSONArray("collections").getJSONObject(1).getString("id"), "secondcollection", 10, object.getJSONArray("collections").getJSONObject(1).getString("title"));

                fetchCollectionData(collection_widget.getJSONObject(2).getString("id"), "thirdcollection", 10, collection_widget.getJSONObject(2).getString("title"));

                if (object.getJSONArray("collections").length() == 4) {
                    fetchCollectionData(object.getJSONArray("collections").getJSONObject(3).getString("id"), "fourthcollection", 6, object.getJSONArray("collections").getJSONObject(3).getString("title"));
                } else if (object.getJSONArray("collections").length() == 5) {
                    fetchCollectionData(object.getJSONArray("collections").getJSONObject(3).getString("id"), "fourthcollection", 6, object.getJSONArray("collections").getJSONObject(3).getString("title"));
                    fetchCollectionData(object.getJSONArray("collections").getJSONObject(4).getString("id"), "fifthcollection", 6, object.getJSONArray("collections").getJSONObject(4).getString("title"));

                } else if (object.getJSONArray("collections").length() == 6) {
                    fetchCollectionData(object.getJSONArray("collections").getJSONObject(3).getString("id"), "fourthcollection", 6, object.getJSONArray("collections").getJSONObject(3).getString("title"));
                    fetchCollectionData(object.getJSONArray("collections").getJSONObject(4).getString("id"), "fifthcollection", 6, object.getJSONArray("collections").getJSONObject(4).getString("title"));
                    fetchCollectionData(object.getJSONArray("collections").getJSONObject(5).getString("id"), "sixthcollection", 6, object.getJSONArray("collections").getJSONObject(5).getString("title"));
                }

                if (object.has("bannersAdditional")) {
                    if (object.get("bannersAdditional") instanceof JSONObject) {
                        if (object.getJSONObject("bannersAdditional").has("middle")) {
                            banners_widget2 = object.getJSONObject("bannersAdditional").getJSONArray("middle");
                            this.middlebanner.setAdapter(new HomePageBanner(getActivity().getSupportFragmentManager(), getActivity(), object.getJSONObject("bannersAdditional").getJSONArray("middle")));
                            pageSwitcher2(10);
                            middlebanner.setVisibility(View.VISIBLE);
                        }
                        if (object.getJSONObject("bannersAdditional").has("bottom")) {
                            inflateBottomBanners(object.getJSONObject("bannersAdditional").getJSONArray("bottom"));
                            bannersection.setVisibility(View.VISIBLE);
                        }
                    }
                }
                main.setVisibility(View.VISIBLE);
                new Handler().postDelayed(new Runnable() {
                    @Override
                    public void run() {
                        main.scrollTo(0, 0);
                    }
                }, 100);*/
            }
        } catch (Exception e) {
            e.printStackTrace();
        }
    }

    private void pageSwitcher2(int i) {
        try {
            timer2 = new Timer(); // At this line a new Thread will be created
            timer2.scheduleAtFixedRate(new RemindTask2(), 0, i * 1000); // delay
        } catch (Exception e) {
            e.printStackTrace();
        }
    }

    private void inflateBottomBanners(JSONArray jsonArray) {
        try {
            View view = null;
            ImageView image = null;
            TextView id = null;
            TextView link = null;
            JSONObject object = null;
            for (int i = 0; i < jsonArray.length(); i++) {
                object = jsonArray.getJSONObject(i);
                view = View.inflate(getActivity(), R.layout.banner_layout, null);
                image = view.findViewById(R.id.banner);
                id = view.findViewById(R.id.id);
                link = view.findViewById(R.id.link_to);
                Glide.with(getActivity())
                        .load(object.getString("url"))
                        .apply(new RequestOptions()
                                .placeholder(R.drawable.bannerplaceholder)
                        )
                        .into(image);
                id.setText(object.getString("id"));
                link.setText(object.getString("link_to"));
                TextView finalLink = link;
                TextView finalId = id;
                image.setOnClickListener(new View.OnClickListener() {
                    @Override
                    public void onClick(View v) {

                        if (finalLink.getText().toString().equals("collection")) {
                            String s1 = "gid://shopify/Collection/" + finalId.getText().toString();
                            Intent intent = new Intent(getActivity(), ProductListing.class);
                            intent.putExtra("isFrombrands", "false");
                            intent.putExtra("cat_id", getBase64Encode(s1));
                            startActivity(intent);
                            getActivity().overridePendingTransition(R.anim.magenative_slide_in, R.anim.magenative_slide_out);
                        }
                        if (finalLink.getText().toString().equals("product")) {
                            String s1 = "gid://shopify/Product/" + finalId.getText().toString();
                            Intent prod_link = new Intent(getActivity(), ProductView.class);
                            prod_link.putExtra("id", getBase64Encode(s1));
                            startActivity(prod_link);
                            Animatoo.animateZoom(getActivity());
                            //getActivity().overridePendingTransition(R.anim.magenative_slide_in, R.anim.magenative_slide_out);
                        }
                        if (finalLink.getText().toString().equals("web_address")) {
                            Intent weblink = new Intent(getActivity(), HomeWeblink.class);
                            weblink.putExtra("link", finalId.getText().toString());
                            startActivity(weblink);
                            getActivity().overridePendingTransition(R.anim.magenative_slide_in, R.anim.magenative_slide_out);
                        }
                    }
                });
                bannersection.addView(view);
            }
        } catch (Exception e) {
            e.printStackTrace();
        }
    }

    private void fetchCollectionData(String cat_id, String origin, int number, String tittle) {
        try {
            String s1 = "gid://shopify/Collection/" + cat_id;
            ArrayList<Storefront.CurrencyCode> currencyCodeArrayList = new ArrayList<>();
            Log.d("currencyCodeArrayList", "" + data.getCurrencyCode());
            currencyCodeArrayList.add(Storefront.CurrencyCode.valueOf(data.getCurrencyCode()));
            Log.d("currencyCodeArrayList", "" + currencyCodeArrayList);

            QueryGraphCall call = client.queryGraph(Query.getProducts(getBase64Encode(s1), "nocursor", Storefront.ProductCollectionSortKeys.BEST_SELLING, false, 25, currencyCodeArrayList));
            Response.getGraphQLResponse(call, new AsyncResponse() {
                @Override
                public void finalOutput(@NonNull Object output, @NonNull boolean error) {
                    if (error) {
                        GraphResponse<Storefront.QueryRoot> response = ((GraphCallResult.Success<Storefront.QueryRoot>) output).getResponse();
                        Objects.requireNonNull(getActivity()).runOnUiThread(new Runnable() {
                            @Override
                            public void run() {
                                if (origin.equals("secondcollection")) {
                                    if (CustomProgressDialog.mDialog != null) {
                                        CustomProgressDialog.mDialog.dismiss();
                                        CustomProgressDialog.mDialog = null;
                                    }
                                    processSecondCollection(getBase64Encode(s1), response, tittle);
                                }
                                if (origin.equals("staggeredgrid")) {
                                    if (CustomProgressDialog.mDialog != null) {
                                        CustomProgressDialog.mDialog.dismiss();
                                        CustomProgressDialog.mDialog = null;
                                    }
                                    processStaggeredGrid(getBase64Encode(s1), response, tittle);
                                }
                                if (origin.equals("thirdcollection")) {
                                    if (CustomProgressDialog.mDialog != null) {
                                        CustomProgressDialog.mDialog.dismiss();
                                        CustomProgressDialog.mDialog = null;
                                    }
                                    processThirdCollection(getBase64Encode(s1), response, tittle);
                                }
                                if (origin.equals("fourthcollection")) {
                                    if (CustomProgressDialog.mDialog != null) {
                                        CustomProgressDialog.mDialog.dismiss();
                                        CustomProgressDialog.mDialog = null;
                                    }
                                    processFourCollection(getBase64Encode(s1), response, tittle);
                                }
                                if (origin.equals("fifthcollection")) {
                                    if (CustomProgressDialog.mDialog != null) {
                                        CustomProgressDialog.mDialog.dismiss();
                                        CustomProgressDialog.mDialog = null;
                                    }
                                    processFiveCollection(getBase64Encode(s1), response, tittle);
                                }
                                if (origin.equals("sixthcollection")) {
                                    if (CustomProgressDialog.mDialog != null) {
                                        CustomProgressDialog.mDialog.dismiss();
                                        CustomProgressDialog.mDialog = null;
                                    }
                                    processSixCollection(getBase64Encode(s1), response, tittle);
                                }
                                loader.dismiss();

                            }
                        });
                    } else {
                        Log.i("ResponseError", "" + output.toString());
                    }
                }
            }, getActivity());
        } catch (Exception e) {
            e.printStackTrace();
        }
    }

    private void processFourCollection(String id, GraphResponse<Storefront.QueryRoot> response, String tittle) {
        try {
            Storefront.Collection collectionEdge = (Storefront.Collection) response.getData().getNode();
            List<Storefront.ProductEdge> data = collectionEdge.getProducts().getEdges();
            List<Storefront.ProductEdge> dataFiltered = new ArrayList<>();

            if (data.size() > 0) {

                Observable.fromIterable(data).subscribeOn(Schedulers.io())
                        .filter(x -> x.getNode().getAvailableForSale())
                        .toList()
                        .observeOn(AndroidSchedulers.mainThread())
                        .subscribe(new SingleObserver<List<Storefront.ProductEdge>>() {
                            @Override
                            public void onSubscribe(Disposable d) {
                            }

                            @Override
                            public void onSuccess(List<Storefront.ProductEdge> productEdges) {
                                dataFiltered.addAll(productEdges);
                            }

                            @Override
                            public void onError(Throwable e) {
                            }
                        });
                fourthcollections.setVisibility(View.VISIBLE);
                fourthcatid.setText(id);
                fourthcatname.setText(Html.fromHtml(tittle));

                fourthcatname.setOnClickListener(new View.OnClickListener() {
                    @Override
                    public void onClick(View view) {
                        openCategory(fourthcatid.getText().toString(), fourthcatname.getText().toString());
                    }
                });

                fourthviewall.setText("Shop " + fourthcatname.getText().toString());

                fourthviewall.setOnClickListener(new View.OnClickListener() {
                    @Override
                    public void onClick(View view) {
                        openCategory(fourthcatid.getText().toString(), fourthcatname.getText().toString());
                    }
                });


                //done

                Log.d("dataFiltered " + tittle, "" + dataFiltered);
                fourthproducts.setHasFixedSize(true);
                LinearLayoutManager ll = new LinearLayoutManager(getActivity());
                ll.setOrientation(LinearLayoutManager.HORIZONTAL);
                fourthproducts.setLayoutManager(ll);
                fourthproducts.setNestedScrollingEnabled(false);
                final Handler handler = new Handler();
                handler.postDelayed(new Runnable() {
                    @Override
                    public void run() {
                        RelatedProducts_Adapter related = new RelatedProducts_Adapter(getActivity(), dataFiltered);
                        fourthproducts.setAdapter(related);
                        related.notifyDataSetChanged();
                    }
                }, 500);


            }
        } catch (Exception e) {
            e.printStackTrace();
        }
    }

    private void processFiveCollection(String id, GraphResponse<Storefront.QueryRoot> response, String tittle) {
        try {
            Storefront.Collection collectionEdge = (Storefront.Collection) response.getData().getNode();
            List<Storefront.ProductEdge> data = collectionEdge.getProducts().getEdges();
            List<Storefront.ProductEdge> dataFiltered = new ArrayList<>();
            Log.i("datasizzee", "five " + data.size());
            Log.i("datasizzee", "five " + tittle);
            if (data.size() > 0) {

                Observable.fromIterable(data).subscribeOn(Schedulers.io())
                        .filter(x -> x.getNode().getAvailableForSale())
                        .toList()
                        .observeOn(AndroidSchedulers.mainThread())
                        .subscribe(new SingleObserver<List<Storefront.ProductEdge>>() {
                            @Override
                            public void onSubscribe(Disposable d) {
                            }

                            @Override
                            public void onSuccess(List<Storefront.ProductEdge> productEdges) {
                                dataFiltered.addAll(productEdges);
                            }

                            @Override
                            public void onError(Throwable e) {
                            }
                        });

                fivecollections.setVisibility(View.VISIBLE);
                fivecatid.setText(id);
                fivecatname.setText("Shop " + Html.fromHtml(tittle));
                fivecatname.setOnClickListener(new View.OnClickListener() {
                    @Override
                    public void onClick(View view) {
                        openCategory(fivecatid.getText().toString(), fivecatname.getText().toString());
                    }
                });
                if (!fivecatname.getText().toString().contains("Shop")) {
                    fiveviewall.setText("Shop " + fivecatname.getText().toString());
                } else {
                    fiveviewall.setText(fivecatname.getText().toString());

                }

                fiveviewall.setOnClickListener(new View.OnClickListener() {
                    @Override
                    public void onClick(View view) {
                        openCategory(fivecatid.getText().toString(), fivecatname.getText().toString());
                    }
                });

                //done
                Log.d("dataFiltered " + tittle, "" + dataFiltered);
                fiveproducts.setHasFixedSize(true);
                LinearLayoutManager ll = new LinearLayoutManager(getActivity());
                ll.setOrientation(LinearLayoutManager.HORIZONTAL);
                fiveproducts.setLayoutManager(ll);
                fiveproducts.setNestedScrollingEnabled(false);
                final Handler handler = new Handler();
                handler.postDelayed(new Runnable() {
                    @Override
                    public void run() {
                        RelatedProducts_Adapter related = new RelatedProducts_Adapter(getActivity(), dataFiltered);
                        fiveproducts.setAdapter(related);
                        related.notifyDataSetChanged();
                    }
                }, 500);


            }
        } catch (Exception e) {
            e.printStackTrace();
        }
    }

    private void processSixCollection(String id, GraphResponse<Storefront.QueryRoot> response, String tittle) {
        try {
            Storefront.Collection collectionEdge = (Storefront.Collection) response.getData().getNode();
            List<Storefront.ProductEdge> data = collectionEdge.getProducts().getEdges();
            List<Storefront.ProductEdge> dataFiltered = new ArrayList<>();
            Log.i("datasizzee", "six " + data.size());
            if (data.size() > 0) {

                Observable.fromIterable(data).subscribeOn(Schedulers.io())
                        .filter(x -> x.getNode().getAvailableForSale())
                        .toList()
                        .observeOn(AndroidSchedulers.mainThread())
                        .subscribe(new SingleObserver<List<Storefront.ProductEdge>>() {
                            @Override
                            public void onSubscribe(Disposable d) {
                            }

                            @Override
                            public void onSuccess(List<Storefront.ProductEdge> productEdges) {
                                dataFiltered.addAll(productEdges);
                            }

                            @Override
                            public void onError(Throwable e) {
                            }
                        });
                sixcollections.setVisibility(View.VISIBLE);
                sixcatid.setText(id);
                sixcatname.setText(Html.fromHtml(tittle));
                sixcatname.setOnClickListener(new View.OnClickListener() {
                    @Override
                    public void onClick(View view) {
                        openCategory(sixcatid.getText().toString(), sixcatname.getText().toString());
                    }
                });
                if (!sixcatname.getText().toString().contains("Shop")) {
                    sixviewall.setText("Shop " + sixcatname.getText().toString());
                }
                sixviewall.setOnClickListener(new View.OnClickListener() {
                    @Override
                    public void onClick(View view) {
                        openCategory(sixcatid.getText().toString(), sixcatname.getText().toString());
                    }
                });

                //done
                Log.d("dataFiltered " + tittle, "" + dataFiltered);
                sixproducts.setHasFixedSize(true);
                LinearLayoutManager ll = new LinearLayoutManager(getActivity());
                ll.setOrientation(LinearLayoutManager.HORIZONTAL);
                sixproducts.setLayoutManager(ll);
                sixproducts.setNestedScrollingEnabled(false);
                final Handler handler = new Handler();
                handler.postDelayed(new Runnable() {
                    @Override
                    public void run() {
                        RelatedProducts_Adapter related = new RelatedProducts_Adapter(getActivity(), dataFiltered);
                        sixproducts.setAdapter(related);
                        related.notifyDataSetChanged();
                    }
                }, 500);

            }
        } catch (Exception e) {
            e.printStackTrace();
        }
    }

    private void processSecondCollection(String id, GraphResponse<Storefront.QueryRoot> response, String tittle) {
        try {
            Storefront.Collection collectionEdge = (Storefront.Collection) response.getData().getNode();
            List<Storefront.ProductEdge> data = collectionEdge.getProducts().getEdges();
            List<Storefront.ProductEdge> datafiltred = new ArrayList<>();
            if (data.size() > 0) {

                Observable.fromIterable(data).subscribeOn(Schedulers.io())
                        .filter(x -> x.getNode().getAvailableForSale())
                        .toList()
                        .observeOn(AndroidSchedulers.mainThread())
                        .subscribe(new SingleObserver<List<Storefront.ProductEdge>>() {
                            @Override
                            public void onSubscribe(Disposable d) {
                            }

                            @Override
                            public void onSuccess(List<Storefront.ProductEdge> productEdges) {
                                datafiltred.addAll(productEdges);
                            }

                            @Override
                            public void onError(Throwable e) {
                            }
                        });

                secondcollections.setVisibility(View.VISIBLE);
                secondcatid.setText(id);
                secondcatname.setText(Html.fromHtml(tittle)

                );
                secondcatname.setOnClickListener(new View.OnClickListener() {
                    @Override
                    public void onClick(View view) {
                        openCategory(secondcatid.getText().toString(), secondcatname.getText().toString());
                    }
                });

                secondviewall.setText("Shop " + secondcatname.getText().toString());

                secondviewall.setOnClickListener(new View.OnClickListener() {
                    @Override
                    public void onClick(View view) {
                        openCategory(secondcatid.getText().toString(), secondcatname.getText().toString());
                    }
                });

                //done

                secondproducts.setHasFixedSize(true);
                LinearLayoutManager ll = new LinearLayoutManager(getActivity());
                ll.setOrientation(LinearLayoutManager.HORIZONTAL);
                secondproducts.setLayoutManager(ll);
                secondproducts.setNestedScrollingEnabled(false);
                Log.d("dataFiltered " + tittle, "" + datafiltred);

                final Handler handler = new Handler();
                handler.postDelayed(new Runnable() {
                    @Override
                    public void run() {
                        RelatedProducts_Adapter related = new RelatedProducts_Adapter(getActivity(), datafiltred);
                        secondproducts.setAdapter(related);
                        related.notifyDataSetChanged();
                    }
                }, 500);

              /*  RelatedProducts_Adapter related = new RelatedProducts_Adapter(getActivity(), datafiltred);
                secondproducts.setAdapter(related);
                related.notifyDataSetChanged();*/
            }
        } catch (Exception e) {
            e.printStackTrace();
        }
    }

    private void processThirdCollection(String id, GraphResponse<Storefront.QueryRoot> response, String tittle) {
        try {
            Storefront.Collection collectionEdge = (Storefront.Collection) response.getData().getNode();
            List<Storefront.ProductEdge> data = collectionEdge.getProducts().getEdges();
            List<Storefront.ProductEdge> dataFiltered = new ArrayList<>();
            if (data.size() > 0) {

                Observable.fromIterable(data).subscribeOn(Schedulers.io())
                        .filter(x -> x.getNode().getAvailableForSale())
                        .toList()
                        .observeOn(AndroidSchedulers.mainThread())
                        .subscribe(new SingleObserver<List<Storefront.ProductEdge>>() {
                            @Override
                            public void onSubscribe(Disposable d) {
                            }

                            @Override
                            public void onSuccess(List<Storefront.ProductEdge> productEdges) {
                                dataFiltered.addAll(productEdges);
                            }

                            @Override
                            public void onError(Throwable e) {
                            }
                        });

                thirdcollections.setVisibility(View.VISIBLE);
                thirdcatid.setText(id);
                thirdcatname.setText(Html.fromHtml(tittle)

                );
                thirdcatname.setOnClickListener(new View.OnClickListener() {
                    @Override
                    public void onClick(View view) {
                        openCategory(thirdcatid.getText().toString(), thirdcatname.getText().toString());
                    }
                });

                viewall.setText("Shop " + thirdcatname.getText().toString());

                viewall.setOnClickListener(new View.OnClickListener() {
                    @Override
                    public void onClick(View view) {
                        openCategory(thirdcatid.getText().toString(), thirdcatname.getText().toString());
                    }
                });

                //done
                Log.d("dataFiltered " + tittle, "" + dataFiltered);
                products.setHasFixedSize(true);
                products.setLayoutManager(new LinearLayoutManager(getActivity(), LinearLayoutManager.HORIZONTAL, false));
                products.setNestedScrollingEnabled(false);
                final Handler handler = new Handler();
                handler.postDelayed(new Runnable() {
                    @Override
                    public void run() {
                        RelatedProducts_Adapter related = new RelatedProducts_Adapter(getActivity(), dataFiltered);
                        products.setAdapter(related);
                        related.notifyDataSetChanged();
                    }
                }, 500);

            }
        } catch (Exception e) {
            e.printStackTrace();
        }
    }

    private void processStaggeredGrid(String id, GraphResponse<Storefront.QueryRoot> response, String tittle) {
        try {
            Storefront.Collection collectionEdge = (Storefront.Collection) response.getData().getNode();
            List<Storefront.ProductEdge> data = collectionEdge.getProducts().getEdges();
            List<Storefront.ProductEdge> dataFiltered = new ArrayList<>();
            if (data.size() > 0) {
                Observable.fromIterable(data).subscribeOn(Schedulers.io())
                        .filter(x -> x.getNode().getAvailableForSale())
                        .toList()
                        .observeOn(AndroidSchedulers.mainThread())
                        .subscribe(new SingleObserver<List<Storefront.ProductEdge>>() {
                            @Override
                            public void onSubscribe(Disposable d) {
                            }

                            @Override
                            public void onSuccess(List<Storefront.ProductEdge> productEdges) {
                                dataFiltered.addAll(productEdges);
                            }

                            @Override
                            public void onError(Throwable e) {
                            }
                        });
                staggeredcatname.setText(Html.fromHtml(tittle));
                staggeredcatid.setText(id);
                staggeredcatname.setOnClickListener(new View.OnClickListener() {
                    @Override
                    public void onClick(View view) {
                        openCategory(staggeredcatid.getText().toString(), staggeredcatname.getText().toString());
                    }
                });

                sectionviewall.setText("Shop " + staggeredcatname.getText().toString());

                sectionviewall.setOnClickListener(new View.OnClickListener() {
                    @Override
                    public void onClick(View view) {
                        openCategory(staggeredcatid.getText().toString(), staggeredcatname.getText().toString());
                    }
                });

                //done

                LinearLayoutManager linearLayoutManager = new LinearLayoutManager(getActivity(), LinearLayoutManager.HORIZONTAL, false);
                staggeredproducts.setLayoutManager(linearLayoutManager);
                final Handler handler = new Handler();
                handler.postDelayed(new Runnable() {
                    @Override
                    public void run() {
                        RelatedProducts_Adapter related = new RelatedProducts_Adapter(getActivity(), dataFiltered);
                        staggeredproducts.setAdapter(related);
                        staggaredgridsection.setVisibility(View.VISIBLE);
                    }
                }, 500);

            }
        } catch (Exception e) {
            e.printStackTrace();
        }
    }

    private void process9Grids(String id, GraphResponse<Storefront.QueryRoot> response, String tittle) {
        try {
            Storefront.Collection collectionEdge = (Storefront.Collection) Objects.requireNonNull(response.getData()).getNode();
            grid9data = collectionEdge.getProducts().getEdges();
            Log.i("grid9imagesection", "" + grid9data.size());
            Log.i("grid9imagesection1", "" + collectionEdge.getProducts().getEdges().size());
            if (grid9data.size() > 0) {
                grid9tittle.setText(Html.fromHtml(tittle));
                grid9tittle.setOnClickListener(new View.OnClickListener() {
                    @Override
                    public void onClick(View view) {
                        openCategory(catid.getText().toString(), grid9tittle.getText().toString());
                    }
                });
                catviewallcard.setOnClickListener(new View.OnClickListener() {
                    @Override
                    public void onClick(View view) {
                        openCategory(catid.getText().toString(), grid9tittle.getText().toString());
                    }
                });
                catid.setText(id);

                Observable.fromIterable(grid9data).subscribeOn(Schedulers.io())
                        .filter(x -> x.getNode().getAvailableForSale())
                        .toList()
                        .observeOn(AndroidSchedulers.mainThread())
                        .subscribe(new SingleObserver<List<Storefront.ProductEdge>>() {
                            @Override
                            public void onSubscribe(Disposable d) {
                            }

                            @Override
                            public void onSuccess(List<Storefront.ProductEdge> productEdges) {
                                grid9data.addAll(productEdges);
                            }

                            @Override
                            public void onError(Throwable e) {
                            }
                        });//done


                Iterator<Storefront.ProductEdge> iterator = grid9data.iterator();
                int counter = 0;
                Storefront.ProductEdge edge;
                while (iterator.hasNext()) {
                    edge = iterator.next();
                    counter = counter + 1;
                    /*Log.i("getTransformedSrc1",""+edge.getNode().getImages().getEdges());
                    Log.i("getTransformedSrc2",""+edge.getNode().getImages().getEdges().get(0));
                    Log.i("getTransformedSrc3",""+edge.getNode().getImages().getEdges().get(0).getNode());
                    Log.i("getTransformedSrc4",""+edge.getNode().getImages().getEdges().get(0).getNode().getTransformedSrc());*/
                    gridData(counter, edge.getNode().getImages().getEdges().get(0).getNode().getTransformedSrc(), edge.getNode().getId().toString());
                    //gridData(counter,"https://wow.olympus.eu/webfile/img/1632/oly_testwow_stage.jpg",edge.getNode().getId().toString());
                }
                grid9imagesection.setVisibility(View.VISIBLE);
                catviewallcard.setVisibility(View.VISIBLE);
            }

        } catch (Exception e) {
            e.printStackTrace();
        }
    }

    private void openCategory(String id, String name) {
        try {
            Intent intent = new Intent(getActivity(), ProductListing.class);
            intent.putExtra("cat_id", id);
            intent.putExtra("isFrombrands", "false");
            intent.putExtra("cat_name", name);
            startActivity(intent);
            getActivity().overridePendingTransition(R.anim.magenative_slide_in, R.anim.magenative_slide_out);
        } catch (Exception e) {
            e.printStackTrace();
        }
    }

    private void openProduct(String origin, int position, String id) {
        try {
            Intent intent = new Intent(getActivity(), ProductView.class);
            intent.putExtra("object", (Serializable) grid9data.get(position));
            intent.putExtra("id", id);
            startActivity(intent);
            Animatoo.animateZoom(getActivity());
            //getActivity().overridePendingTransition(R.anim.magenative_slide_in, R.anim.magenative_slide_out);
        } catch (Exception e) {
            e.printStackTrace();
        }
    }

    @Override
    public void onDestroyView() {
        try {
            super.onDestroyView();
            unbinder.unbind();
        } catch (Exception e) {
            e.printStackTrace();
        }
    }

    public void pageSwitcher(int seconds) {
        timer = new Timer(); // At this line a new Thread will be created
        timer.scheduleAtFixedRate(new RemindTask(), 0, seconds * 1000); // delay
        // in
        // milliseconds
    }

    public String getBase64Encode(String id) {
        byte[] data = Base64.encode(id.getBytes(), Base64.DEFAULT);
        try {
            id = new String(data, "UTF-8").trim();
        } catch (UnsupportedEncodingException e) {
            e.printStackTrace();
        }
        return id;
    }

    public void gridData(int index, String url, String id) {
        try {
            switch (index) {
                case 1:
                    cat1id.setText(id);
                    cat1section.setVisibility(View.VISIBLE);
                    Glide.with(getActivity())
                            .load(url)
                            .apply(new RequestOptions().placeholder(R.drawable.placeholder).error(R.drawable.placeholder).dontTransform())
                            .into(cat1image);
                    cat1section.setOnClickListener(new View.OnClickListener() {
                        @Override
                        public void onClick(View view) {
                            openProduct("9grid", 0, cat1id.getText().toString());
                        }
                    });
                    break;
                case 2:
                    cat2id.setText(id);
                    cat2section.setVisibility(View.VISIBLE);
                    Glide.with(getActivity())
                            .load(url)
                            .apply(new RequestOptions().placeholder(R.drawable.placeholder).error(R.drawable.placeholder).dontTransform())
                            .into(cat2image);
                    cat2section.setOnClickListener(new View.OnClickListener() {
                        @Override
                        public void onClick(View view) {
                            openProduct("9grid", 1, cat2id.getText().toString());
                        }
                    });
                    break;
                case 3:
                    cat3id.setText(id);
                    cat3section.setVisibility(View.VISIBLE);
                    Glide.with(getActivity())
                            .load(url)
                            .apply(new RequestOptions().placeholder(R.drawable.placeholder).error(R.drawable.placeholder).dontTransform())
                            .into(cat3image);
                    cat3section.setOnClickListener(new View.OnClickListener() {
                        @Override
                        public void onClick(View view) {
                            openProduct("9grid", 2, cat3id.getText().toString());
                        }
                    });
                    break;
                case 4:
                    cat4id.setText(id);
                    cat4section.setVisibility(View.VISIBLE);
                    Glide.with(getActivity())
                            .load(url)
                            .apply(new RequestOptions().placeholder(R.drawable.placeholder).error(R.drawable.placeholder).dontTransform())
                            .into(cat4image);
                    cat4section.setOnClickListener(new View.OnClickListener() {
                        @Override
                        public void onClick(View view) {
                            openProduct("9grid", 3, cat4id.getText().toString());
                        }
                    });
                    break;
                case 5:
                    cat5id.setText(id);
                    cat5section.setVisibility(View.VISIBLE);
                    Glide.with(getActivity())
                            .load(url)
                            .apply(new RequestOptions().placeholder(R.drawable.placeholder).error(R.drawable.placeholder).dontTransform())
                            .into(cat5image);
                    cat5section.setOnClickListener(new View.OnClickListener() {
                        @Override
                        public void onClick(View view) {
                            openProduct("9grid", 4, cat5id.getText().toString());
                        }
                    });
                    break;
                case 6:
                    cat6id.setText(id);
                    cat6section.setVisibility(View.VISIBLE);
                    Glide.with(getActivity())
                            .load(url)
                            .apply(new RequestOptions().placeholder(R.drawable.placeholder).error(R.drawable.placeholder).dontTransform())
                            .into(cat6image);
                    cat6section.setOnClickListener(new View.OnClickListener() {
                        @Override
                        public void onClick(View view) {
                            openProduct("9grid", 5, cat6id.getText().toString());
                        }
                    });
                    break;
                case 7:
                    cat7id.setText(id);
                    cat7section.setVisibility(View.VISIBLE);
                    Glide.with(getActivity())
                            .load(url)
                            .apply(new RequestOptions().placeholder(R.drawable.placeholder).error(R.drawable.placeholder).dontTransform())
                            .into(cat7image);
                    cat7section.setOnClickListener(new View.OnClickListener() {
                        @Override
                        public void onClick(View view) {
                            openProduct("9grid", 6, cat7id.getText().toString());
                        }
                    });
                    break;
                case 8:
                    cat8id.setText(id);
                    cat8section.setVisibility(View.VISIBLE);
                    Glide.with(getActivity())
                            .load(url)
                            .apply(new RequestOptions().placeholder(R.drawable.placeholder).error(R.drawable.placeholder).dontTransform())
                            .into(cat8image);
                    cat8section.setOnClickListener(new View.OnClickListener() {
                        @Override
                        public void onClick(View view) {
                            openProduct("9grid", 7, cat8id.getText().toString());
                        }
                    });
                    break;
                case 9:
                    cat9id.setText(id);
                    cat9section.setVisibility(View.VISIBLE);
                    Glide.with(getActivity())
                            .load(url)
                            .apply(new RequestOptions().placeholder(R.drawable.placeholder).error(R.drawable.placeholder).dontTransform())
                            .into(cat9image);
                    cat9section.setOnClickListener(new View.OnClickListener() {
                        @Override
                        public void onClick(View view) {
                            openProduct("9grid", 8, cat9id.getText().toString());
                        }
                    });
                    break;

            }
        } catch (Exception e) {
            e.printStackTrace();
        }
    }

    @Override
    public void onResume() {
        super.onResume();
        Log.i("Recent", "IN10");
        Log.i("Recent", "IN10" + data.getRecentlyViewed());
        if (data.getRecentlyViewed() != null) {
            recentsproducts.setHasFixedSize(true);
            recentsproducts.setLayoutManager(new LinearLayoutManager(getActivity(), LinearLayoutManager.HORIZONTAL, true));
            //GridLayoutManager mLayoutManager =new GridLayoutManager(getActivity(), 2);
            //mLayoutManager.setReverseLayout(true);
            //recentsproducts.setLayoutManager(mLayoutManager);
            recentsproducts.setNestedScrollingEnabled(false);
            RecentlyViewedProducts_Adapter related = new RecentlyViewedProducts_Adapter(getActivity(), data.getRecentlyViewed());
            recentsproducts.setAdapter(related);
            related.notifyDataSetChanged();
            // recentlyviewedsection.setVisibility(View.VISIBLE);
        } else {
            Log.i("Recent", "IN");
            recentlyviewedsection.setVisibility(View.GONE);
        }
/*
        if (collection_widget != null) {
            try {
                fetchCollectionData(collection_widget.getJSONObject(2).getString("id"), "thirdcollection", 10, collection_widget.getJSONObject(2).getString("title"));
            } catch (Exception e) {
                e.printStackTrace();
            }

        }
*/
    }

    class RemindTask extends TimerTask {

        @Override
        public void run() {

            // As the TimerTask run on a seprate thread from UI thread we have
            // to call runOnUiThread to do work on UI thread.
            if (getActivity() == null)
                return;
            getActivity().runOnUiThread(new Runnable() {
                public void run() {
                    if (MageNative_homepagebanner != null) {
                        if (key == 0) {
                            if (page > banners_widget.length()) {
                                page = 0;
                                MageNative_homepagebanner.setCurrentItem(page++);
                            } else {
                                MageNative_homepagebanner.setCurrentItem(page++);
                            }
                        } else {
                            if (page > collection_widget.length()) {
                                page = 0;
                                MageNative_homepagebanner.setCurrentItem(page++);
                            } else {
                                MageNative_homepagebanner.setCurrentItem(page++);
                            }
                        }

                    }

                }
            });

        }
    }

    class RemindTask2 extends TimerTask {

        @Override
        public void run() {

            // As the TimerTask run on a seprate thread from UI thread we have
            // to call runOnUiThread to do work on UI thread.
            if (getActivity() == null)
                return;
            getActivity().runOnUiThread(new Runnable() {
                public void run() {

                    if (page2 > banners_widget2.length()) {
                        page2 = 0;
                        middlebanner.setCurrentItem(page2++);
                    } else {
                        middlebanner.setCurrentItem(page2++);
                    }
                }
            });

        }
    }
}

