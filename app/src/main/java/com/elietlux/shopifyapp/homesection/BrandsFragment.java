package com.elietlux.shopifyapp.homesection;
import android.annotation.SuppressLint;
import android.content.Intent;
import android.graphics.drawable.ColorDrawable;
import android.os.Bundle;
import android.support.annotation.NonNull;
import android.support.annotation.Nullable;
import android.support.v4.app.Fragment;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.AbsListView;
import android.widget.AdapterView;
import android.widget.ListView;
import android.widget.TextView;

import com.shopify.buy3.GraphCallResult;
import com.shopify.buy3.GraphClient;
import com.shopify.buy3.GraphResponse;
import com.shopify.buy3.QueryGraphCall;
import com.shopify.buy3.Storefront;
import com.shopify.buy3.Storefront.QueryRoot;
import com.elietlux.shopifyapp.R;
import com.elietlux.shopifyapp.adaptersection.BrandsAdapter;
import com.elietlux.shopifyapp.adaptersection.Collections_Adapter;
import com.elietlux.shopifyapp.loadersection.CustomProgressDialog;
import com.elietlux.shopifyapp.productlistingsection.ProductListing;
import com.elietlux.shopifyapp.requestsection.ApiClient;
import com.elietlux.shopifyapp.requestsection.ApiInterface;
import com.elietlux.shopifyapp.storefrontqueries.Query;
import com.elietlux.shopifyapp.storefrontresponse.AsyncResponse;
import com.elietlux.shopifyapp.storefrontresponse.Response;
import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;
import java.util.Iterator;
import java.util.List;
import java.util.Objects;

import butterknife.BindView;
import butterknife.ButterKnife;
import butterknife.Unbinder;
import in.myinnos.alphabetsindexfastscrollrecycler.IndexFastScrollRecyclerView;
import okhttp3.ResponseBody;
import retrofit2.Call;
import retrofit2.Retrofit;
import timber.log.Timber;



public class  BrandsFragment extends Fragment
{
    @Nullable
    Unbinder unbinder;
    @Nullable
    @BindView(R.id.categories_list)
    IndexFastScrollRecyclerView categories_list;



    GraphClient client=null;
    ApiInterface apiClient = null;
    String cursor="nocursor";
    boolean hasNextPage=false;
    //JSONArray array=null;
    List<Storefront.CollectionEdge> edges=null;

    JSONArray all;

    @Override
    public View onCreateView(@NonNull LayoutInflater inflater, ViewGroup container, Bundle savedInstanceState)
    {
        View view = inflater.inflate(R.layout.magenative_categories, container, false);
        unbinder = ButterKnife.bind(this, view);

        //array=new JSONArray();
        // categories_list.setDivider(new ColorDrawable(getResources().getColor(R.color.transparent)));
        //categories_list.setDividerHeight(0);

        client= ApiClient.getGraphClient(getActivity(),true);
        apiClient = ApiClient.getClient(getActivity()).create(ApiInterface.class);
        Call<ResponseBody> call = apiClient.getcollectionproperties(getResources().getString(R.string.mid),"vendors");
        categories_list.setLayoutManager(new LinearLayoutManager(getActivity()));
        categories_list.setIndexBarColor("#FFFFFF");
        categories_list.setIndexBarTextColor("#000000");






        // getCollections(cursor);
        getBrands(call);
       /* categories_list.setOnItemClickListener(new AdapterView.OnItemClickListener()
        {
            @Override
            public void onItemClick(AdapterView<?> adapterView, View view, int i, long l)
            {
                try
                {
                    TextView id=view.findViewById(R.id.cat_id);
                    TextView cat_title=view.findViewById(R.id.cat_title);
                    Intent intent=new Intent(getActivity(), ProductListing.class);
                    intent.putExtra("cat_id",id.getText().toString());
                    intent.putExtra("cat_name",cat_title.getText().toString());
                    startActivity(intent);
                    getActivity().overridePendingTransition(R.anim.magenative_slide_in, R.anim.magenative_slide_out);
                }
                catch (Exception e)
                {
                    e.printStackTrace();
                }


            }
        });*/
        /*categories_list.setOnScrollListener(new AbsListView.OnScrollListener()
        {
            @Override
            public void onScrollStateChanged(AbsListView absListView, int i)
            {

            }
            @Override
            public void onScroll(AbsListView absListView, int firstVisibleItem, int visibleItemCount, int totalItemCount)
            {
                if ((firstVisibleItem + visibleItemCount) != 0)
                {
                    if (((firstVisibleItem + visibleItemCount) == totalItemCount) && hasNextPage)
                    {
                        hasNextPage=false;
                        getCollections(cursor);
                    }
                }
            }
        });*/



        return  view;
    }

    private void getBrands(Call<ResponseBody> call) {
        try {
            Response.getRetrofitResponse(call, new AsyncResponse() {
                @Override
                public void finalOutput(@NonNull Object output, @NonNull boolean error) {
                    if (error) {
                        if (CustomProgressDialog.mDialog != null) {
                            CustomProgressDialog.mDialog.dismiss();
                            CustomProgressDialog.mDialog = null;
                        }
                        creatBrands(output.toString());
                    } else {
                        Log.i("ErrorHomePage", "" + output.toString());
                    }
                }
            }, getActivity());
        } catch (Exception e) {
            e.printStackTrace();
        }
    }

    private void creatBrands(String toString) {
        try {
            JSONObject main = new JSONObject(toString);
            JSONObject data = main.getJSONObject("data");
            all = data.getJSONArray("all");
            Log.d("vaibhav",""+all.toString());

            BrandsAdapter adapter = new BrandsAdapter(getActivity(), all);
            categories_list.setAdapter(adapter);
            adapter.notifyDataSetChanged();




        } catch (JSONException e) {
            e.printStackTrace();
        }
    }

    private void getCollections(String cursor)
    {
        try
        {
            Timber.i("Cursor%s", cursor);
            QueryGraphCall call = client.queryGraph(Query.getCollections(cursor));
            Response.getGraphQLResponse(call,new AsyncResponse()
            {
                @Override
                public void finalOutput(@NonNull Object output,@NonNull boolean error )
                {
                    if(error)
                    {
                        GraphResponse<QueryRoot> response  = ((GraphCallResult.Success<QueryRoot>) output).getResponse();
                        Objects.requireNonNull(getActivity()).runOnUiThread(new Runnable()
                        {
                            @Override
                            public void run()
                            {
                                if(CustomProgressDialog.mDialog!=null)
                                {
                                    CustomProgressDialog.mDialog.dismiss();
                                    CustomProgressDialog.mDialog=null;
                                }
                                processCollections(response);
                            }
                        });
                    }
                    else
                    {
                        Log.i("ResponseError32",""+output.toString());
                        hasNextPage=false;
                    }
                }
            },getActivity());
        }
        catch (Exception e)
        {
            e.printStackTrace();
        }

    }

    @SuppressLint("TimberArgCount")
    private void processCollections(GraphResponse<QueryRoot> response)
    {
        try
        {
            hasNextPage= Objects.requireNonNull(response.getData()).getCollections().getPageInfo().getHasNextPage();
            Timber.i("hasNextPage", "%s", hasNextPage);
            List<Storefront.CollectionEdge>  data=response.getData().getCollections().getEdges();
            if(edges==null)
            {
                edges=data;
            }
            else
            {
                edges.addAll(data);
            }
            cursor=edges.get(edges.size()-1).getCursor();

           /* Collections_Adapter adapter = new Collections_Adapter(getActivity(), edges);
            int cp = Objects.requireNonNull(categories_list).getFirstVisiblePosition();
            categories_list.setAdapter(adapter);
            categories_list.setSelection(cp);
            adapter.notifyDataSetChanged();*/
        }
        catch (Exception e)
        {
            e.printStackTrace();
        }
    }
    @Override
    public void onDestroyView()
    {
        try
        {
            super.onDestroyView();
            unbinder.unbind();
        }
        catch (Exception e)
        {
            e.printStackTrace();
        }
    }

    private int getPositionFromData(String character) {
        int position = 0;
        for (String s : toStringArray(all)) {
            String letter = "" + s.charAt(0);
            if (letter.equals("" + character)) {
                return position;
            }
            position++;
        }
        return 0;
    }

    public static String[] toStringArray(JSONArray array) {
        if(array==null)
            return null;

        String[] arr=new String[array.length()];
        for(int i=0; i<arr.length; i++) {
            arr[i]=array.optString(i);
        }
        return arr;
    }
}
