package com.elietlux.shopifyapp.homesection;

import android.content.Intent;
import android.os.Bundle;
import android.support.annotation.NonNull;
import android.support.annotation.Nullable;
import android.support.v4.app.Fragment;
import android.support.v7.widget.LinearLayoutCompat;
import android.util.Base64;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.TextView;
import android.widget.Toast;

import com.blogspot.atifsoftwares.animatoolib.Animatoo;
import com.shopify.buy3.GraphClient;
import com.shopify.buy3.Storefront;
import com.elietlux.shopifyapp.R;
import com.elietlux.shopifyapp.loadersection.Loader;
import com.elietlux.shopifyapp.productlistingsection.ProductListing;
import com.elietlux.shopifyapp.requestsection.ApiClient;
import com.elietlux.shopifyapp.requestsection.ApiInterface;
import com.elietlux.shopifyapp.storagesection.LocalData;
import com.elietlux.shopifyapp.storefrontresponse.AsyncResponse;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import java.io.UnsupportedEncodingException;
import java.util.List;
import java.util.Timer;

import butterknife.BindView;
import butterknife.ButterKnife;
import butterknife.Unbinder;
import okhttp3.ResponseBody;
import retrofit2.Call;

public class MenWomenFragmentCategory extends Fragment {
    @Nullable
    Unbinder unbinder;
    ApiInterface apiService;

    @Nullable
    @BindView(R.id.mainLinear)
    LinearLayoutCompat mainLinear;


    List<Storefront.ProductEdge> grid9data;
    Timer timer;
    Timer timer2;
    int page = 0;
    int page2 = 0;
    JSONArray banners_widget;
    JSONArray collection_widget = null;
    JSONArray banners_widget2;
    GraphClient client;
    LocalData data = null;
    Loader loader = null;
    Call<ResponseBody> callCollectionData = null;

    @Override
    public View onCreateView(@NonNull LayoutInflater inflater, ViewGroup container, Bundle savedInstanceState) {
        View view = inflater.inflate(R.layout.new_layout_hompagecategory, container, false);
        unbinder = ButterKnife.bind(this, view);
        apiService = ApiClient.getClient(getActivity()).create(ApiInterface.class);
        client = ApiClient.getGraphClient(getActivity(), true);
        Call<ResponseBody> call = apiService.getHomePage(getResources().getString(R.string.mid));

        data = new LocalData(getActivity());
        loader = new Loader(getActivity());
        loader.show();
        if (!getArguments().isEmpty()) {
            int key = getArguments().getInt("key");
            Log.d("Arguments", getArguments().getInt("key") + "");
            switch (key) {
                case 0:
                    callCollectionData = apiService.getCategoryMenus(getResources().getString(R.string.mid), "apps-categories-men");

                    break;
                case 1:
                    callCollectionData = apiService.getCategoryMenus(getResources().getString(R.string.mid), "apps-categories-women");
                    break;
            }
        }
        getResponseCollectionsData(callCollectionData);
        //getResponseBanners(call);
        // bindCategories();
        return view;
    }

    private void getResponseCollectionsData(Call<ResponseBody> call) {
        try {
            com.elietlux.shopifyapp.storefrontresponse.Response.getRetrofitResponse(call, new AsyncResponse() {
                @Override
                public void finalOutput(@NonNull Object output, @NonNull boolean error) {
                    if (error) {
                        loader.dismiss();
                        /*processMenus(output.toString());*/
                        //  processCollectionsData(output.toString());
                        processCategories(output.toString());
                        Log.d("vaibhav", "" + output);
                    } else {
                        Log.i("vaibhav", "" + output.toString());
                    }
                }
            }, getActivity());
        } catch (Exception e) {
            e.printStackTrace();
        }
    }

    private void processCategories(String toString) {
        try {
            JSONObject jsonObject = new JSONObject(toString);
            JSONArray data = jsonObject.getJSONArray("data");
            View category = null;
            TextView id = null;
            ImageView arrow = null;
            TextView categorytitle = null;
            for (int i = 0; i < data.length(); i++) {
                JSONObject inner = data.getJSONObject(i);
                category = getLayoutInflater().inflate(R.layout.category, null);
                id = category.findViewById(R.id.id);
                categorytitle = category.findViewById(R.id.categorytitle);
                arrow = category.findViewById(R.id.arrow);

                if (inner.has("id")) {
                    id.setText(inner.getString("id").toString());
                }

                if (inner.has("menus")) {
                    arrow.setVisibility(View.VISIBLE);
                } else {
                    arrow.setVisibility(View.GONE);
                }
                categorytitle.setText(inner.getString("title"));

                category.setOnClickListener(new View.OnClickListener() {
                    @Override
                    public void onClick(View v) {

                        try {
                            if (inner.has("menus")) {
                                Intent in = new Intent(getActivity(), Subcats.class);
                                in.putExtra("menus", inner.getString("menus"));
                                startActivity(in);
                                Animatoo.animateFade(getActivity());
                                getActivity().overridePendingTransition(R.anim.magenative_slide_in, R.anim.magenative_slide_out);

                            }else{

                                String s1 = "gid://shopify/Collection/" +  inner.getString("id");
                                Intent intent=new Intent(getActivity(), ProductListing.class);
                                intent.putExtra("cat_id",getBase64Encode(s1));
                                intent.putExtra("isFrombrands","false");
                                intent.putExtra("cat_name",inner.getString("title"));
                                //startActivity(intent);
                               //Animatoo.animateFade(getActivity());
                                getActivity().overridePendingTransition(R.anim.magenative_slide_in, R.anim.magenative_slide_out);
                            }

                        } catch (JSONException e) {
                            e.printStackTrace();
                        }
                    }
                });
                mainLinear.addView(category);


            }

        } catch (JSONException e) {
            e.printStackTrace();
        }

    }

    @Override
    public void onDestroyView() {
        try {
            super.onDestroyView();
            unbinder.unbind();
        } catch (Exception e) {
            e.printStackTrace();
        }
    }

    public String getBase64Encode(String id) {
        byte[] data = Base64.encode(id.getBytes(), Base64.DEFAULT);
        try {
            id = new String(data, "UTF-8").trim();
        } catch (UnsupportedEncodingException e) {
            e.printStackTrace();
        }
        return id;
    }


}

