package com.elietlux.shopifyapp.currencysection;

import android.util.Log;
import java.lang.Double;
import java.text.NumberFormat;
import java.util.Currency;
import java.util.Locale;
public class NewCurrencyFormatter {

    public static String setsymbol(String data, String currency_symbol){
        Log.i("MageNative", "Amount : $data "+data);
        Log.i("MageNative", "Amount : $data "+currency_symbol);
        NumberFormat format = NumberFormat.getCurrencyInstance(Locale.ENGLISH);
        format.setCurrency(Currency.getInstance(currency_symbol));
        return format.format(Double.valueOf(data));
    }
}
