package com.elietlux.shopifyapp.currencysection;
import android.content.Context;
import android.util.Log;

import com.elietlux.shopifyapp.storagesection.LocalData;

import org.json.JSONObject;

import java.math.BigDecimal;
import java.math.RoundingMode;
import java.text.NumberFormat;
import java.util.Currency;
import java.util.Locale;
public class CurrencyFormatter {

    public static String setsymbol(BigDecimal data, Context context,String currency_symbol)
    {
        String result = null;
        try
        {
            NumberFormat format = NumberFormat.getCurrencyInstance(Locale.ENGLISH);
            LocalData localData=new LocalData(context);
            JSONObject jsonObject=new JSONObject(localData.getCurrencyRates());
            JSONObject rates=jsonObject.getJSONObject("data").getJSONObject("rates");
            Log.i("CurrentSymbol",""+localData.getCurrencyCode());
            Log.i("defaultSymbol",""+currency_symbol);

            double currentrating=rates.getDouble(localData.getCurrencyCode());
            double bydefaultrating=rates.getDouble(currency_symbol);
            BigDecimal ratevalue= BigDecimal.valueOf(currentrating/bydefaultrating);

            Log.d("currcal currentrating",""+currentrating);
            Log.d("currcal bydefaultrating",""+bydefaultrating);
            Log.d("currcal ratevalue",""+ratevalue);
            Log.i("convertedmulrate",""+ratevalue);

            BigDecimal pricedata=data.divide(ratevalue,2, RoundingMode.HALF_UP);

            Log.i("convertedprice",""+pricedata);
            data=pricedata;
            if(localData.getCurrencyCode()!=null)
            {
                format.setCurrency(Currency.getInstance(localData.getCurrencyCode()));
                result = format.format(data);
            }
            else
            {
                format.setCurrency(Currency.getInstance(currency_symbol));
                result = format.format(data);
            }
        }
        catch (Exception e)
        {
            e.printStackTrace();
        }

        return result;
    }
}
