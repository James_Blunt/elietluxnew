package com.elietlux.shopifyapp.datasection;
import android.content.Context;
import com.elietlux.shopifyapp.R;
import org.json.JSONArray;
import org.json.JSONObject;

import java.lang.ref.WeakReference;
public class Data
{
    static WeakReference<Context> con;
    public  static JSONArray getSortOptions(Context context)
    {
        con=new WeakReference<Context>(context);
        JSONArray array=new JSONArray();
        array.put(con.get().getResources().getString(R.string.name_a_to_z));
        array.put(con.get().getResources().getString(R.string.name_z_to_a));
        array.put(con.get().getResources().getString(R.string.price_h_to_l));
        array.put(con.get().getResources().getString(R.string.price_l_to_h));
        array.put(con.get().getResources().getString(R.string.best_selling));
        array.put(con.get().getResources().getString(R.string.latest_collection));
        return array;
    }
    public  static JSONArray getSortOptionsAllProducts(Context context)
    {
        con=new WeakReference<Context>(context);
        JSONArray array=new JSONArray();
        array.put(con.get().getResources().getString(R.string.name_a_to_z));
        array.put(con.get().getResources().getString(R.string.name_z_to_a));
        array.put(con.get().getResources().getString(R.string.latest_collection));
        return array;
    }
    public  static JSONArray getLangauages( )
    {

        JSONArray array=new JSONArray();
        try
        {
            JSONObject object1=new JSONObject();
            object1.put("name","English");
            object1.put("code","en");
            array.put(object1);
            JSONObject object2=new JSONObject();
            object2.put("name","Arabic");
            object2.put("code","ar");
            array.put(object2);
        }
        catch (Exception e)

        {
            e.printStackTrace();
        }

        return array;
    }

    public  static JSONArray getCountry( )
    {

        JSONArray array=new JSONArray();
        try
        {
            JSONObject object1=new JSONObject();
            object1.put("name","India");
            object1.put("code","en");
            array.put(object1);

            JSONObject object2=new JSONObject();
            object2.put("name","Pakistan");
            object2.put("code","es");
            array.put(object2);
        }
        catch (Exception e)
        {
            e.printStackTrace();
        }
        return array;
    }
    public  static JSONArray getCurrency()
    {
        JSONArray array=new JSONArray();
        try
        {
            JSONObject object8=new JSONObject();
            object8.put("name","United States dollar");
            object8.put("code","USD");
            array.put(object8);

            JSONObject object3=new JSONObject();
            object3.put("name","Euro");
            object3.put("code","EUR");
            array.put(object3);

            JSONObject object1=new JSONObject();
            object1.put("name","UAE");
            object1.put("code","AED");
            array.put(object1);

            JSONObject object2=new JSONObject();
            object2.put("name","Saudi Arab");
            object2.put("code","SAR");
            array.put(object2);

            JSONObject object4=new JSONObject();
            object4.put("name","Great Britain");
            object4.put("code","GBP");
            array.put(object4);

            JSONObject object5=new JSONObject();
            object5.put("name","Canada");
            object5.put("code","CAD");
            array.put(object5);

            JSONObject object9=new JSONObject();
            object9.put("name","Australia");
            object9.put("code","AUD");
            array.put(object9);

            /*JSONObject object6=new JSONObject();
            object6.put("name","India");
            object6.put("code","INR");
            array.put(object6);*/

            JSONObject object7=new JSONObject();
            object7.put("name","BGN");
            object7.put("code","BGN");
            array.put(object7);

            JSONObject object10=new JSONObject();
            object10.put("name","CHF");
            object10.put("code","CHF");
            array.put(object10);

            JSONObject object11=new JSONObject();
            object11.put("name","CZK");
            object11.put("code","CZK");
            array.put(object11);

            JSONObject object12=new JSONObject();
            object12.put("name","DKK");
            object12.put("code","DKK");
            array.put(object12);

            JSONObject object13=new JSONObject();
            object13.put("name","HRK");
            object13.put("code","HRK");
            array.put(object13);

            JSONObject object14=new JSONObject();
            object14.put("name","HUF");
            object14.put("code","HUF");
            array.put(object14);

            JSONObject object15=new JSONObject();
            object15.put("name","MXN");
            object15.put("code","MXN");
            array.put(object15);

            JSONObject object16=new JSONObject();
            object16.put("name","PLN");
            object16.put("code","PLN");
            array.put(object16);

            JSONObject object17=new JSONObject();
            object17.put("name","QAR");
            object17.put("code","QAR");
            array.put(object17);

            JSONObject object18=new JSONObject();
            object18.put("name","RON");
            object18.put("code","RON");
            array.put(object18);

            JSONObject object19=new JSONObject();
            object19.put("name","RUB");
            object19.put("code","RUB");
            array.put(object19);

            JSONObject object20=new JSONObject();
            object20.put("name","SEK");
            object20.put("code","SEK");
            array.put(object20);

            JSONObject object21=new JSONObject();
            object21.put("name","VND");
            object21.put("code","VND");
            array.put(object21);


        }
        catch (Exception e)
        {
            e.printStackTrace();
        }

        return array;
    }
 }
