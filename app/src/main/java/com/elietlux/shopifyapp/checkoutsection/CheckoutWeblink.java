/*
 * Copyright/**
 *          * CedCommerce
 *           *
 *           * NOTICE OF LICENSE
 *           *
 *           * This source file is subject to the End User License Agreement (EULA)
 *           * that is bundled with this package in the file LICENSE.txt.
 *           * It is also available through the world-wide-web at this URL:
 *           * http://cedcommerce.com/license-agreement.txt
 *           *
 *           * @category  Ced
 *           * @package   MageNative
 *           * @author    CedCommerce Core Team <connect@cedcommerce.com >
 *           * @copyright Copyright CEDCOMMERCE (http://cedcommerce.com/)
 *           * @license      http://cedcommerce.com/license-agreement.txt
 *
 */
package com.elietlux.shopifyapp.checkoutsection;
import android.annotation.SuppressLint;
import android.content.Intent;
import android.net.http.SslError;
import android.os.Build;
import android.os.Bundle;
import android.support.annotation.NonNull;
import android.support.annotation.Nullable;
import android.util.Log;
import android.view.Menu;
import android.view.ViewGroup;
import android.webkit.SslErrorHandler;
import android.webkit.ValueCallback;
import android.webkit.WebChromeClient;
import android.webkit.WebSettings;
import android.webkit.WebView;
import android.webkit.WebViewClient;
import android.widget.Toast;

import com.elietlux.shopifyapp.R;
import com.elietlux.shopifyapp.homesection.HomePage;
import com.elietlux.shopifyapp.loadersection.Loader;
import com.elietlux.shopifyapp.maincontainer.MainActivity;
import com.elietlux.shopifyapp.requestsection.ApiClient;
import com.elietlux.shopifyapp.requestsection.ApiInterface;
import com.elietlux.shopifyapp.splashsection.Splash;
import com.elietlux.shopifyapp.storagesection.LocalData;
import com.elietlux.shopifyapp.storefrontresponse.AsyncResponse;
import com.elietlux.shopifyapp.storefrontresponse.Response;

import java.net.URLEncoder;
import java.util.HashMap;
import java.util.Map;

import butterknife.BindView;
import butterknife.ButterKnife;
import okhttp3.ResponseBody;
import retrofit2.Call;

@SuppressWarnings("ALL")
public class CheckoutWeblink extends MainActivity
{
    LocalData data=null;
    String postData = null;
    ApiInterface apiService;
    String currentUrl;
    @Nullable @BindView(R.id.MageNative_webview) WebView webView;
    Loader loader=null;
    @SuppressLint("SetJavaScriptEnabled")
    @Override
    protected void onCreate(Bundle savedInstanceState)
    {
        super.onCreate(savedInstanceState);
        ViewGroup content = findViewById(R.id.MageNative_frame_container);
        getLayoutInflater().inflate(R.layout.magenative_webpage, content, true);
        ButterKnife.bind(CheckoutWeblink.this);
        //showbackbutton();

        apiService = ApiClient.getClient(CheckoutWeblink.this).create(ApiInterface.class);
        data=new LocalData(CheckoutWeblink.this);
        loader=new Loader(CheckoutWeblink.this);
        loader.show();
        showTittle(getResources().getString(R.string.checkout));
       // getSupportActionBar().setDisplayHomeAsUpEnabled(true);
        currentUrl = getIntent().getStringExtra("link");
        webView.getSettings().setJavaScriptEnabled(true);
        webView.getSettings().setLoadWithOverviewMode(true);
        webView.getSettings().setUseWideViewPort(true);
        setUpWebViewDefaults(webView);

        if(data.isLogin())
        {
            try
            {
                HashMap<String, String> headers = new HashMap<>();
                headers.put("X-Shopify-Customer-Access-Token",data.getAccessToken());
                webView.loadUrl(currentUrl,headers);
            }
            catch (Exception e)
            {
                e.printStackTrace();
            }
        }
        else
        {
            webView.loadUrl(currentUrl);
        }
        webView.setWebChromeClient(new WebChromeClient());
    }
    @SuppressLint("SetJavaScriptEnabled")
    private void setUpWebViewDefaults(WebView webView)
    {
        WebSettings settings = webView.getSettings();
        settings.setJavaScriptEnabled(true);
        settings.setDomStorageEnabled(true);
        settings.setUseWideViewPort(true);
        settings.setLoadWithOverviewMode(true);
        settings.setBuiltInZoomControls(true);
        settings.setDisplayZoomControls(false);
        if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.KITKAT)
        {
            WebView.setWebContentsDebuggingEnabled(true);
        }
        webView.setWebViewClient(new WebViewClient()
        {
            @Override
            public void onReceivedError(WebView view, int errorCode, String description, String failingUrl)
            {
                super.onReceivedError(view, errorCode, description, failingUrl);
                Log.i("URL", "" + description);
            }
            @Override
            public void onLoadResource(WebView view, @NonNull String url)
            {
                Log.i("URL", "" + url);
                if(url.contains("thank_you"))
                {
                    data.clearCoupon();
                    data.clearCheckoutId();
                    data.clearLineItems();
                    Toast.makeText(CheckoutWeblink.this,getResources().getString(R.string.ordersuccessfulyplaced),Toast.LENGTH_LONG).show();
                    Call<ResponseBody> call=apiService.setOrder(getResources().getString(R.string.mid),CartListing.checkout_id);
                    try
                    {
                        Response.getRetrofitResponse(call,new AsyncResponse()
                        {
                            @Override
                            public void finalOutput(@NonNull Object output, @NonNull boolean error )
                            {

                            }
                        },CheckoutWeblink.this);
                    }
                    catch (Exception e)
                    {
                        e.printStackTrace();
                    }
                    /*Intent intent=new Intent(CheckoutWeblink.this, HomePage.class);
                    intent.addFlags(Intent.FLAG_ACTIVITY_CLEAR_TASK);
                    intent.addFlags(Intent.FLAG_ACTIVITY_NEW_TASK);
                    startActivity(intent);
                    overridePendingTransition(R.anim.magenative_slide_in,R.anim.magenative_slide_out);*/
                }
            }
            @Override
            public void onPageFinished(WebView view, String url)
            {
                Log.i("pageURL", "" + url);
                String javascript="javascript: document.getElementsByClassName('section__header')[0].style.display = 'none' ";
                String javascript1="javascript: document.getElementsByClassName('logged-in-customer-information')[0].style.display = 'none' ";
                if(Build.VERSION.SDK_INT >= Build.VERSION_CODES.KITKAT)
                {
                    webView.evaluateJavascript(javascript, new ValueCallback<String>()
                    {
                        @Override
                        public void onReceiveValue(String value) {
                            Log.i("pageVALUE1", "" + value);
                        }
                    });
                    webView.evaluateJavascript(javascript1, new ValueCallback<String>()
                    {
                        @Override
                        public void onReceiveValue(String value) {
                            Log.i("pageVALUE1", "" + value);
                        }
                    });
                }
                else
                {
                    webView.loadUrl(javascript);
                    webView.loadUrl(javascript1);
                }
                loader.dismiss();
            }
            @Override
            public void onReceivedSslError(WebView view, SslErrorHandler handler, SslError error)
            {
                super.onReceivedSslError(view, handler, error);
                Log.i("URL", "" + error.getUrl());
            }
           /* @Override
            public boolean shouldOverrideUrlLoading(WebView view, String url) {
                if (url.equals(currentUrl)) {
                    view.loadUrl(url);
                }
                return true;
            }*/
        });
    }

    @Override
    public void onBackPressed() {
        if (webView.canGoBack()) {
            webView.goBack();
            Intent intent=new Intent(CheckoutWeblink.this, HomePage.class);
            intent.addFlags(Intent.FLAG_ACTIVITY_CLEAR_TASK);
            intent.addFlags(Intent.FLAG_ACTIVITY_NEW_TASK);
            startActivity(intent);
            overridePendingTransition(R.anim.magenative_slide_in,R.anim.magenative_slide_out);
        } else {
            super.onBackPressed();
        }
    }
    @Override
    public boolean onCreateOptionsMenu(@NonNull Menu menu)
    {
        return false;
    }

}
