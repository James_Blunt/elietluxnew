/*
 * Copyright/**
 *          * CedCommerce
 *           *
 *           * NOTICE OF LICENSE
 *           *
 *           * This source file is subject to the End User License Agreement (EULA)
 *           * that is bundled with this package in the file LICENSE.txt.
 *           * It is also available through the world-wide-web at this URL:
 *           * http://cedcommerce.com/license-agreement.txt
 *           *
 *           * @category  Ced
 *           * @package   MageNative
 *           * @author    CedCommerce Core Team <connect@cedcommerce.com >
 *           * @copyright Copyright CEDCOMMERCE (http://cedcommerce.com/)
 *           * @license   http://cedcommerce.com/license-agreement.txt
 *
 */
package com.elietlux.shopifyapp.checkoutsection;
import android.annotation.SuppressLint;
import android.app.Dialog;
import android.content.Context;
import android.content.Intent;
import android.content.res.Resources;
import android.graphics.Paint;
import android.os.Bundle;
import android.support.annotation.NonNull;
import android.support.annotation.Nullable;
import android.text.Html;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.Menu;
import android.view.View;
import android.view.ViewGroup;
import android.widget.Button;
import android.widget.CompoundButton;
import android.widget.EditText;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.RadioButton;
import android.widget.RelativeLayout;
import android.widget.TextView;
import android.widget.Toast;

import com.blogspot.atifsoftwares.animatoolib.Animatoo;
import com.bumptech.glide.Glide;
import com.bumptech.glide.load.engine.DiskCacheStrategy;
import com.bumptech.glide.request.RequestOptions;
import com.elietlux.shopifyapp.currencysection.NewCurrencyFormatter;
import com.shopify.buy3.GraphCallResult;
import com.shopify.buy3.GraphClient;
import com.shopify.buy3.GraphResponse;
import com.shopify.buy3.MutationGraphCall;
import com.shopify.buy3.Storefront;
import com.elietlux.shopifyapp.R;
import com.elietlux.shopifyapp.currencysection.CurrencyFormatter;
import com.elietlux.shopifyapp.loginandregistrationsection.Login;
import com.elietlux.shopifyapp.maincontainer.MainActivity;
import com.elietlux.shopifyapp.productlistingsection.ProductListing;
import com.elietlux.shopifyapp.requestsection.ApiClient;
import com.elietlux.shopifyapp.storagesection.LocalData;
import com.elietlux.shopifyapp.storefrontqueries.MutationQuery;
import com.elietlux.shopifyapp.storefrontresponse.AsyncResponse;
import com.elietlux.shopifyapp.storefrontresponse.Response;
import org.json.JSONObject;

import java.text.SimpleDateFormat;
import java.time.LocalDateTime;
import java.time.format.DateTimeFormatter;
import java.util.Date;
import java.util.Iterator;
import java.util.List;
import java.util.Objects;

import butterknife.BindView;
import butterknife.ButterKnife;
public class CartListing extends MainActivity
{
    @Nullable @BindView(R.id.cartcontainer) LinearLayout cartcontainer;
    @Nullable @BindView(R.id.relativemain) LinearLayout relativemain;
    @Nullable @BindView(R.id.taxsection) RelativeLayout taxsection;
    @Nullable @BindView(R.id.subtotalprice) TextView subtotalprice;
    @Nullable @BindView(R.id.discountprice) TextView discountprice;
    @Nullable @BindView(R.id.taxprice) TextView taxprice;
    @Nullable @BindView(R.id.grandtotalprice) TextView grandtotalprice;
    @Nullable @BindView(R.id.MageNative_applycoupantag) EditText MageNative_applycoupantag;
    @Nullable @BindView(R.id.MageNative_applycoupan) Button MageNative_applycoupan;
    @Nullable @BindView(R.id.upperpart) LinearLayout upperpart;
    @Nullable @BindView(R.id.MageNative_couponcode) LinearLayout MageNative_couponcode;
    @Nullable @BindView(R.id.imageView_applycoupon) ImageView imageView_applycoupon;
    @Nullable @BindView(R.id.MageNative_checkout) TextView MageNative_checkout;
    LocalData data=null;
    GraphClient client=null;
    String cursor="nocursor";
    CheckoutLineItems items=null;
    boolean isfromqtycheck=false;
    static String checkout_id="noid";
    static String is="noid";
    String couponcode="nocouponcode";
    boolean isapplied=false;
    final boolean[] coupon = {false};
    String checkouturl;
    String presentmentcurrency=null;
    @Override
    protected void onCreate(Bundle savedInstanceState)
    {
        try
        {
            super.onCreate(savedInstanceState);
            ViewGroup content = findViewById(R.id.MageNative_frame_container);
            getLayoutInflater().inflate(R.layout.magenative_activity_cart_listing, content, true);
            ButterKnife.bind(CartListing.this);
            //showbackbutton();
            showTittle(getResources().getString(R.string.shopingcart));
            data=new LocalData(CartListing.this);
            items=new CheckoutLineItems(CartListing.this);
            client= ApiClient.getGraphClient(CartListing.this,true);
            selectCart();
            presentmentcurrency=data.getCurrencyCode();
            if(data.getCheckoutId()!=null)
            {
                checkout_id=data.getCheckoutId();
                isapplied=true;
            }
            if(data.getCoupon()!=null)
            {
                couponcode=data.getCoupon();
                MageNative_applycoupan.setText(getResources().getString(R.string.remove));
                MageNative_applycoupantag.setText(couponcode);
                MageNative_applycoupantag.setEnabled(false);
            }

            MageNative_applycoupan.setOnClickListener(new View.OnClickListener()
            {
                @Override
                public void onClick(View view)
                {
                    if(MageNative_applycoupantag.getText().toString().isEmpty())
                    {
                        Toast.makeText(CartListing.this,getResources().getString(R.string.empty),Toast.LENGTH_LONG).show();
                    }
                    else
                    {
                        if(MageNative_applycoupan.getText().equals(getResources().getString(R.string.remove)))
                        {
                            data.clearCheckoutId();
                            data.clearCoupon();
                            checkout_id="noid";
                            couponcode="nocouponcode";
                            isapplied=false;
                            getCartData();
                            MageNative_applycoupan.setText(getResources().getString(R.string.apply));
                            MageNative_applycoupantag.setText(" ");
                            MageNative_applycoupantag.setEnabled(true);
                        }
                        else
                        {
                            isapplied=true;
                            data.saveCouponCode(MageNative_applycoupantag.getText().toString());
                            couponcode=MageNative_applycoupantag.getText().toString();
                            getCartData();
                        }
                    }
                }
            });
          /*  upperpart.setOnClickListener(new View.OnClickListener()
            {
                @Override
                public void onClick(View view)
                {

                    if (coupon[0]) {
                        imageView_applycoupon.setImageDrawable(getResources().getDrawable(R.drawable.caretdown));
                        MageNative_couponcode.setVisibility(View.GONE);
                        coupon[0] = false;
                    } else {
                        imageView_applycoupon.setImageDrawable(getResources().getDrawable(R.drawable.sortup));
                        MageNative_couponcode.setVisibility(View.VISIBLE);
                        coupon[0] = true;
                    }
                }
            });*/
           /* MageNative_checkout.setOnClickListener(new View.OnClickListener()
            {
                @Override
                public void onClick(View view)
                {
                    if(data.isLogin())
                    {
                        Intent intent=new Intent(CartListing.this, CheckoutWeblink.class);
                        intent.putExtra("link",checkouturl);
                        startActivity(intent);
                        overridePendingTransition(R.anim.magenative_slide_in, R.anim.magenative_slide_out);
                    }
                    else
                    {
                        showPopUp();
                    }
                }
            });*/

            MageNative_checkout.setOnClickListener(new View.OnClickListener()
            {
                @Override
                public void onClick(View view)
                {
/*if(data.isLogin())
{
Intent intent=new Intent(CartListing.this, CheckoutWeblink.class);
intent.putExtra("link",checkouturl);
startActivity(intent);
overridePendingTransition(R.anim.magenative_slide_in, R.anim.magenative_slide_out);
}
else
{
showPopUp();
}*/
                    if (data.isLogin()) {
                        MutationGraphCall call = client.mutateGraph(MutationQuery.createCustomerCheckout(CartListing.this, cursor, checkout_id, data.getAccessToken(),data.getCurrencyCode()));
                        Response.getMutationGraphQLResponse(call, new AsyncResponse() {
                            @Override
                            public void finalOutput(@NonNull Object output, @NonNull boolean error) {
                                if (error) {
                                    GraphResponse<Storefront.Mutation> response = ((GraphCallResult.Success<Storefront.Mutation>) output).getResponse();
                                    runOnUiThread(new Runnable() {
                                        @Override
                                        public void run() {
                                            Storefront.Checkout checkout = response.getData().getCheckoutCustomerAssociateV2().getCheckout();
                                            checkouturl = checkout.getWebUrl();
                                            Intent intent = new Intent(CartListing.this, CheckoutWeblink.class);
                                            intent.putExtra("link", checkouturl);
                                            startActivity(intent);
                                           // overridePendingTransition(R.anim.magenative_slide_in, R.anim.magenative_slide_out);
                                            Animatoo.animateZoom(CartListing.this);

                                        }
                                    });
                                }
                            }
                        }, CartListing.this);
                    }
                    else {
/*
                        showPopUp();
*/
                        Intent intent=new Intent(CartListing.this, Login.class);
                        intent.putExtra("link",checkouturl);
                        intent.putExtra("checkout","checkout");
                        startActivity(intent);
                        //overridePendingTransition(R.anim.magenative_slide_in, R.anim.magenative_slide_out);
                        Animatoo.animateZoom(CartListing.this);
                    }
                }
            });
            if(data.getLineItems()!=null)
            {
                getCartData();
            }
            else
            {
                Toast.makeText(CartListing.this,getResources().getString(R.string.emptycart),Toast.LENGTH_LONG).show();
                finish();
            }
        }
        catch (Exception e)
        {
            e.printStackTrace();
        }
    }

    private void getCartData()
    {
        try
        {
            try
            {
                SimpleDateFormat formatter = new SimpleDateFormat("dd/MM/yyyy HH:mm:ss");
                Date date = new Date();
                Log.i("CartDate",""+formatter.format(date));
                data.saveCartDate(formatter.format(date));
                MutationGraphCall call = client.mutateGraph(MutationQuery.createCheckout(CartListing.this,cursor,couponcode,isapplied,checkout_id,data.getCurrencyCode()));
                Response.getMutationGraphQLResponse(call,new AsyncResponse()
                {
                    @Override
                    public void finalOutput(@NonNull Object output,@NonNull boolean error)
                    {
                        if(error)
                        {
                            GraphResponse<Storefront.Mutation> response  = ((GraphCallResult.Success<Storefront.Mutation>) output).getResponse();
                            runOnUiThread(new Runnable()
                            {
                                @Override
                                public void run()
                                {
                                    List<Storefront.UserError> errors=null;
                                    if(isapplied)
                                    {
                                        errors = response.getData().getCheckoutDiscountCodeApply().getUserErrors();
                                    }
                                    else
                                    {
                                        errors = response.getData().getCheckoutCreate().getUserErrors();
                                    }
                                    if(errors.size() > 0)
                                    {
                                        boolean iserror=false;
                                        Iterator iterator = errors.iterator();
                                        String err = "";
                                        String csv="";
                                        while (iterator.hasNext())
                                        {
                                            Storefront.UserError error = (Storefront.UserError) iterator.next();
                                            err += error.getMessage();
                                            csv=android.text.TextUtils.join("-", error.getField());
                                            Log.e("USERerrors", "" + csv);
                                            if(error.getField().size()==4)
                                            {
                                                if(error.getField().get(1).equals("lineItems")&&error.getField().get(3).equals("quantity"))
                                                {
                                                    try
                                                    {
                                                        iserror=true;
                                                        String str = error.getMessage().replaceAll("[^0-9]+", "");
                                                        JSONObject object=new JSONObject(data.getLineItems());
                                                        if(object.has(items.getLineItems().get(Integer.parseInt(error.getField().get(2))).getVariantId().toString()))
                                                        {
                                                            if(Integer.parseInt(str.trim())==0)
                                                            {
                                                                object.remove(items.getLineItems().get(Integer.parseInt(error.getField().get(2))).getVariantId().toString());
                                                            }
                                                            else
                                                            {
                                                                object.put(items.getLineItems().get(Integer.parseInt(error.getField().get(2))).getVariantId().toString(),Integer.parseInt(str.trim()));
                                                            }
                                                            data.saveLineItems(object);
                                                        }
                                                    }
                                                    catch (Exception e)
                                                    {
                                                        e.printStackTrace();
                                                    }
                                                    if(isfromqtycheck)
                                                    {
                                                        Toast.makeText(CartListing.this,err,Toast.LENGTH_LONG).show();
                                                    }
                                                }
                                            }
                                        }
                                        if(csv.equals("discountCode"))
                                        {
                                            data.clearCheckoutId();
                                            data.clearCoupon();
                                            checkout_id="noid";
                                            couponcode="nocouponcode";
                                            isapplied=false;
                                            Toast.makeText(CartListing.this,err,Toast.LENGTH_LONG).show();
                                            getCartData();
                                        }
                                        if(iserror)
                                        {
                                            Toast.makeText(CartListing.this,err,Toast.LENGTH_LONG).show();
                                            getCartData();
                                        }
                                        else
                                        {
                                            Toast.makeText(CartListing.this,err,Toast.LENGTH_LONG).show();
                                        }
                                    }
                                    else
                                    {
                                        processCartData(response);
                                    }
                                }
                            });
                        }
                        else
                        {
                            Log.i("ResponseError",""+output.toString());
                        }
                    }
                },CartListing.this);
            }
            catch (Exception e)
            {
                e.printStackTrace();
            }
        }
        catch (Exception e)
        {
            e.printStackTrace();
        }
    }

    private void processCartData(GraphResponse<Storefront.Mutation> response)
    {
        try
        {
          Storefront.Checkout checkout=null;
          if(isapplied)
          {
              checkout=response.getData().getCheckoutDiscountCodeApply().getCheckout();
              MageNative_applycoupan.setText(getResources().getString(R.string.remove));
              MageNative_applycoupantag.setText(couponcode);
              MageNative_applycoupantag.setEnabled(false);
          }
          else
          {
              checkout=response.getData().getCheckoutCreate().getCheckout();
          }
          checkout_id=checkout.getId().toString();
          if(data.getCoupon()!=null)
          {
              data.saveCheckoutId(checkout_id);
          }
          checkouturl=checkout.getWebUrl();
          List<Storefront.CheckoutLineItemEdge> lineedges = checkout.getLineItems().getEdges();
          if(lineedges.size()>0)
          {
              View cart_item=null;
              TextView pro_id=null;
              TextView var_id=null;
              TextView productname=null;
              TextView available_stock=null;
              TextView price=null;
              TextView special_price=null;
              TextView variantoptions=null;
              TextView quantity=null;
              View outofstockback=null;
              ImageView productimage=null;
              ImageView increase=null;
              ImageView decrese=null;
              RelativeLayout deletesection=null;
              Storefront.CheckoutLineItemEdge edge=null;
              Iterator<Storefront.CheckoutLineItemEdge> iterator=lineedges.iterator();
              if(cartcontainer.getChildCount()>0)
              {
                  cartcontainer.removeAllViews();
              }
              while (iterator.hasNext())
              {
                  cart_item=View.inflate(CartListing.this,R.layout.cart_item,null);
                  pro_id=cart_item.findViewById(R.id.pro_id);
                  var_id=cart_item.findViewById(R.id.var_id);
                  productname=cart_item.findViewById(R.id.productname);
                  productimage=cart_item.findViewById(R.id.productimage);
                  outofstockback=cart_item.findViewById(R.id.outofstockback);
                  available_stock=cart_item.findViewById(R.id.available_stock);
                  special_price=cart_item.findViewById(R.id.special_price);
                  price=cart_item.findViewById(R.id.price);
                  variantoptions=cart_item.findViewById(R.id.options);
                  quantity=cart_item.findViewById(R.id.quantity);
                  increase=cart_item.findViewById(R.id.increase);
                  decrese=cart_item.findViewById(R.id.decrese);
                  deletesection=cart_item.findViewById(R.id.deletesection);
                  edge= iterator.next();
                  pro_id.setText(edge.getNode().getId().toString());
                  productname.setText(edge.getNode().getTitle());
                  quantity.setText(edge.getNode().getQuantity().toString());
                  if(!(edge.getNode().getVariant().getAvailableForSale()))
                  {
                      outofstockback.setVisibility(View.VISIBLE);
                      available_stock.setVisibility(View.VISIBLE);
                  }
                  Log.i("ResponseErrorcurr", "cart " + data.getMoneyFormat());
                  Storefront.ProductVariantPricePairEdge pedge = getEdge(edge.getNode().getVariant().getPresentmentPrices().getEdges());

                  String price_normal= NewCurrencyFormatter.setsymbol(pedge.getNode().getPrice().getAmount(),pedge.getNode().getPrice().getCurrencyCode().toString());
                  price.setText(price_normal);
                  var_id.setText(edge.getNode().getVariant().getId().toString());
                  if(edge.getNode().getVariant().getCompareAtPrice()!=null)
                  {
                      if(edge.getNode().getVariant().getCompareAtPrice().compareTo(edge.getNode().getVariant().getPrice())==1)
                      {
                          String price_special=NewCurrencyFormatter.setsymbol(pedge.getNode().getCompareAtPrice().getAmount(),pedge.getNode().getCompareAtPrice().getCurrencyCode().toString());
                          price.setPaintFlags(Paint.STRIKE_THRU_TEXT_FLAG);
                          special_price.setText(price_normal);
                          price.setText(price_special);
                      }
                      else
                      {
                          special_price.setVisibility(View.GONE);
                      }
                  }
                  else
                  {
                      special_price.setVisibility(View.GONE);
                  }
                  String url="";
                  if(edge.getNode().getVariant().getImage()!=null)
                  {
                      url=edge.getNode().getVariant().getImage().getTransformedSrc();
                      Log.i("url",""+url);
                  }
                  Glide.with(CartListing.this)
                          .load(url)
                          .thumbnail(0.5f)
                          .apply(new RequestOptions().placeholder(R.drawable.placeholder).error(R.drawable.placeholder).dontTransform().diskCacheStrategy(DiskCacheStrategy.ALL))
                          .into(productimage);
                  List<Storefront.SelectedOption> options=edge.getNode().getVariant().getSelectedOptions();
                  Iterator<Storefront.SelectedOption> optionIterator=options.iterator();
                  Storefront.SelectedOption option=null;
                  String optiondata="";
                  while (optionIterator.hasNext())
                  {
                      option=optionIterator.next();
                      optiondata=optiondata+option.getName() +" : "+option.getValue() +"\n";
                  }
                  variantoptions.setText(optiondata);
                  TextView finalQuantity = quantity;
                  TextView finalVar_id = var_id;
                  increase.setOnClickListener(new View.OnClickListener()
                  {
                      @Override
                      public void onClick(View v)
                      {
                          int qty = Integer.parseInt(finalQuantity.getText().toString());
                          updatecheckout(finalVar_id.getText().toString(),qty + 1);
                      }
                  });
                  decrese.setOnClickListener(new View.OnClickListener()
                  {
                      @Override
                      public void onClick(View v)
                      {
                          int qty = Integer.parseInt(finalQuantity.getText().toString());
                          if (qty > 1)
                          {
                              updatecheckout(finalVar_id.getText().toString(),qty - 1);
                          }
                      }
                  });
                  deletesection.setOnClickListener(new View.OnClickListener()
                  {
                      @Override
                      public void onClick(View view)
                      {
                          updatecheckout(finalVar_id.getText().toString(),-1);
                      }
                  });
                  cartcontainer.addView(cart_item);
              }

              String subtotal=NewCurrencyFormatter.setsymbol(checkout.getSubtotalPriceV2().getAmount().toString(),checkout.getSubtotalPriceV2().getCurrencyCode().toString());
              subtotalprice.setText(subtotal);

              if(checkout.getTaxExempt())
              {
                 taxsection.setVisibility(View.VISIBLE);
                 String tax=NewCurrencyFormatter.setsymbol(checkout.getTotalTax().toString(),data.getCurrencyCode());
                 taxprice.setText(tax);
              }
              String grandtotal=NewCurrencyFormatter.setsymbol(checkout.getTotalPriceV2().getAmount().toString(),checkout.getTotalPriceV2().getCurrencyCode().toString());
              grandtotalprice.setText(grandtotal);
              relativemain.setVisibility(View.VISIBLE);
          }
          else
          {
              Toast.makeText(CartListing.this,getResources().getString(R.string.emptycart),Toast.LENGTH_LONG).show();
              finish();
          }
        }
        catch (Exception e)
        {
            e.printStackTrace();
        }
    }

    @Override
    protected void onResume() {
        super.onResume();
        invalidateOptionsMenu();
    }

    @Override
    public void onBackPressed() {
        invalidateOptionsMenu();
        super.onBackPressed();
    }

    @Override
    public boolean onCreateOptionsMenu(@NonNull Menu menu)
    {
        return false;
    }
    public void updatecheckout(String variant_id,int qty)
    {
        try
        {
            JSONObject object=new JSONObject(data.getLineItems());
            if(object.has(variant_id))
            {
              if(qty==-1)
              {
                  object.remove(variant_id);
              }
              else
              {
                  object.put(variant_id,qty);
              }
              isfromqtycheck=true;
              data.saveLineItems(object);
             /* if(data.getCoupon()==null)
              {
                  checkout_id="noid";
                  Log.i("checkout_id",""+checkout_id);
              }*/
              getCartData();
            }
        }
        catch (Exception e)
        {
            e.printStackTrace();
        }
    }
    private void showPopUp()
    {
        try
        {
            final Dialog listDialog = new Dialog(CartListing.this, R.style.PauseDialog);
            ((ViewGroup) ((ViewGroup) Objects.requireNonNull(listDialog.getWindow()).getDecorView()).getChildAt(0))
                    .getChildAt(1)
                    .setBackgroundColor(CartListing.this.getResources().getColor(R.color.black));
            listDialog.setTitle(Html.fromHtml("<font color='#ffffff'>" + getResources().getString(R.string.logintype) + "</font>"));
            LayoutInflater li = (LayoutInflater) CartListing.this.getSystemService(Context.LAYOUT_INFLATER_SERVICE);
            @SuppressLint("InflateParams") View loginoptions = Objects.requireNonNull(li).inflate(R.layout.magenative_custom_options, null, false);
            RadioButton Guest = loginoptions.findViewById(R.id.Guest);
            RadioButton User = loginoptions.findViewById(R.id.User);
            int id = Resources.getSystem().getIdentifier("btn_check_holo_light", "drawable", "android");
            Guest.setButtonDrawable(id);
            User.setButtonDrawable(id);
            Guest.setOnCheckedChangeListener(new CompoundButton.OnCheckedChangeListener() {
                @Override
                public void onCheckedChanged(CompoundButton buttonView, boolean isChecked)
                {
                    if (isChecked)
                    {
                        listDialog.dismiss();
                        Intent intent=new Intent(CartListing.this, CheckoutWeblink.class);
                        intent.putExtra("link",checkouturl);
                        startActivity(intent);
                        overridePendingTransition(R.anim.magenative_slide_in, R.anim.magenative_slide_out);
                    }
                }
            });
            User.setOnCheckedChangeListener(new CompoundButton.OnCheckedChangeListener() {
                @Override
                public void onCheckedChanged(CompoundButton buttonView, boolean isChecked)
                {
                    if (isChecked)
                    {
                        listDialog.dismiss();
                        Intent intent=new Intent(CartListing.this, Login.class);
                        intent.putExtra("link",checkouturl);
                        intent.putExtra("checkout","checkout");
                        startActivity(intent);
                        overridePendingTransition(R.anim.magenative_slide_in, R.anim.magenative_slide_out);
                    }
                }
            });
            listDialog.setContentView(loginoptions);
            listDialog.setCancelable(true);
            listDialog.show();
        }
        catch (Exception e)
        {
            e.printStackTrace();
        }

    }
    private Storefront.ProductVariantPricePairEdge getEdge(List<Storefront.ProductVariantPricePairEdge> edges) {
        Storefront.ProductVariantPricePairEdge pairEdge = null;
        try {
            for (int i = 0; i < edges.size(); i++) {
                Log.i("presentment", edges.get(i).getNode().getPrice().getCurrencyCode().toString());
                Log.i("presentment", presentmentcurrency);

                if (edges.get(i).getNode().getPrice().getCurrencyCode().toString().equals(presentmentcurrency)) {
                    pairEdge = edges.get(i);
                    break;
                }
            }
        } catch (Exception e) {
            e.printStackTrace();
        }
        return  pairEdge;
    }

}