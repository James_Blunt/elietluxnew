package com.elietlux.shopifyapp.viewholders;
import android.app.Activity;
import android.support.annotation.NonNull;
import android.support.v7.widget.RecyclerView;
import android.view.View;
import android.widget.ImageView;
import android.widget.RelativeLayout;
import android.widget.TextView;
import com.elietlux.shopifyapp.R;
/**
 * Created by cedcoss on 2/5/18.
 */
public class RelatedProductViewHolder extends RecyclerView.ViewHolder
{
    public final TextView MageNative_specialprice;
    public final TextView MageNative_reguralprice;
    public final TextView title;
    public final TextView vedor;
    public final TextView id;
    public final ImageView imageview;
    public final ImageView wishlist;
    public final RelativeLayout main_container;
    public RelatedProductViewHolder(@NonNull View view, @NonNull final Activity context)
    {
        super(view);
        this.MageNative_specialprice = view.findViewById(R.id.MageNative_specialprice);
        this.MageNative_reguralprice = view.findViewById(R.id.MageNative_reguralprice);
        this.title = view.findViewById(R.id.MageNative_title);
        this.vedor = view.findViewById(R.id.MageNative_vendor);
        this.id = view.findViewById(R.id.product_id);
        this.imageview = view.findViewById(R.id.MageNative_image);
        this.wishlist = view.findViewById(R.id.wishlist);
        this.main_container = view.findViewById(R.id.main_container);
    }
}
