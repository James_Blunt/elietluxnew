/*
 * Copyright/**
 *          * CedCommerce
 *           *
 *           * NOTICE OF LICENSE
 *           *
 *           * This source file is subject to the End User License Agreement (EULA)
 *           * that is bundled with this package in the file LICENSE.txt.
 *           * It is also available through the world-wide-web at this URL:
 *           * http://cedcommerce.com/license-agreement.txt
 *           *
 *           * @category  Ced
 *           * @package   MageNative
 *           * @author    CedCommerce Core Team <connect@cedcommerce.com >
 *           * @copyright Copyright CEDCOMMERCE (http://cedcommerce.com/)
 *           * @license      http://cedcommerce.com/license-agreement.txt
 *
 */
package com.elietlux.shopifyapp.dashboardsection;

import android.content.Intent;
import android.content.pm.PackageInfo;
import android.os.Bundle;
import android.support.annotation.NonNull;
import android.support.annotation.Nullable;
import android.util.Log;
import android.view.Menu;
import android.view.View;
import android.view.ViewGroup;
import android.widget.Button;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.RelativeLayout;
import android.widget.TextView;

import com.bumptech.glide.Glide;
import com.elietlux.shopifyapp.R;
import com.elietlux.shopifyapp.addresssection.AddressList;
import com.elietlux.shopifyapp.homesection.HomePage;
import com.elietlux.shopifyapp.homesection.HomeWeblink;
import com.elietlux.shopifyapp.loginandregistrationsection.Login;
import com.elietlux.shopifyapp.loginandregistrationsection.Register;
import com.elietlux.shopifyapp.maincontainer.MainActivity;
import com.elietlux.shopifyapp.orderssection.OrderListing;
import com.elietlux.shopifyapp.storagesection.LocalData;
import com.elietlux.shopifyapp.wishlistsection.WishListing;

import org.json.JSONObject;

import java.util.Objects;

import butterknife.BindView;
import butterknife.ButterKnife;

public class AccountDashboard extends MainActivity {
    @Nullable
    @BindView(R.id.appversion)
    TextView appversion;

    @Nullable
    @BindView(R.id.copyright)
    TextView copyright;
    @Nullable
    @BindView(R.id.wish_section)
    RelativeLayout wish_section;
    @Nullable
    @BindView(R.id.profile)
    RelativeLayout profile;
    /*@Nullable
    @BindView(R.id.logout)
    TextView logoutsection;*/
    @Nullable
    @BindView(R.id.address_section)
    RelativeLayout address_section;
    @Nullable
    @BindView(R.id.order_section)
    RelativeLayout order_section;

    @Nullable
    @BindView(R.id.currency_section)
    RelativeLayout currency_section;

    @Nullable
    @BindView(R.id.MageNative_FAQ)
    TextView MageNative_FAQ;
    @Nullable
    @BindView(R.id.MageNative_ReturnsandRefunds)
    TextView MageNative_ReturnsandRefunds;
    @Nullable
    @BindView(R.id.MageNative_TermsandConditions)
    TextView MageNative_TermsandConditions;
    @Nullable
    @BindView(R.id.MageNative_PrivacyPolicy)
    TextView MageNative_PrivacyPolicy;
    @Nullable
    @BindView(R.id.MageNative_ShippingPolicy)
    TextView MageNative_ShippingPolicy;
    @Nullable
    @BindView(R.id.MageNative_TrackYourOrder)
    TextView MageNative_TrackYourOrder;

    @Nullable
    @BindView(R.id.MageNative_Reviews)
    TextView MageNative_Reviews;

    @Nullable
    @BindView(R.id.btnCreateAccount)
    Button btnCreateAccount;
    @Nullable
    @BindView(R.id.btnSignin)
    Button btnSignin;

    @Nullable
    @BindView(R.id.loggedOutSection)
    LinearLayout loggedOutSection;

    @Nullable
    @BindView(R.id.logout)
    TextView logout;

    @Nullable
    @BindView(R.id.loggedInSection)
    LinearLayout loggedInSection;

    LocalData data = null;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        try {
            super.onCreate(savedInstanceState);
            ViewGroup content = findViewById(R.id.MageNative_frame_container);
            getLayoutInflater().inflate(R.layout.magenative_account_page, content, true);
            ButterKnife.bind(AccountDashboard.this);
            data = new LocalData(AccountDashboard.this);
            //showbackbutton();
            showTittle(getResources().getString(R.string.myaccount));
            selectAcount();
            PackageInfo pInfo = getPackageManager().getPackageInfo(AccountDashboard.this.getPackageName(), 0);
            String version = pInfo.versionName;
            int versioncode = pInfo.versionCode;
            String app_version = "App Version " + version + "(" + versioncode + ")";
            appversion.setText(app_version);
            copyright.setText(getResources().getString(R.string.copyright) + " " + getResources().getString(R.string.app_name));

            LocalData localData;

            Log.d("login",""+data.isLogin());
            if (data.isLogin()) {
                loggedInSection.setVisibility(View.VISIBLE);
                loggedOutSection.setVisibility(View.GONE);
                logout.setVisibility(View.VISIBLE);
            } else {
                loggedInSection.setVisibility(View.GONE);
                loggedOutSection.setVisibility(View.VISIBLE);
                logout.setVisibility(View.GONE);
            }


            MageNative_FAQ.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View view) {
                    Intent weblink = new Intent(AccountDashboard.this, HomeWeblink.class);
                    weblink.putExtra("link", getResources().getString(R.string.faqUrl));
                    startActivity(weblink);
                    overridePendingTransition(R.anim.magenative_slide_in, R.anim.magenative_slide_out);
                }
            });
            MageNative_ReturnsandRefunds.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View view) {
                    Intent weblink = new Intent(AccountDashboard.this, HomeWeblink.class);
                    weblink.putExtra("link", getResources().getString(R.string.returnsUrl));
                    startActivity(weblink);
                    overridePendingTransition(R.anim.magenative_slide_in, R.anim.magenative_slide_out);
                }
            });
            MageNative_TermsandConditions.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View view) {
                    Intent weblink = new Intent(AccountDashboard.this, HomeWeblink.class);
                    weblink.putExtra("link", getResources().getString(R.string.termsandconditionUrl));
                    startActivity(weblink);
                    overridePendingTransition(R.anim.magenative_slide_in, R.anim.magenative_slide_out);
                }
            });
            MageNative_PrivacyPolicy.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View view) {
                    Intent weblink = new Intent(AccountDashboard.this, HomeWeblink.class);
                    weblink.putExtra("link", getResources().getString(R.string.privacypolicyUrl));
                    startActivity(weblink);
                    overridePendingTransition(R.anim.magenative_slide_in, R.anim.magenative_slide_out);
                }
            });
            MageNative_ShippingPolicy.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View view) {
                    Intent weblink = new Intent(AccountDashboard.this, HomeWeblink.class);
                    weblink.putExtra("link", getResources().getString(R.string.shippingpolicyUrl));
                    startActivity(weblink);
                    overridePendingTransition(R.anim.magenative_slide_in, R.anim.magenative_slide_out);
                }
            });
            MageNative_TrackYourOrder.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View view) {
                    Intent weblink = new Intent(AccountDashboard.this, HomeWeblink.class);
                    weblink.putExtra("link", getResources().getString(R.string.trackorderUrl));
                    startActivity(weblink);
                    overridePendingTransition(R.anim.magenative_slide_in, R.anim.magenative_slide_out);
                }
            });
            MageNative_Reviews.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View view) {
                    Intent weblink = new Intent(AccountDashboard.this, HomeWeblink.class);
                    weblink.putExtra("link", "https://www.elitelux.store/pages/reviews");
                    startActivity(weblink);
                    overridePendingTransition(R.anim.magenative_slide_in, R.anim.magenative_slide_out);
                }
            });

            btnSignin.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View view) {
                    Intent intent = new Intent(AccountDashboard.this, Login.class);
                    intent.setFlags(Intent.FLAG_ACTIVITY_SINGLE_TOP);
                    startActivity(intent);
                    overridePendingTransition(R.anim.magenative_slide_in, R.anim.magenative_slide_out);
                }
            });

            btnCreateAccount.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View view) {
                    Intent intent = new Intent(AccountDashboard.this, Register.class);
                    startActivity(intent);
                    overridePendingTransition(R.anim.magenative_slide_in, R.anim.magenative_slide_out);

                }
            });

            wish_section.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View view) {
                    Intent intent = new Intent(AccountDashboard.this, WishListing.class);
                    intent.setFlags(Intent.FLAG_ACTIVITY_SINGLE_TOP);
                    startActivity(intent);
                    overridePendingTransition(R.anim.magenative_slide_in, R.anim.magenative_slide_out);
                }
            });
            profile.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View view) {
                    Intent intent = new Intent(AccountDashboard.this, ProfilePage.class);
                    intent.setFlags(Intent.FLAG_ACTIVITY_SINGLE_TOP);
                    startActivity(intent);
                    overridePendingTransition(R.anim.magenative_slide_in, R.anim.magenative_slide_out);
                }
            });
            logout.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View view) {
                    data.logout();
                    Intent intent = new Intent(AccountDashboard.this, HomePage.class);
                    intent.addFlags(Intent.FLAG_ACTIVITY_CLEAR_TASK);
                    intent.addFlags(Intent.FLAG_ACTIVITY_NEW_TASK);
                    startActivity(intent);
                    overridePendingTransition(R.anim.magenative_slide_in, R.anim.magenative_slide_out);
                }
            });
            address_section.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View view) {
                    Intent intent = new Intent(AccountDashboard.this, AddressList.class);
                    intent.setFlags(Intent.FLAG_ACTIVITY_SINGLE_TOP);
                    startActivity(intent);
                    overridePendingTransition(R.anim.magenative_slide_in, R.anim.magenative_slide_out);
                }
            });
            order_section.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View view) {
                    Intent intent = new Intent(AccountDashboard.this, OrderListing.class);
                    intent.setFlags(Intent.FLAG_ACTIVITY_SINGLE_TOP);
                    startActivity(intent);
                    overridePendingTransition(R.anim.magenative_slide_in, R.anim.magenative_slide_out);
                }
            });

            currency_section.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View v) {
                    showAppSetting();
                }
            });
        } catch (Exception e) {
            e.printStackTrace();
        }
    }

    @Override
    public boolean onCreateOptionsMenu(@NonNull Menu menu) {
        return false;
    }

    @Override
    protected void onResume() {
        String name = "";
        super.onResume();
//        if (Objects.requireNonNull(data.getLastName()).isEmpty()) {
//            name = Objects.requireNonNull(data.getFirstName()).substring(0, 2);
//        } else {
//            name = Objects.requireNonNull(data.getFirstName()).substring(0, 1) + " " + data.getLastName().substring(0, 1);
//        }
        //customername.setText(name);
    }

}
