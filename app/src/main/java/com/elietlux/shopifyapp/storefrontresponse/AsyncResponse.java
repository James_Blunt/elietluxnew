package com.elietlux.shopifyapp.storefrontresponse;

public interface AsyncResponse
{
    void finalOutput(Object output,boolean error) throws Exception;
}
